package com.teewell.samsunggolfmate.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.common.Contexts;

/**
 * 关于我们
 * 
 * @author chengmingyan 展示"关于我们"的相关界面。
 */
public class AboutOursActivity extends ParentActivity implements
		OnClickListener {
	private static final String TAG = AboutOursActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.aboutours);
		TextView tv_titleName = (TextView) findViewById(R.id.tv_titleName);
		tv_titleName.setText(getString(R.string.aboutOurs));
		Button btn_back = (Button) findViewById(R.id.btn_back);
		Button btn_feedback = (Button) findViewById(R.id.btn_rightTitle);
		btn_back.setText(getString(R.string.setting));
		btn_feedback.setText(getString(R.string.feedback));
		btn_back.setOnClickListener(this);
		btn_feedback.setOnClickListener(this);
		btn_feedback.setVisibility(View.GONE);
		((TextView)findViewById(R.id.textView4)).setText(getString(R.string.version, getAppVersionName(getApplicationContext())));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.btn_rightTitle:
			Intent intent = new Intent(AboutOursActivity.this,
					FeedbackActivity.class);
			startActivity(intent);
			break;
		}

	}

	public static String getAppVersionName(Context context) {
		String versionName = "";
		try {
			// Get the package info
			PackageManager pm = context.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(Contexts.PACKAGE_NAME, 0);
			versionName = pi.versionName;
			if (TextUtils.isEmpty(versionName)) {
				return "";
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception", e);
		}
		return versionName;
	}
}
