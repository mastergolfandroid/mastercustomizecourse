package com.teewell.samsunggolfmate.activities;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.DownloadAerialPhotoBean;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.course.CourseInfoModel;
import com.teewell.samsunggolfmate.utils.FileUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.utils.VolleyUitl;
import com.teewell.samsunggolfmate.views.CourseListAdapter;
import com.teewell.samsunggolfmate.views.ProgressDialogTitleView;
import com.teewell.samsunggolfmate.views.XListView;

/**
 * 周边球场 该界面要注意：下拉刷新容易出问题
 * 
 * @Project GolfMate
 * @package com.teewell.golfmate.activities
 * @title SurroundingActivity.java
 * @Description TODO
 * @author chengmingyan
 * @date 2013-1-15 14:52:36
 * @Copyright Copyright(C) 2013-1-15
 * @version 1.0.0
 */
public class AroundActivity extends ParentActivity implements OnClickListener,
		OnItemClickListener, OnScrollListener {
	private final String TAG = AroundActivity.class.getSimpleName();
	private XListView listView;
	private Button btn_cancel, btn_continue; // 搜索框中的清除按钮
	private EditText text_search; //
	private List<CoursesObject> courseList;
	private CourseListAdapter adapter;
	private CourseInfoModel courseInfoModel;
	public static final int WHAT_GOTTEN_LOCATION = 102;
	public static final int WHAT_LOAD_COURSELIST = 104;
	public static final int WHAT_LOAD_NETDATA = 105;
	private static final int REFRESH_DELAY = 1000 * 2;

	private int firstVisibleItem;
	private int visibleItemCount;

	private int scrollState;
	private ProgressDialog mProgress;
	private CoursesObject coursesObject;
//	private PlayGameObject playGameObject;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.around);
		initView();
	}

	void reloadAllData() {
		// 启动location util
		LocationUtil location = LocationUtil.getInstance(this
				.getApplicationContext());
		location.setHandlerInfo(handler, WHAT_GOTTEN_LOCATION,
				LocationUtil.LOC_ONCE);

		handler.sendEmptyMessageDelayed(WHAT_LOAD_COURSELIST, REFRESH_DELAY);
	}

	/**
	 * 进度条Dialog
	 */
	private void progressDialog(CoursesObject object) {
		mProgress = null;
		mProgress = new ProgressDialog(this);
		mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

		mProgress.setButton(getString(R.string.cancel_aysncCourse),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						VolleyUitl.getInstence(getApplicationContext()).getRequestQueue().cancelAll(handler);
					}
		});

		mProgress.setCustomTitle(new ProgressDialogTitleView(this, object)
				.getView());
		if (!mProgress.isShowing()) {
			mProgress.show();
			mProgress.setCancelable(false);
			mProgress.setProgress(0);
		}
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

//		view.setSelected(false);

		if ((scrollState != SCROLL_STATE_IDLE)
				&& (scrollState != SCROLL_STATE_FLING || (firstVisibleItem + visibleItemCount) < adapter
						.getCount())) {
			return;
		}
		coursesObject = adapter.getItem(position - 1);

		if (coursesObject==null) {
			return;
		}
		if (PlayGameModel.getInstance(this).getPlayGameObject() == null) {
			progressDialog(coursesObject);
			courseInfoModel.getSubCourseInfo(coursesObject, handler);
		} else {
			Dialog dialog = new AlertDialog.Builder(AroundActivity.this)
					.setMessage(R.string.play_hint)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface arg0,
										int arg1) {
									arg0.dismiss();
									PlayGameModel.getInstance(AroundActivity.this).setPlayGameObject(null);
									progressDialog(coursesObject);
									courseInfoModel.getSubCourseInfo(
											coursesObject, handler);
								};
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									Intent intent = new Intent(
											AroundActivity.this,
											PlayTabActivity.class);
									// 如果已经打开过的实例，将不会重新打开新的Activity
									intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
									intent.putExtra(
											KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
											PlayGameModel.getInstance(AroundActivity.this).getPlayGameObject());
									startActivity(intent);
								}
							}).create();
			dialog.show();
		}
	}

	/**
	 * 初始化组件及事件
	 */
	private void initView() {
		TextView tv_titleName = (TextView) findViewById(R.id.tv_titleName);
		tv_titleName.setText(getString(R.string.surrouding_courses));
		btn_continue = (Button) findViewById(R.id.btn_rightTitle);
		btn_continue.setText(getString(R.string.play_continue));
		btn_continue.setOnClickListener(this);
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		btn_cancel.setOnClickListener(this);
		text_search = (EditText) findViewById(R.id.edit_search);
		text_search.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				if (s != null && s.length() > 0) {
					btn_cancel.setVisibility(View.VISIBLE);

				} else {
					btn_cancel.setVisibility(View.GONE);
				}

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				actionRefreshCourseList();
			}
		});
		text_search.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_UP:
					int curX = (int) event.getX();
					if (curX > v.getWidth() - 38
							&& !TextUtils.isEmpty(text_search.getText())) {
						text_search.setText("");
						int cacheInputType = text_search.getInputType();// backup
						text_search.setInputType(InputType.TYPE_NULL);// disable
						text_search.onTouchEvent(event);// call native handler
						text_search.setInputType(cacheInputType);// restore
																	// input
						return true;// consume touch even
					}
					break;
				}
				return false;
			}
		});

		listView = (XListView) this.findViewById(R.id.listView);
		courseInfoModel = CourseInfoModel.getInstance(this);
		adapter = new CourseListAdapter(this);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		listView.setOnScrollListener(this);
		listView.setXListViewListener(new XListView.IXListViewListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				reloadAllData();
			}

			@Override
			public void onLoadMore() {
				// TODO Auto-generated method stub
				listView.stopLoadMore();
			}
		});
		listView.launchRefresh();
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_cancel:
			text_search.setText("");
			break;
		case R.id.btn_rightTitle:
			Intent intent = new Intent(AroundActivity.this,
					PlayTabActivity.class);
			// 如果已经打开过的实例，将不会重新打开新的Activity
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
					PlayGameModel.getInstance(this).getPlayGameObject());
			startActivity(intent);
			break;
		}
	}

	private final Handler handler = new Handler() {
		private DownloadAerialPhotoBean downloadAerialPhotoBean;
		private int aerialPhotoCount;

		public void handleMessage(Message msg) {
			switch (msg.what) {
			case CourseInfoModel.WHAT_SYNCDATA_SUCCEED:
				if (!"".equals(text_search.getText().toString())) {
					text_search.setText("");
				}
				SimpleDateFormat format = new SimpleDateFormat(
						getString(R.string.date_format));
				Date now = new Date();

				listView.setRefreshTime(format.format(now));
				actionRefreshCourseList();
				listView.stopRefresh();
				listView.setPullLoadEnable(false);
				showToast(getString(R.string.update_succ_tip));
				break;
			case CourseInfoModel.WHAT_SYNCDATA_FAILED: {
				if (!"".equals(text_search.getText().toString())) {
					text_search.setText("");
				}
				showToast(getString(R.string.bad_network));
				actionRefreshCourseList();
				listView.stopRefresh();
				listView.setPullLoadEnable(false);
			}
				break;

			case CourseInfoModel.WHAT_GETSUBCOURSE_SUCCEED:
				if (mProgress != null && mProgress.isShowing()) {
					mProgress.dismiss();
					mProgress = null;
				}
				Intent intent = new Intent(AroundActivity.this,
						StartGameActivity.class);
				intent.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
						(CoursesObject) msg.obj);
				PlayGameModel.fromActivity = 0;
				startActivity(intent);
				// 通过以下方法设置动画，第一个参数设置启动另一个Activity时的动画，第二个参数设置退出另一个Activity时的动画。值为0时表示什么都不做
				// 参数为int型，即动画XML的ID
				// overridePendingTransition(R.anim.zoom_enter,
				// R.anim.zoom_exit);
				break;
			case CourseInfoModel.WHAT_GETSUBCOURSEINFO_SUCCEED:
				downloadAerialPhotoBean = (DownloadAerialPhotoBean) msg.obj;
				aerialPhotoCount = downloadAerialPhotoBean
						.getAerialPhotoBeanList().size();
				mProgress.setMax(aerialPhotoCount);
				Message message = new Message();
				message.what = CourseInfoModel.WHAT_PROGRESS;
				message.arg1 = msg.arg1;
				message.arg2 = AerialPhotoBean.DOWNLOAD_SUCCESSED;
				handler.sendMessage(message);
				break;
			case WHAT_GOTTEN_LOCATION: {

				// stop the GPS Function
				LocationUtil.getInstance(null).clearHandlerInfo();
				Log.i(TAG, "gotten gps position!");
			}
				break;
			case WHAT_LOAD_COURSELIST: {
				courseInfoModel.startSyncCourseList(handler);
			}
				break;
			case WHAT_LOAD_NETDATA:{
				courseInfoModel.loadNetworkData(1, 6, adapter, listView);
			}
				break;
			case CourseInfoModel.WHAT_FILD: {
				if (mProgress != null && mProgress.isShowing()) {
					mProgress.dismiss();
					mProgress = null;
				}
				showToast(getString(R.string.bad_network));
			}
				break;
			case CourseInfoModel.WHAT_PROGRESS:
				if (mProgress != null && mProgress.isShowing()) {
					if (msg.arg2 != AerialPhotoBean.DOWNLOAD_SUCCESSED) {
						mProgress.dismiss();
						mProgress = null;
						showToast(getString(R.string.bad_network));
					} else {
						mProgress.setProgress(msg.arg1);
						if (msg.arg1 < aerialPhotoCount) {
							courseInfoModel.downloadAerialPhoto(handler,
									downloadAerialPhotoBean, msg.arg1);
						} else {
							mProgress.dismiss();
							mProgress = null;
							CoursesObject coursesObject = downloadAerialPhotoBean
									.getCoursesObject();
							coursesObject.setSubCourses(downloadAerialPhotoBean
									.getSubCourses());
							Message message2 = new Message();
							message2.what = CourseInfoModel.WHAT_GETSUBCOURSE_SUCCEED;
							message2.obj = coursesObject;
							handler.sendMessage(message2);
						}
					}
				}
				break;
			default:
				break;
			}
		}
	};
	private boolean isSave = false;

	@Override
	protected void onStop() {
		LocationUtil location = LocationUtil.getInstance(this
				.getApplicationContext());
		location.clearHandlerInfo();
		if (isSave ) {
			
			try {
				String courseInfo = FileUtil
						.setBase64Object(courseList);
				Log.d(TAG, courseInfo);
				long timestamp = System.currentTimeMillis();
				String fileName = "crash-" + timestamp + ".txt";
				if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
					File dir = new File(Contexts.EXCEPTION_PATH);
					if (!dir.exists()) {
						dir.mkdirs();
					}
					FileOutputStream fos = new FileOutputStream(Contexts.EXCEPTION_PATH + fileName);
					fos.write(courseInfo.getBytes());
					fos.close();
				}
			} catch (Exception e) {
				Log.e(TAG, "an error occured while writing file...", e);
			}
		}
		super.onStop();
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// save the item property
		this.firstVisibleItem = firstVisibleItem;
		this.visibleItemCount = visibleItemCount;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		this.scrollState = scrollState;
		Log.i(TAG, "scroll state:" + scrollState);
		// start the loader
		if (scrollState == SCROLL_STATE_IDLE && firstVisibleItem > 0) {
			Log.i(TAG, "start to load network data");
			courseInfoModel.loadNetworkData(firstVisibleItem, visibleItemCount,
					adapter, listView);
		}
	}

	void actionRefreshCourseList() {
		LocationUtil util = LocationUtil.getInstance(AroundActivity.this
				.getApplicationContext());
		Log.i(TAG, "lon:" + util.longitude + " lat:" + util.latitude);
		courseList = courseInfoModel.searchAroundCourseList(util.longitude,
				util.latitude, text_search.getText().toString());
		adapter.setCoursesList(courseList);
		adapter.notifyDataSetChanged();
		
		handler.sendEmptyMessageDelayed(WHAT_LOAD_NETDATA, 500);
	}

	@Override
	protected void onResume() {
		if (PlayGameModel.getInstance(this).getPlayGameObject() != null) {
			btn_continue.setVisibility(View.VISIBLE);
		} else {
			btn_continue.setVisibility(View.GONE);
		}
		super.onResume();
	}
	
}
