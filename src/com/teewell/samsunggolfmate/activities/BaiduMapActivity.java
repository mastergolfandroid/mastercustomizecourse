package com.teewell.samsunggolfmate.activities;

import java.util.List;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKGeneralListener;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MKEvent;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.PoiOverlay;
import com.baidu.mapapi.map.RouteOverlay;
import com.baidu.mapapi.search.MKAddrInfo;
import com.baidu.mapapi.search.MKBusLineResult;
import com.baidu.mapapi.search.MKDrivingRouteResult;
import com.baidu.mapapi.search.MKPlanNode;
import com.baidu.mapapi.search.MKPoiResult;
import com.baidu.mapapi.search.MKRoute;
import com.baidu.mapapi.search.MKSearch;
import com.baidu.mapapi.search.MKSearchListener;
import com.baidu.mapapi.search.MKShareUrlResult;
import com.baidu.mapapi.search.MKSuggestionResult;
import com.baidu.mapapi.search.MKTransitRouteResult;
import com.baidu.mapapi.search.MKWalkingRouteResult;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.views.AroundSearchAdapter;

public class BaiduMapActivity extends Activity implements OnClickListener {
	private static final String KEY = "A03e2328328f31b094467c8c2661f79f";// 定义我的key变量
	private MapView bMapView;// 定义地图层变量
	private Button btn_course_position, btn_mapType, btn_route, btn_around,
			btn_trafic;
	private BMapManager mBMapMan = null;// 百度MapAPI的管理类
	private MyLocationOverlay mLocationOverlay = null;// 定位图层
	private MKSearch searchModel;

	private GeoPoint startPoint;
	private GeoPoint endPoint;

	private String mapType = MAPTYPE_STREET;
	private static final String MAPTYPE_SATELLITE = "satellite";
	private static final String MAPTYPE_STREET = "street";
	private boolean isTrafic;

	Bitmap myBitmap;// 起点终点和画线
	Paint linePaint;// 画笔
	private boolean locationFlag = false; // 是否处于画线阶段
	private Point endScreenCoords;
	private MyOverLay mapOverlay;
	private List<Overlay> listOfOverlays;
	private CoursesObject coursesObject;
	private LocationClient mLocClient;
	private LocationData locData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBMapMan = new BMapManager(this);// 初始化MapAPI
		mBMapMan.init(KEY, new MyGeneralListener());// 使用所申请的KEY
		setContentView(R.layout.baidu_map);
		coursesObject = (CoursesObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_COURSESOBJECT);
		initView();
		note_Intent(BaiduMapActivity.this);// 检查网络是否打开
		initMap();
		initData();
		initLocation();
	}

	void initLocation() {
		// 定位初始化
		LocationUtil.getInstance(getApplicationContext()).refreshLocation();
		mLocClient = new LocationClient(this);
		mLocClient.registerLocationListener(mLocationListener);
		locData = new LocationData();
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(1000);
		mLocClient.setLocOption(option);
		mLocClient.start();
		// 定位图层初始化
		mLocationOverlay = new MyLocationOverlay(bMapView);
		// 设置定位数据
		mLocationOverlay.setData(locData);
		// 添加定位图层
		bMapView.getOverlays().add(mLocationOverlay);
		mLocationOverlay.enableCompass();
		// 修改定位数据后刷新图层生效
		bMapView.refresh();

	}

	private void initView() {
		bMapView = (MapView) findViewById(R.id.bmapView);// 初始化地图层变量
		btn_around = (Button) findViewById(R.id.btn_around);
		btn_course_position = (Button) findViewById(R.id.btn_course_position);
		btn_mapType = (Button) findViewById(R.id.btn_mapType);
		btn_route = (Button) findViewById(R.id.btn_route);
		btn_trafic = (Button) findViewById(R.id.btn_trafic);
		btn_around.setOnClickListener(this);
		btn_course_position.setOnClickListener(this);
		btn_mapType.setOnClickListener(this);
		btn_route.setOnClickListener(this);
		btn_trafic.setOnClickListener(this);

		myBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.mymap_baidu);// 调用资源
	}

	private void initMap() {

		// bMapView.setBuiltInZoomControls(true);// 设置启用内置的缩放控件
		MapController bMapController = bMapView.getController();// 得到地图层的控制权，可以用它控制和驱动平移和缩放
		bMapController.setZoom(17);// 设置地图ZOOM级别
		bMapView.getOverlays().add(mLocationOverlay);// //将当前位置的层添加到地图底层中
		// mLocationOverlay.enableCompass(); // 打开指南针

		// 初始化搜索模块
		searchModel = new MKSearch();
		// 设置路线策略为最短距离
		searchModel.setDrivingPolicy(MKSearch.ECAR_DIS_FIRST);
		searchModel.init(mBMapMan, mkSearchListener);
	}

	private void initData() {
		linePaint = new Paint();
		linePaint.setColor(Color.GREEN);// 画笔颜色
		linePaint.setStyle(Paint.Style.FILL);// 画笔风格
		linePaint.setStrokeWidth(3);// 画笔粗度
		endScreenCoords = new Point();// 创建屏幕一个结束坐标的点
		linePaint.setAntiAlias(true);// 抗锯齿

		// 用得到的经纬度构造一个GeoPoint
		endPoint = new GeoPoint((int) (Double.valueOf(coursesObject
				.getLatitude()) * 1e6), (int) (Double.valueOf(coursesObject
				.getLongitude()) * 1e6));

		// 设置地图中心
		bMapView.getController().animateTo(endPoint);
		mapOverlay = new MyOverLay();
		listOfOverlays = bMapView.getOverlays();
		listOfOverlays.clear();
		listOfOverlays.add(mapOverlay);
	}

	private BDLocationListener mLocationListener = new BDLocationListener() {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// TODO Auto-generated method stub
			System.out.println(location);
			if (location != null) {
				// 用得到的经纬度构造一个GeoPoint
				locData.latitude = location.getLatitude();
				locData.longitude = location.getLongitude();
				// 如果不显示定位精度圈，将accuracy赋值为0即可
				locData.accuracy = location.getRadius();
				// 此处可以设置 locData的方向信息, 如果定位 SDK 未返回方向信息，用户可以自己实现罗盘功能添加方向信息。
				locData.direction = location.getDerect();
				// 更新定位数据
				mLocationOverlay.setData(locData);
				// 更新图层数据执行刷新后生效
				bMapView.refresh();
				startPoint = new GeoPoint((int) (location.getLatitude() * 1e6),
						(int) (location.getLongitude() * 1e6));

			}
		}

		@Override
		public void onReceivePoi(BDLocation arg0) {
			// TODO Auto-generated method stub
			System.out.println("onReceiverPoi" + arg0);
		}
	};
	private MKSearchListener mkSearchListener = new MKSearchListener() {
		// 获取驾车路线回调方法
		@Override
		public void onGetDrivingRouteResult(MKDrivingRouteResult res, int error) {
			// 错误号可参考MKEvent中的定义
			Log.e("baidumapdebug", error + "          " + res.getNumPlan());
			if (error != 0 || res == null) {
				Toast.makeText(BaiduMapActivity.this, "抱歉，未找到结果",
						Toast.LENGTH_SHORT).show();
				return;
			}
			RouteOverlay routeOverlay = new RouteOverlay(BaiduMapActivity.this,
					bMapView);

			// 此处仅展示一个方案作为示例
			MKRoute route = res.getPlan(0).getRoute(0);
//			int distanceM = route.getDistance();

//			for (int i = 0; i < route.getNumSteps(); i++) {
//				MKStep step = route.getStep(i);
//			}
			routeOverlay.setData(route);
			bMapView.getOverlays().clear();
			bMapView.getOverlays().add(routeOverlay);
			bMapView.refresh();
			bMapView.getController().animateTo(res.getStart().pt);
		}

		// 以下两种方式和上面的驾车方案实现方法一样
		@Override
		public void onGetWalkingRouteResult(MKWalkingRouteResult res, int error) {
			// 获取步行路线
		}

		@Override
		public void onGetTransitRouteResult(MKTransitRouteResult arg0, int arg1) {
			// 获取地铁线路
		}

		@Override
		public void onGetBusDetailResult(MKBusLineResult arg0, int arg1) {
			// 获取公交线路
		}

		@Override
		public void onGetAddrResult(MKAddrInfo arg0, int arg1) {
		}

		// @Override
		// public void onGetSuggestionResult(MKSuggestionResult arg0, int arg1)
		// {
		// }
		@Override
		public void onGetPoiResult(MKPoiResult result, int type, int error) {
			if (error != 0) {
				Toast.makeText(BaiduMapActivity.this, "抱歉，未查找到结果",
						Toast.LENGTH_SHORT).show();
			} else {
				if (result == null) {
					Toast.makeText(BaiduMapActivity.this,
							"抱歉，您填写的搜索条件，未查找到结果，换个条件试试！", Toast.LENGTH_SHORT)
							.show();
					return;
				}
				// 创建POI内置的Overlay对象
				PoiOverlay poiOverlay = new PoiOverlay(BaiduMapActivity.this,
						bMapView);
				// 符合搜索条件的所有点
				poiOverlay.setData(result.getAllPoi());
				// 向覆盖物列表中添加覆盖物对象PoiOverlay
				bMapView.getOverlays().add(poiOverlay);
				// 刷新地图
				bMapView.refresh();
			}

		}

		@Override
		public void onGetPoiDetailSearchResult(int arg0, int arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onGetShareUrlResult(MKShareUrlResult arg0, int arg1,
				int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onGetSuggestionResult(MKSuggestionResult arg0, int arg1) {
			// TODO Auto-generated method stub

		}
	};

	public boolean note_Intent(Context context) {
		ConnectivityManager con = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkinfo = con.getActiveNetworkInfo();
		if (networkinfo == null || !networkinfo.isAvailable()) {
			// 当前网络不可用
			Toast.makeText(this, "网络连接失败，请检查是否连接到网络", Toast.LENGTH_SHORT)
					.show();
			return false;
		}
		boolean wifi = con.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
				.isConnectedOrConnecting();
		if (!wifi) { // 提示使用wifi
			Toast.makeText(this, "建议您使用WIFI以减少流量", Toast.LENGTH_SHORT).show();
		}
		return true;
	}

	@Override
	protected void onDestroy() {
		// 建议在您app的退出之前调用mapadpi的destroy()函数，避免重复初始化带来的时间消耗
		bMapView.destroy();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// mLocationOverlay.disableMyLocation();
		// mLocationOverlay.disableCompass(); // 关闭指南针
		bMapView.onPause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// mLocationOverlay.enableMyLocation();
		// mLocationOverlay.enableCompass(); // 打开指南针
		bMapView.onResume();
		super.onResume();
	}

	protected boolean isRouteDisplayed() {
		return false;
	}

	class MyOverLay extends Overlay {
		public void draw(Canvas canvas, MapView bMapView, boolean shadow) {
			if (!locationFlag) {
				bMapView.getProjection().toPixels(endPoint, endScreenCoords);// 转换为屏幕坐标
				canvas.drawBitmap(myBitmap,
						endScreenCoords.x - (myBitmap.getWidth() / 2),
						endScreenCoords.y - myBitmap.getHeight(), linePaint);// 在终点画出终点图kjkjkj
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_course_position:
			locationFlag = false;
			// 设置地图中心
			bMapView.getController().animateTo(endPoint);
			listOfOverlays.clear();
			listOfOverlays.add(mapOverlay);
			break;
		case R.id.btn_mapType:
			if (mapType.equals(MAPTYPE_STREET)) {
				mapType = MAPTYPE_SATELLITE;
				btn_mapType.setBackgroundResource(R.drawable.btn_satelite);
				bMapView.setSatellite(true);// 卫星地图
			} else {
				mapType = MAPTYPE_STREET;
				btn_mapType.setBackgroundResource(R.drawable.btn_map);
				bMapView.setSatellite(false);// 卫星地图
			}
			break;
		case R.id.btn_route:
			// 设置起始地（当前位置）
			if (startPoint == null) {
				double latitudel = LocationUtil
						.getInstance(getApplicationContext()).latitude;
				double lLongitude = LocationUtil
						.getInstance(getApplicationContext()).longitude;
				startPoint = new GeoPoint((int) (latitudel * 1e6),
						(int) (lLongitude * 1e6));
			}
			MKPlanNode startNode = new MKPlanNode();
			startNode.pt = startPoint;
			// 设置目的地
			MKPlanNode endNode = new MKPlanNode();
			endNode.pt = endPoint;

			// 展开搜索的城市
			// String city = getResources().getString(R.string.beijing);
			// System.out.println("----"+city+"---"+destination+"---"+pt);
			// searchModel.drivingSearch(null, startNode, null, endNode);
			Log.e("baidumapdebug",
					"startsearch stat:"
							+ searchModel.drivingSearch(null, startNode, null,
									endNode) + "Start poi " + startNode.pt
							+ "end poi = " + endNode.pt);
			// searchModel.drivingSearch(city, startNode, city, endNode);

			locationFlag = true;
			listOfOverlays.clear();
			break;
		case R.id.btn_around:
			final Dialog dialog_search = new Dialog(BaiduMapActivity.this,
					R.style.tip_dialog);
			dialog_search.setContentView(R.layout.around_search);
			ListView listView = (ListView) dialog_search
					.findViewById(R.id.list);
			final AroundSearchAdapter adapter = new AroundSearchAdapter(
					BaiduMapActivity.this);
			listView.setAdapter(adapter);
			listView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					dialog_search.dismiss();
					locationFlag = false;
					// 设置地图中心
					bMapView.getController().animateTo(endPoint);
					listOfOverlays.clear();
					listOfOverlays.add(mapOverlay);
					// 根据中心点、半径与检索词发起周边检索.
					// 参数列表：关键词、中心点地理坐标、半径，单位:米
					searchModel.poiSearchNearBy(adapter.getItem(position),
							endPoint, 1000);
				}
			});
			dialog_search.show();

			break;
		case R.id.btn_trafic:
			if (!isTrafic) {
				isTrafic = true;
				bMapView.setTraffic(true);// 交通地图
				btn_trafic
						.setBackgroundResource(R.drawable.nav_btn_check_its_on_normal);
			} else {
				isTrafic = false;
				bMapView.setTraffic(false);// 交通地图
				btn_trafic
						.setBackgroundResource(R.drawable.nav_btn_check_its_off_normal);
			}
			break;

		}
	}

	// 常用事件监听，用来处理通常的网络错误，授权验证错误等
	class MyGeneralListener implements MKGeneralListener {
		@Override
		public void onGetNetworkState(int iError) {
			Log.d("MyGeneralListener", "onGetNetworkState error is " + iError);
			Toast.makeText(BaiduMapActivity.this, "您的网络出错啦！", Toast.LENGTH_LONG)
					.show();
		}

		@Override
		public void onGetPermissionState(int iError) {
			Log.d("MyGeneralListener", "onGetPermissionState error is "
					+ iError);
			if (iError == MKEvent.ERROR_PERMISSION_DENIED) {
				// 授权Key错误：
				Toast.makeText(BaiduMapActivity.this,
						"请在BMapApiDemoApp.java文件输入正确的授权Key！", Toast.LENGTH_LONG)
						.show();
			}
		}
	}
}
