package com.teewell.samsunggolfmate.activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.DownloadAerialPhotoBean;
import com.teewell.samsunggolfmate.beans.ProvinceBean;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.course.CourseInfoModel;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.utils.VolleyUitl;
import com.teewell.samsunggolfmate.views.CourseListAdapter;
import com.teewell.samsunggolfmate.views.ProgressDialogTitleView;
import com.teewell.samsunggolfmate.views.ProvinceNameAdapter;
import com.teewell.samsunggolfmate.views.XListView;

/**
 * @Project GolfMate
 * @package com.teewell.golfmate.activities
 * @title CountryCoursesActivity.java
 * @Description TODO
 * @author Administrator
 * @date 2013-1-16 下午1:26:44
 * @Copyright Copyright(C) 2013-1-16
 * @version 1.0.0
 */
public class CountryCoursesActivity extends ParentActivity implements
		OnScrollListener, OnItemClickListener, OnClickListener {
	private final String TAG = CountryCoursesActivity.class.getName();
	private ListView listView_name;
	private XListView listView;
	private List<CoursesObject> courseList;
	private List<ProvinceBean> provinceList;
	private CoursesObject coursesObject;
	private CourseListAdapter adapter;

	private Button btn_cancel, btn_continue;
	private EditText edit_search;
	private CourseInfoModel courseInfoModel;
	private ProvinceNameAdapter provinceAdapter;
	private int firstVisibleItem;
	private int visibleItemCount;
	private int scrollState;
	private ProgressDialog mProgress;

	public static final int WHAT_GOTTEN_LOCATION = 102;
	public static final int WHAT_LOAD_COURSELIST = 104;	
	public static final int WHAT_LOAD_NETDATA = 105;
	private static final int REFRESH_DELAY = 1000 * 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.country_courses);
		initView();
	}

	void reloadAllData() {
		// 启动location util
		LocationUtil location = LocationUtil.getInstance(this
				.getApplicationContext());
		location.setHandlerInfo(handler, WHAT_GOTTEN_LOCATION,
				LocationUtil.LOC_ONCE);

		handler.sendEmptyMessageDelayed(WHAT_LOAD_COURSELIST, REFRESH_DELAY);
	}

	/**
	 * 进度条Dialog
	 */
	private void progressDialog(CoursesObject object) {
		mProgress = null;
		mProgress = new ProgressDialog(this);
		mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

		mProgress.setButton(getString(R.string.cancel_aysncCourse),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						VolleyUitl.getInstence(getApplicationContext()).getRequestQueue().cancelAll(handler);
					}
				});

		mProgress.setCustomTitle(new ProgressDialogTitleView(this, object)
				.getView());
		if (!mProgress.isShowing()) {
			mProgress.show();
			mProgress.setCancelable(false);
			mProgress.setProgress(0);
		}
	}

	/**
	 * 初始化组件及监听
	 */
	private void initView() {
		TextView tv_titleName = (TextView) findViewById(R.id.tv_titleName);
		tv_titleName.setText(getString(R.string.allcourses));
		btn_continue = (Button) findViewById(R.id.btn_rightTitle);
		btn_continue.setText(getString(R.string.play_continue));
		btn_continue.setOnClickListener(this);
		edit_search = (EditText) findViewById(R.id.edit_search);
		edit_search.addTextChangedListener(tbxSearch_TextChanged);
		edit_search.setOnTouchListener(txtSearch_OnTouch);
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		btn_cancel.setOnClickListener(this);

		listView = (XListView) findViewById(R.id.listView);
		listView.setOnItemClickListener(this);
		listView.setOnScrollListener(this);
		listView.setXListViewListener(new XListView.IXListViewListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				reloadAllData();
			}

			@Override
			public void onLoadMore() {
				// TODO Auto-generated method stub
				listView.stopLoadMore();

			}
		});
		listView_name = (ListView) findViewById(R.id.list_name);
		listView_name.setOnItemClickListener(listener_provinceName);

		courseInfoModel = CourseInfoModel.getInstance(this);
		adapter = new CourseListAdapter(this);
		provinceAdapter = new ProvinceNameAdapter(this);
		listView_name.setAdapter(provinceAdapter);
		listView.setAdapter(adapter);

		listView.launchRefresh();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		view.setSelected(false);

		if ((scrollState != SCROLL_STATE_IDLE)
				&& (scrollState != SCROLL_STATE_FLING || (firstVisibleItem + visibleItemCount) < adapter
						.getCount())) {
			return;
		}
		coursesObject = adapter.getItem(position - 1);
		if (PlayGameModel.getInstance(this).getPlayGameObject() == null) {
			progressDialog(coursesObject);
			courseInfoModel.getSubCourseInfo(coursesObject, handler);
		} else {
			Dialog dialog = new AlertDialog.Builder(CountryCoursesActivity.this)
					.setMessage(R.string.play_hint)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface arg0,
										int arg1) {
									arg0.dismiss();
									PlayGameModel.getInstance(CountryCoursesActivity.this).setPlayGameObject(null); 
									progressDialog(coursesObject);
									courseInfoModel.getSubCourseInfo(
											coursesObject, handler);
								};
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									Intent intent = new Intent(
											CountryCoursesActivity.this,
											PlayTabActivity.class);
									// 如果已经打开过的实例，将不会重新打开新的Activity
									intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
									intent.putExtra(
											KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
											PlayGameModel.getInstance(CountryCoursesActivity.this).getPlayGameObject());
									startActivity(intent);
								}
							}).create();
			dialog.show();
		}
	}

	private OnItemClickListener listener_provinceName = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			ProvinceBean bean = provinceList.get(position);
			Log.e(TAG, "Province POS:" + bean.getProvinceName() + bean.getPos());
			listView.setSelection(bean.getPos());

			handler.sendEmptyMessageDelayed(WHAT_LOAD_NETDATA, 500);

		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_cancel:
			edit_search.setText("");
			break;
		case R.id.btn_rightTitle:
			Intent intent = new Intent(CountryCoursesActivity.this,
					PlayTabActivity.class);
			// 如果已经打开过的实例，将不会重新打开新的Activity
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
					PlayGameModel.getInstance(this).getPlayGameObject());
			startActivity(intent);
			break;
		}
	}

	/**
	 * 动态搜索
	 */
	private TextWatcher tbxSearch_TextChanged = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			if (s != null && s.length() > 0) {
				btn_cancel.setVisibility(View.VISIBLE);
			} else {
				btn_cancel.setVisibility(View.GONE);
			}
			actionRefreshCourseList();
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		/**
		 * 随着文本框内容改变动态改变列表内容
		 */
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}
	};
	private OnTouchListener txtSearch_OnTouch = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_UP:
				int curX = (int) event.getX();
				if (curX > v.getWidth() - 38
						&& !TextUtils.isEmpty(edit_search.getText())) {
					edit_search.setText("");
					int cacheInputType = edit_search.getInputType();// backup
																	// the input
																	// type
					edit_search.setInputType(InputType.TYPE_NULL);// disable
																	// soft
																	// input
					edit_search.onTouchEvent(event);// call native handler
					edit_search.setInputType(cacheInputType);// restore input
																// type
					return true;// consume touch even
				}
				break;
			}
			return false;
		}
	};

	private Handler handler = new Handler() {
		private DownloadAerialPhotoBean downloadAerialPhotoBean;
		private int aerialPhotoCount;

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case WHAT_GOTTEN_LOCATION:

				// stop the GPS Function
				LocationUtil.getInstance(null).clearHandlerInfo();
				Log.i(TAG, "gotten gps position!");
				break;
			case CourseInfoModel.WHAT_SYNCDATA_SUCCEED:
				if (!"".equals(edit_search.getText().toString())) {
					edit_search.setText("");
				}
				SimpleDateFormat format = new SimpleDateFormat(
						getString(R.string.date_format));
				Date now = new Date();

				listView.setRefreshTime(format.format(now));
				actionRefreshCourseList();
				listView.stopRefresh();
				listView.setPullLoadEnable(false);
				showToast(getString(R.string.update_succ_tip));
				break;
			case CourseInfoModel.WHAT_SYNCDATA_FAILED: {
				if (!"".equals(edit_search.getText().toString())) {
					edit_search.setText("");
				}
				showToast(getString(R.string.bad_network));
				actionRefreshCourseList();
				listView.stopRefresh();
				listView.setPullLoadEnable(false);
			}
				break;
			case CourseInfoModel.WHAT_GETSUBCOURSE_SUCCEED:
				if (mProgress != null && mProgress.isShowing()) {
					mProgress.dismiss();
					mProgress = null;
				}
				Intent intent = new Intent(CountryCoursesActivity.this,
						StartGameActivity.class);
				intent.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
						(CoursesObject) msg.obj);
				PlayGameModel.fromActivity = 1;
				startActivity(intent);
				break;
			case CourseInfoModel.WHAT_GETSUBCOURSEINFO_SUCCEED:
				downloadAerialPhotoBean = (DownloadAerialPhotoBean) msg.obj;
				aerialPhotoCount = downloadAerialPhotoBean
						.getAerialPhotoBeanList().size();
				mProgress.setMax(aerialPhotoCount);
				Message message = new Message();
				message.what = CourseInfoModel.WHAT_PROGRESS;
				message.arg1 = msg.arg1;
				message.arg2 = AerialPhotoBean.DOWNLOAD_SUCCESSED;
				handler.sendMessage(message);
				break;
			case WHAT_LOAD_COURSELIST: {
				courseInfoModel.startSyncCourseList(handler);
			}
				break;
			case WHAT_LOAD_NETDATA:{
				courseInfoModel.loadNetworkData(1, 6, adapter, listView);
			}
				break;
			case CourseInfoModel.WHAT_FILD: {
				if (mProgress != null && mProgress.isShowing()) {
					mProgress.dismiss();
					mProgress = null;
				}
				showToast(getString(R.string.bad_network));
			}
				break;
			case CourseInfoModel.WHAT_PROGRESS:
				if (mProgress != null && mProgress.isShowing()) {
					if (msg.arg2 != AerialPhotoBean.DOWNLOAD_SUCCESSED) {
						mProgress.dismiss();
						mProgress = null;
						showToast(getString(R.string.bad_network));
					} else {
						mProgress.setProgress(msg.arg1);
						if (msg.arg1 < aerialPhotoCount) {
							courseInfoModel.downloadAerialPhoto(handler,
									downloadAerialPhotoBean, msg.arg1);
						} else {
							mProgress.dismiss();
							mProgress = null;
							CoursesObject coursesObject = downloadAerialPhotoBean
									.getCoursesObject();
							coursesObject.setSubCourses(downloadAerialPhotoBean
									.getSubCourses());
							Message message2 = new Message();
							message2.what = CourseInfoModel.WHAT_GETSUBCOURSE_SUCCEED;
							message2.obj = coursesObject;
							handler.sendMessage(message2);
						}
					}
				}
				break;
			}
		}
	};

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		// save the item property
		this.firstVisibleItem = firstVisibleItem;
		this.visibleItemCount = visibleItemCount;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		this.scrollState = scrollState;
		Log.i(TAG, "scroll state:" + scrollState);
		// start the loader
		if (scrollState == SCROLL_STATE_IDLE && firstVisibleItem > 0) {
			Log.i(TAG, "start to load network data");
			courseInfoModel.loadNetworkData(firstVisibleItem, visibleItemCount,
					adapter, listView);
		}
	}

	void actionRefreshCourseList() {
		if (null == provinceList) {
			provinceList = new ArrayList<ProvinceBean>();
		}

		courseList = courseInfoModel.searchAllCourseByProvinceCity(edit_search
				.getText().toString(), provinceList);
		if (null != courseList) {
			adapter.setCoursesList(courseList);
			adapter.notifyDataSetChanged();

			provinceAdapter.setList(provinceList);
			courseInfoModel.loadNetworkData(firstVisibleItem, visibleItemCount,
					adapter, listView);
			
			handler.sendEmptyMessageDelayed(WHAT_LOAD_NETDATA, 500);

		}
		if (provinceList.size() > 0) {
			// listView.setImageFooterViewVisiable(true);
			listView_name.setVisibility(View.VISIBLE);
		} else {
			// listView.setImageFooterViewVisiable(false);
			listView_name.setVisibility(View.GONE);
		}

	}

	@Override
	protected void onResume() {
		if (PlayGameModel.getInstance(this).getPlayGameObject() != null) {
			btn_continue.setVisibility(View.VISIBLE);
		} else {
			btn_continue.setVisibility(View.GONE);
		}
		super.onResume();
	}

}
