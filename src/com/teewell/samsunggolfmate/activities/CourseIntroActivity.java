package com.teewell.samsunggolfmate.activities;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;
import com.baidu.mobstat.StatService;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;

public class CourseIntroActivity extends TabActivity implements
		OnCheckedChangeListener {
	/**
	 * tabhost标签按钮
	 */
	public static final String ONE = "situation";
	public static final String TWO = "baidumap";
	public static final String THREE = "fairway";
	public static final String FOUR = "images";
	public static final String FIVE = "evaluate";
	private TabHost tabHost;
	private Button btn_back;
	private TextView tv_courseName;
	private CoursesObject coursesObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.course_intro);
		coursesObject = (CoursesObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_COURSESOBJECT);
		initRadios();
		setupIntent();
	}

	private void initRadios() {
		((RadioButton) findViewById(R.id.btn_situation))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.btn_navigation))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.btn_fairway))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.btn_images))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.btn_evaluate))
				.setOnCheckedChangeListener(this);

		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setText(coursesObject.getShortName());
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		tv_courseName = (TextView) findViewById(R.id.tv_titleName);
		tv_courseName.setText(coursesObject.getShortName());
	}

	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			switch (buttonView.getId()) {
			case R.id.btn_situation:
				this.tabHost.setCurrentTabByTag(ONE);
				break;
			case R.id.btn_navigation:
				this.tabHost.setCurrentTabByTag(TWO);
				break;
			case R.id.btn_fairway:
				this.tabHost.setCurrentTabByTag(THREE);
				break;
			case R.id.btn_images:
				this.tabHost.setCurrentTabByTag(FOUR);
				break;
			case R.id.btn_evaluate:
				this.tabHost.setCurrentTabByTag(FIVE);
				break;
			}
		}
	}

	private void setupIntent() {
		this.tabHost = getTabHost();
		TabHost localTabHost = this.tabHost;
		Intent intent_situation = new Intent(this, SituationActivity.class);
		intent_situation.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
				coursesObject);
		Intent intent_baidumap = new Intent(this, BaiduMapActivity.class);
		intent_baidumap.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
				coursesObject);
		Intent intent_fairway = new Intent(this, FairwayActivity.class);
		Intent intent_images = new Intent(this, ImagesActivity.class);
		Intent intent_evaluate = new Intent(this, EvaluateActivity.class);
		intent_evaluate.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
				coursesObject);
		localTabHost.setCurrentTabByTag(ONE);
		localTabHost.addTab(buildTabSpec(ONE, R.string.situation,
				R.drawable.btn_courseintro_select, intent_situation));

		localTabHost.addTab(buildTabSpec(TWO, R.string.navigation,
				R.drawable.btn_baidu_select, intent_baidumap));

		localTabHost.addTab(buildTabSpec(THREE, R.string.fairway,
				R.drawable.btn_fairway_select, intent_fairway));

		localTabHost.addTab(buildTabSpec(FOUR, R.string.scores,
				R.drawable.btn_scores_select, intent_images));

		localTabHost.addTab(buildTabSpec(FIVE, R.string.evaluate,
				R.drawable.btn_evaluate_select, intent_evaluate));

	}

	private TabHost.TabSpec buildTabSpec(String tag, int resLabel, int resIcon,
			final Intent content) {
		return this.tabHost
				.newTabSpec(tag)
				.setIndicator(getString(resLabel),
						getResources().getDrawable(resIcon))
				.setContent(content);
	}

	@Override
	protected void onResume() {
		super.onResume();
		StatService.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		/**
		 * 页面结束（每个Activity中都需要添加，如果有继承的父Activity中已经添加了该调用，那么子Activity中务必不能添加）
		 */
		StatService.onPause(this);
	}
}
