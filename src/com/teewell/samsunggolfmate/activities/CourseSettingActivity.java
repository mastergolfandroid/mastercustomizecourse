package com.teewell.samsunggolfmate.activities;

import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.utils.DistanceDataUtil;
import com.teewell.samsunggolfmate.utils.DistanceUtil;
import com.teewell.samsunggolfmate.utils.SharedPreferencesUtils;
import com.teewell.samsunggolfmate.utils.StringUtil;
import com.teewell.samsunggolfmate.views.SeekListAdapter;

public class CourseSettingActivity extends ParentActivity implements
		OnClickListener {
//	private final String TAG = CourseSettingActivity.class.getName();
	private SharedPreferencesUtils utils;
	private Button btn_back, btn_add, btn_remove;
	private ListView list_seek;
	private TextView tv_distance_near, tv_distance_far;
	private RelativeLayout layout_distance_near, layout_distance_far;
	public static final int MIN_PROGRESS = 120;
	public static final int MAX_PROGRESS = 360;
	public static final int DISTANCE = 10;
	private int[] distanceArc;
	private String show_distance = "";
	private boolean isdel = false;
	private ArrayList<Integer> sbr_date;
	private SeekListAdapter seekListAdapter;
	private ArrayList<Integer> templist;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.course_setting);
		utils = new SharedPreferencesUtils(this);
		distanceArc = DistanceDataUtil.getDistanceArc(this);
		show_distance = DistanceUtil.getDistanceUnit(this,
				PlayGameModel.shortDistanceIndex);
		sbr_date = new ArrayList<Integer>();
		for (int i = 0; i < distanceArc.length; i++) {
			sbr_date.add(distanceArc[i]);
		}
		initView();
	}

	private void initData() {
		init(true);
	}

	private void init(boolean flag) {
		distanceArc = DistanceDataUtil.getDistanceArc(this);
		show_distance = DistanceUtil.getDistanceUnit(this,
				PlayGameModel.shortDistanceIndex);
		String distance_type_far = DistanceUtil.getDistanceUnit(this,
				PlayGameModel.longDistanceIndex);
		if (seekListAdapter != null) {
			seekListAdapter.setUnit(show_distance);
		}
		tv_distance_far.setText(distance_type_far);
		tv_distance_near.setText(show_distance);

	}

	private void initView() {
		seekListAdapter = new SeekListAdapter(show_distance, sbr_date,
				getApplicationContext());
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setText(getString(R.string.return_));
		btn_add = (Button) findViewById(R.id.btn_add);
		btn_remove = (Button) findViewById(R.id.btn_remove);
		list_seek = (ListView) findViewById(R.id.list_seekbar);
		tv_distance_near = (TextView) findViewById(R.id.tv_distance_near);
		tv_distance_far = (TextView) findViewById(R.id.tv_distance_far);
		layout_distance_far = (RelativeLayout) findViewById(R.id.layout_distance_far);
		layout_distance_near = (RelativeLayout) findViewById(R.id.layout_distance_near);
		list_seek.setAdapter(seekListAdapter);
		list_seek.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				System.out.println(isdel);
				if (isdel) {
					isdel = false;
					templist = seekListAdapter.getList();
					templist.remove(position);
					seekListAdapter.setList(templist);
					seekListAdapter.setIsdel(isdel);
				}
			}
		});
		btn_add.setOnClickListener(this);
		btn_back.setOnClickListener(this);
		btn_remove.setOnClickListener(this);
		layout_distance_far.setOnClickListener(this);
		layout_distance_near.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.btn_add:

			templist = seekListAdapter.getList();
			if (templist.size() >= 5) {
				showToast("辅助线不能超过5条！");
			} else {
				templist.add(MIN_PROGRESS);
				seekListAdapter.setList(templist);
			}
			break;

		case R.id.btn_remove:
			if (isdel) {
				isdel = false;
				seekListAdapter.setIsdel(isdel);
				break;
			}
			templist = seekListAdapter.getList();
			if (templist.size() <= 0) {
				showToast("没有可删除的辅助线！");
			} else {
				isdel = true;
				seekListAdapter.setIsdel(isdel);
			}
			break;
		case R.id.layout_distance_near: {
			Intent intent = new Intent(CourseSettingActivity.this,
					ShortDistanceSettingActivity.class);
			startActivity(intent);
		}
			break;
		case R.id.layout_distance_far: {
			Intent intent = new Intent(CourseSettingActivity.this,
					LongDistanceSettingActivity.class);
			startActivity(intent);
		}
			break;
		}

	}

	@Override
	protected void onResume() {
		initData();
		super.onResume();
	}

	@Override
	protected void onPause() {
		templist = seekListAdapter.getList();
		if (templist.size() == 0) {
			utils.commitString(PlayGameModel.DISTANCEARC, "");
		} else {
			String[] strings = new String[templist.size()];
			for (int i = 0; i < templist.size(); i++) {
				strings[i] = String.valueOf(templist.get(i));
			}
			utils.commitString(PlayGameModel.DISTANCEARC,
					StringUtil.addString(strings, Contexts.STRING_SEPARATOR));
		}

		super.onPause();
	}

	@Override
	public void onBackPressed() {
		if (isdel) {
			isdel = false;
			seekListAdapter.setIsdel(isdel);
		} else
			super.onBackPressed();
	}
}
