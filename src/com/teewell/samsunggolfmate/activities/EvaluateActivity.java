package com.teewell.samsunggolfmate.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.course.CourseInfoModel;
import com.teewell.samsunggolfmate.utils.DialogUtil;

public class EvaluateActivity extends ParentActivity {
	public static final int WHAT_SAVE_SUCCEED = 0x1;
	public static final int WHAT_OBTAIN_SUCCEED = 0x4;
	public static final int WHAT_FIALED = 0x2;
	public static final int WHAT_BAD_NET = 0x3;
	private final static int MAXTEXTCOUNT = 200;
	private Button btn_commit;
	private RatingBar ratingBar;
	private EditText ideaMessage;
	private ImageView image;
	private LinearLayout layout_index;
	private RelativeLayout layout_reference;
	private TextView tv_1, tv_2, tv_3, tv_4, tv_5, tv_6;
	private CourseInfoModel courseInfoModel;
	private CoursesObject coursesObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.evaluate);
		coursesObject = (CoursesObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_COURSESOBJECT);
		initView();
		courseInfoModel = CourseInfoModel.getInstance(this);
		Double evaluate = coursesObject.getEvaluate();
		if (evaluate != null) {
			ratingBar.setRating(evaluate.floatValue());
		} else {
			courseInfoModel.obtainEvaluateInfo(handler,
					coursesObject.getCouseID(),null,null);
		}
	}

	private void initView() {
		ratingBar = (RatingBar) findViewById(R.id.ratingBar);
		btn_commit = (Button) findViewById(R.id.btn_commit);
		ideaMessage = (EditText) findViewById(R.id.edit_ideaMessage);
		image = (ImageView) findViewById(R.id.imageView);
		layout_reference = (RelativeLayout) findViewById(R.id.layout_reference);
		layout_index = (LinearLayout) findViewById(R.id.layout_index);
		tv_1 = (TextView) findViewById(R.id.tv_1);
		tv_2 = (TextView) findViewById(R.id.tv_2);
		tv_3 = (TextView) findViewById(R.id.tv_3);
		tv_4 = (TextView) findViewById(R.id.tv_4);
		tv_5 = (TextView) findViewById(R.id.tv_5);
		tv_6 = (TextView) findViewById(R.id.tv_6);
		btn_commit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				commit(null);
			}
		});
		layout_reference.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (layout_index.getVisibility() == View.GONE) {
					layout_index.setVisibility(View.VISIBLE);
					image.setImageResource(R.drawable.group_up);
				} else {
					layout_index.setVisibility(View.GONE);
					image.setImageResource(R.drawable.group_down);
				}
			}
		});
		ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				Log.d("rtbar", "New Rating: " + rating);
				tv_1.setText(String.valueOf(20 * rating / 5));
				tv_2.setText(String.valueOf(20 * rating / 5));
				tv_3.setText(String.valueOf(20 * rating / 5));
				tv_4.setText(String.valueOf(20 * rating / 5));
				tv_5.setText(String.valueOf(10 * rating / 5));
				tv_6.setText(String.valueOf(10 * rating / 5));
			}
		});
		beyondTextAll();
	}

	/**
	 * 监听输入的字数，如超出限制，提示信息
	 */
	private void beyondTextAll() {
		ideaMessage.addTextChangedListener(new TextWatcher() {
			private CharSequence temp;
//			private boolean isEdit = true;
			private int selectionStart;
			private int selectionEnd;

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				temp = s;
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				selectionStart = ideaMessage.getSelectionStart();
				selectionEnd = ideaMessage.getSelectionEnd();
				Log.i("gongbiao1", "" + selectionStart);
				if (temp.length() > MAXTEXTCOUNT) {
					showToast(getString(R.string.beyond_text));
					s.delete(selectionStart - 1, selectionEnd);
					int tempSelection = selectionStart;
					ideaMessage.setText(s);
					ideaMessage.setSelection(tempSelection);
				}
			}
		});
	}

	/**
	 * 将反馈信息发送给服务器
	 */
	private void commit(String message) {
		DialogUtil.setLoadingState(this, true);
		courseInfoModel.saveEvaluateInfo(handler, coursesObject.getCouseID(),
				(int) ratingBar.getRating());
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case WHAT_SAVE_SUCCEED:
				DialogUtil.setLoadingState(EvaluateActivity.this, false);
				showToast("评价成功，感谢您的支持!");
				break;
			case WHAT_FIALED:
				DialogUtil.setLoadingState(EvaluateActivity.this, false);
				showToast(getString(R.string.bad_network));
				break;
			case WHAT_BAD_NET:
				DialogUtil.setLoadingState(EvaluateActivity.this, false);
				showToast(getString(R.string.bad_network));
				break;
			case WHAT_OBTAIN_SUCCEED:
				float values = ((Double) (msg.obj)).floatValue();
				ratingBar.setRating(values);
				break;
			default:
				break;
			}
		};
	};
}
