package com.teewell.samsunggolfmate.activities;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.PlayGameModel;

public class EvaluateOursActivity extends ParentActivity implements
		OnClickListener {
	private RadioButton btn_good, btn_general, btn_bad, btn_suggest;
	private TextView tv_good, tv_general, tv_bad, tv_courseName;
//	private EditText edit_suggest, edit_phone;
	private RelativeLayout layout_phone;
//	private String suggest;
	private Button btn_back, btn_send;
	private CoursesObject coursesObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.evaluate_ours);
		coursesObject = (CoursesObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_COURSESOBJECT);
		initView();
		initData();
	}

	private void initData() {
		tv_courseName.setText(coursesObject.getShortName());
		tv_good.setText(PlayGameModel.curHoleNumber + 1
				+ tv_good.getText().toString());
		tv_general.setText(PlayGameModel.curHoleNumber + 1
				+ tv_general.getText().toString());
		tv_bad.setText(PlayGameModel.curHoleNumber + 1
				+ tv_bad.getText().toString());
		btn_good.setChecked(true);
//		suggest = tv_good.getText().toString();
	}

	private void initView() {
		tv_courseName = (TextView) findViewById(R.id.tv_titleName);
		tv_good = (TextView) findViewById(R.id.tv_good);
		tv_general = (TextView) findViewById(R.id.tv_general);
		tv_bad = (TextView) findViewById(R.id.tv_bad);
		btn_bad = (RadioButton) findViewById(R.id.btn_bad);
		btn_good = (RadioButton) findViewById(R.id.btn_good);
		btn_general = (RadioButton) findViewById(R.id.btn_general);
		btn_suggest = (RadioButton) findViewById(R.id.btn_suggest);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_send = (Button) findViewById(R.id.btn_rightTitle);
//		edit_phone = (EditText) findViewById(R.id.edit_phone);
//		edit_suggest = (EditText) findViewById(R.id.edit_suggest);
		layout_phone = (RelativeLayout) findViewById(R.id.layout_phone);
//		(RelativeLayout) findViewById(R.id.layout_web);
		btn_bad.setOnClickListener(this);
		btn_good.setOnClickListener(this);
		btn_general.setOnClickListener(this);
		btn_suggest.setOnClickListener(this);
		layout_phone.setOnClickListener(this);
		btn_bad.setOnClickListener(this);
		btn_back.setOnClickListener(this);
		btn_send.setOnClickListener(this);

		btn_back.setText(getString(R.string.return_));
		btn_send.setText(getString(R.string.send));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_good:
			btn_good.setChecked(true);
			btn_suggest.setChecked(false);
			btn_bad.setChecked(false);
			btn_general.setChecked(false);
//			suggest = tv_good.getText().toString();
			break;
		case R.id.btn_bad:
			btn_bad.setChecked(true);
			btn_suggest.setChecked(false);
			btn_general.setChecked(false);
			btn_good.setChecked(false);
//			suggest = btn_bad.getText().toString();
			break;
		case R.id.btn_general:
			btn_general.setChecked(true);
			btn_suggest.setChecked(false);
			btn_bad.setChecked(false);
			btn_good.setChecked(false);
//			suggest = btn_general.getText().toString();
			break;
		case R.id.btn_suggest:
			btn_suggest.setChecked(true);
			btn_bad.setChecked(false);
			btn_general.setChecked(false);
			btn_good.setChecked(false);
			break;
		case R.id.layout_phone:
			final Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getString(R.string.apk_title));
			dialog.setMessage(getString(R.string.make_a_phone_call)
					+ getString(R.string.service_hotline));
			dialog.setPositiveButton(getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							Intent intent_call = new Intent(
									Intent.ACTION_CALL,
									Uri.parse("tel:"
											+ getString(R.string.service_hotline)));
							startActivity(intent_call);
						}
					});
			dialog.setNegativeButton(getString(R.string.return_),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			dialog.create().show();
			break;
		case R.id.layout_web:
			Intent intent = new Intent();
			intent.setAction("android.intent.action.VIEW");
			Uri content_url = Uri.parse("http://weibo.cn/yamigolf");
			intent.setData(content_url);
			startActivity(intent);
			break;
		case R.id.btn_rightTitle:
			showToast("评价成功");
			finish();
			break;
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}
	}

}
