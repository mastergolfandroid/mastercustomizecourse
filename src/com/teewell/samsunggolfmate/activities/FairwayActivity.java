package com.teewell.samsunggolfmate.activities;

import java.text.SimpleDateFormat;
import java.util.Date;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.models.course.CourseInfoModel;
import com.teewell.samsunggolfmate.utils.StringUtil;
import com.teewell.samsunggolfmate.utils.TimeUtil;

public class FairwayActivity extends Activity {
//	private final String TAG = FairwayActivity.class.getName();
	private TextView tv_openingTime, tv_designer, tv_totalArea, tv_countHoles,
			tv_totalPar, tv_totalLength, tv_fairGrass, tv_greenGrass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fairway);
		initView();
		initData();
	}

	private void initView() {
		tv_openingTime = (TextView) findViewById(R.id.tv_openingTime);
		tv_designer = (TextView) findViewById(R.id.tv_designer);
		tv_totalArea = (TextView) findViewById(R.id.tv_totalArea);
		tv_countHoles = (TextView) findViewById(R.id.tv_countHoles);
		tv_totalPar = (TextView) findViewById(R.id.tv_totalPar);
		tv_totalLength = (TextView) findViewById(R.id.tv_totalLength);
		tv_fairGrass = (TextView) findViewById(R.id.tv_fairGrass);
		tv_greenGrass = (TextView) findViewById(R.id.tv_greenGrass);

	}

	@SuppressLint("SimpleDateFormat")
	private void initData() {
		SimpleDateFormat localFormater = new SimpleDateFormat(
				TimeUtil.LOCAL_TIME_PATTERN_STRING);
		Date date = CourseInfoModel.coursesInfoBean.getOpeningTime();
		if (date != null) {

			tv_openingTime.setText(localFormater.format(date));
		}
		tv_designer.setText(StringUtil
				.emptyProcess(CourseInfoModel.coursesInfoBean.getDesigner()));
		tv_totalArea.setText(StringUtil
				.emptyProcess(CourseInfoModel.coursesInfoBean.getTotalArea()));
		tv_countHoles.setText(StringUtil
				.emptyProcess(CourseInfoModel.coursesInfoBean.getCountHoles()));
		tv_totalPar.setText(StringUtil
				.emptyProcess(CourseInfoModel.coursesInfoBean.getTotalPar()));
		tv_totalLength
				.setText(StringUtil
						.emptyProcess(CourseInfoModel.coursesInfoBean
								.getTotalLength()));
		tv_fairGrass.setText(StringUtil
				.emptyProcess(CourseInfoModel.coursesInfoBean.getFairGrass()));
		tv_greenGrass.setText(StringUtil
				.emptyProcess(CourseInfoModel.coursesInfoBean.getGreenGrass()));
	}
}
