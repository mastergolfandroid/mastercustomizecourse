package com.teewell.samsunggolfmate.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.UserInfoModel;
import com.teewell.samsunggolfmate.models.course.CourseInfoModel;
import com.teewell.samsunggolfmate.utils.DialogUtil;

/**
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.activities
 * @title FeedbackActivity.java
 * @Description TODO
 * @author Administrator
 * @date 2013-3-15 下午5:17:01
 * @Copyright Copyright(C) 2013-3-15
 * @version 1.0.0
 */
public class FeedbackActivity extends ParentActivity implements OnClickListener {
	private Button btn_back, btn_commit, btn_clean;
	private EditText ideaMessage;
//	private static final String TAG = FeedbackActivity.class.getName();
	private final static int MAXTEXTCOUNT = 200;
	private CourseInfoModel courseInfoModel;
	public static final int WHAT_SAVEFEEDBACKINFO = 101;
	private UserInfoModel userInfoModel;
//	private Dialog alertDialog;
//	private Button btn_no_dialog;
//	private Button btn_yes_dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);
		initView();
		courseInfoModel = CourseInfoModel.getInstance(this);
		userInfoModel = UserInfoModel.getInstance(this);
	}

	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_commit = (Button) findViewById(R.id.btn_commitFeedback);
		btn_clean = (Button) findViewById(R.id.btn_clean);
		ideaMessage = (EditText) findViewById(R.id.edit_ideaMessage);
		btn_back.setOnClickListener(this);
		btn_commit.setOnClickListener(this);
		btn_clean.setOnClickListener(this);
		beyondTextAll();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_commitFeedback:
			String ideaString = ideaMessage.getText().toString().trim();
			if ("".equals(ideaString)) {
				showToast(getString(R.string.feedback_null));
			} else {
				commit(ideaString);
			}
			break;
		case R.id.btn_back:
			finish();
			break;
		case R.id.btn_clean:
			ideaMessage.setText("");
			break;
		default:
			break;
		}
	}

	/**
	 * 将反馈信息发送给服务器
	 */
	private void commit(String message) {
		if (userInfoModel.hasLoggedIn()) {
			DialogUtil.setLoadingState(this, true);
			courseInfoModel.saveFeedbackInfo(handler,
					userInfoModel.getUserId(), 0, message);
		} else {
			showDialog();
		}
	}

	private void showDialog() {
		final Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle(getString(R.string.apk_title));
		dialog.setMessage(getString(R.string.not_login_and_isLogin));
		dialog.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent intent = new Intent(FeedbackActivity.this,
								RegisterLoginActivity.class);
						intent.putExtra(KeyValuesConstant.INTENT_FROM_ACTIVITY,
								getString(R.string.feedback));
						startActivity(intent);
					}
				});
		dialog.setNegativeButton(getString(R.string.return_),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		dialog.create().show();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case WHAT_SAVEFEEDBACKINFO:
				DialogUtil.setLoadingState(FeedbackActivity.this, false);
				showToast(getString(R.string.feedback_successed));
				finish();
				break;
			case CourseInfoModel.WHAT_FILD:
				DialogUtil.setLoadingState(FeedbackActivity.this, false);
				showToast(getString(R.string.bad_network));
				break;
			case UserInfoActivity.MSG_GET_USER_INFO_FAILED:
				if (!userInfoModel.hasLoggedIn()) {
					showDialog();
				}
				break;
			case UserInfoActivity.MSG_GET_USER_INFO_START:
				courseInfoModel.saveFeedbackInfo(handler, userInfoModel
						.getUserId(), 1, ideaMessage.getText().toString());
				break;
			}
		};
	};

	/**
	 * 监听输入的字数，如超出限制，提示信息
	 */
	private void beyondTextAll() {
		ideaMessage.addTextChangedListener(new TextWatcher() {
			private CharSequence temp;
//			private boolean isEdit = true;
			private int selectionStart;
			private int selectionEnd;

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				temp = s;
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				selectionStart = ideaMessage.getSelectionStart();
				selectionEnd = ideaMessage.getSelectionEnd();
				Log.i("gongbiao1", "" + selectionStart);
				if (temp.length() > MAXTEXTCOUNT) {
					showToast(getString(R.string.beyond_text));
					s.delete(selectionStart - 1, selectionEnd);
					int tempSelection = selectionStart;
					ideaMessage.setText(s);
					ideaMessage.setSelection(tempSelection);
				}
			}
		});
	}
}