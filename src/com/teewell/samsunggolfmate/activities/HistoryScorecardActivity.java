package com.teewell.samsunggolfmate.activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.datebase.MyDataBaseAdapter;
import com.teewell.samsunggolfmate.models.UserInfoModel;
import com.teewell.samsunggolfmate.models.UserScoreCardModel;
import com.teewell.samsunggolfmate.views.HistoryScoreCardAdapter;
import com.teewell.samsunggolfmate.views.XListView;

/**
 * 查询历史记分卡界面,用户在该界面输入框中输入查询的球场名关键字， 点击列表跳转到查看记分卡(Scorecard)界面。 历史记分卡不提供修改功能。
 * 
 * @author chengmingyan
 * 
 */
@SuppressLint("HandlerLeak")
public class HistoryScorecardActivity extends ParentActivity implements
		OnItemClickListener, OnClickListener, TextWatcher, OnTouchListener,
		OnScrollListener {
	public static final String TAG = HistoryScorecardActivity.class.getName();
	public static final int WHAT_ADDSCORECARD_SUCCEED = 0x10;
	public static final int WHAT_MODIFYSCORECARD_SUCCEED = 0x11;
	public static final int WHAT_DELETESCORECARD_SUCCEED = 0x12;
	public static final int WHAT_OBTIANSCORECARD_SUCCEED = 0x13;
	public static final int WHAT_BAD_NETWORK = 0x14;
	public static final int WHAT_SYNCHRONIZATION_FAILED = 0x15;
	private static final int WHAT_REFRECHDATA = 0x16;
	private static final int WHAT_REFRECHVIEW = 0x17;
	private static final int CREATE_LOGIN_DIALOG = 0x18;
//	private static final int REFRESH_DELAY = 1000 * 2;
	private MyDataBaseAdapter mdb;
	private EditText mSearchView;
	private Button btn_delete; // 删除记分卡
	private Button btn_cancel;
	private EditText searchText; // 搜索文本框
	// private TextView scorecardCountText; // 记分卡数量
	private XListView listView;
	// private HistoryScordcardListView listView;
	public static boolean isDelete;
	private HistoryScoreCardAdapter adapter;
	private List<UserScoreCardObject> list_ScoreBean;

	private enum Control {
		addScoreCard, obtainScoreCard, modifyScoreCard, deleteScoreCard
	}

	private UserInfoModel userModel;
	private UserScoreCardModel scoreCardModel;
	private List<UserScoreCardObject> scoreCardObjects;
	private int firstVisibleItem;
	private int visibleItemCount;

	private int scrollState;

	private final int INTENT_REQUESTCODE = 0;
	private EditText text_search;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.history_scorecard);
		findView();
		list_ScoreBean = new ArrayList<UserScoreCardObject>();
		userModel = UserInfoModel.getInstance(this);
		scoreCardModel = UserScoreCardModel.getInstance(this);
	}

	@Override
	protected void onPause() {
		isDelete = false;
		btn_delete.setText(getString(R.string.delete).toString());
		searchText.setText("");
		super.onPause();
	}

	@Override
	protected void onResume() {
		mdb = DataBaseSingle.getInstance(this).getDataBase();
		actionRefreshCourseList();
		if (list_ScoreBean.size() == 0) {
			btn_delete.setVisibility(View.GONE);
		} else {
			btn_delete.setVisibility(View.VISIBLE);
		}
		// scorecardCountText.setText(String.valueOf(list_ScoreBean.size())+getString(R.string.scores).toString());
		super.onResume();
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		view.setSelected(false);

		if ((scrollState != SCROLL_STATE_IDLE)
				&& (scrollState != SCROLL_STATE_FLING || (firstVisibleItem + visibleItemCount) < adapter
						.getCount())) {
			return;
		}
		final UserScoreCardObject singleSc = adapter.getItem(position - 1);

		if (isDelete) {
			final Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getString(R.string.apk_title));
			dialog.setMessage(getString(R.string.delete_score));
			dialog.setPositiveButton(getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							mdb.deleteScoreCard(singleSc);
							doSearchAction("");
						}
					});
			dialog.setNegativeButton(getString(R.string.return_),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			dialog.create().show();
		} else {
			Intent intent = new Intent(HistoryScorecardActivity.this,
					SingleScoreCardActivity.class);
			PlayGameObject playGameObject = new PlayGameObject();
			playGameObject.setUserScoreCardObject(singleSc);
			CoursesObject coursesObject = new CoursesObject();
			coursesObject.setCoursesName(singleSc.getGeneralScoreBean()
					.getCourse_Name());
			coursesObject.setCouseID(singleSc.getGeneralScoreBean()
					.getCourseID());
			playGameObject.setCoursesObject(coursesObject);
			intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
					playGameObject);
			intent.putExtra(KeyValuesConstant.INTENT_FROM_ACTIVITY, TAG);
			startActivity(intent);
		}
	}

	/**
	 * 取得控件的句柄
	 */
	@SuppressLint("CutPasteId")
	private void findView() {
		TextView tv_titleName = (TextView) findViewById(R.id.tv_titleName);
		tv_titleName.setText(getString(R.string.historyScoreCard));
		((Button)findViewById(R.id.btn_back)).setOnClickListener(this);
		btn_delete = (Button) findViewById(R.id.btn_rightTitle);
		btn_delete.setOnClickListener(this);
		btn_delete.setText(getString(R.string.delete));
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		btn_cancel.setOnClickListener(this);
		searchText = (EditText) findViewById(R.id.edit_search);
		searchText.setOnTouchListener(this);
		searchText.addTextChangedListener(this);
		// scorecardCountText = (TextView) findViewById(R.id.scorecardCount);
		listView = (XListView) findViewById(R.id.listView);
		listView.setXListViewListener(new XListView.IXListViewListener() {
			@Override
			public void onRefresh() {
//				control(Control.addScoreCard);
				isDelete = false;
				btn_cancel.setText(getString(R.string.delete).toString());
				listView.stopRefresh();
			}

			@Override
			public void onLoadMore() {
				// TODO Auto-generated method stub
				listView.stopLoadMore();
			}
		});
		adapter = new HistoryScoreCardAdapter(this);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		listView.setOnScrollListener(this);
		// listView.setImageFooterViewVisiable(false);
		listView.stopRefresh();
		listView.setPullLoadEnable(false);

		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		btn_cancel.setOnClickListener(this);
		text_search = (EditText) findViewById(R.id.edit_search);
		text_search.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				if (s != null && s.length() > 0) {
					btn_cancel.setVisibility(View.VISIBLE);

				} else {
					btn_cancel.setVisibility(View.GONE);
				}
				actionRefreshCourseList();
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});
		text_search.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_UP:
					int curX = (int) event.getX();
					if (curX > v.getWidth() - 38
							&& !TextUtils.isEmpty(text_search.getText())) {
						text_search.setText("");
						int cacheInputType = text_search.getInputType();// backup
						text_search.setInputType(InputType.TYPE_NULL);// disable
						text_search.onTouchEvent(event);// call native handler
						text_search.setInputType(cacheInputType);// restore
																	// input
						return true;// consume touch even
					}
					break;
				}
				return false;
			}
		});
	}

	private void control(Control control) {
		switch (control) {
		case addScoreCard: {
			scoreCardObjects = mdb.selectLocalScoreCardObjects();
			if (scoreCardObjects.size() == 0) {
				control(Control.modifyScoreCard);
			} else {
				scoreCardSuccessCount = 0;
				UserScoreCardObject object = scoreCardObjects.get(0);
				scoreCardModel.addScoreCard(handler, object);
			}
		}
			break;
		case obtainScoreCard: {
			scoreCardSuccessCount = 0;
			int maxVersion = mdb.selectScoreCardMaxVersion();
			scoreCardModel.obtianScoreCard(handler, maxVersion);
		}
			break;
		case modifyScoreCard: {
			scoreCardObjects = mdb.selectModifyScoreCardObjects();
			if (scoreCardObjects.size() == 0) {
				control(Control.deleteScoreCard);
			} else {
				scoreCardSuccessCount = 0;
				UserScoreCardObject object = scoreCardObjects.get(0);
				scoreCardModel.modifyScoreCard(handler, object.getScoreCard(),
						object.getId(), object.getChangeTime());
			}
		}
			break;
		case deleteScoreCard: {
			scoreCardObjects = mdb.selectDeleteScoreCardObjects();
			if (scoreCardObjects.size() == 0) {
				control(Control.obtainScoreCard);
			} else {
				scoreCardSuccessCount = 0;
				UserScoreCardObject object = scoreCardObjects.get(0);
				scoreCardModel.deleteScoreCard(handler, object.getId(),
						object.getChangeTime());
			}
		}
			break;
		default:
			new Exception("没有选项");
			break;
		}
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.btn_cancel:
			searchText.setText("");// 点击删除按钮后将搜索框数据请空
			break;
		case R.id.btn_rightTitle:
			adapter.notifyDataSetChanged();
			if (isDelete) {
				isDelete = false;
				btn_delete.setText(getString(R.string.delete).toString());
			} else {
				isDelete = true;
				btn_delete.setText(getString(R.string.restore).toString());
			}
			break;
		}
	}

	/**
	 * 取得所有的历史积分卡记录，按时间倒序排列
	 */
	public void actionRefreshCourseList() {
		List<UserScoreCardObject> list = scoreCardModel
				.getScoreCardObjects(mdb);
		list_ScoreBean = new ArrayList<UserScoreCardObject>();

		Iterator<UserScoreCardObject> srcIt = list.iterator();
		while (srcIt.hasNext()) {
			UserScoreCardObject course = (UserScoreCardObject) srcIt.next();
			if (course.getGeneralScoreBean().getCourse_Name()
					.indexOf(text_search.getText().toString()) >= 0) {
				list_ScoreBean.add(course);
			}
		}

		adapter.setList(list_ScoreBean);
		handler.sendEmptyMessage(WHAT_REFRECHVIEW);
		// if (list_ScoreBean!=null&&list_ScoreBean.size()>0) {
		// listView.setImageFooterViewVisiable(true);
		// }else {
		// listView.setImageFooterViewVisiable(false);
		// }
	}

	/**
	 * 查询记分卡
	 */
	private void doSearchAction(String key) {
		Log.d(TAG, "do search action");
		list_ScoreBean = scoreCardModel.getScoreCardObjectsByKey(mdb, key);
		// scorecardCountText.setText(String.valueOf(list_ScoreBean.size())+getString(R.string.scores).toString());
		adapter.setList(list_ScoreBean);
		if (list_ScoreBean.size() == 0) {
			isDelete = false;
			btn_delete.setText(getString(R.string.delete).toString());
			btn_delete.setVisibility(View.GONE);
		}
		handler.sendEmptyMessage(WHAT_REFRECHVIEW);
	}

	private boolean isnull = true;
	protected int scoreCardSuccessCount;

	public void afterTextChanged(Editable s) {
		if (TextUtils.isEmpty(s)) {
			if (!isnull) {
				btn_cancel.setVisibility(View.GONE);
				isnull = true;
			}
		} else {
			if (isnull) {
				btn_cancel.setVisibility(View.VISIBLE);
				isnull = false;
			}
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		doSearchAction(s.toString().trim());
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_UP:
			int curX = (int) event.getX();
			if (curX > v.getWidth() - 38
					&& !TextUtils.isEmpty(mSearchView.getText())) {
				mSearchView.setText("");
				int cacheInputType = mSearchView.getInputType();// backup the
																// input type
				mSearchView.setInputType(InputType.TYPE_NULL);// disable soft
																// input
				mSearchView.onTouchEvent(event);// call native handler
				mSearchView.setInputType(cacheInputType);// restore input type
				return true;// consume touch even
			}
			break;
		}
		return false;
	}

	@SuppressLint("SimpleDateFormat")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case WHAT_REFRECHDATA:
				adapter.notifyDataSetChanged();
				// listView.onRefreshComplete();
				SimpleDateFormat format = new SimpleDateFormat(
						getString(R.string.date_format));
				Date now = new Date();

				listView.setRefreshTime(format.format(now));
				listView.stopRefresh();
				listView.setPullLoadEnable(false);
				break;
			case WHAT_REFRECHVIEW:

				adapter.notifyDataSetChanged();
				listView.stopRefresh();
				listView.setPullLoadEnable(false);
				break;

			case RegisterLoginActivity.MSG_REGISTER_LOGIN_SUCCEED:
				control(Control.addScoreCard);
				break;
			case RegisterLoginActivity.MSG_REGISTER_LOGIN_FAILED:
				break;
			case RegisterLoginActivity.MSG_REGISTER_LOGIN_BAD_NETWORK:
				showToast(getString(R.string.bad_network));
				break;
			case WHAT_ADDSCORECARD_SUCCEED: {
				scoreCardSuccessCount++;
				mdb.deleteLocalScoreCard(msg.arg1);
				if (scoreCardSuccessCount == scoreCardObjects.size()) {
					control(Control.modifyScoreCard);
				} else {
					UserScoreCardObject object = scoreCardObjects
							.get(scoreCardSuccessCount);
					scoreCardModel.addScoreCard(handler, object);
				}
			}
				break;
			case WHAT_MODIFYSCORECARD_SUCCEED: {
				scoreCardSuccessCount++;
				if (scoreCardSuccessCount == scoreCardObjects.size()) {
					control(Control.deleteScoreCard);
				} else {
					UserScoreCardObject object = scoreCardObjects
							.get(scoreCardSuccessCount);
					scoreCardModel.modifyScoreCard(handler,
							object.getScoreCard(), object.getId(),
							object.getChangeTime());
				}
			}
				break;
			case WHAT_DELETESCORECARD_SUCCEED: {
				scoreCardSuccessCount++;
				mdb.deleteHistoryScoreCard(msg.arg1);
				if (scoreCardSuccessCount == scoreCardObjects.size()) {
					control(Control.obtainScoreCard);
				} else {
					UserScoreCardObject object = scoreCardObjects
							.get(scoreCardSuccessCount);
					scoreCardModel.deleteScoreCard(handler, object.getId(),
							object.getChangeTime());
				}
			}
				break;
			case WHAT_OBTIANSCORECARD_SUCCEED:
				@SuppressWarnings("unchecked")
				List<UserScoreCardObject> userScoreCardObjects = (List<UserScoreCardObject>) msg.obj;
				if (userScoreCardObjects.size() == 0) {
					handler.sendEmptyMessage(WHAT_REFRECHDATA);
					break;
				}
				boolean flag = mdb.synchScoreCard(userScoreCardObjects);
				if (!"".equals(searchText)) {
					searchText.setText("");
				}
				if (flag) {
					actionRefreshCourseList();
				} else {
					handler.sendEmptyMessage(WHAT_REFRECHVIEW);
				}
				break;
			case WHAT_SYNCHRONIZATION_FAILED:
				listView.stopRefresh();
				listView.setPullLoadEnable(false);
				showToast("同步失败!");
				break;
			case WHAT_BAD_NETWORK:
				listView.stopRefresh();
				listView.setPullLoadEnable(false);
				showToast(getString(R.string.bad_network));
				break;
			case UserInfoActivity.MSG_GET_USER_INFO_FAILED:
				if (!userModel.hasLoggedIn()) {

					listView.stopRefresh();
					listView.setPullLoadEnable(false);
					handler.sendEmptyMessageDelayed(CREATE_LOGIN_DIALOG, 200);
				}
				break;
			case CREATE_LOGIN_DIALOG:
				final Builder dialog = new AlertDialog.Builder(
						HistoryScorecardActivity.this);
				dialog.setTitle(getString(R.string.apk_title));
				dialog.setMessage(getString(R.string.not_login_and_isLogin));
				dialog.setPositiveButton(getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								Intent intent = new Intent(
										HistoryScorecardActivity.this,
										RegisterLoginActivity.class);
								intent.putExtra(
										KeyValuesConstant.INTENT_FROM_ACTIVITY,
										getString(R.string.historyScoreCard));
								startActivityForResult(intent,
										INTENT_REQUESTCODE);
							}
						});
				dialog.setNegativeButton(getString(R.string.return_),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
				dialog.create().show();
				break;
			case UserInfoActivity.MSG_GET_USER_INFO_START:
				control(Control.addScoreCard);
				break;
			}
		};
	};

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		// save the item property
		this.firstVisibleItem = firstVisibleItem;
		this.visibleItemCount = visibleItemCount;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		this.scrollState = scrollState;
		Log.i(TAG, "scroll state:" + scrollState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == INTENT_REQUESTCODE && resultCode == RESULT_OK) {
			listView.launchRefresh();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
