package com.teewell.samsunggolfmate.activities;

import android.app.Activity;
import android.graphics.BitmapFactory.Options;
import android.os.Bundle;
import android.widget.Gallery;
import android.widget.LinearLayout;
import com.master.mastercourse.singlecourse.R;

/**
 * Android实现左右滑动效果
 * 
 * @author Administrator
 * 
 */
@SuppressWarnings("unused")
public class ImagesActivity extends Activity {

	
	private Gallery gallery;
	private int screenWidth;
	private int screenHeight;
	private LinearLayout layout;
	private Options options;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.images);
		initView();
		// initData();
	}

	private void initView() {
		layout = (LinearLayout) findViewById(R.id.layout);
		gallery = (Gallery) findViewById(R.id.gallery);
	}
}