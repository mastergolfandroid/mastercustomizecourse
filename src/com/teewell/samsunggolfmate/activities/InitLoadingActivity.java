package com.teewell.samsunggolfmate.activities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.DownloadAerialPhotoBean;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
import com.teewell.samsunggolfmate.models.course.SingleCourseInfoModel;
import com.teewell.samsunggolfmate.utils.FileUtil;
import com.teewell.samsunggolfmate.utils.GpsSwitchUtil;
import com.teewell.samsunggolfmate.utils.InternetUtil;
import com.teewell.samsunggolfmate.utils.ZipMe;

/**
 * 启动画面 主要功能：检查并打开GPS、检查网络、创建SD卡路径、拷贝数据
 * 
 * @Project GolfMate
 * @package com.teewell.golfmate.activities
 * @title LoadingActivity.java
 * @Description TODO
 * @author chengmingyan
 * @date 2013-1-15 14:28:30
 * @Copyright Copyright(C) 2013-1-15
 * @version 1.0.0
 */
@SuppressLint("HandlerLeak")
public class InitLoadingActivity extends ParentActivity {
	/**
	 * 启动tabhost的标志
	 */
	private static final int START_TABHOST = 10;
	private boolean isContinue = true;
	private Builder builder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.loading);
		initSystem();
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {

		// there is no need to check internet,directly jump to tabhost
		// checkIntenet();
//		handler.sendEmptyMessageDelayed(START_TABHOST, 3000);
		super.onResume();
	}

	/**
	 * 检查网络
	 */
	@SuppressWarnings("unused")
	private void checkIntenet() {
		if (InternetUtil.checkConnect(this)) {
			handler.sendEmptyMessageDelayed(START_TABHOST, 3000);
		} else {
			final AlertDialog.Builder openWifiDialog = new Builder(this);
			openWifiDialog.setTitle(R.string.apk_title);
			openWifiDialog.setMessage(getString(R.string.intenet_not_connect)
					.toString());
			openWifiDialog.setPositiveButton(getString(R.string.ok).toString(),
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							InternetUtil.setWirless(InitLoadingActivity.this);
						}
					});
			openWifiDialog.setNegativeButton(getString(R.string.exit)
					.toString(), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					finish();
				}
			});
			openWifiDialog.create();
			openWifiDialog.show();
			openWifiDialog.setCancelable(false);
		}
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (!isContinue) {
				return;
			}
			switch (msg.what) {
			case START_TABHOST: {
				Intent intent = new Intent(InitLoadingActivity.this,
						MainActivity.class);
				// 如果已经打开过的实例，将不会重新打开新的Activity
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				intent.putExtra(CoursesObject.class.getCanonicalName(),(Serializable) msg.obj);
				startActivity(intent);
				finish();
			}
				break;
			}
		}
	};

	/**
	 * 卡贝项目数据，创建文件路径
	 */
	private void initSystem() {
		// if (Environment.getExternalStorageState().equals(  
		// Environment.MEDIA_MOUNTED)) {// 表示sdcard存在，并且可以进
		// 行读写 
		// check sdcard,if not exist app exit
		if (!FileUtil.isExternalStorageState()) {
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(R.string.app_name);
			builder.setMessage(R.string.not_sdcard_app_will_finish);
			builder.setPositiveButton(R.string.exit,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							finish();
						}
					});
			builder.create();
			builder.setCancelable(false);
			
		} else {
			// 取得sdcard文件路径
			File pathFile = android.os.Environment
					.getExternalStorageDirectory();
			android.os.StatFs statfs = new android.os.StatFs(pathFile.getPath());
			// 获取SDCard上每个block的SIZE
			long nBlocSize = statfs.getBlockSize();
			// 获取可供程序使用的Block的数量
			long nAvailaBlock = statfs.getAvailableBlocks();
			// 获取剩下的所有Block的数量(包括预留的一般程序无法使用的块)
			@SuppressWarnings("unused")
			long nFreeBlock = statfs.getFreeBlocks();
			// 计算 SDCard 剩余大小MB
			long nSDFreeSize = nAvailaBlock * nBlocSize / 1024 / 1024;
			if (nSDFreeSize < 6) {
				AlertDialog.Builder builder = new Builder(this);
				builder.setTitle(R.string.app_name);
				builder.setMessage("SDCard容量不足,请清除一些容量再试用本软件!");
				builder.setPositiveButton(R.string.exit,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								finish();
							}
						});
				builder.create();
				builder.setCancelable(false);
			}
		}

		new DataTask().execute(true);
		// makesure gps open
		GpsSwitchUtil.ensureOpenGPS(this);
		
		
	}
	class DataTask extends AsyncTask<Boolean, Void, List<CoursesObject>>{

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#doInBackground(java.lang.Object[])
		 */
		@Override
		protected List<CoursesObject> doInBackground(Boolean... params) {
			File file = new File(Contexts.SD_PATH +Contexts.APPLICATION_PATH+ "aerialPhoto/");
			if (file.exists()) {
				FileUtil.deleteFile(file);
			}
			// Create the ball friend photo path
			File f_golfer = new File(Contexts.GOLFERPHOTO_PATH);
			if (!f_golfer.exists()) {
				f_golfer.mkdirs();
			}
			SingleCourseInfoModel model = SingleCourseInfoModel.getInstance(getApplicationContext());
			List<CoursesObject> list = model.getCourseList(getApplicationContext());
			CoursesObject coursesObject = null;
			if (list!=null&&list.size()>0) {
				boolean isExist = true;
				for (int i = 0; i < list.size(); i++) {
					coursesObject = list.get(i);
					String photoPath = Contexts.AERIALPHOTO_PATH
							+ coursesObject.getCouseID() + "/";
					if (!new File(photoPath).exists()) {
						isExist = false;
						copyCoursePhoto(coursesObject.getCouseID());
					}
				}
				if (isExist) {
					try {
						Thread.sleep(1200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} 
			}else {
				isContinue = false;
			}
			
			return list;
		}

		@Override
		protected void onPostExecute(List<CoursesObject> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Message message = new Message();
			message.what = START_TABHOST;
			message.obj = result;
			handler.sendMessage(message);
		}
		
	}
	/**
	 * 拷贝天气图片
	 */
	private void copyCoursePhoto(int courseId) {
		InputStream is;
		String path = Contexts.AERIALPHOTO_PATH
				+ courseId + "/";
		try {
			is = this.getAssets().open("photos_"+courseId+".zip");
			ZipMe file = new ZipMe(is);
			is.close();
			@SuppressWarnings("rawtypes")
			Vector p = file.list();
			for (int i = 0; i < p.size(); i++) {
				String name = (String) p.elementAt(i);
				byte[] b = file.get(name);
				if (b != null && b.length > 0) {
					FileUtil.writeHex2ToFile(b, path + name);
				}
			}
		} catch (IOException e) {
			FileUtil.deleteFile(path);
			showToast("很抱歉,程序出现异常,即将退出");
			finish();
		} catch (Exception e) {
			FileUtil.deleteFile(path);
			showToast("很抱歉,程序出现异常,即将退出");
			finish();
		}
	}
	/**
	 * 拷贝球场logo
	 */
	private void copyLogo(String path) {
		InputStream is;
		@SuppressWarnings("unused")
		int number = 0;
		@SuppressWarnings("unused")
		String nameString = null;
		try {
			is = this.getAssets().open("logo.zip");
			ZipMe file = new ZipMe(is);
			is.close();
			@SuppressWarnings("rawtypes")
			Vector p = file.list();
			for (int i = 0; i < p.size(); i++) {
				String name = (String) p.elementAt(i);
				byte[] b = file.get(name);
				if (b != null && b.length > 0) {
					FileUtil.writeHex2ToFile(b, path + name);
				}
				number = i;
			}
		} catch (IOException e) {
			FileUtil.deleteFile(path);
			showToast("很抱歉,程序出现异常,即将退出");
			finish();
		} catch (Exception e) {
			FileUtil.deleteFile(path);
			showToast("很抱歉,程序出现异常,即将退出");
			finish();
		} finally {
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		// 判断用户是否按了返回键
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			isContinue = false;
			if (builder == null) {
				builder = new Builder(InitLoadingActivity.this);
				builder.setMessage(getString(R.string.exit_system_tip));
				builder.setTitle(getString(R.string.app_name));
				builder.setPositiveButton(getString(R.string.exit),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								builder = null;
								finish();
							}
						});
				builder.setNegativeButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								builder = null;
								isContinue = true;
								handler.sendEmptyMessage(START_TABHOST);
							}
						});
				builder.create();
			}
			builder.show();
			builder.setCancelable(false);
		}
		return super.onKeyDown(keyCode, event);
	}
}
