package com.teewell.samsunggolfmate.activities;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.activities
 * @title DistanceSelectActivity.java
 * @Description TODO
 * @author Administrator
 * @date 2013-3-18 上午11:36:59
 * @Copyright Copyright(C) 2013-3-18
 * @version 1.0.0
 */
public class LongDistanceSettingActivity extends ParentActivity implements
		OnClickListener {
	private Button btn_back;
	private TextView tv_title, tv_distance_default, tv_distance;
	private ImageView image_distance_default, image_distance;
	private RelativeLayout layout_distance_default, layout_distance;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.distance_select);
		initView();
		initData();
		super.onCreate(savedInstanceState);
	}

	private void initData() {
		tv_title.setText(getString(R.string.far_distance_setting));
		tv_distance_default.setText(getString(R.string.mile));
		tv_distance.setText(getString(R.string.kilometer));
		if (PlayGameModel.longDistanceIndex == 2) {
			image_distance_default.setVisibility(View.VISIBLE);
			image_distance.setVisibility(View.GONE);
		} else {
			image_distance_default.setVisibility(View.GONE);
			image_distance.setVisibility(View.VISIBLE);
		}
	}

	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setText(getString(R.string.return_));
		tv_title = (TextView) findViewById(R.id.tv_titleName);
		tv_distance_default = (TextView) findViewById(R.id.tv_distance_default);
		tv_distance = (TextView) findViewById(R.id.tv_distance);
		image_distance_default = (ImageView) findViewById(R.id.image_distance_default);
		image_distance = (ImageView) findViewById(R.id.image_distance);
		layout_distance_default = (RelativeLayout) findViewById(R.id.layout_distance_default);
		layout_distance = (RelativeLayout) findViewById(R.id.layout_distance);
		layout_distance_default.setOnClickListener(this);
		layout_distance.setOnClickListener(this);
		btn_back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_distance_default:
			image_distance_default.setVisibility(View.VISIBLE);
			image_distance.setVisibility(View.GONE);
			PlayGameModel.longDistanceIndex = 2;
			finish();
			break;
		case R.id.layout_distance:
			image_distance_default.setVisibility(View.GONE);
			image_distance.setVisibility(View.VISIBLE);
			PlayGameModel.longDistanceIndex = 3;
			finish();
			break;
		case R.id.btn_back:
			finish();
			break;
		}
	}
}
