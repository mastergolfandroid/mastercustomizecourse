/**
 * 
 */
package com.teewell.samsunggolfmate.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.utils.ActivityManagerUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.utils.UpdateApp;

/**
 * @author 程明炎 E-mail: 345021109@qq.com
 * @version 创建时间：2014年6月26日 下午6:15:52 类说明 入口类，包含：开始打球、记分卡、设置
 */
public class MainActivity extends ParentActivity implements OnClickListener {
	private Button btn_continue;
	private Builder builder;
	private AlertDialog listDialog;
	public static final int WHAT_GOTTEN_LOCATION = 102;
	private List<CoursesObject> coursesObjects;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btn_continue = (Button) findViewById(R.id.btn_restart);
		coursesObjects = (List<CoursesObject>) getIntent().getSerializableExtra(
				CoursesObject.class.getCanonicalName());
		ActivityManagerUtil.getScreenManager().pushActivity(this);
		DataBaseSingle.getInstance(this.getApplicationContext());
//		CrashHandler crashHandler = CrashHandler.getInstance();
		// crashHandler.init(this);
		new UpdateApp(this).update();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.teewell.samsunggolfmate.activities.ParentActivity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (PlayGameModel.getInstance(this).getPlayGameObject() != null) {
			btn_continue.setVisibility(View.VISIBLE);
		} else {
			btn_continue.setVisibility(View.INVISIBLE);
		}
		// 启动location util
				LocationUtil location = LocationUtil.getInstance(this
						.getApplicationContext());
				location.setHandlerInfo(handler, WHAT_GOTTEN_LOCATION,
						LocationUtil.LOC_ONCE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_restart: {
			Intent intent = new Intent(MainActivity.this, PlayTabActivity.class);
			// 如果已经打开过的实例，将不会重新打开新的Activity
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
					PlayGameModel.getInstance(MainActivity.this)
							.getPlayGameObject());
			startActivity(intent);
		}
			break;
		case R.id.btn_start: {
			if (PlayGameModel.getInstance(this).getPlayGameObject() == null) {
				
				showListDialog();
				break;
			}
			Dialog dialog = new AlertDialog.Builder(MainActivity.this)
					.setMessage(R.string.play_hint)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface arg0,
										int arg1) {
									arg0.dismiss();
									PlayGameModel
											.getInstance(MainActivity.this)
											.setPlayGameObject(null);
									Intent intent = new Intent(
											MainActivity.this,
											StartGameActivity.class);
									intent.putExtra(
											KeyValuesConstant.INTENT_COURSESOBJECT,
											(CoursesObject) getIntent()
													.getSerializableExtra(
															CoursesObject.class
																	.getCanonicalName()));
									PlayGameModel.fromActivity = 0;
									startActivity(intent);

								};
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									Intent intent = new Intent(
											MainActivity.this,
											PlayTabActivity.class);
									// 如果已经打开过的实例，将不会重新打开新的Activity
									intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
									intent.putExtra(
											KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
											PlayGameModel.getInstance(
													MainActivity.this)
													.getPlayGameObject());
									startActivity(intent);
								}
							}).create();
			dialog.show();

		}
			break;
		case R.id.btn_scorecard:
			startActivity(new Intent(MainActivity.this,
					HistoryScorecardActivity.class));
			break;
		case R.id.btn_setting:
			startActivity(new Intent(MainActivity.this, SettingActivity.class));
			break;
		default:
			break;
		}
	}
	private void showListDialog(){
		if (listDialog != null && !listDialog.isShowing()) {
			listDialog.show();return;
		}
		LinearLayout linearLayoutMain = new LinearLayout(this);//自定义一个布局文件
		linearLayoutMain.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		linearLayoutMain.setGravity(Gravity.CENTER);
		ListView listView = new ListView(this);//this为获取当前的上下文
		listView.setFadingEdgeLength(0);
		listView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		List<String> nameList = new ArrayList<String>();
		for (int i = 0; i < coursesObjects.size(); i++) {
			nameList.add(coursesObjects.get(i).getCoursesName()+"Golf");
		}
		ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, nameList);
		listView.setAdapter(adapter);

		linearLayoutMain.addView(listView);//往这个布局中加入listview

		listDialog = new AlertDialog.Builder(this)
				.setTitle("选择球场").setView(linearLayoutMain)//在这里把写好的这个listview的布局加载dialog中
				.setNegativeButton("取消", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				}).create();
		listDialog.setCanceledOnTouchOutside(false);//使除了dialog以外的地方不能被点击
		listDialog.show();
		listView.setOnItemClickListener(new OnItemClickListener() {//响应listview中的item的点击事件
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				listDialog.dismiss();
				Intent intent = new Intent(MainActivity.this,
						StartGameActivity.class);
				intent.putExtra(
						KeyValuesConstant.INTENT_COURSESOBJECT,coursesObjects.get(arg2) );
				PlayGameModel.fromActivity = 0;
				startActivity(intent);
			}
		});
	
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		// 判断用户是否按了返回键
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (builder == null) {
				builder = new Builder(MainActivity.this);
				builder.setMessage(getString(R.string.exit_system_tip));
				builder.setTitle(getString(R.string.app_name));
				builder.setPositiveButton(getString(R.string.exit),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								builder = null;
								finish();
							}
						});
				builder.setNegativeButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								builder = null;
							}
						});
				builder.create();
			}
			builder.show();
			builder.setCancelable(false);
		}
		return super.onKeyDown(keyCode, event);
	}
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case WHAT_GOTTEN_LOCATION: {

				// stop the GPS Function
				LocationUtil.getInstance(null).clearHandlerInfo();
				Log.i("handler", "gotten gps position!");
			}
				break;
			default:
				break;
			}
		};
	};
}
