package com.teewell.samsunggolfmate.activities;

import java.util.List;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;
import com.teewell.samsunggolfmate.beans.FairwayObject;
import com.teewell.samsunggolfmate.beans.FuncButtonState;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.WeatherInfoBean;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.datebase.MyDataBaseAdapter;
import com.teewell.samsunggolfmate.models.MapModel;
import com.teewell.samsunggolfmate.models.MapModel.Tee;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.weather.WeatherInfoModel;
import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.CoordinateUtil;
import com.teewell.samsunggolfmate.utils.DistanceUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;
import com.teewell.samsunggolfmate.views.MapView;

/**
 * 航拍图界面
 * 
 * @author chengmingyan
 */
public class MapActivity extends ParentActivity implements OnClickListener,
		SensorEventListener ,OnCheckedChangeListener{
	private static final String TAG = MapActivity.class.getName();
	private int lastHoleIndex;// 上一次球洞号
	private Bitmap bitmapOrg;
	private MapView mapView;
	private MapModel model;
	private AerialPhotoBean mapBean;
	private int[] validArea;//
	private List<FairwayObject> holesList;
	private List<AerialPhotoBean> aerialPhotoBeans;
	private float density;

	private ImageView image_wind;
	private TextView tv_wind;
	private SlidingDrawer drawer;
	// private LinearLayout layout_menuBar;
	private Button  btn_distance, btn_evaluate,  btn_help;
	private CheckBox btn_location,btn_wind;
	private LinearLayout btn_cursor,btn_addMark,btn_removeMarke;
	private FrameLayout layout_wind;
	public static final float INITIAL_ROTATION = -90;
	private static final int WHAT_CHANGE_MAP = 0x0;
	private static final int WHAT_CHANGE_WIND = 0x1;
	public static final int WHAT_CHANGE_MENUBAR = 0x2;

	private static final int WHAT_LOCATION_REFRESH = 0x10;
	private static final String DIRECTION[] = { "北风", "南风", "西风", "东风", "西北风",
			"东北风", "西南风", "东南风" };
	private static final int ANGLE[] = { -90, -270, 0, -180, -45, -135, -315,
			-225 };
	private int wind_angle;

	public static final String ACTION_BROADCASTRECEIVER = "com.teewell.golfmate.activities.MapActivity.MapBroadcastReceiver";
//	private SharedPreferencesUtils utils;
	private MyDataBaseAdapter db;
	private AnimationDrawable animationDrawable;
	private PlayGameObject playGameObject;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_layout);

		initView();
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
//		utils = new SharedPreferencesUtils(this);
		lastHoleIndex = -1;
		density = ScreenUtil.getScreenDensity(this);
		WeatherInfoBean weatherInfoBean = WeatherInfoModel.getInstance(
				this.getApplicationContext()).getWeatherInfoBean(
				playGameObject.getCoursesObject().getCity());

		if (weatherInfoBean != null) {
			tv_wind.setText(weatherInfoBean.getWS());
			for (int i = 0; i < DIRECTION.length; i++) {
				if (weatherInfoBean.getWD().equals(DIRECTION[i])) {
					wind_angle = ANGLE[i];
					break;
				}
			}
		}
		location = LocationUtil.getInstance(getApplicationContext());
		initData();
	}

	private void initData() {
		holesList = PlayGameModel.getInstance(this).getFairwayObjectArray(playGameObject);
		aerialPhotoBeans = playGameObject.getAerialPhotoBeanArray();
	}

	private void initView() {
		mapView = (MapView) findViewById(R.id.map);

		btn_evaluate = (Button) findViewById(R.id.btn_evaluate);
		btn_location = (CheckBox) findViewById(R.id.btn_location);
		btn_distance = (Button) findViewById(R.id.btn_share);
		btn_wind = (CheckBox) findViewById(R.id.btn_wind);
		btn_help = (Button) findViewById(R.id.btn_more);
		drawer = (SlidingDrawer) findViewById(R.id.slidingDrawer1);
		drawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {

			@Override
			public void onDrawerClosed() {
				// TODO Auto-generated method stub
				drawer.getHandle().setBackgroundResource(
						R.drawable.slidingsrawer_btn_open);
			}
		});
		drawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {

			@Override
			public void onDrawerOpened() {
				// TODO Auto-generated method stub
				drawer.getHandle().setBackgroundResource(
						R.drawable.slidingsrawer_btn_close);
			}
		});
		mapView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (drawer.isOpened()) {
					drawer.close();
				}
				mapView.onTouchEvent(event);
				return false;
			}
		});
		btn_evaluate.setOnClickListener(this);
		btn_location.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				mapView.setState_direction(isChecked);
	
				SensorManager sensor_manager = (SensorManager) getSystemService(SENSOR_SERVICE); // 获取管理服务
				if (isChecked) {
					// 注册监听器
					sensor_manager.registerListener(MapActivity.this,
							sensor_manager
									.getDefaultSensor(Sensor.TYPE_ORIENTATION),
							SensorManager.SENSOR_DELAY_GAME);
				} else {
					sensor_manager.unregisterListener(MapActivity.this);
					mapView.invalidate();
				}
			}
		});
		btn_distance.setOnClickListener(this);
		btn_wind.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					handler.sendEmptyMessage(WHAT_CHANGE_WIND);
				} else {
					animationDrawable.stop();
					image_wind.clearAnimation();
					/** 通知imageView 刷新屏幕 **/
					image_wind.postInvalidate();
					layout_wind.setVisibility(View.GONE);
				}
			}
		});
		btn_help.setOnClickListener(this);

		// layout_menuBar.setVisibility(View.GONE);
		image_wind = (ImageView) findViewById(R.id.image_wind);
		tv_wind = (TextView) findViewById(R.id.tv_wind);
		/** 通过ImageView对象拿到背景显示的AnimationDrawable **/
		animationDrawable = (AnimationDrawable) image_wind.getDrawable();
		layout_wind = (FrameLayout) findViewById(R.id.layout_wind);
		layout_wind.setVisibility(View.GONE);

		personBmp_location = BitmapFactory.decodeResource(getResources(),
				R.drawable.personwithdirection);
		
		btn_cursor = (LinearLayout) findViewById(R.id.btn_cursor);
		btn_addMark = (LinearLayout) findViewById(R.id.btn_addMark);
		btn_removeMarke = (LinearLayout) findViewById(R.id.btn_removeMark);
		btn_cursor.setOnClickListener(this);
		btn_addMark.setOnClickListener(this);
		btn_removeMarke.setOnClickListener(this);
		
	}
	private void uploadPhoto() {
		mapBean = aerialPhotoBeans.get(PlayGameModel.curHoleNumber);
		bitmapOrg = BitmapUtil.readBitMapByNative(Contexts.AERIALPHOTO_PATH
				+ playGameObject.getCoursesObject().getCouseID() + "/"
				+ mapBean.getPhotoName() + "." + mapBean.getPhotoMode());
		Log.d(TAG, "bitmap.width" + bitmapOrg.getWidth() + "-bitmap.height"
				+ bitmapOrg.getHeight());
		validArea = BitmapUtil.getValidArea(bitmapOrg);
		handler.sendEmptyMessageDelayed(0, 200);
	}

	public void slideRightView(final View view, final float p1, final float p2) {
		TranslateAnimation animation = new TranslateAnimation(p1, p2, 0, 0);
		animation.setInterpolator(new OvershootInterpolator());
		animation.setDuration(800);
		animation.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// layout_menuBar.setVisibility(View.GONE);
				// image_menu.setVisibility(View.VISIBLE);
			}
		});
		view.startAnimation(animation);
	}

	public void slideLeftView(final View view, final float p1, final float p2) {
		TranslateAnimation animation = new TranslateAnimation(p1, p2, 0, 0);
		animation.setInterpolator(new OvershootInterpolator());
		animation.setDuration(800);
		animation.setStartOffset(1);
		animation.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// layout_menuBar.setVisibility(View.VISIBLE);
				// image_menu.setVisibility(View.GONE);
			}
		});
		view.startAnimation(animation);
	}

	@SuppressLint("HandlerLeak")
	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case WHAT_CHANGE_MAP:
				initMap();
				break;
			case WHAT_CHANGE_WIND:
				// 功能：构建一个旋转动画。
				// 参数：fromDegress 为开始的角度； toDegress 为结束的角度。
				// pivotXType、pivotYType分别为x、y 的伸缩模式。
				// pivotXValue、pivotYValue 分别为伸缩动画相对于x、y的坐标的开始位置。
				image_wind.clearAnimation();
				RotateAnimation mLeftAnimation = new RotateAnimation(0.0f,
						wind_angle + model.getRotateAngle(),
						Animation.RELATIVE_TO_SELF, 0.5f,
						Animation.RELATIVE_TO_SELF, 0.5f);
				mLeftAnimation.setDuration(200);
				mLeftAnimation.setFillAfter(true);
				mLeftAnimation.setFillEnabled(true);
				layout_wind.setVisibility(View.VISIBLE);
				image_wind.startAnimation(mLeftAnimation);
				animationDrawable.start();// 开始这个动画
				/** 通知imageView 刷新屏幕 **/
				image_wind.postInvalidate();
				break;
			case WHAT_LOCATION_REFRESH:
				// model.setPointMe(model.getCoordinateUtil().convertLonLatToOldXY(location.longitude,
				// location.latitude));
				mapView.invalidate();
				break;
			default:
				break;
			}
		};
	};

	private void initMap() {
		try {
			if (bitmapOrg == null || mapBean == null) {
				return;
			}
			float ratio = 0;
			float offset_h = 0;
			float offset_w = 0;
			int x1 = validArea[0];
			int y1 = validArea[1];
			int x2 = validArea[2];
			int y2 = validArea[3];

			ratio = (float) mapView.getMeasuredHeight() / (float) (y2 - y1);// 16*322
																			// 446*1205
																			// A7
																			// //45Ϊ�����水ť�ĸ߶�
			float ratio1 = (float) mapView.getMeasuredWidth()
					/ (float) (x2 - x1);
			Log.d(TAG, "mapView.getHeight()" + mapView.getMeasuredHeight()
					+ "---mapView.getWidth()" + mapView.getMeasuredWidth());
			if (ratio > ratio1) {// 取消比例
				ratio = ratio1;
			}
			offset_h = y1 * ratio;
			offset_w = x1 * ratio;

			Bitmap rebitmapOrg = BitmapUtil.zoomBitmap(bitmapOrg, ratio);
			CoordinateUtil util = new CoordinateUtil(ratio, density, validArea,
					offset_h, offset_w, mapBean.getRotationView(),
					rebitmapOrg.getWidth(), rebitmapOrg.getHeight(),
					mapView.getWidth(), mapView.getHeight(),
					Double.parseDouble(mapBean.getLuLongitude()),
					Double.parseDouble(mapBean.getLuLatitude()),
					Double.parseDouble(mapBean.getRdLongitude()),
					Double.parseDouble(mapBean.getRdLatitude()));
			model = new MapModel(MapActivity.this, rebitmapOrg, util);
			// model.setPointTee(teeLon, teeLat);
			model.setHandler(handler);
			model.setTeeType(Tee.GOLD);

			if (mapBean.getHoleId()[1] > 0) {
				for (FairwayObject fairwayObject : holesList) {
					if (mapBean.getAerialPhotoID().intValue() == fairwayObject
							.getAerialPhotoBean().getAerialPhotoID().intValue()) {
						model.addPointGreen(Double.parseDouble(fairwayObject
								.getGreenLongitude()), Double
								.parseDouble(fairwayObject.getGreenLatitude()));
					}
				}
			} else {
				model.addPointGreen(
						Double.parseDouble(holesList.get(
								PlayGameModel.curHoleNumber)
								.getGreenLongitude()),
						Double.parseDouble(holesList.get(
								PlayGameModel.curHoleNumber).getGreenLatitude()));
			}
			mapView.setNavigationMapModel(model);
//			LocationUtil location = LocationUtil.getInstance(this
//					.getApplicationContext());
			// model.setPointMe(model.getCoordinateUtil().convertLonLatToOldXY(location.longitude,
			// location.latitude));
			if (layout_wind.getVisibility() == View.VISIBLE) {
				handler.sendEmptyMessage(WHAT_CHANGE_WIND);
			}
			mapView.invalidate();

		} catch (Exception e) {
			Log.e(TAG, "", e);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(MapBroadcastReceiver);
		MapView.changeMarkeVo(db, holesList.get(PlayGameModel.curHoleNumber)
				.getId(), holesList.get(PlayGameModel.curHoleNumber).getId());

		if (mapView.isState_direction()) {
			SensorManager sensor_manager = (SensorManager) getSystemService(SENSOR_SERVICE); // 获取管理服务
			sensor_manager.unregisterListener(this);
		}
	}

	@Override
	protected void onStop() {
		LocationUtil location = LocationUtil.getInstance(this
				.getApplicationContext());
		location.clearHandlerInfo();
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerBoradcastReceiver();
		Log.d(TAG, "isChangeSubCourse=" + PlayGameModel.isChangeSubCourse);
		LocationUtil location = LocationUtil.getInstance(this
				.getApplicationContext());
		if (hideFlag) {
			location.setHandlerInfo(handler, WHAT_LOCATION_REFRESH,
					LocationUtil.LOC_PERIOD);
		}else {
			location.setHandlerInfo(handler1, PlayTabActivity.WHAT_DISTANCETRACKING, LocationUtil.LOC_PERIOD);
		}
		db = DataBaseSingle.getInstance(this).getDataBase();
		changeMapByHoleID();
		if (mapView.isState_direction()) {
			SensorManager sensor_manager = (SensorManager) getSystemService(SENSOR_SERVICE); // 获取管理服务
			sensor_manager.registerListener(MapActivity.this,
					sensor_manager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
					SensorManager.SENSOR_DELAY_GAME);
		}
	}

	private void changeMapByHoleID() {
		Log.d(TAG, "lastHoleID=" + lastHoleIndex + ",curHoleNumber="
				+ PlayGameModel.curHoleNumber);
		// 同一洞且子球场不改变
		if (lastHoleIndex == PlayGameModel.curHoleNumber
				&& !PlayGameModel.isChangeSubCourse) {
			return;
		}
		if (PlayGameModel.isChangeSubCourse) {
			initData();
		}
		if (lastHoleIndex > -1) {
			MapView.changeMarkeVo(db, aerialPhotoBeans.get(lastHoleIndex)
					.getAerialPhotoID(),
					aerialPhotoBeans.get(PlayGameModel.curHoleNumber)
							.getAerialPhotoID());
		} else {
			MapView.changeMarkeVo(db, -1,
					aerialPhotoBeans.get(PlayGameModel.curHoleNumber)
							.getAerialPhotoID());
		}

		lastHoleIndex = PlayGameModel.curHoleNumber;
		uploadPhoto();
		PlayGameModel.isChangeSubCourse = false;
	}

	BroadcastReceiver MapBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(ACTION_BROADCASTRECEIVER)) {
				// Toast.makeText(ObstadeActivity.this,
				// "处理action名字相对应的广播"+PlayGameModel.curHoleNumber, 200);
				changeMapByHoleID();
			}
		}
	};
	private int requestCode_courseSetting = 0x1;
	private AlertDialog distanceDialog;
	private TextView distanceTextView;
	private boolean hideFlag = true;
	private String distance = "0";
	private String distanceUnit_near;
	private double curLat;
	private double curLon;
	private LocationUtil location;

	private void registerBoradcastReceiver() {
		IntentFilter myIntentFilter = new IntentFilter();
		myIntentFilter.addAction(ACTION_BROADCASTRECEIVER);
		// 注册广播
		registerReceiver(MapBroadcastReceiver, myIntentFilter);
	}
	private void getDistanceDialog() {
		distanceDialog = new AlertDialog.Builder(this).create();
		distanceDialog.show();
		distanceDialog.getWindow().setContentView(R.layout.dialog_distance);
		WindowManager.LayoutParams layoutParams = distanceDialog.getWindow()
				.getAttributes();
		layoutParams.width = ScreenUtil.getScreenWidth(this);
		Log.e(TAG, "width=" + layoutParams.width);
		distanceDialog.getWindow().setAttributes(layoutParams);
		distanceDialog.getWindow().setGravity(Gravity.BOTTOM);
		// distanceDialog.setCancelable(false);
		distanceDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				location.resetHandlerInfo(handler, WHAT_LOCATION_REFRESH);
				hideFlag = true;
			}
		});
		LayoutParams btnLayoutParams = null;
		LinearLayout layout_distance = (LinearLayout) distanceDialog.findViewById(R.id.layout_distance);
		distanceTextView = (TextView) distanceDialog
				.findViewById(R.id.distance_text);
		btnLayoutParams = layout_distance.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		layout_distance.setLayoutParams(btnLayoutParams);
		Button reset = (Button) distanceDialog
				.findViewById(R.id.distance_reset);
		btnLayoutParams = reset.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		reset.setLayoutParams(btnLayoutParams);
		reset.setOnClickListener(this);

		Button hide = (Button) distanceDialog.findViewById(R.id.distance_hide);
		btnLayoutParams = hide.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		hide.setLayoutParams(btnLayoutParams);
		hide.setOnClickListener(this);

		
	}
		
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.distance_reset: {
			distance = "0";
			distanceTextView.setText(distance+distanceUnit_near);
			LocationUtil util = LocationUtil.getInstance(this
					.getApplicationContext());
			curLat = util.latitude;
			curLon = util.longitude;
		}
			break;
		case R.id.distance_hide:
			hideFlag = true;
			distanceDialog.dismiss();
			location.resetHandlerInfo(handler, WHAT_LOCATION_REFRESH);
			break;
		case R.id.btn_share: {
			location.resetHandlerInfo(handler1,PlayTabActivity.WHAT_DISTANCETRACKING);
			if (distanceDialog == null) {
				
				getDistanceDialog();
			}
			distanceDialog.show();
			hideFlag = false;
			if (!hideFlag) {
				LocationUtil util = LocationUtil.getInstance(this
						.getApplicationContext());
				curLat = util.latitude;
				curLon = util.longitude;
				distance = "0";
			}
			distanceUnit_near = DistanceUtil.getDistanceUnit(this, PlayGameModel.shortDistanceIndex);
			distanceTextView.setText(distance+distanceUnit_near);
		}
			break;
		case R.id.btn_evaluate: {
			Intent intent = new Intent(MapActivity.this,
					EvaluateOursActivity.class);
			intent.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
					playGameObject.getCoursesObject());
			startActivity(intent);
		}
			break;
		case R.id.btn_more: {
			Intent intent = new Intent(MapActivity.this,
					CourseSettingActivity.class);
			startActivityForResult(intent, requestCode_courseSetting);
		}
			break;
		case R.id.map:
			if (drawer.isOpened()) {
				drawer.close();
			}
			break;
		case R.id.btn_cursor:
			mapView.setFuncButtonState(FuncButtonState.cursor);
			btn_cursor.setBackgroundResource(R.drawable.bg_cursor);
			btn_addMark.setBackgroundResource(R.color.black);
			btn_removeMarke.setBackgroundResource(R.color.black);
			mapView.invalidate();
			break;
		case R.id.btn_addMark:
			mapView.setFuncButtonState(FuncButtonState.addMark);
			btn_cursor.setBackgroundResource(R.color.black);
			btn_addMark.setBackgroundResource(R.drawable.bg_cursor);
			btn_removeMarke.setBackgroundResource(R.color.black);
			mapView.invalidate();
			break;
		case R.id.btn_removeMark:
			mapView.setFuncButtonState(FuncButtonState.removeMark);
			btn_cursor.setBackgroundResource(R.color.black);
			btn_addMark.setBackgroundResource(R.color.black);
			btn_removeMarke.setBackgroundResource(R.drawable.bg_cursor);
			mapView.invalidate();
			break;	
		default:
			break;
		}
	}

	// 传感器值改变
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	Bitmap personBmp_location;
	protected double currentLat;
	protected double currentLong;

	// 精度改变
	@Override
	public void onSensorChanged(SensorEvent event) {
		// 获取触发event的传感器类型
		int sensorType = event.sensor.getType();
		switch (sensorType) {
		case Sensor.TYPE_ORIENTATION:
			{
			float degree = event.values[0] ; // 获取z转过的角度
			double mapDegree = mapBean.getRotationView();
			Log.e(TAG, "degree:" + degree + ":mapViewRotation:" + mapDegree);
			
			Bitmap personBmp = BitmapUtil.rotateBitmap(personBmp_location,
					degree + mapDegree);
			mapView.setPersonBmp_direction(personBmp);
			mapView.invalidate();
			}
			break;

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == requestCode_courseSetting) {
			mapView.initDistance(this);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	private Handler handler1 = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case PlayTabActivity.WHAT_DISTANCETRACKING:
				currentLat = location.latitude;
				currentLong = location.longitude;
//				Toast.makeText(PlayTabActivity.this, "currentLat="+currentLat+"'currentLong="+currentLong, 1500).show();
				distance = DistanceUtil.getDistanceByMeter(currentLat,
						currentLong, curLat, curLon) + "";
				if (!hideFlag) {
					distanceTextView.setText(distance+distanceUnit_near);
				}
				break;

			default:
				break;
			}
		};
	};
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			switch (buttonView.getId()) {
			case R.id.btn_cursor:
				mapView.setFuncButtonState(FuncButtonState.cursor);
				mapView.invalidate();
				break;
			case R.id.btn_addMark:
				mapView.setFuncButtonState(FuncButtonState.addMark);
				mapView.invalidate();
				break;
			case R.id.btn_removeMark:
				mapView.setFuncButtonState(FuncButtonState.removeMark);
				mapView.invalidate();
				break;
			default:
				break;
			}
		}
	}
}
