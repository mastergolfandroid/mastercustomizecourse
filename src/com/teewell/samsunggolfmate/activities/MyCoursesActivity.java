package com.teewell.samsunggolfmate.activities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.DownloadAerialPhotoBean;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.datebase.MyDataBaseAdapter;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.course.CourseInfoModel;
import com.teewell.samsunggolfmate.utils.JsonProcessUtil;
import com.teewell.samsunggolfmate.utils.SharedPreferencesUtils;
import com.teewell.samsunggolfmate.utils.VolleyUitl;
import com.teewell.samsunggolfmate.views.CourseListAdapter;
import com.teewell.samsunggolfmate.views.ProgressDialogTitleView;
import com.teewell.samsunggolfmate.views.XListView;

/**
 * @Project GolfMate
 * @package com.teewell.golfmate.activities
 * @title MyCoursesActivity.java
 * @Description TODO
 * @author Administrator
 * @date 2013-1-16 上午11:21:28
 * @Copyright Copyright(C) 2013-1-16
 * @version 1.0.0
 */
public class MyCoursesActivity extends ParentActivity implements
		OnItemClickListener, OnScrollListener, OnClickListener {
	private final String TAG = MyCoursesActivity.class.getName();
	public static final int WHAT_ADAPTER = 100;
	private XListView listView;
	private Button btn_continue;
	private List<CoursesObject> coursesList;
	private CourseListAdapter adapter;
	private MyDataBaseAdapter mdb;
	private CourseInfoModel courseInfoModel;

	private int firstVisibleItem;
	private int visibleItemCount;

	private int scrollState;
	private ProgressDialog mProgress;
	private PlayGameObject playGameObject;
	private Button btn_cancel;
	private EditText text_search;
	public static final int WHAT_REFRESH_VIEWDATA = 103;
	private static final int REFRESH_DELAY = 1000 * 2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mycourses);
		courseInfoModel = CourseInfoModel.getInstance(this);
		initView();
	}

	/**
	 */
	private void actionRefreshCourseList() {
		List<CoursesObject> list = courseInfoModel.getMyCoursesData(mdb);
		coursesList = new ArrayList<CoursesObject>();
		Iterator<CoursesObject> srcIt = list.iterator();
		while (srcIt.hasNext()) {
			CoursesObject course = (CoursesObject) srcIt.next();
			if (course.getCoursesName().indexOf(
					text_search.getText().toString()) >= 0) {
				coursesList.add(course);
			}
		}
		adapter.setCoursesList(coursesList);
		adapter.notifyDataSetChanged();
		if (coursesList != null && coursesList.size() > 0) {
			// listView.setImageFooterViewVisiable(true);
			handler.sendEmptyMessageDelayed(WHAT_REFRESH_VIEWDATA,
					REFRESH_DELAY);
		} else {
			// listView.setImageFooterViewVisiable(false);
		}
	}

	/**
	 */
	private void initView() {
		TextView tv_titleName = (TextView) findViewById(R.id.tv_titleName);
		tv_titleName.setText(getString(R.string.mycourses));
		btn_continue = (Button) findViewById(R.id.btn_rightTitle);
		btn_continue.setText(getString(R.string.play_continue));
		btn_continue.setOnClickListener(this);
		listView = (XListView) findViewById(R.id.listView);
		listView.setOnItemClickListener(this);
		adapter = new CourseListAdapter(this);
		listView.setAdapter(adapter);
		listView.setOnScrollListener(this);
		listView.setPullLoadEnable(false);
		listView.setPullRefreshEnable(false);
		// listView.setImageFooterViewVisiable(false);
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		btn_cancel.setOnClickListener(this);
		text_search = (EditText) findViewById(R.id.edit_search);
		text_search.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				if (s != null && s.length() > 0) {
					btn_cancel.setVisibility(View.VISIBLE);

				} else {
					btn_cancel.setVisibility(View.GONE);
				}

				actionRefreshCourseList();
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});
		text_search.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_UP:
					int curX = (int) event.getX();
					if (curX > v.getWidth() - 38
							&& !TextUtils.isEmpty(text_search.getText())) {
						text_search.setText("");
						int cacheInputType = text_search.getInputType();// backup
						text_search.setInputType(InputType.TYPE_NULL);// disable
						text_search.onTouchEvent(event);// call native handler
						text_search.setInputType(cacheInputType);// restore
																	// input
						return true;// consume touch even
					}
					break;
				}
				return false;
			}
		});
	}

	@Override
	protected void onResume() {
		SharedPreferencesUtils shareUtils = new SharedPreferencesUtils(this);
		String infoString = shareUtils.getString(
				SharedPreferenceConstant.SHAREDPREFERENCE_PLAYGAMEOBJECT, null);
		playGameObject = null;
		if (infoString != null) {
			playGameObject = (PlayGameObject) JsonProcessUtil.fromJSON(
					infoString, PlayGameObject.class);
		}
		if (playGameObject != null) {
			btn_continue.setVisibility(View.VISIBLE);
		} else {
			btn_continue.setVisibility(View.GONE);
		}
		mdb = DataBaseSingle.getInstance(this).getDataBase();
		actionRefreshCourseList();
		super.onResume();
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_rightTitle:
			Intent intent = new Intent(MyCoursesActivity.this,
					PlayTabActivity.class);
			// 如果已经打开过的实例，将不会重新打开新的Activity
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
					playGameObject);
			startActivity(intent);
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2,
			long arg3) {
		arg1.setSelected(false);

		if (scrollState != SCROLL_STATE_IDLE) {
			return;
		}
		final CoursesObject coursesObject = adapter.getItem(arg2 - 1);
		if (coursesObject == null) {
			return;
		}

		if (playGameObject == null) {
			progressDialog(coursesObject);
			courseInfoModel.getSubCourseInfo(coursesObject, handler);
		} else {
			Dialog dialog = new AlertDialog.Builder(MyCoursesActivity.this)
					.setMessage(R.string.play_hint)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface arg0,
										int arg1) {
									arg0.dismiss();
									PlayGameModel.getInstance(MyCoursesActivity.this).setPlayGameObject(null);;
									progressDialog(coursesObject);
									courseInfoModel.getSubCourseInfo(
											coursesObject, handler);
								};
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									Intent intent = new Intent(
											MyCoursesActivity.this,
											PlayTabActivity.class);
									// 如果已经打开过的实例，将不会重新打开新的Activity
									intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
									startActivity(intent);
								}
							}).create();
			dialog.show();
		}
	}

	/**
	 * 进度条Dialog
	 */
	private void progressDialog(CoursesObject object) {
		mProgress = null;
		mProgress = new ProgressDialog(this);
		mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

		mProgress.setButton(getString(R.string.cancel_aysncCourse),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						VolleyUitl.getInstence(getApplicationContext()).getRequestQueue().cancelAll(handler);
					}
				});

		mProgress.setCustomTitle(new ProgressDialogTitleView(this, object)
				.getView());
		if (!mProgress.isShowing()) {
			mProgress.show();
			mProgress.setCancelable(false);
			mProgress.setProgress(0);
		}
	}

	private Handler handler = new Handler() {
		private DownloadAerialPhotoBean downloadAerialPhotoBean;
		private int aerialPhotoCount;

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {

			case WHAT_ADAPTER:
				adapter.notifyDataSetChanged();
				break;
			case CourseInfoModel.WHAT_GETSUBCOURSE_SUCCEED:
				// setLoadingState(MyCoursesActivity.this, false);
				if (mProgress != null && mProgress.isShowing()) {
					mProgress.dismiss();
				}
				Intent intent = new Intent(MyCoursesActivity.this,
						StartGameActivity.class);
				intent.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
						(CoursesObject) msg.obj);
				PlayGameModel.fromActivity = 3;
				startActivity(intent);
				break;
			case CourseInfoModel.WHAT_GETSUBCOURSEINFO_SUCCEED:
				downloadAerialPhotoBean = (DownloadAerialPhotoBean) msg.obj;
				aerialPhotoCount = downloadAerialPhotoBean
						.getAerialPhotoBeanList().size();
				mProgress.setMax(aerialPhotoCount);
				Message message = new Message();
				message.what = CourseInfoModel.WHAT_PROGRESS;
				message.arg1 = 0;
				message.arg2 = AerialPhotoBean.DOWNLOAD_SUCCESSED;
				handler.sendMessage(message);
				break;
			case CourseInfoModel.WHAT_FILD:
				if (mProgress != null && mProgress.isShowing()) {
					mProgress.dismiss();
				}
				showToast(getString(R.string.bad_network));
				break;
			case CourseInfoModel.WHAT_PROGRESS:
				if (mProgress != null && mProgress.isShowing()) {
					if (msg.arg2 != AerialPhotoBean.DOWNLOAD_SUCCESSED) {
						mProgress.dismiss();
						showToast(getString(R.string.bad_network));
					} else {
						mProgress.setProgress(msg.arg1);
						if (msg.arg1 < aerialPhotoCount) {
							courseInfoModel.downloadAerialPhoto(handler,
									downloadAerialPhotoBean, msg.arg1);
						} else {
							mProgress.dismiss();
							CoursesObject coursesObject = downloadAerialPhotoBean
									.getCoursesObject();
							coursesObject.setSubCourses(downloadAerialPhotoBean
									.getSubCourses());
							Message message2 = new Message();
							message2.what = CourseInfoModel.WHAT_GETSUBCOURSE_SUCCEED;
							message2.obj = coursesObject;
							handler.sendMessage(message2);
						}
					}
				}
				break;
			case WHAT_REFRESH_VIEWDATA: {
				courseInfoModel.loadNetworkData(firstVisibleItem,
						visibleItemCount, adapter, listView);
			}
				break;
			}
		}
	};

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		// save the item property
		this.firstVisibleItem = firstVisibleItem;
		this.visibleItemCount = visibleItemCount;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		this.scrollState = scrollState;
		// start the loader
		if (scrollState == SCROLL_STATE_IDLE) {
			Log.i(TAG, "start to load network data");
			courseInfoModel.loadNetworkData(firstVisibleItem, visibleItemCount,
					adapter, listView);
		}
	}
}
