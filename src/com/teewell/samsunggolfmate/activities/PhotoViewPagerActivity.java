package com.teewell.samsunggolfmate.activities;

import java.util.ArrayList;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.models.PhotoModel;
import com.teewell.samsunggolfmate.views.PhotoViewPagerAdapter;
import com.teewell.samsunggolfmate.views.photoview.HackyViewPager;

public class PhotoViewPagerActivity extends ParentActivity {
	private HackyViewPager viewPager;
	private Button btn_back,btn_delete;
	private TextView tv_count;
	public static final String PHOTOPATHLIST = "photoPathList";
	public static final String PHOTO_INDEX = "photoIndex";
	
	private ArrayList<String> photoPathList;
	private PhotoViewPagerAdapter adapter;
	private PhotoModel photoMedel;
	private int index;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_viewpager);
		viewPager = (HackyViewPager) findViewById(R.id.view_photo);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_delete = (Button) findViewById(R.id.btn_rightTitle);
		tv_count = (TextView) findViewById(R.id.tv_titleName);
		btn_back.setOnClickListener(onClickListener);
		btn_delete.setOnClickListener(onClickListener);
		viewPager.setOnPageChangeListener(onPageChangeListener);
		
		index = getIntent().getIntExtra(PHOTO_INDEX, 0);
		photoMedel = PhotoModel.getInstance(this);		
		photoPathList = getIntent().getStringArrayListExtra(PHOTOPATHLIST);
		tv_count.setText((index+1)+"/"+photoPathList.size());
		adapter = new PhotoViewPagerAdapter(this, photoPathList);
		viewPager.setAdapter(adapter);
		viewPager.setCurrentItem(index);
	}
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.btn_back:
					finish();
					break;
				case R.id.btn_delete:
					photoPathList.remove(index);
					photoMedel.removeBeanOfList_uploadPhotoBean(index);
					if (photoPathList.size()==0) {
						finish();
					}else {
						adapter.notifyDataSetChanged();
					}
				default:
					break;
			}
		}
	};
	private OnPageChangeListener onPageChangeListener = new OnPageChangeListener() {
		
		@Override
		public void onPageSelected(int arg0) {
			index = arg0;
			tv_count.setText((arg0+1)+"/"+photoPathList.size());
		}
		
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onPageScrollStateChanged(int arg0) {
//			tv_titleName.setText((arg0+1)+"/"+photoPathList.size());
		}
	};
	@Override
	protected void onStop() {
		for (Bitmap bitmap : adapter.getList()) {
			if (bitmap!=null&&!bitmap.isRecycled()) {
				bitmap.recycle();
			}
		}
		super.onStop();
	}
	
}
