package com.teewell.samsunggolfmate.activities;

import java.util.List;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.SubCoursesObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.utils.ActivityManagerUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;

@SuppressWarnings("unused")
public class PlaySettingActivity extends ParentActivity implements
		OnClickListener {
	private final String TAG = PlaySettingActivity.class.getName();
	private Button btnBack, btnFinish, btn_courseInfo;
	private TextView tvCourseName;
	private RelativeLayout layoutSubcourse1, layoutSubcourse2, layoutHalfpace,
			layoutHole;
	private TextView tvSelect, tvSubcourse1, tvSubcourse2, tvHalfpace, tvHole,
			textView, golfNameView;

	private ImageView ivPerson;
	private List<SubCoursesObject> list_subObject;
	private SubCoursesObject subObject;
	private int select;
	private String subName;
	private int subId;
	private int chickId;
	private int btnId;
	// private boolean isSelect;
	private int[] score;
	private int[] putts;
	private AlertDialog dialog, dialogFinish;

	private PlayGameObject playGameObject;
	public static final int PHOTOGRAPH = 20;
	public static final int SHARE = 30;
	public static final int REQUESTCODE_SELECT = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.playerset);
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);

		initView();
		initDate();
	}

	private void initDate() {
		tvCourseName.setText(playGameObject.getCoursesObject().getShortName());

		layoutHole.setEnabled(false);
		layoutHalfpace.setEnabled(false);
		btnBack.setText(getString(R.string.play_continue).toString());
		btnFinish.setVisibility(View.VISIBLE);

		initSetting();
	}

	private void initSetting() {
		tvHalfpace.setText(playGameObject.getStartTee());
		tvHole.setText((playGameObject.getStartHole() + 1) + "号洞");
		if (playGameObject.getSubCourseNameArray()[1] != null) {
			layoutSubcourse2.setVisibility(View.VISIBLE);
			tvSubcourse1.setText(playGameObject.getSubCourseNameArray()[0]);
			tvSubcourse2.setText(playGameObject.getSubCourseNameArray()[1]);
			tvSelect.setText(getString(R.string.select_frontnine_hole)
					.toString());
		} else {
			tvSelect.setText(getString(R.string.select_eighteen_hole)
					.toString());
			tvSubcourse1.setText(playGameObject.getSubCourseNameArray()[0]);
			layoutSubcourse2.setVisibility(View.GONE);
		}
		if (tvSubcourse1.getText().length() > 10) {
			tvSubcourse1.setTextSize(14);
			tvSubcourse2.setTextSize(14);
		}
		PlayGameModel.getInstance(this).asyncParAndAerialPhotoBeans(playGameObject);
	}


	private void initView() {
		tvCourseName = (TextView) findViewById(R.id.tv_titleName);
		layoutSubcourse1 = (RelativeLayout) findViewById(R.id.layout_subCourse1);
		layoutSubcourse2 = (RelativeLayout) findViewById(R.id.layout_subCourse2);
		tvSelect = (TextView) findViewById(R.id.tv_select);
		tvSubcourse1 = (TextView) findViewById(R.id.tv_subCourse1);
		tvSubcourse2 = (TextView) findViewById(R.id.tv_subCourse2);

		layoutHole = (RelativeLayout) findViewById(R.id.layout_hole);
		layoutHalfpace = (RelativeLayout) findViewById(R.id.layout_halfpace);
		tvHalfpace = (TextView) findViewById(R.id.tv_halfpace);
		tvHole = (TextView) findViewById(R.id.tv_hole);
		((Button) findViewById(R.id.btn_begin)).setVisibility(View.INVISIBLE);;
		btnFinish = (Button) findViewById(R.id.btn_finish);
		btnBack = (Button) findViewById(R.id.btn_back);
		btn_courseInfo = (Button) findViewById(R.id.btn_rightTitle);
		btn_courseInfo.setText(getString(R.string.courseintro));

		btnFinish.setOnClickListener(this);
		btnBack.setOnClickListener(this);
		btn_courseInfo.setOnClickListener(this);
		layoutSubcourse1.setOnClickListener(this);
		layoutSubcourse2.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		chickId = v.getId();
		switch (v.getId()) {
		case R.id.layout_subCourse1:
			doSelect(SelectActivity.SELECT_SUB_I);
			break;
		case R.id.layout_subCourse2:
			doSelect(SelectActivity.SELECT_SUB_II);
			break;
		case R.id.btn_back:
			finish();
			break;
		case R.id.btn_begin:
			PlayGameModel.getInstance(this).setPlayGameObject(null);
			ActivityManagerUtil.getScreenManager().popActivityToSomeActivity(
					PlayTabActivity.class);
			break;
		case R.id.btn_finish:
			finishingd();
			break;
		case R.id.btn_rightTitle: {
			Intent intent = new Intent(this, CourseIntroActivity.class);
			intent.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
					playGameObject.getCoursesObject());
			startActivity(intent);
		}
			break;
		case R.id.btn_select1: {
			dialog.dismiss();
			PlayGameModel.getInstance(this).cleanCacheDate(this,
					playGameObject.getUserScoreCardObject());
			// Intent intent = new Intent(this,ShareActivity.class);
			// intent.putExtra(ConstantStorage.SCOREBEAN,
			// PlayGameModel.scoreCardObject);
			// startActivity(intent);
			PlayGameModel.fromActivity = 0;
			finish();
		}
			break;
		case R.id.btn_select2:
			dialog.dismiss();
			PlayGameModel.getInstance(this).cleanCacheDate(this,
					playGameObject.getUserScoreCardObject());
			PlayGameModel.fromActivity = 0;
			finish();
			break;
		case R.id.distance_reset:
			PlayGameModel.getInstance(this).cleanCacheDate(this,
					playGameObject.getUserScoreCardObject());
			PlayGameModel.fromActivity = 0;
			finish();
			break;
		case R.id.distance_hide:
			dialogFinish.dismiss();
			break;
		}
	}

//	private void finishGame() {
//		dialog = new AlertDialog.Builder(this).create();
//		dialog.show();
//		dialog.getWindow().setContentView(R.layout.btn_exitselect_item);
//		WindowManager.LayoutParams layoutParams = dialog.getWindow()
//				.getAttributes();
//		layoutParams.width = ScreenUtil.getScreenWidth(this);
//		dialog.getWindow().setAttributes(layoutParams);
//		dialog.getWindow().setGravity(Gravity.BOTTOM);
//
//		LayoutParams btnLayoutParams = null;
//		Button btn_select1 = (Button) dialog.findViewById(R.id.btn_select1);
//		btnLayoutParams = btn_select1.getLayoutParams();
//		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
//		btn_select1.setLayoutParams(btnLayoutParams);
//
//		btn_select1.setText(getString(R.string.share).toString());
//		btn_select1.setOnClickListener(this);
//		// btn_select2.setVisibility(View.GONE); //���غ�Ӱ����
//
//		Button btn_select2 = (Button) dialog.findViewById(R.id.btn_select2);
//		btnLayoutParams = btn_select2.getLayoutParams();
//		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
//		btn_select2.setLayoutParams(btnLayoutParams);
//
//		btn_select2.setText(getString(R.string.play_end).toString());
//		btn_select2.setOnClickListener(this);
//	}

	private void finishing() {
		dialogFinish = new AlertDialog.Builder(this).create();
		dialogFinish.show();
		dialogFinish.getWindow().setContentView(R.layout.dialog_finish_select);
		WindowManager.LayoutParams layoutParams = dialogFinish.getWindow()
				.getAttributes();
		layoutParams.width = ScreenUtil.getScreenWidth(this);
		dialogFinish.getWindow().setAttributes(layoutParams);
		dialogFinish.getWindow().setGravity(Gravity.BOTTOM);

		LinearLayout linearLayout = (LinearLayout) dialogFinish
				.findViewById(R.id.linearLayuot_select);
		linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);

		LayoutParams btnLayoutParams = null;
		Button btn1 = (Button) dialogFinish.findViewById(R.id.btn1);
		btnLayoutParams = btn1.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		btn1.setLayoutParams(btnLayoutParams);

		btn1.setText(getString(R.string.play_end).toString());
		btn1.setOnClickListener(this);

		Button btn2 = (Button) dialogFinish.findViewById(R.id.btn2);
		btnLayoutParams = btn2.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		btn2.setLayoutParams(btnLayoutParams);
		btn2.setText(getString(R.string.return_).toString());
		btn2.setOnClickListener(this);
	}
	private void finishingd() {
		dialogFinish = new AlertDialog.Builder(this).create();
		dialogFinish.show();
		dialogFinish.setContentView(R.layout.dialog_distance);
		dialogFinish.getWindow().setGravity(Gravity.BOTTOM);

		LinearLayout linearLayout = (LinearLayout) dialogFinish
				.findViewById(R.id.layout_distance);
		linearLayout.setVisibility(View.GONE);

		Button btn1 = (Button) dialogFinish.findViewById(R.id.distance_reset);
		LayoutParams params = btn1.getLayoutParams();
		params.width = ScreenUtil.getScreenWidth(getApplicationContext())-100;
		btn1.setLayoutParams(params);
		btn1.setText(R.string.play_end);
		btn1.setOnClickListener(this);

		Button btn2 = (Button) dialogFinish.findViewById(R.id.distance_hide);
		params = btn2.getLayoutParams();
		params.width = ScreenUtil.getScreenWidth(getApplicationContext())-100;
		btn2.setText(getString(R.string.return_).toString());
		btn2.setOnClickListener(this);
	}
	private void doSelect(String type) {
		Intent i = new Intent(this, SelectActivity.class);
		i.putExtra(SelectActivity.SELECT_TYPE, type);
		i.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT, playGameObject);
		startActivityForResult(i, REQUESTCODE_SELECT);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d(TAG, "onKeyDown.....");
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.d(TAG, "onKeyDown..... back is click");
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUESTCODE_SELECT && resultCode == RESULT_OK) {
			initSetting();
		} else if (requestCode == SHARE) {
			// Intent intent = new Intent(Actions.SHARE_ACTIVITY);
			// startActivity(intent);
			// finish();
		} else if (requestCode == PHOTOGRAPH) {
			// Intent intent = new Intent(Actions.SHARE_ACTIVITY);
			// startActivity(intent);
			// finish();
		}
	}
}
