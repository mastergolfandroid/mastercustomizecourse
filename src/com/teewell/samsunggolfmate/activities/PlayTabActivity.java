package com.teewell.samsunggolfmate.activities;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.utils.ActivityManagerUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.utils.MD5;
import com.teewell.samsunggolfmate.utils.SharedPreferencesUtils;

public class PlayTabActivity extends TabActivity implements
		OnCheckedChangeListener, OnItemClickListener, OnItemSelectedListener,
		OnClickListener {
	private static final String TAG = PlayTabActivity.class.getName();
	private TabHost tabHost;
	private Resources res;
	private Gallery gallery;
	private int nowIndex = -1;
	private int lastIndex = -1;
	private TextView tv_courseName;
	private View view = null;
	private int count = 0;
	private TextView distanceTextView;

	private RadioButton map_btn;
	private RadioButton obstade_btn;
	private RadioButton distance_btn;
	private RadioButton score_btn;
	private RadioButton set_btn;

	private static final String tabButtonOne = "MapActivity";
	private static final String tabButtonTwo = "RoadbackActivity";
	private static final String tabButtonFour = "PayActivity";
	private static final String tabButtonThree = "SharePhotosActivity";
	private ButtonSelect broadcastTag = ButtonSelect.mapBroadcastTag;

	private enum ButtonSelect {
		mapBroadcastTag, obstadeBroadcastTag,shareBroadcastTag, entrypointBroadcastTag, payBroadcastTag
	}

	private int bigballWidth = 80;
	private int smallballWidth = 40;

	private double currentLat = 0.0; //
	private double currentLong = 0.0; //

	private boolean danceDiaLog = false;

	private double curLat = 0.0;//
	private double curLon = 0.0;
	private String distance = "0";
	public static final int WHAT_DISTANCETRACKING = 0x10;

	private LocationUtil locatio;
	private Integer[] imageInteger = { R.drawable.ball1, R.drawable.ball2,
			R.drawable.ball3, R.drawable.ball4, R.drawable.ball5,
			R.drawable.ball6, R.drawable.ball7, R.drawable.ball8,
			R.drawable.ball9, R.drawable.ball10, R.drawable.ball11,
			R.drawable.ball12, R.drawable.ball13, R.drawable.ball14,
			R.drawable.ball15, R.drawable.ball16, R.drawable.ball17,
			R.drawable.ball18 };

	private PlayGameObject playGameObject;
	private UserScoreCardObject scoreCardObject;
	private CoursesObject coursesObject;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.playtab);
		ActivityManagerUtil.getScreenManager().pushActivity(this);
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
		PlayGameModel.getInstance(this).setPlayGameObject(playGameObject);
		scoreCardObject = playGameObject.getUserScoreCardObject();
		coursesObject = playGameObject.getCoursesObject();
		res = getResources(); // Resource object to get Drawables
		tabHost = getTabHost(); // The activity TabHost
		bigballWidth = (int) res.getDimension(R.dimen.bigballWidth);
		smallballWidth = (int) res.getDimension(R.dimen.smallballWidth);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		initIntent();
		findViewsByIds();
	}

	private void initData() {
		int[] pars = scoreCardObject.getGeneralScoreBean().getPars();
		tv_courseName.setText(coursesObject.getShortName() + "-"
				+ (PlayGameModel.curHoleNumber + 1) + "(Par:"
				+ pars[PlayGameModel.curHoleNumber] + ")");
	}

	private void findViewsByIds() {
		tv_courseName = (TextView) findViewById(R.id.tv_courseName);
		gallery = (Gallery) findViewById(R.id.gallery);
		map_btn = (RadioButton) findViewById(R.id.map_btn);
		obstade_btn = (RadioButton) findViewById(R.id.obstade_btn);
		distance_btn = (RadioButton) findViewById(R.id.distance_btn);
		score_btn = (RadioButton) findViewById(R.id.score_btn);
		set_btn = (RadioButton) findViewById(R.id.set_btn);
		gallery.setAdapter(new ImageAdapter(this));
		gallery.setOnItemClickListener(this);
		gallery.setOnItemSelectedListener(this);
		gallery.setCallbackDuringFling(false);
		map_btn.setOnCheckedChangeListener(this);
		obstade_btn.setOnCheckedChangeListener(this);
		distance_btn.setOnCheckedChangeListener(this);
		score_btn.setOnCheckedChangeListener(this);
		set_btn.setOnCheckedChangeListener(this);
	}

	private void goToItem() {
		gallery.setSelection(PlayGameModel.curHoleNumber);
		initData();
	}

	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			
			switch (buttonView.getId()) {
			case R.id.map_btn:
				broadcastTag = ButtonSelect.mapBroadcastTag;
				changeRadioState();
				break;
			case R.id.obstade_btn:
				broadcastTag = ButtonSelect.obstadeBroadcastTag;
				changeRadioState();
				break;
			case R.id.distance_btn:
//				broadcastTag = ButtonSelect.shareBroadcastTag;
				Intent intent_share = new Intent(PlayTabActivity.this,
						SharePhotosActivity.class);
				intent_share.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
						playGameObject);	
				startActivity(intent_share);
				
				break;
			case R.id.score_btn:
				Intent intent = new Intent(PlayTabActivity.this,
						SingleScoreCardActivity.class);
//				intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
//						playGameObject);
				startActivity(intent);
				intent = null;
				break;
			case R.id.set_btn:
				playGameObject = PlayGameModel.getInstance(PlayTabActivity.this).getPlayGameObject();
				Intent i = new Intent(this, PlaySettingActivity.class);
				i.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
						playGameObject);
				startActivity(i);
				i = null;
				break;
			}
		}
	}

	private TabHost.TabSpec buildTabSpec(String tag, int resLabel, int resIcon,
			final Intent content) {
		return this.tabHost.newTabSpec(tag)
				.setIndicator(getString(resLabel), res.getDrawable(resIcon))
				.setContent(content);
	}

	private void initIntent() {
		Intent intent_map = new Intent(this, MapActivity.class);
		intent_map.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
				playGameObject);
		Intent intent_obstade = new Intent(this, RoadblockActivity.class);
		intent_obstade.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
				playGameObject);	
		
		this.tabHost = getTabHost();
		TabHost localTabHost = this.tabHost;
		localTabHost.setCurrentTabByTag(null);
		localTabHost.addTab(buildTabSpec(tabButtonOne, R.string.map,
				R.drawable.map_btn, intent_map));
		localTabHost.addTab(buildTabSpec(tabButtonTwo, R.string.obstade,
				R.drawable.obstade_btn, intent_obstade));
	}

	@Override
	protected void onResume() {
		Log.d(TAG, "onResume....");
		super.onResume();
		setPayState();
		changeRadioState();
		goToItem();
	}
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		Log.d(TAG, "onRestart....");
		super.onRestart();
	}
	private boolean payState;

	private void setPayState() {

		String key = MD5.getMD5Key("courseId" + coursesObject.getCouseID());
		String time = new SharedPreferencesUtils(this).getString(key, null);
		if (time != null) {
			payState = true;
		} else {
			payState = false;
		}

		payState = true;
	}

	@Override
	protected void onPause() {
		super.onPause();		
	}

	private void changeRadioState() {
		Log.d(TAG, "broadcastTag=" + broadcastTag);
		switch (broadcastTag) {
		case mapBroadcastTag:
			map_btn.setChecked(true);
			break;
		case obstadeBroadcastTag:
			obstade_btn.setChecked(true);
			break;
//		case shareBroadcastTag:
//			distance_btn.setChecked(true);
//			break;
		default:
			break;
		}
//		if (activityState) {
//			return;
//		}
		switch (broadcastTag) {
		case mapBroadcastTag:
			Log.e(TAG, tabButtonOne);
			this.tabHost.setCurrentTabByTag(tabButtonOne);
			break;
		case obstadeBroadcastTag:
			Log.e(TAG, tabButtonTwo);
			this.tabHost.setCurrentTabByTag(tabButtonTwo);
			break;
//		case shareBroadcastTag:
//			this.tabHost.setCurrentTabByTag(tabButtonTwo);
//			break;
		default:
			break;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		nowIndex = arg2;
		if (view != null) {
			view.setLayoutParams(new Gallery.LayoutParams(smallballWidth,
					smallballWidth));
		}
		arg1.setLayoutParams(new Gallery.LayoutParams(bigballWidth,
				bigballWidth));
		view = arg1;
		if (payState) {
			changTag();
			return;
		}
		if (nowIndex < 3) {
			setActivityState(nowIndex, false);
			changTag();
		} else {
			PlayGameModel.curHoleNumber = (int) gallery.getSelectedItemId();
			initData();
			setActivityState(nowIndex, true);
		}
	}

	public synchronized void changTag() {
		Thread myThread = new Thread() {
			public void run() {
				super.run();
				try {
					int index = -1;
					while (true) {
						index = nowIndex;
						Thread.sleep(500);
						if (index == nowIndex) {
							myHandler.sendEmptyMessage(index);
							break;
						}
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		myThread.start();
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		count++;
		nowIndex = arg2;
		if (view != null) {
			view.setLayoutParams(new Gallery.LayoutParams(smallballWidth,
					smallballWidth));
		}
		arg1.setLayoutParams(new Gallery.LayoutParams(bigballWidth,
				bigballWidth));
		view = arg1;
		if (payState) {
			changTag();
			return;
		}
		if (nowIndex < 3) {
			setActivityState(nowIndex, false);
			changTag();
		} else {
			PlayGameModel.curHoleNumber = (int) gallery.getSelectedItemId();
			initData();
			setActivityState(nowIndex, true);
		}
	}

	private boolean activityState;

	private void setActivityState(int index, boolean state) {
		if (state != activityState) {
			activityState = state;
			if (index < 3) {
				changeRadioState();
			} else {
				this.tabHost.setCurrentTabByTag(tabButtonFour);
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	class ImageAdapter extends BaseAdapter {
		private Context context;

		public ImageAdapter(Context c) {
			context = c;
		}

		@Override
		public int getCount() {
			return imageInteger.length;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public View getView(int arg0, View arg1, ViewGroup arg2) {
			ImageView imageView = new ImageView(context);
			imageView.setImageResource(imageInteger[arg0]);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			imageView.setLayoutParams(new Gallery.LayoutParams(smallballWidth,
					smallballWidth));
			return imageView;
		}

	}

	public Handler myHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			int reInt = msg.what;
			Log.d(TAG, "reInt=" + reInt + ",lastIndex=" + lastIndex);
			if (reInt >= 0) {
				if (lastIndex == reInt) {
					return;
				}
				lastIndex = reInt;
				if (count > 1) {
					Intent intent = null;
					switch (broadcastTag) {
					case mapBroadcastTag:
						intent = new Intent(
								MapActivity.ACTION_BROADCASTRECEIVER);
						PlayGameModel.curHoleNumber = (int) gallery
								.getSelectedItemId();
						sendBroadcast(intent);
						break;
					case obstadeBroadcastTag:
						intent = new Intent(
								RoadblockActivity.ACTION_BROADCASTRECEIVER);
						PlayGameModel.curHoleNumber = (int) gallery
								.getSelectedItemId();
						sendBroadcast(intent);
						break;
					default:
						break;
					}
					initData();
				}
			}
		};
	};

	private boolean hideFlag = false;
	private String distanceUnit_near;


	
	
	public static String strFormat(String distance) {
		String reString = "";
		double distance_d = Double.parseDouble(distance);
		if (distance_d > 1000) {
			int temp = (int) distance_d;
			reString = String.valueOf(temp);
		} else {
			reString = distance;
		}
		return reString;
	}

	
	private Builder builder;

	/*
	 * TabHost中的返回键最好使用此方法监听返回键操作
	 */
	public boolean dispatchKeyEvent(KeyEvent event) {

		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {

			this.showDialog(1);
			return false;
		}
		return super.dispatchKeyEvent(event);
	}

	protected Dialog onCreateDialog(int id) {
		if (builder == null) {
			builder = new Builder(PlayTabActivity.this);
			builder.setMessage(getString(R.string.exit_round_tip));
			builder.setTitle(getString(R.string.exit_round));
			builder.setPositiveButton(getString(R.string.save),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							builder = null;
							finish();
						}
					});
			builder.setNegativeButton(getString(R.string.return_),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							builder = null;
						}
					});
			builder.setNeutralButton(getString(R.string.not_save),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							PlayGameModel.getInstance(PlayTabActivity.this).setPlayGameObject(null);
							DataBaseSingle.getInstance(getApplicationContext()).getDataBase().deleteAllLocalScoreCard();
							builder = null;
							finish();
						}
					});
		}
		return builder.create();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
}
