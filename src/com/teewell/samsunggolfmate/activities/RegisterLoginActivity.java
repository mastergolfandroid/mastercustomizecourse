package com.teewell.samsunggolfmate.activities;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.GolferObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.UserInfoModel;
import com.teewell.samsunggolfmate.utils.DialogUtil;

/**
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.activities
 * @title A.java
 * @Description TODO
 * @author Administrator
 * @date 2013-3-21 下午4:14:41
 * @Copyright Copyright(C) 2013-3-21
 * @version 1.0.0
 */
public class RegisterLoginActivity extends ParentActivity implements
		OnClickListener, OnFocusChangeListener {
	public static final String TAG = RegisterLoginActivity.class.getName();
	private Button btn_back;
	private Button btn_next;
	private Button btn_getCode;
	private EditText edit_phone;
	private EditText edit_code;

	Pattern phonePattern;

	public static final int MSG_SEND_VERIFY_CODE_START = 0x10;
	public static final int MSG_SEND_VERIFY_CODE_SUCCEED = 0x11;
	public static final int MSG_SEND_VERIFY_CODE_FAILED = 0x12;

	public static final int MSG_SEND_VERIFY_CODE_BAD_NETWORK = 0x1;

	public static final int MSG_REGISTER_START = 0x20;
	public static final int MSG_LOGIN_START = 0x21;

	public static final int MSG_REGISTER_LOGIN_SUCCEED = 0x22;
	public static final int MSG_REGISTER_LOGIN_FAILED = 0x23;
	public static final int MSG_REGISTER_LOGIN_BAD_NETWORK = 0x24;

	public static final int MSG_TIMER = 0x25;

	private Timer verifyCodeTimer;
	private int timerCounter;

	public static final int TIMER_COUNTER_MAX = 90;
	public static final int TIMER_PERIOD = 1000;

	private String intent_fromActivity;
	private GolferObject golferObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.register_login);
		findView();
		intent_fromActivity = getIntent().getStringExtra(
				KeyValuesConstant.INTENT_FROM_ACTIVITY);
		if (intent_fromActivity != null) {

			btn_back.setText(intent_fromActivity);
		} else {
			btn_back.setText(getString(R.string.setting));
		}

		phonePattern = Pattern.compile("^[1][3458][0-9]{9}$");

		handler.sendEmptyMessageDelayed(100, 1000);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			// do something...
			cancelHandlerOperation();
			DialogUtil.setLoadingState(RegisterLoginActivity.this, false);
		}
		return super.onKeyDown(keyCode, event);
	}

	private void cancelHandlerOperation() {
		UserInfoModel userInfoModel = UserInfoModel.getInstance(this);
		userInfoModel.cancelHandlerOperation();
	}

	private void finishWithHandler() {
		cancelHandlerOperation();
		if (getString(R.string.single_scorecard).equals(intent_fromActivity)) {
			golferObject = (GolferObject) getIntent().getSerializableExtra(
					KeyValuesConstant.INTENT_GOLFEROBJECT);
			Intent intent = new Intent(RegisterLoginActivity.this,
					UserInfoActivity.class);
			intent.putExtra(KeyValuesConstant.INTENT_GOLFEROBJECT, golferObject);
			intent.putExtra(KeyValuesConstant.INTENT_FROM_ACTIVITY,
					intent_fromActivity);
			startActivityForResult(intent, RESULT_OK);
		} else {
			this.finish();
		}
	}

	/**
	 * 获取控件的句柄
	 */
	private void findView() {
		TextView tv_titleName = (TextView) findViewById(R.id.tv_titleName);
		tv_titleName.setText(getString(R.string.user_register));
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_next = (Button) findViewById(R.id.btn_next);
		btn_getCode = (Button) findViewById(R.id.btn_getCode);

		edit_phone = (EditText) findViewById(R.id.edit_phone);
		edit_phone.setOnFocusChangeListener(this);
		edit_phone.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() == 1 && !"1".equals(s.toString())) {
					showToast("您输入的不是正确的手机号码，请从新输入!");
					return;
				} else if (s.length() == 2
						&& !("13".equals(s.toString())
								|| "14".equals(s.toString())
								|| "15".equals(s.toString()) || "18".equals(s
								.toString()))) {
					showToast("您输入的不是正确的手机号码，请从新输入!");
					return;
				} else if (s.length() > 11) {
					showToast("您输入的不是正确的手机号码，请从新输入!");
				}
				Matcher m = phonePattern.matcher(s.toString());
				btn_getCode.setEnabled(m.matches());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

		edit_code = (EditText) findViewById(R.id.edit_rerifyCode);
		btn_back.setOnClickListener(this);
		btn_next.setOnClickListener(this);
		btn_getCode.setOnClickListener(this);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.btn_next:
			if (rerifyInputContent()) {
				handler.sendEmptyMessage(MSG_REGISTER_START);
			}
			break;
		case R.id.btn_getCode:

			handler.sendEmptyMessage(MSG_SEND_VERIFY_CODE_START);
			break;
		}
	}

	/**
	 * 数据校验
	 */
	private boolean rerifyInputContent() {
		boolean rerifyResult = false;
		while (true) {
			String phone = edit_phone.getText().toString();
			if (phone.trim().length() <= 0) {
				break;
			}

			String code = edit_code.getText().toString();
			if (code.trim().length() <= 0) {
				break;
			}

			rerifyResult = true;
			break;
		}

		if (!rerifyResult) {
			showInputTips();
		}

		return rerifyResult;
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {

		public void handleMessage(Message msg) {

			switch (msg.what) {
			case MSG_REGISTER_START: {
				// show the waitting dialog
				DialogUtil.setLoadingState(RegisterLoginActivity.this, true);

				UserInfoModel userInfoModel = UserInfoModel
						.getInstance(RegisterLoginActivity.this);
				userInfoModel.registerWithVerifyCode(handler, edit_phone
						.getText().toString(), edit_code.getText().toString());
				break;
			}

			case MSG_REGISTER_LOGIN_SUCCEED:
				// show the waitting dialog
				DialogUtil.setLoadingState(RegisterLoginActivity.this, false);
				// UserScoreCardModel.getInstance(RegisterLoginActivity.this).changeUserDataBase();
				showToast(getString(R.string.register_login_success));

				// exit the activity
				RegisterLoginActivity.this.finishWithHandler();
				break;
			case MSG_REGISTER_LOGIN_FAILED:
				// show the waitting dialog
				DialogUtil.setLoadingState(RegisterLoginActivity.this, false);

				showToast(getString(R.string.register_login_error));
				break;
			case MSG_REGISTER_LOGIN_BAD_NETWORK:
				// show the waitting dialog
				DialogUtil.setLoadingState(RegisterLoginActivity.this, false);
				showToast(getString(R.string.bad_network));
				break;
			case MSG_SEND_VERIFY_CODE_START: {
				// show the waitting dialog
				DialogUtil.setLoadingState(RegisterLoginActivity.this, true);

				UserInfoModel userInfoModel = UserInfoModel
						.getInstance(RegisterLoginActivity.this);
				userInfoModel.sendVerifyCode(handler, edit_phone.getText()
						.toString());
				break;
			}
			case MSG_SEND_VERIFY_CODE_SUCCEED:
				// show the waitting dialog
				DialogUtil.setLoadingState(RegisterLoginActivity.this, false);

				showToast(getString(R.string.send_verify_code_succeed));

				startVerifyCodeButtonTimer();
				break;
			case MSG_SEND_VERIFY_CODE_FAILED:
				// show the waitting dialog
				DialogUtil.setLoadingState(RegisterLoginActivity.this, false);

				showToast(getString(R.string.send_verify_code_failed));

				break;
			case MSG_SEND_VERIFY_CODE_BAD_NETWORK:
				DialogUtil.setLoadingState(RegisterLoginActivity.this, false);
				showToast(getString(R.string.bad_network));
				break;
			case MSG_LOGIN_START:
				UserInfoModel userInfoModel = UserInfoModel
						.getInstance(RegisterLoginActivity.this);
				userInfoModel.login(handler, edit_phone.getText().toString());
				break;
			case MSG_TIMER:
				timerCounter--;

				String timerString = timerCounter
						+ getString(R.string.get_verification_code_again);
				btn_getCode.setText(timerString);

				if (timerCounter <= 0) {
					stopVerifyCodeButtonTimer();
				}

			}
		}
	};

	private void startVerifyCodeButtonTimer() {

		// change the button
		btn_getCode.setEnabled(false);
		btn_getCode.setBackgroundResource(R.drawable.btn_vcode_again);

		timerCounter = TIMER_COUNTER_MAX;
		verifyCodeTimer = new Timer(true);
		verifyCodeTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				handler.sendEmptyMessage(MSG_TIMER);
			}
		}, 0, TIMER_PERIOD);
	}

	private void stopVerifyCodeButtonTimer() {
		// change the button
		btn_getCode.setEnabled(true);
		btn_getCode.setBackgroundResource(R.drawable.btn_vcode);

		btn_getCode.setText(R.string.get_verification_code);
		verifyCodeTimer.cancel();
	}

	private void showInputTips() {
		showToast(getString(R.string.register_input_tips));
	}

	private void showPhoneTips() {
		showToast(getString(R.string.register_phone_tips));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.view.View.OnFocusChangeListener#onFocusChange(android.view.View,
	 * boolean)
	 */
	@Override
	public void onFocusChange(View arg0, boolean arg1) {
		// TODO Auto-generated method stub

		if (arg0.getId() == edit_phone.getId()) {
			if (!arg1) {
				Matcher m = phonePattern.matcher(edit_phone.getText()
						.toString());
				if (!m.matches()) {
					showPhoneTips();
				}

			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			setResult(RESULT_OK, data);
		}
		finish();
		super.onActivityResult(requestCode, resultCode, data);
	}

}
