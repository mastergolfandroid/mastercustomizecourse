package com.teewell.samsunggolfmate.activities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;
import com.teewell.samsunggolfmate.beans.FairwayObject;
import com.teewell.samsunggolfmate.beans.GolfObstadeOrHole;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.RoadblockObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.utils.ComparatorRoadblock;
import com.teewell.samsunggolfmate.utils.DistanceUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.views.ObstadeAdapter;

public class RoadblockActivity extends ParentActivity {
	private static final String TAG = RoadblockActivity.class.getName();
	private ListView listView;
	private List<FairwayObject> fairwayList;
	private FairwayObject holesBean;
	private List<GolfObstadeOrHole> holeVoList;
	private TextView dis_par;
	private TextView dis_center_len;
	private Button btn_green;
	private ObstadeAdapter obstadeAdapter;
	public static final String ACTION_BROADCASTRECEIVER = "com.teewell.golfmate.activities.RoadblockActivity.RoadblockBroadcastReceiver";
	private int distanceIndex;
	private String distanceUnit;
	private LocationUtil location;
	private List<FairwayObject> holesList;
	private List<AerialPhotoBean> aerialPhotoList;

	private static final int WHAT_ROADBLOCK = 0x10;
	private int greenNumber;
	private PlayGameObject playGameObject;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.obstade);
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
		findView();
		obstadeAdapter = new ObstadeAdapter(this);
		listView.setAdapter(obstadeAdapter);

		location = LocationUtil.getInstance(this.getApplicationContext());

		initData();
	}

	private void findView() {
		listView = (ListView) findViewById(R.id.obstade_list);
		dis_par = (TextView) findViewById(R.id.dis_par);
		dis_center_len = (TextView) findViewById(R.id.dis_center_len);
		btn_green = (Button) findViewById(R.id.btn_green);
		btn_green.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (greenNumber == 0) {
					greenNumber = 1;
				} else {
					greenNumber = 0;
				}
				refreshData(greenNumber);
			}
		});
	}

	private boolean isPersonPosition() {
		LocationUtil util = LocationUtil.getInstance(this
				.getApplicationContext());

		return (DistanceUtil.distanceBetween(util.latitude, util.longitude,
				Double.parseDouble(holesBean.getGreenLongitude()),
				Double.parseDouble(holesBean.getGreenLatitude())) < 1000) ? true
				: false;
	}

	private void initData() {
		holesList = PlayGameModel.getInstance(this).getFairwayObjectArray(playGameObject);
		aerialPhotoList = playGameObject.getAerialPhotoBeanArray();
	}

	private void refreshData(int greenNumber) {
		AerialPhotoBean aerialPhotoBean = aerialPhotoList
				.get(PlayGameModel.curHoleNumber);
		fairwayList = new ArrayList<FairwayObject>();
		for (FairwayObject bean : holesList) {
			if (bean.getAerialPhotoBean().getAerialPhotoID().intValue() == aerialPhotoBean
					.getAerialPhotoID().intValue()) {
				if (fairwayList.size() > 0 && bean == fairwayList.get(0)) {
					continue;
				}
				fairwayList.add(bean);
			}
		}
		holesBean = fairwayList.get(greenNumber);
		dis_par.setText(Integer.toString(holesBean.getPoleNumber())
				+ getString(R.string.par_standard));
		if (isPersonPosition()) {
			distanceIndex = PlayGameModel.shortDistanceIndex;
			distanceUnit = DistanceUtil.getDistanceUnit(this, distanceIndex);
		} else {
			distanceIndex = PlayGameModel.longDistanceIndex;
			distanceUnit = DistanceUtil.getDistanceUnit(this, distanceIndex);
		}
		refreshRoadblock();
		obstadeAdapter.setObstadelist(holeVoList);
		obstadeAdapter.notifyDataSetChanged();

		if (fairwayList.size() > 1) {
			btn_green.setVisibility(View.VISIBLE);
		} else {
			btn_green.setVisibility(View.GONE);
		}
		btn_green.setText("果岭" + (greenNumber + 1));
	}

	public static boolean shouldDisplayObstade(float start1r, float Obstacle1) {
		boolean flag = false;
		flag = start1r > Obstacle1 ? true : false;
		return flag;
	}

	private void refreshRoadblock() {
		ArrayList<RoadblockObject> list_allRoadblockObjects = holesBean
				.getRoadblockList();
		ArrayList<RoadblockObject> list_validRoadblockObjects = new ArrayList<RoadblockObject>();
		if (holeVoList != null) {
			holeVoList.clear();
		} else {
			holeVoList = new ArrayList<GolfObstadeOrHole>();
		}
		float length_personToGreen = Float.parseFloat(DistanceUtil.getDistance(
				Double.parseDouble(holesBean.getGreenLatitude()),
				Double.parseDouble(holesBean.getGreenLongitude()),
				location.latitude, location.longitude, distanceIndex));// 人到果岭距离
		dis_center_len.setText(length_personToGreen + distanceUnit);
		for (RoadblockObject bean : list_allRoadblockObjects) {
			float length_backRoadblockToGreen = Float.parseFloat(DistanceUtil
					.getDistance(
							Double.parseDouble(holesBean.getGreenLatitude()),
							Double.parseDouble(holesBean.getGreenLongitude()),
							Double.parseDouble(bean.getBackLatitude()),
							Double.parseDouble(bean.getBackLongitude()),
							distanceIndex));// 障碍物后沿到果岭距离
			boolean isDisplay = shouldDisplayObstade(length_personToGreen,
					length_backRoadblockToGreen);
			if (!isDisplay) {
				continue;
			}
			bean.setLenth_backRoadblockToGreen(length_backRoadblockToGreen);
			list_validRoadblockObjects.add(bean);
		}
		ComparatorRoadblock comparator = new ComparatorRoadblock();
		Collections.sort(list_validRoadblockObjects, comparator);
		addGreenLenByList();
		int leftWaterCount = 0;
		int midWaterCount = 0;
		int rightWaterCount = 0;

		int leftSandCount = 0;
		int midSandCount = 0;
		int rightSandCount = 0;

		int leftWoodCount = 0;
		int rightWoodCount = 0;
		int midWoodCount = 0;

		int leftWaterIndex = 0;
		int midWaterIndex = 0;
		int rightWaterIndex = 0;

		int leftSandIndex = 1;
		int midSandIndex = 1;
		int rightSandIndex = 1;

		int leftWoodIndex = 1;
		int rightWoodIndex = 1;
		int midWoodIndex = 1;

		/* count the obstacle count about different type */
		for (int i = 0; i < list_validRoadblockObjects.size(); i++) {
			RoadblockObject bean = list_validRoadblockObjects.get(i);
			switch (bean.getRoadblockType().intValue()) {
			case RoadblockObject.ONE:
				switch (bean.getDirection()) {
				case RoadblockObject.ONE:
					leftWaterCount++;
					break;
				case RoadblockObject.TWO:
					midWaterCount++;
					break;
				case RoadblockObject.THREE:
					rightWaterCount++;
					break;
				}
				break;
			case RoadblockObject.TWO:
				switch (bean.getDirection()) {
				case RoadblockObject.ONE:
					leftSandCount++;
					break;
				case RoadblockObject.TWO:
					midSandCount++;
					break;
				case RoadblockObject.THREE:
					rightSandCount++;
					break;
				}
				break;
			case RoadblockObject.THREE:
				switch (bean.getDirection()) {
				case RoadblockObject.ONE:
					leftWoodCount++;
					break;
				case RoadblockObject.TWO:
					midWoodCount++;
					break;
				case RoadblockObject.THREE:
					rightWoodCount++;
					break;
				}
				break;
			}
		}
		for (int i = 0; i < list_validRoadblockObjects.size(); i++) {
			RoadblockObject bean = list_validRoadblockObjects.get(i);
			GolfObstadeOrHole gHoleFront = new GolfObstadeOrHole();
			String obLen = DistanceUtil
					.getDistance(location.latitude, location.longitude,
							Double.parseDouble(bean.getFrontLatitude()),
							Double.parseDouble(bean.getFrontLongitude()),
							distanceIndex);
			gHoleFront.setLen(obLen + distanceUnit);
			GolfObstadeOrHole gHoleBack = new GolfObstadeOrHole();
			obLen = DistanceUtil.getDistance(location.latitude,
					location.longitude,
					Double.parseDouble(bean.getBackLatitude()),
					Double.parseDouble(bean.getBackLongitude()), distanceIndex);
			gHoleBack.setLen(obLen + distanceUnit);
			String nameWater = getString(R.string.water);
			String nameSand = getString(R.string.sand);
			String nameWood = getString(R.string.woods);
			String nameLeft = getString(R.string.left);
			String nameMid = "";
			String nameRight = getString(R.string.right);

			String nameSplit = "";
			switch (bean.getRoadblockType()) {
			case RoadblockObject.ONE:
				if (RoadblockObject.ONE == (leftWaterCount + midWaterCount + rightWaterCount)) {
					nameSplit = nameWater;
				} else {
					switch (bean.getDirection()) {
					case RoadblockObject.ONE:
						if (leftWaterCount == RoadblockObject.ONE) {
							nameSplit = nameLeft + nameWater;
						} else {
							nameSplit = nameLeft + leftWaterIndex + nameWater;
						}
						leftWaterIndex++;
						break;
					case RoadblockObject.TWO:
						if (midWaterCount == RoadblockObject.ONE) {
							nameSplit = nameMid + nameWater;
						} else {
							nameSplit = nameMid + midWaterIndex + nameWater;
						}
						midWaterIndex++;
						break;
					case RoadblockObject.THREE:
						if (rightWaterCount == RoadblockObject.ONE) {
							nameSplit = nameRight + nameWater;
						} else {
							nameSplit = nameRight + rightWaterIndex + nameWater;
						}
						rightWaterIndex++;
						break;
					}
				}
				break;
			case RoadblockObject.TWO:
				if (RoadblockObject.ONE == (leftSandCount + midSandCount + rightSandCount)) {
					nameSplit = nameSand;
				} else {
					switch (bean.getDirection()) {
					case RoadblockObject.ONE:
						if (leftSandCount == RoadblockObject.ONE) {
							nameSplit = nameLeft + nameSand;
						} else {
							nameSplit = nameLeft + leftSandIndex + nameSand;
						}
						leftSandIndex++;
						break;
					case RoadblockObject.TWO:
						if (midSandCount == RoadblockObject.ONE) {
							nameSplit = nameMid + nameSand;
						} else {
							nameSplit = nameMid + midSandIndex + nameSand;
						}
						midSandIndex++;
						break;
					case RoadblockObject.THREE:
						if (rightSandCount == RoadblockObject.ONE) {
							nameSplit = nameRight + nameSand;
						} else {
							nameSplit = nameRight + rightSandIndex + nameSand;
						}
						rightSandIndex++;
						break;
					}
				}
				break;
			case RoadblockObject.THREE:
				if (1 == (leftWoodCount + midWoodCount + rightWoodCount)) {
					nameSplit = nameLeft + nameWood;
				} else {
					switch (bean.getDirection()) {
					case RoadblockObject.ONE:
						if (leftWoodCount == RoadblockObject.ONE) {
							nameSplit = nameLeft + nameWood;
						} else {
							nameSplit = nameLeft + leftWoodIndex + nameWood;
						}
						leftWoodIndex++;
						break;
					case RoadblockObject.TWO:
						if (midWoodCount == RoadblockObject.ONE) {
							nameSplit = nameMid + nameWood;
						} else {
							nameSplit = nameMid + midWoodIndex + nameWood;
						}
						midWoodIndex++;
						break;
					case RoadblockObject.THREE:
						if (rightWoodCount == RoadblockObject.ONE) {
							nameSplit = nameRight + nameWood;
						} else {
							nameSplit = nameRight + rightWoodIndex + nameWood;
						}
						rightWoodIndex++;
						break;
					}
				}
				break;
			}
			gHoleFront.setName(nameSplit + getString(R.string.front));
			gHoleBack.setName(nameSplit + getString(R.string.back));
			holeVoList.add(gHoleFront);
			holeVoList.add(gHoleBack);
		}
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause....");
		super.onPause();
		unregisterReceiver(RoadbackBroadcastReceiver);
	}

	@Override
	protected void onStop() {
		LocationUtil location = LocationUtil.getInstance(this
				.getApplicationContext());
		location.clearHandlerInfo();
		super.onStop();
	}

	@Override
	protected void onResume() {
		Log.d(TAG, "onResume....");
		super.onResume();
		registerBoradcastReceiver();
		location.setHandlerInfo(handler, WHAT_ROADBLOCK,
				LocationUtil.LOC_PERIOD);
		refreshData(0);
	}

	private void addGreenLenByList() {
		String greenLen = "0";
		GolfObstadeOrHole gHole = new GolfObstadeOrHole();
		gHole.setName(getString(R.string.green_front));
		greenLen = DistanceUtil.getDistance(
				Double.parseDouble(holesBean.getGreenFrontLatitude()),
				Double.parseDouble(holesBean.getGreenFrontLongitude()),
				location.latitude, location.longitude, distanceIndex);
		gHole.setLen(greenLen + distanceUnit);
		holeVoList.add(gHole);
		gHole = new GolfObstadeOrHole();
		gHole.setName(getString(R.string.green_back));
		greenLen = DistanceUtil.getDistance(
				Double.parseDouble(holesBean.getGreenBackLatitude()),
				Double.parseDouble(holesBean.getGreenBackLongitude()),
				location.latitude, location.longitude, distanceIndex);
		gHole.setLen(greenLen + distanceUnit);
		holeVoList.add(gHole);
	}

	BroadcastReceiver RoadbackBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(ACTION_BROADCASTRECEIVER)) {
				greenNumber = 0;
				refreshData(greenNumber);
			}
		}
	};

	private void registerBoradcastReceiver() {
		IntentFilter myIntentFilter = new IntentFilter();
		myIntentFilter.addAction(ACTION_BROADCASTRECEIVER);
		registerReceiver(RoadbackBroadcastReceiver, myIntentFilter);
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case WHAT_ROADBLOCK:
				refreshData(greenNumber);
				obstadeAdapter.notifyDataSetChanged();
				break;
			}
		};
	};
}
