package com.teewell.samsunggolfmate.activities;

import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.SubCoursesObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.UserInfoModel;
import com.teewell.samsunggolfmate.views.TextAdapter;

public class SelectActivity extends ParentActivity implements
		View.OnClickListener, OnItemClickListener {
	private Button btnCanael;
	private TextView title;
	private ListView listView;
	private String selectType;
	private ArrayList<String> list;
	private TextAdapter adapter;
	private PlayGameObject playGameObject;
	public static final String SELECT_TYPE = "select_type";
	public static final String SELECT_SUB_I = "sub1";
	public static final String SELECT_SUB_II = "sub2";
	public static final String SELECT_TEE = "tee";
	public static final String SELECT_HOLE = "hole";
	public static final String SELECT_SEX = "sex";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select);

		initView();
		initDate();
	}

	private void initDate() {
		selectType = getIntent().getStringExtra(SELECT_TYPE);
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
		if (selectType.equals(SELECT_TEE)) {
			list = getTeeList();
			title.setText(getString(R.string.select_T));
			adapter = new TextAdapter(this, list, playGameObject.getStartTee());
		} else if (selectType.equals(SELECT_HOLE)) {
			list = getHoleList();
			title.setText(getString(R.string.select_hole));
			adapter = new TextAdapter(this, list,
					(playGameObject.getStartHole() + 1) + "号洞");
		} else if (selectType.equals(SELECT_SEX)) {
			list = getSexList();
			title.setText(R.string.set_sex);
			String sexString = getIntent().getStringExtra(
					UserInfoActivity.TAG_SEX);
			if ("".equals(sexString)) {
				sexString = list.get(0);
			}
			adapter = new TextAdapter(this, list, sexString);
		} else if (selectType.equals(SELECT_SUB_I)) {
			list = getSubCourseNameList();
			title.setText(getString(R.string.select_eighteen_hole));
			adapter = new TextAdapter(this, list,
					playGameObject.getSubCourseNameArray()[0]);
			PlayGameModel.isChangeSubCourse = true;
		} else {
			list = getSubCourseNameList();
			title.setText(getString(R.string.select_eighteen_hole));
			adapter = new TextAdapter(this, list,
					playGameObject.getSubCourseNameArray()[1]);
			PlayGameModel.isChangeSubCourse = true;
		}

		if (selectType.equals(SELECT_SEX)) {
			btnCanael.setText(R.string.return_);
		} else {
			btnCanael.setText(playGameObject.getCoursesObject().getShortName());
			// adapter.setSelect(PlayGameModel.select);
		}
		listView.setAdapter(adapter);

	}

	private ArrayList<String> getSexList() {
		ArrayList<String> list = new ArrayList<String>();
		for (String sex : UserInfoModel.sexStrings) {
			list.add(sex);
		}
		return list;
	}

	private ArrayList<String> getHoleList() {
		ArrayList<String> holeList = new ArrayList<String>();
		for (int i = 0; i < 18; i++) {
			holeList.add(Integer.toString(i + 1) + "号洞");
		}
		return holeList;
	}

	/**
	 * 获取梯台列表
	 * 
	 * @return ArrayList<String>
	 */
	private ArrayList<String> getTeeList() {
		ArrayList<String> teeList = new ArrayList<String>();
		teeList.add("金T");
		teeList.add("墨T");
		teeList.add("蓝T");
		teeList.add("白T");
		teeList.add("红T");
		return teeList;
	}

	private ArrayList<String> getSubCourseNameList() {
		ArrayList<String> nameList = new ArrayList<String>();
		for (int i = 0; i < playGameObject.getCoursesObject().getSubCourses()
				.size(); i++) {
			int count = playGameObject.getCoursesObject().getSubCourses()
					.get(i).getFairwayCount();
			if (count >= PlayGameModel.FAIRWAY_COUNT) {
				count = PlayGameModel.FAIRWAY_COUNT;
			} else {
				count = PlayGameModel.FAIRWAY_COUNT / 2;
			}
			nameList.add(playGameObject.getCoursesObject().getSubCourses()
					.get(i).getSubCoursesName()
					+ "-" + count + "洞");
		}
		return nameList;
	}

	private void initView() {
		btnCanael = (Button) findViewById(R.id.btn_return);
		title = (TextView) findViewById(R.id.tv_title);
		listView = (ListView) findViewById(R.id.select_list);
		btnCanael.setOnClickListener(this);
		listView.setOnItemClickListener(this);
//		ImageView listfoot = new ImageView(getApplicationContext());
//		listfoot.setBackgroundResource(R.drawable.bg_footerview_backgroud);
//		listView.addFooterView(listfoot);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(this, PlaySettingActivity.class);
		if (selectType.equals(SELECT_SUB_I)) {
			SubCoursesObject subCoursesObject = playGameObject
					.getCoursesObject().getSubCourses().get(position);
			if (subCoursesObject.getFairwayCount() >= PlayGameModel.FAIRWAY_COUNT) {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObject
						.getSubCoursesName();
				playGameObject.getSubCourseIdArray()[1] = 0;
				playGameObject.getSubCourseNameArray()[1] = null;
			} else {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObject
						.getSubCoursesName();
			}
			PlayGameModel.getInstance(this).asyncParAndAerialPhotoBeans(playGameObject);
		} else if (selectType.equals(SELECT_SUB_II)) {
			SubCoursesObject subCoursesObject = playGameObject
					.getCoursesObject().getSubCourses().get(position);
			if (subCoursesObject.getFairwayCount() >= PlayGameModel.FAIRWAY_COUNT) {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObject
						.getSubCoursesName();
				playGameObject.getSubCourseIdArray()[1] = 0;
				playGameObject.getSubCourseNameArray()[1] = null;
			} else {
				playGameObject.getSubCourseIdArray()[1] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[1] = subCoursesObject
						.getSubCoursesName();
			}
			PlayGameModel.getInstance(this).asyncParAndAerialPhotoBeans(playGameObject);
		} else if (selectType.equals(SELECT_HOLE)) {
			playGameObject.setStartHole(position);
			PlayGameModel.curHoleNumber = position;
		} else if (selectType.equals(SELECT_SEX)) {
			intent.putExtra(UserInfoActivity.TAG_SEX, list.get(position));
		} else {
			playGameObject.setStartTee(list.get(position));
		}
		intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT, playGameObject);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_return:
			finish();
			break;
		}
	}

}
