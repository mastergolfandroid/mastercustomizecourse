package com.teewell.samsunggolfmate.activities;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.models.UserInfoModel;

/**
 * @Project GolfMate
 * @package com.teewell.golfmate.activities
 * @title SettingActivity.java
 * @Description TODO
 * @author Administrator
 * @date 2013-1-16 上午11:25:30
 * @Copyright Copyright(C) 2013-1-16
 * @version 1.0.0
 */
public class SettingActivity extends ParentActivity implements OnClickListener {
//	private final String TAG = SettingActivity.class.getName();
	private LinearLayout layout_login, layout_aboutOurs,
			layout_singlePersonInfo, layout_courseSetting,
			layout_company_phone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		initView();
	}

	/**
	 * 初始化组件及事件
	 */
	private void initView() {
		TextView tv_titleName = (TextView) findViewById(R.id.tv_titleName);
		tv_titleName.setText(getString(R.string.setting));
		findViewById(R.id.btn_rightTitle).setVisibility(View.GONE);
		((Button)findViewById(R.id.btn_back)).setOnClickListener(this);
		layout_login = (LinearLayout) findViewById(R.id.layout_login);
		layout_aboutOurs = (LinearLayout) findViewById(R.id.layout_aboutOurs);
		layout_courseSetting = (LinearLayout) findViewById(R.id.layout_courseSetting);
		layout_singlePersonInfo = (LinearLayout) findViewById(R.id.layout_singlePersonInfo);
		layout_company_phone = (LinearLayout) findViewById(R.id.layout_company_phone);
		layout_login.setOnClickListener(this);
		layout_aboutOurs.setOnClickListener(this);
		layout_courseSetting.setOnClickListener(this);
		layout_singlePersonInfo.setOnClickListener(this);
		layout_company_phone.setOnClickListener(this);
		
//		layout_aboutOurs.setVisibility(View.GONE);
//		layout_company_phone.setVisibility(View.GONE);
	}

	@Override
	protected void onResume() {
		/*UserInfoModel userInfoModel = UserInfoModel.getInstance(this);
		if (userInfoModel.hasLoggedIn()) {
			layout_login.setVisibility(View.GONE);
			layout_singlePersonInfo.setVisibility(View.VISIBLE);
		} else {
			layout_login.setVisibility(View.VISIBLE);
			layout_singlePersonInfo.setVisibility(View.GONE);
		}*/
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.layout_login:
			startActivity(new Intent(SettingActivity.this,
					RegisterLoginActivity.class));
			break;
		case R.id.layout_singlePersonInfo:
			startActivity(new Intent(SettingActivity.this,
					UserInfoActivity.class));
			break;
		case R.id.layout_courseSetting:
			startActivity(new Intent(SettingActivity.this,
					CourseSettingActivity.class));
			break;
		case R.id.layout_aboutOurs:
			startActivity(new Intent(SettingActivity.this,
					AboutOursActivity.class));
			break;
		case R.id.layout_company_phone:
			final Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getString(R.string.apk_title));
			dialog.setMessage(getString(R.string.make_a_phone_call)
					+ getString(R.string.service_hotline));
			dialog.setPositiveButton(getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							Intent intent_call = new Intent(
									Intent.ACTION_CALL,
									Uri.parse("tel:"
											+ getString(R.string.service_hotline)));
							startActivity(intent_call);
						}
					});
			dialog.setNegativeButton(getString(R.string.return_),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			dialog.create().show();
			break;
		}
	}
}
