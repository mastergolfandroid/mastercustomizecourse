package com.teewell.samsunggolfmate.activities;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.CameraHead;
import com.teewell.samsunggolfmate.utils.ShareUtil;

import de.nikwen.dynamicshareactionprovider.library.DynamicShareActionProvider;


public class ShareActivity extends ActionBarActivity implements OnClickListener,OnEditorActionListener{
//	private static final String TAG = "Share";

	private String filePath = null;

	private EditText shareText = null;
	// private Button endSessionBtn = null;
	private ImageView deleteImgBtn = null;
	private ImageView imageID = null;
	private TextView contentLen = null;
	private LinearLayout clearContent = null;
	private FrameLayout flPic = null;

//	private MyDataBaseAdapter db;

//	private int screenheight;
//	private int screenwidth;
	private UserScoreCardObject scoreCardObject;
	private Bitmap bitmap;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.share);
//		db = DataBaseSingle.getInstance(this).getDataBase();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
//		screenheight = dm.widthPixels;
//		screenwidth = dm.heightPixels;

		scoreCardObject = (UserScoreCardObject) getIntent()
				.getSerializableExtra(
						KeyValuesConstant.INTENT_USERSCORECARDOBJECT);
		shareText = (EditText) findViewById(R.id.shareText);
		shareText.setText("今天在@"
				+ scoreCardObject.getGeneralScoreBean().getCourse_Name()
				+ "打球,总杆数"
				+ scoreCardObject.getGeneralScoreBean().getMySelfScore() + ".@"
				+ getString(R.string.app_name));

		deleteImgBtn = (ImageView) findViewById(R.id.deleteImgBtn);
		imageID = (ImageView) findViewById(R.id.imageID);
		flPic = (FrameLayout) findViewById(R.id.flPic);
		contentLen = (TextView) findViewById(R.id.contentLen);
		clearContent = (LinearLayout) findViewById(R.id.clearContent);

		deleteImgBtn.setOnClickListener(this);
		clearContent.setOnClickListener(this);
		imageID.setOnClickListener(this);
		shareText.setOnEditorActionListener(this);
		bitmap = initPhoto();
		
	}
	 @Override  
	    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {  
	        switch(actionId){  
	        case EditorInfo.IME_ACTION_SEND:  
	            System.out.println("action send for email_content: "  + v.getText());  
	            break;  
	        case EditorInfo.IME_ACTION_DONE:  
	            System.out.println("action done for number_content: "  + v.getText());  
	            break;  
	        }  
	        //Toast.makeText(this, v.getText()+"--" + actionId, Toast.LENGTH_LONG).show();  
	        return true;  
	    }  
	private Bitmap initPhoto() {
		flPic.setVisibility(View.VISIBLE);
		filePath = Contexts.SHARE_SCORECARD_PATH_NAME;
		Bitmap rebitmap = BitmapUtil
				.readBitMap(Contexts.SHARE_SCORECARD_PATH_NAME);
		imageID.setImageBitmap(rebitmap);

		return rebitmap;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.news_menu, menu);
		DynamicShareActionProvider provider = (DynamicShareActionProvider) MenuItemCompat.getActionProvider(menu.findItem(R.id.menu_item_share));
        provider.setShareDataType(ShareUtil.TEXT_PLAIN);
        provider.setOnShareIntentUpdateListener(new DynamicShareActionProvider.OnShareIntentUpdateListener() {

            @Override
            public Bundle onShareIntentExtrasUpdate() {
            	Uri uri = CameraHead.convertBitmap2Uri(ShareActivity.this, bitmap);
            	Bundle extras = ShareUtil.SHARE_INTENT.getImageBundle(uri, shareText.getText().toString());
                return extras;
            }
        });
        //这是设置是否有返回键
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        这是设置是否有其他图标
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// 授权+分享
		case R.id.btn_rightTitle:
			if (!"".equals(shareText.getText().toString())
					|| (imageID != null && imageID.getDrawable() != null)) {
				
			}
			break;
		// 删除图片
		case R.id.deleteImgBtn: {
			final Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getString(R.string.apk_title));
			dialog.setMessage(getString(R.string.del_pic));
			dialog.setPositiveButton(getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							flPic.setVisibility(View.GONE);
							filePath = null;
							imageID.setImageBitmap(null);
							bitmap = null;
						}
					});
			dialog.setNegativeButton(getString(R.string.return_),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			dialog.create().show();
		}
			break;
		// 删除微博内容
		case R.id.clearContent: {
			final Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getString(R.string.apk_title));
			dialog.setMessage(getString(R.string.delete_all));
			dialog.setPositiveButton(getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							shareText.setText("");
							contentLen.setText("140");
							contentLen.setTextColor(R.color.text_num_gray);
						}
					});
			dialog.setNegativeButton(getString(R.string.return_),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			dialog.create().show();
		}
			break;
		// 浏览图片
		case R.id.imageID:
			Intent intent = new Intent(ShareActivity.this,
					SimplePhotoShowActivity.class);
			intent.putExtra(SimplePhotoShowActivity.EXTRA_PIC_URI, filePath);
			intent.putExtra(SimplePhotoShowActivity.IS_SCORECARD, true);
			startActivity(intent);
			break;
		}
	}


}
