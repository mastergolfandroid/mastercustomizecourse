package com.teewell.samsunggolfmate.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.ScoreBitmap;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.utils.CameraHead;
import com.teewell.samsunggolfmate.utils.ShareUtil;

import de.nikwen.dynamicshareactionprovider.library.DynamicShareActionProvider;


public class ShareCoursePhotoActivity extends  ActionBarActivity implements OnClickListener{
	private static final String TAG = ShareCoursePhotoActivity.class.getSimpleName();
	private EditText shareText = null; 
	private ImageView deleteImgBtn = null; 
	private ImageView imageID = null; 
	
	private TextView contentLen = null; 
	private LinearLayout clearContent = null; 
	private FrameLayout flPic = null;
	private String filePath;
	private PlayGameObject playGameObject;
	public static final String PHOTO_DATA = "photo_data";
	private Bitmap bitmap;
	@Override
	protected void onCreate ( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.share);
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
		shareText = (EditText) findViewById(R.id.shareText);
		shareText.setText("今天在@"+playGameObject.getCoursesObject().getCoursesName()+"@"+PlayGameModel.curHoleNumber+1+"号洞打球,风景优美.@"+getString(R.string.app_name));
		deleteImgBtn = (ImageView) findViewById(R.id.deleteImgBtn);
		imageID = (ImageView) findViewById(R.id.imageID);
		flPic = (FrameLayout) findViewById(R.id.flPic);
		contentLen = (TextView) findViewById(R.id.contentLen);
		clearContent = (LinearLayout) findViewById(R.id.clearContent);

		deleteImgBtn.setOnClickListener(this);
		clearContent.setOnClickListener(this);
		imageID.setOnClickListener(this);
		bitmap = initPhoto(null);
	}

	private Bitmap initPhoto (String name){
		
		flPic.setVisibility(View.VISIBLE);
		ArrayList<String> list_photoPaths = getIntent().getStringArrayListExtra(PHOTO_DATA);
		Bitmap bitmap = ScoreBitmap.createShareBitmap(this, list_photoPaths);
//		Bitmap bitmap = BitmapUtil.compressImage(rebitmap);
//		imageID.setImageBitmap();
		imageID.setImageBitmap(bitmap);
		filePath = Contexts.SHARE_COURSE_PATH_NAME;
//		DataOperationUtil.save2File(bitmap, filePath);
		return bitmap;
	}

	@Override
	public void onClick ( View v ){
		switch (v.getId()){
				//删除图片
			case R.id.deleteImgBtn:{
				final Builder dialog = new AlertDialog.Builder(this);
				dialog.setTitle(getString(R.string.apk_title));
				dialog.setMessage(getString(R.string.del_pic));
				dialog.setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						
						flPic.setVisibility(View.GONE);
						filePath = null;
						imageID.setImageBitmap(null);
					}
				});
				dialog.setNegativeButton(getString(R.string.return_),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dialog.create().show();
			}break;
			//删除微博内容
			case R.id.clearContent:{
				final Builder dialog = new AlertDialog.Builder(this);
				dialog.setTitle(getString(R.string.apk_title));
				dialog.setMessage(getString(R.string.delete_all));
				dialog.setPositiveButton(getString(R.string.ok),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						shareText.setText("");
						contentLen.setText("140");
						contentLen.setTextColor(R.color.text_num_gray);
					}
				});
				dialog.setNegativeButton(getString(R.string.return_),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dialog.create().show();
			}break;
			//浏览图片
			case R.id.imageID:
				Intent intent = new Intent(ShareCoursePhotoActivity.this,SimplePhotoShowActivity.class);
				intent.putExtra(SimplePhotoShowActivity.EXTRA_PIC_URI, filePath);
				intent.putExtra(SimplePhotoShowActivity.IS_SCORECARD, false);
				startActivity(intent);
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.news_menu, menu);
		DynamicShareActionProvider provider = (DynamicShareActionProvider) MenuItemCompat.getActionProvider(menu.findItem(R.id.menu_item_share));
        provider.setShareDataType(ShareUtil.TEXT_PLAIN);
        provider.setOnShareIntentUpdateListener(new DynamicShareActionProvider.OnShareIntentUpdateListener() {

            @Override
            public Bundle onShareIntentExtrasUpdate() {
            	Uri uri = CameraHead.convertBitmap2Uri(ShareCoursePhotoActivity.this, bitmap);
            	Bundle extras = ShareUtil.SHARE_INTENT.getImageBundle(uri, shareText.getText().toString());
                return extras;
            }
        });
        //这是设置是否有返回键
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        这是设置是否有其他图标
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
}
