package com.teewell.samsunggolfmate.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.SharePhotoBean;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.PhotoModel;
import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.Photos;
import com.teewell.samsunggolfmate.utils.ScreenUtil;
import com.teewell.samsunggolfmate.utils.ShareUtil;
import com.teewell.samsunggolfmate.views.SharePhotosGridViewAdapter;

import de.nikwen.dynamicshareactionprovider.library.DynamicShareActionProvider;

public class SharePhotosActivity extends ParentActivity implements
		OnClickListener {
	private Button btn_back,btn_share;
	private GridView gridView;
	private Dialog dialog;
	private SharePhotosGridViewAdapter adapter;
	private PhotoModel photoModel;
	private String photographPath = null;
	private int photoShowWidth;
	private int photoShowHeight;
	private PlayGameObject playGameObject;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sharephotos);
		initView();
		photoModel = PhotoModel.getInstance(this);
		photoShowWidth = (ScreenUtil.getScreenWidth(this) - 50) / 2;
		photoShowHeight = (ScreenUtil.getScreenHeight(this)) / 3;
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
		adapter.setLayoutParams(photoShowWidth, photoShowHeight);
		
	}

	@Override
	protected void onResume() {
		adapter.setList_data(photoModel.getList_uploadPhotoBean());
		adapter.notifyDataSetChanged();
		super.onResume();
	}

	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_share = (Button) findViewById(R.id.btn_share);
		btn_back.setOnClickListener(this);
		btn_share.setOnClickListener(this);
		gridView = (GridView) findViewById(R.id.gridView);

		gridView.setOnItemClickListener(onItemClickListener);

		adapter = new SharePhotosGridViewAdapter(this);
		gridView.setAdapter(adapter);
	}

	private OnItemClickListener onItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			if (adapter.getItem(position) == null) {
				createDialog();
			} else {

				Intent intent = new Intent(SharePhotosActivity.this,
						PhotoViewPagerActivity.class);
				intent.putExtra(PhotoViewPagerActivity.PHOTO_INDEX, position);
				intent.putStringArrayListExtra(
						PhotoViewPagerActivity.PHOTOPATHLIST,
						adapter.getPhotoPathList());
				startActivity(intent);
			}
		}
	};

	private void createDialog() {
		dialog = new AlertDialog.Builder(this).create();
		dialog.show();
		dialog.getWindow().setContentView(R.layout.dialog_select_item);
		WindowManager.LayoutParams layoutParams = dialog.getWindow()
				.getAttributes();
		layoutParams.width = ScreenUtil.getScreenWidth(this);
		dialog.getWindow().setAttributes(layoutParams);
		dialog.getWindow().setGravity(Gravity.BOTTOM);

		LayoutParams btnLayoutParams = null;
		Button btn_select1 = (Button) dialog.findViewById(R.id.btn_select1);
		btnLayoutParams = btn_select1.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		btn_select1.setLayoutParams(btnLayoutParams);

		btn_select1.setOnClickListener(this);

		Button btn_select2 = (Button) dialog.findViewById(R.id.btn_select2);
		btnLayoutParams = btn_select2.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		btn_select2.setLayoutParams(btnLayoutParams);

		btn_select2.setOnClickListener(this);

		Button btn_select3 = (Button) dialog.findViewById(R.id.btn_select3);
		btnLayoutParams = btn_select3.getLayoutParams();
		btnLayoutParams.width = ScreenUtil.getScreenWidth(this) - 100;
		btn_select3.setLayoutParams(btnLayoutParams);
		btn_select3.setOnClickListener(this);

		btn_select1.setText(getString(R.string.camera).toString());
		btn_select2.setText(getString(R.string.mapstore).toString());
		btn_select3.setText(getString(R.string.return_).toString());
		dialog.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_share:
			if (adapter.getItem(0) == null) {
				showToast("没有图片可分享");
			}else {
				Intent intent = new
				Intent(SharePhotosActivity.this,ShareCoursePhotoActivity.class);
				intent.putStringArrayListExtra(ShareCoursePhotoActivity.PHOTO_DATA,
				adapter.getPhotoPathList());
				intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
						playGameObject);	
				startActivity(intent);
			}
			break;
		case R.id.btn_select1:
			dialog.dismiss();
			photographPath = photoModel.getPhotoFullPath();
			Photos.Photograph(this, photographPath);
			break;
		case R.id.btn_select2:
			dialog.dismiss();
			Photos.Album(this);
			break;
		case R.id.btn_select3:
			dialog.dismiss();
			break;
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case Photos.PHOTOGRAPH: {
				Bitmap bitmap = BitmapUtil.getBitmapByWidthAndHeight(
						photographPath, photoShowWidth, photoShowHeight);
				if (bitmap!=null) {
					SharePhotoBean bean = new SharePhotoBean();
					bean.setBitmap(bitmap);
					bean.setBitmapPath(photographPath);
					photoModel.addList_uploadPhotoBean(bean);
					adapter.notifyDataSetChanged();
				}
			}
				break;
			case Photos.ALBLUM: {
				String photoPath = Photos.getAlbumResultPhotoPath(this, data);
				if (photoPath!=null) {
					Bitmap bitmap = BitmapUtil.getBitmapByWidthAndHeight(photoPath,
							photoShowWidth, photoShowHeight);
					SharePhotoBean bean = new SharePhotoBean();
					bean.setBitmap(bitmap);
					bean.setBitmapPath(photoPath);
					photoModel.addList_uploadPhotoBean(bean);
					adapter.notifyDataSetChanged();
				}
			}
				break;
			default: {
			}
				break;
			}
		}
	}
}