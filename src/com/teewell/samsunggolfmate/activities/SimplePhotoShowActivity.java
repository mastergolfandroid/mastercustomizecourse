/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.teewell.samsunggolfmate.activities;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;
import com.teewell.samsunggolfmate.views.photoview.PhotoViewAttacher;
import com.teewell.samsunggolfmate.views.photoview.PhotoViewAttacher.OnMatrixChangedListener;
import com.teewell.samsunggolfmate.views.photoview.PhotoViewAttacher.OnPhotoTapListener;

public class SimplePhotoShowActivity extends ParentActivity {

	static final String PHOTO_TAP_TOAST_STRING = "Photo Tap! X: %.2f %% Y:%.2f %%";

	private ImageView mImageView;

	private PhotoViewAttacher mAttacher;

	private Toast mCurrentToast;
	public static final String EXTRA_PIC_URI = "PIC_URI";
	public static final String IS_SCORECARD = "isScoreCard";

	private int screenWidth;
	private int screenHeight;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		boolean isScorecard = getIntent().getBooleanExtra(IS_SCORECARD, true);
		/**
		 * 设置为横屏
		 */
		if (isScorecard) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		screenHeight = ScreenUtil.getScreenHeight(this);
		screenWidth = ScreenUtil.getScreenWidth(this);
		setContentView(R.layout.activity_simplephotoshow);

		mImageView = (ImageView) findViewById(R.id.iv_photo);

		String filePath = getIntent().getStringExtra(EXTRA_PIC_URI);

		Bitmap bitmap = BitmapFactory.decodeFile(filePath);
		// Bitmap bitmap_image = BitmapUtil.zoomBitmap(bitmap, 2);
		Bitmap bitmap_image = BitmapUtil.zoomPhotoBitmap(bitmap, screenWidth,
				screenHeight);
		mImageView.setImageBitmap(bitmap_image);

		// The MAGIC happens here!
		mAttacher = new PhotoViewAttacher(mImageView);

		// Lets attach some listeners, not required though!
		// mAttacher.setOnMatrixChangeListener(new MatrixChangeListener());
		// mAttacher.setOnPhotoTapListener(new PhotoTapListener());
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		// Need to call clean-up
		mAttacher.cleanup();
	}

	@SuppressWarnings("unused")
	private class PhotoTapListener implements OnPhotoTapListener {

		@Override
		public void onPhotoTap(View view, float x, float y) {
			float xPercentage = x * 100f;
			float yPercentage = y * 100f;

			if (null != mCurrentToast) {
				mCurrentToast.cancel();
			}

			mCurrentToast = Toast.makeText(SimplePhotoShowActivity.this, String
					.format(PHOTO_TAP_TOAST_STRING, xPercentage, yPercentage),
					Toast.LENGTH_SHORT);
			mCurrentToast.show();
		}
	}

	@SuppressWarnings("unused")
	private class MatrixChangeListener implements OnMatrixChangedListener {

		@Override
		public void onMatrixChanged(RectF rect) {
			// mCurrMatrixTv.setText(rect.toString());
		}
	}

}
