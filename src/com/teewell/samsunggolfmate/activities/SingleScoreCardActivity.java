package com.teewell.samsunggolfmate.activities;

import java.io.File;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.GeneralScoreBean;
import com.teewell.samsunggolfmate.beans.GolferObject;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.ScoreBitmap;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.UserInfoModel;
import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;
import com.teewell.samsunggolfmate.views.ScoreDialog;
import com.teewell.samsunggolfmate.views.ScoreListener;
import com.teewell.samsunggolfmate.views.WhellYearMonthDayHourMinuteDialog;
import com.teewell.samsunggolfmate.views.YMDListener;

public class SingleScoreCardActivity extends ParentActivity implements
		OnClickListener {
	private final String TAG = SingleScoreCardActivity.class.getSimpleName();
	private TextView tv_courseName, tv_startInfo, tv_name, tv_position,
			tv_total;
	private ImageView imageView_photo, imageView_logo;
	private TableRow tableRow_holeNumbeRow, tableRow_par, tableRow_strokes,
			tableRow_status;
	private LinearLayout layout_activity;
	// private String fromActivityString;
	private static final String[] HOLENUMBER = { "1", "2", "3", "4", "5", "6",
			"7", "8", "9", "OUT", "10", "11", "12", "13", "14", "15", "16",
			"17", "18", "IN", "TOT" };
	private static final String[] LABEL = { "Hole", "Par", "Strokes", "To Par" };

	private int scoreArray[] = new int[21];
	private int pushRodArray[] = new int[21];
	private int parArray[] = new int[21];
	private String toParArray[] = new String[21];
	private int baseLandspaceWidth;
	private int baseLandspaceHeight;
	private int labelLandspaceWidth;

	private UserScoreCardObject scoreBean;
	private GeneralScoreBean generalScoreBean;
	private PlayGameObject playGameObject;
	private LayoutInflater layoutInflater;

	private String[][] select1 = { { "一杆进洞" }, { "一杆进洞", "博蒂" },
			{ "一杆进洞", "老鹰", "博蒂" }, { "一杆进洞", "信天翁", "老鹰", "博蒂" },
			{ "一杆进洞", "秃鹰", "信天翁", "老鹰", "博蒂" } };
	private String[] select2 = { "标准杆", "柏忌", "2柏忌", "3柏忌", "4柏忌", "5柏忌",
			"6柏忌", "7柏忌", "8柏忌", "9柏忌", "10柏忌", "11柏忌", "12柏忌", "13柏忌", "14柏忌",
			"15柏忌", "16柏忌", "17柏忌" };
	private String[] select = { "0", "1", "2", "3", "4", "5", "6", "7", "8",
			"9", "10", "11", "12", "13", "14", "15", "16", "17", "18" };
	private String[] select_flag = new String[18];
	private String[][] select_push = new String[18][];
	private GolferObject golferObject;
	private final int INTENT_REQUESTCODE = 0;
	private ImageView imageView_exit;
	private ImageView imageView_share;
	
	private boolean dialogShowing;
	private WhellYearMonthDayHourMinuteDialog YMDdialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 无title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_single_scorecord);
		// fromActivityString =
		// getIntent().getStringExtra(KeyValuesConstant.INTENT_FROM_ACTIVITY);
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
		
		if (playGameObject==null) {
			playGameObject = PlayGameModel.getInstance(this).getPlayGameObject();
		}
		
		
		scoreBean = playGameObject.getUserScoreCardObject();
		generalScoreBean = scoreBean.getGeneralScoreBean();
		layoutInflater = LayoutInflater.from(this);
		initView();
		int orientation = this.getResources().getConfiguration().orientation;
		Log.e(TAG, "orientation==" + orientation);
		setOrientationData(orientation);
		golferObject = UserInfoModel.getInstance(this).getGolferObject();
		initData();
		refreshData();
	}

	private void initView() {
		layout_activity = (LinearLayout) findViewById(R.id.layout_activity);
		imageView_logo = (ImageView) findViewById(R.id.image_courseLogo);
		tv_courseName = (TextView) findViewById(R.id.tv_courseName);
		tv_startInfo = (TextView) findViewById(R.id.tv_startInfo);
		tv_name = (TextView) findViewById(R.id.tv_playerName);
		tv_position = (TextView) findViewById(R.id.tv_position);
		tv_total = (TextView) findViewById(R.id.tv_total);

		ImageView imageView_editTime = (ImageView) findViewById(R.id.image_editTime);
		imageView_photo = (ImageView) findViewById(R.id.imageView_photo);
		imageView_exit = (ImageView) findViewById(R.id.image_exit);
		imageView_share = (ImageView) findViewById(R.id.image_share);
		tableRow_holeNumbeRow = (TableRow) findViewById(R.id.tableRow_holeNumbeRow);
		tableRow_par = (TableRow) findViewById(R.id.tableRow_par);
		tableRow_strokes = (TableRow) findViewById(R.id.tableRow_strokes);
		tableRow_status = (TableRow) findViewById(R.id.tableRow_status);

		imageView_photo.setOnClickListener(this);
		imageView_editTime.setOnClickListener(this);
		imageView_exit.setOnClickListener(this);
		imageView_share.setOnClickListener(this);
	}

	private void initData() {
		CoursesObject coursesObject = playGameObject.getCoursesObject();
		String logoName = coursesObject.getImages();
		if (logoName != null && !"".equals(logoName)) {
			File file = new File(Contexts.LOGO_PATH + logoName);
			if (file.exists()) {
				Bitmap logoBitmap = BitmapUtil
						.readBitMapByNative(Contexts.LOGO_PATH + logoName);
				imageView_logo.setImageBitmap(logoBitmap);
			}else {
				int logoID[] = {R.drawable.logo,R.drawable.club_26,R.drawable.club_28,R.drawable.club_59};
				int logoId = logoID[0];
				/*switch (coursesObject.getCouseID()) {
				case 26:
					logoId = logoID[1];
					break;
				case 28:
					logoId = logoID[2];
					break;
				case 59:
					logoId = logoID[3];
					break;
				default:
					logoId = logoID[0];
					break;
				}*/
				Drawable logo = getResources().getDrawable(logoId);
				if (logo!=null) {
					imageView_logo.setImageDrawable(logo);
				}
			}
		}

		tv_courseName.setText(coursesObject.getCoursesName());
		tv_startInfo.setText(playGameObject.getUserScoreCardObject()
				.getSaveTime()
				+ " ("
				+ playGameObject.getStartTee()
				+ ","
				+ playGameObject.getStartHole() + "号洞出发)");
		tv_total.setText(scoreBean.getGeneralScoreBean().getMySelfScore() + "");

		tv_name.setText(golferObject.getUserInfoObject().getUserName());
		String photoPath = golferObject.getPhotoPath();
		if (photoPath != null) {
			imageView_photo.setImageBitmap(BitmapUtil
					.readBitMapByNative(photoPath));
		} else {
			imageView_photo.setImageResource(R.drawable.defaultperson);
		}

		for (int i = 0; i < select_push.length; i++) {
			String[] push = new String[i + 1];
			for (int j = 0; j <= i; j++) {
				push[j] = select[j];
			}
			select_push[i] = push;
		}
	}

	private void refreshData() {
		int[] par = generalScoreBean.getPars();
		int count = 0;
		for (int i = 0; i < parArray.length; i++) {
			if (i < 9) {
				parArray[i] = par[i];
				count += parArray[i];
			} else if (i == 9) {
				parArray[i] = count;
			} else if (i < 19) {
				parArray[i] = par[i - 1];
				count += parArray[i];
			} else if (i == 19) {
				parArray[i] = count - parArray[9];
			} else {
				parArray[i] = count;
			}
		}
		scoreArray = new int[21];
		pushRodArray = new int[21];
		int scoreCount = 0;
		for (int i = 0; i < generalScoreBean.getScores().length; i++) {
			if (i < 9) {
				scoreArray[i] = generalScoreBean.getScores()[i];
				pushRodArray[i] = generalScoreBean.getPushRods()[i];
			} else {
				scoreArray[i + 1] = generalScoreBean.getScores()[i];
				pushRodArray[i + 1] = generalScoreBean.getPushRods()[i];
			}
			scoreCount += scoreArray[i];
		}
		generalScoreBean.setMySelfScore(scoreCount);
		toParArray = new String[21];

		int poorCount = 0;
		int outValues = 0;
		int inValues = 0;
		for (int i = 0; i < scoreArray.length; i++) {
			if (i < 9) {
				if (scoreArray[i] == 0) {
					toParArray[i] = "";
				} else {
					outValues += scoreArray[i];
					int poor = scoreArray[i] - parArray[i];
					poorCount += poor;
					toParArray[i] = String.valueOf(poorCount);
				}
			} else if (i == 9 || i == 19) {
				toParArray[i] = "";
				if (i == 9) {
					scoreArray[9] = outValues;
				} else {
					scoreArray[19] = inValues;
				}
			} else if (i == 20) {
				if (poorCount == 0) {
					toParArray[20] = "";
				} else {
					toParArray[20] = String.valueOf(poorCount);
				}
				scoreArray[20] = outValues + inValues;
			} else {
				if (scoreArray[i] == 0) {
					toParArray[i] = "";
				} else {
					inValues += scoreArray[i];
					int poor = scoreArray[i] - parArray[i];
					poorCount += poor;
					toParArray[i] = String.valueOf(poorCount);
				}
			}
		}
		tv_position.setText(scoreArray[20] + "");
		tv_total.setText(toParArray[20]);
		drawLandscapeScoreView(22, HOLENUMBER, parArray, scoreArray,
				pushRodArray, toParArray);
	}

	private void drawLandscapeScoreView(int columns, String holeNumber[],
			int pars[], int scores[], int pushRods[], String toPars[]) {
		for (int i = 0; i < 4; i++) {
			TableRow tableRow = null;
			switch (i) {
			case 0:
				tableRow = tableRow_holeNumbeRow;
				tableRow.removeAllViews();
				for (int j = 0; j < columns; j++) {
					TextView textView = new TextView(this);
					if (j % 2 == 0) {
						textView.setBackgroundResource(R.color.backColor_R);
					} else {
						textView.setBackgroundResource(R.color.backSepColor_R);
					}
					textView.setLayoutParams(new TableRow.LayoutParams(
							baseLandspaceWidth * 2, baseLandspaceHeight));
					if (j == 0) {
						TextView textView1 = new TextView(this);
						textView1.setLayoutParams(new TableRow.LayoutParams(
								labelLandspaceWidth, baseLandspaceHeight));
						textView1.setText(LABEL[i] + "");
						textView1.setTextColor(Color.WHITE);
						textView1.setGravity(Gravity.CENTER);
						textView1.setBackgroundResource(R.color.backColor_R);
						tableRow.addView(textView1);
						continue;
					} else if (j == 10 || j == 20) {
						textView.setLayoutParams(new TableRow.LayoutParams(
								3 * baseLandspaceWidth, baseLandspaceHeight));
						textView.setText(holeNumber[j - 1]);
					} else if (j == 21) {
						// textView.setLayoutParams(new
						// TableRow.LayoutParams(4*baseLandspaceWidth,
						// baseLandspaceHeight));
						textView.setLayoutParams(new TableRow.LayoutParams(
								LayoutParams.WRAP_CONTENT, baseLandspaceHeight,
								1));
						textView.setText(holeNumber[j - 1]);
					} else {

						textView.setText(holeNumber[j - 1]);
					}
					textView.setTextColor(Color.WHITE);
					textView.setGravity(Gravity.CENTER);
					tableRow.addView(textView);
				}

				break;
			case 1:
				tableRow = tableRow_par;
				tableRow.removeAllViews();
				for (int j = 0; j < columns; j++) {
					TextView textView = new TextView(this);
					if (j % 2 == 0) {
						textView.setBackgroundResource(R.color.backColor_R);
					} else {
						textView.setBackgroundResource(R.color.backSepColor_R);
					}
					textView.setLayoutParams(new TableRow.LayoutParams(
							baseLandspaceWidth * 2, baseLandspaceHeight));
					if (j == 0) {
						TextView textView1 = new TextView(this);
						textView1.setLayoutParams(new TableRow.LayoutParams(
								labelLandspaceWidth, baseLandspaceHeight));
						textView1.setText(LABEL[i] + "");
						textView1.setTextColor(Color.WHITE);
						textView1.setGravity(Gravity.CENTER);
						textView1.setBackgroundResource(R.color.backSepColor_R);
						tableRow.addView(textView1);
						continue;
					} else if (j == 10 || j == 20) {
						textView.setLayoutParams(new TableRow.LayoutParams(
								3 * baseLandspaceWidth, baseLandspaceHeight));
						textView.setText(String.valueOf(pars[j - 1]));
					} else if (j == 21) {
						// textView.setLayoutParams(new
						// TableRow.LayoutParams(4*baseLandspaceWidth,
						// baseLandspaceHeight));
						textView.setLayoutParams(new TableRow.LayoutParams(
								LayoutParams.WRAP_CONTENT, baseLandspaceHeight,
								1));
						textView.setText(String.valueOf(pars[j - 1]));
					} else {

						textView.setText(String.valueOf(pars[j - 1]));
					}
					textView.setTextColor(Color.WHITE);
					textView.setGravity(Gravity.CENTER);
					tableRow.addView(textView);
				}
				break;
			case 2:
				tableRow = tableRow_strokes;
				tableRow.removeAllViews();
				for (int j = 0; j < columns; j++) {

					if (j == 0) {
						TextView textView1 = new TextView(this);
						textView1.setLayoutParams(new TableRow.LayoutParams(
								labelLandspaceWidth,
								baseLandspaceHeight * 4 / 3));
						textView1.setText(LABEL[i] + "");
						textView1.setTextColor(Color.WHITE);
						textView1.setGravity(Gravity.CENTER);
						textView1.setBackgroundResource(R.color.backColor_R);
						tableRow.addView(textView1);
						continue;
					} else if (j == 10 || j == 20) {
						TextView textView = new TextView(this);
						textView.setTextColor(Color.BLACK);
						textView.setGravity(Gravity.CENTER);
						textView.setBackgroundResource(R.color.frontSepColor_R);
						textView.setLayoutParams(new TableRow.LayoutParams(
								3 * baseLandspaceWidth,
								baseLandspaceHeight * 4 / 3));
						if (scores[j - 1] != 0) {
							textView.setText(String.valueOf(scores[j - 1]));
						}
						tableRow.addView(textView);
						continue;
					} else if (j == 21) {
						TextView textView = new TextView(this);
						textView.setTextColor(Color.BLACK);
						textView.setBackgroundResource(R.color.frontColor_R);
						textView.setGravity(Gravity.CENTER);
						textView.setLayoutParams(new TableRow.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								baseLandspaceHeight * 4 / 3, 1));
						if (scores[j - 1] != 0) {
							textView.setText(String.valueOf(scores[j - 1]));
						}
						tableRow.addView(textView);
						continue;
					} else {
						final View view = layoutInflater.inflate(
								R.layout.layout_record_score, null);
						view.setLayoutParams(new TableRow.LayoutParams(
								baseLandspaceWidth * 2,
								baseLandspaceHeight * 4 / 3));
						if (j % 2 == 0) {
							view.setBackgroundResource(R.color.frontSepColor_R);
						} else {
							view.setBackgroundResource(R.color.frontColor_R);
						}
						int values = scores[j - 1];
						if (values > 0) {
							TextView tv_score = (TextView) view
									.findViewById(R.id.tv_score);
							TextView tv_pushRod = (TextView) view
									.findViewById(R.id.tv_pushRod);
							tv_score.setText(String.valueOf(values));
							tv_pushRod.setText(String.valueOf(pushRods[j - 1]));
							values -= pars[j - 1];
							if (values < -1) {
								view.setBackgroundResource(R.color.eagleColor_R);
							} else if (values == -1) {
								view.setBackgroundResource(R.color.birdieColor_R);
							} else if (values == 0) {
								view.setBackgroundResource(R.color.parColor_R);
							} else if (values == 1) {
								view.setBackgroundResource(R.color.bogeyColor_R);
							} else {
								view.setBackgroundResource(R.color.bogey2Color_R);

							}
						}
						view.setOnClickListener(new ViewOnClickListener(j - 1));
						view.setOnTouchListener(new OnTouchListener() {
							@Override
							public boolean onTouch(View v, MotionEvent event) {
								view.setBackgroundResource(R.color.listView_item_click);
								return false;
							}
						});
						tableRow.addView(view);
					}

				}
				break;
			case 3:
				tableRow = tableRow_status;
				tableRow.removeAllViews();
				for (int j = 0; j < columns; j++) {
					TextView textView = new TextView(this);
					textView.setBackgroundResource(R.color.white);
					textView.setLayoutParams(new TableRow.LayoutParams(
							baseLandspaceWidth * 2, baseLandspaceHeight));
					if (j % 2 == 0) {
						textView.setBackgroundResource(R.color.frontSepColor_R);
					} else {
						textView.setBackgroundResource(R.color.frontColor_R);
					}
					if (j == 0) {
						TextView textView1 = new TextView(this);
						textView1.setLayoutParams(new TableRow.LayoutParams(
								labelLandspaceWidth, baseLandspaceHeight));
						textView1.setText(LABEL[i] + "");
						textView1.setTextColor(Color.WHITE);
						textView1.setGravity(Gravity.CENTER);
						textView1.setBackgroundResource(R.color.backSepColor_R);
						tableRow.addView(textView1);
						continue;
					} else if (j == 10 || j == 20) {
						textView.setLayoutParams(new TableRow.LayoutParams(
								3 * baseLandspaceWidth, baseLandspaceHeight));
						String toPar = toPars[j - 1];
						if ("0".equals(toPar)) {
							toPar = "E";
						} else if (toPar != null && !"".equals(toPar)) {
							if (Integer.valueOf(toPar).intValue() > 0) {
								toPar = "+" + toPar;
							}
						}
						textView.setText(toPar);
					} else if (j == 21) {
						textView.setLayoutParams(new TableRow.LayoutParams(
								LayoutParams.WRAP_CONTENT, baseLandspaceHeight,
								1));
						String toPar = toPars[j - 1];
						if ("0".equals(toPar)) {
							toPar = "E";
						} else if (toPar != null && !"".equals(toPar)) {
							if (Integer.valueOf(toPar).intValue() > 0) {
								toPar = "+" + toPar;
							}
						}
						textView.setText(toPar);
					} else {
						String toPar = toPars[j - 1];
						if ("0".equals(toPar)) {
							toPar = "E";
						} else if (toPar != null && !"".equals(toPar)) {
							if (Integer.valueOf(toPar).intValue() > 0) {
								toPar = "+" + toPar;
							}
						}
						textView.setText(toPar);
					}
					textView.setTextColor(Color.BLACK);
					textView.setGravity(Gravity.CENTER);
					tableRow.addView(textView);
				}
				break;
			}
		}
	}

	class ViewOnClickListener implements OnClickListener {
		private int location;

		public ViewOnClickListener(int location) {
			this.location = location;
		}

		@Override
		public void onClick(View v) {
			setSelectData(location);
		}
	}

	private void setSelectData(final int holeNum) {
		if (dialogShowing) {
			return;
		}
		int poleSelect;
		int pushSelect;
		if (0 != scoreArray[holeNum]) {
			poleSelect = scoreArray[holeNum] - 1;
			pushSelect = pushRodArray[holeNum];
		} else {
			poleSelect = parArray[holeNum] - 1;
			if (poleSelect < 2) {
				pushSelect = 0;
			} else {
				pushSelect = 2;
			}
		}
		for (int i = 0; i < parArray[holeNum] - 1; i++) {
			select_flag[i] = select1[parArray[holeNum] - 2][i];
		}
		for (int i = 0; i < 19 - parArray[holeNum]; i++) {
			select_flag[i + parArray[holeNum] - 1] = select2[i];
		}
		dialogShowing = true;
		ScoreDialog dialog = new ScoreDialog(this, new ScoreListener() {
			@Override
			public void refreshData(int score, int pushRod) {
				int index = holeNum;
				if (index > 9) {
					index -= 1;
				}
				generalScoreBean.getScores()[index] = score;
				generalScoreBean.getPushRods()[index] = pushRod;
				
			}
		});
		dialog.doView(select_flag, select_push, poleSelect,
				pushSelect,holeNum);
		dialog.show();
		dialog.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				Log.i(TAG, "onDismiss");
				SingleScoreCardActivity.this.refreshData();
				dialogShowing = false;
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imageView_photo: {
			Intent intent = new Intent(SingleScoreCardActivity.this,
					UserInfoActivity.class);
			intent.putExtra(KeyValuesConstant.INTENT_FROM_ACTIVITY,
					getString(R.string.single_scorecard));
			intent.putExtra(KeyValuesConstant.INTENT_GOLFEROBJECT, golferObject);
			startActivityForResult(intent, INTENT_REQUESTCODE);
		}
			break;
		case R.id.image_editTime:
			if(YMDdialog!=null&&YMDdialog.isShowing()){
				break;
			}
			/**
			 * listener机制，完成时间信息获取
			 */
			YMDdialog = new WhellYearMonthDayHourMinuteDialog(
					this, new YMDListener() {

						@Override
						public void refreshYMD(String YMDString) {
							if (!scoreBean.getSaveTime().equals(YMDString)) {
								scoreBean.setSaveTime(YMDString);
								tv_startInfo.setText(scoreBean.getSaveTime()
										+ " (" + generalScoreBean.getStartTee()
										+ "，" + generalScoreBean.getStartHole()
										+ "出发" + ")");
							}
						}
					});

			YMDdialog.setShowDate(scoreBean.getSaveTime());
			YMDdialog.show();
			break;
		case R.id.image_exit:
			finish();
			break;
		case R.id.image_share: {
			String photoPath = Contexts.SHARE_SCORECARD_PATH_NAME;
			imageView_exit.setVisibility(View.GONE);
			imageView_share.setVisibility(View.GONE);
			if (ScoreBitmap.viewToBitmap(layout_activity, photoPath)) {
				Intent intent = new Intent(SingleScoreCardActivity.this,
						ShareActivity.class);
				intent.putExtra(KeyValuesConstant.INTENT_USERSCORECARDOBJECT,
						playGameObject.getUserScoreCardObject());
				startActivity(intent);
			}
		}
			break;
		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		imageView_exit.setVisibility(View.VISIBLE);
		imageView_share.setVisibility(View.VISIBLE);
		super.onResume();
	}

	@Override
	protected void onPause() {
		
		PlayGameModel.getInstance(this).updateUserScoreCard(this, scoreBean);
		super.onPause();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		setOrientationData(newConfig.orientation);
	}

	private void setOrientationData(int orientationValues) {
		if (orientationValues == Configuration.ORIENTATION_LANDSCAPE) {
			// Nothing need to be done here
			Log.i(TAG, "landspace");
			int screenWidth = ScreenUtil.getScreenWidth(this);
			int screenHeight = ScreenUtil.getScreenHeight(this);
			if (screenHeight > screenWidth) {
				baseLandspaceHeight = screenWidth * 60 / 720;
				screenWidth = screenHeight;
			} else {
				baseLandspaceHeight = screenHeight * 60 / 720;
			}
			baseLandspaceWidth = screenWidth / 50;
			labelLandspaceWidth = baseLandspaceWidth * 5;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == INTENT_REQUESTCODE) {
			if (data != null) {

				golferObject = (GolferObject) data
						.getSerializableExtra(KeyValuesConstant.INTENT_GOLFEROBJECT);
			} else {
				golferObject = UserInfoModel.getInstance(this)
						.getGolferObject();
			}
			initData();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler(){
		public void handleMessage(Message msg) {
			dialogShowing = false;
		};
	};

	@Override
	public void onBackPressed() {
		Log.i(TAG, "onBackPressed-*----");
		dialogShowing = false;
		refreshData();
		super.onBackPressed();
	}
	
}
