package com.teewell.samsunggolfmate.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesInfoBean;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.course.CourseInfoModel;
import com.teewell.samsunggolfmate.utils.StringUtil;

public class SituationActivity extends Activity implements OnClickListener {
//	private final String TAG = SituationActivity.class.getName();
	private CourseInfoModel courseInfoModel;
	private TextView tv_courseInfo, tv_info, tv_tel, tv_phone,
			tv_linkman, tv_fax, tv_email, tv_url, tv_address;
	private RelativeLayout layout_courseInfo, layout_phone, layout_email,
			layout_url;
	private LinearLayout layout_situation;
	// private FrameLayout layout_info;
	private Button btn_back;
	public static final String INTENET_COURSEID = "courseID";
	public static final int WHAT_SUCCEED = 0X1;
	public static final int WHAT_FIALED = 0X2;
	public static final int WHAT_BAD_NET = 0X3;
	private CoursesInfoBean coursesInfoBean;
	private CoursesObject coursesObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.situation);
		coursesObject = (CoursesObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_COURSESOBJECT);
		courseInfoModel = CourseInfoModel.getInstance(this);
		initView();
		courseInfoModel.getCoursesInfo(handler, coursesObject.getCouseID());
	}

	private void initView() {
		tv_courseInfo = (TextView) findViewById(R.id.tv_courseInfo);
		tv_info = (TextView) findViewById(R.id.tv_info);
//		tv_postcode = (TextView) findViewById(R.id.tv_postcode);
		tv_tel = (TextView) findViewById(R.id.tv_tel);
		tv_phone = (TextView) findViewById(R.id.tv_phone);
		tv_linkman = (TextView) findViewById(R.id.tv_linkman);
		tv_fax = (TextView) findViewById(R.id.tv_fax);
		tv_email = (TextView) findViewById(R.id.tv_email);
		tv_url = (TextView) findViewById(R.id.tv_url);
		tv_address = (TextView) findViewById(R.id.tv_address);

		layout_courseInfo = (RelativeLayout) findViewById(R.id.layout_courseInfo);
		layout_courseInfo.setOnClickListener(this);
		layout_situation = (LinearLayout) findViewById(R.id.layout_situation);
		layout_situation.setOnClickListener(this);
		layout_phone = (RelativeLayout) findViewById(R.id.layout_phone);
		layout_phone.setOnClickListener(this);
		layout_email = (RelativeLayout) findViewById(R.id.layout_email);
		layout_email.setOnClickListener(this);
		layout_url = (RelativeLayout) findViewById(R.id.layout_url);
		layout_url.setOnClickListener(this);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(this);

		// layout_info = (FrameLayout) findViewById(R.id.layout_info);
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case WHAT_SUCCEED:
				coursesInfoBean = (CoursesInfoBean) msg.obj;
				if (null != coursesInfoBean) {

					tv_courseInfo.setText(StringUtil
							.emptyProcess(coursesInfoBean.getIntroduction()));
					// tv_postcode.setText(coursesInfoBean.get);
					tv_tel.setText(StringUtil.emptyProcess(coursesInfoBean
							.getTelphone()));
					tv_phone.setText(StringUtil.emptyProcess(coursesInfoBean
							.getMobilePhone()));
					tv_linkman.setText(StringUtil.emptyProcess(coursesInfoBean
							.getLinkman()));
					tv_fax.setText(StringUtil.emptyProcess(coursesInfoBean
							.getFax()));
					tv_email.setText(StringUtil.emptyProcess(coursesInfoBean
							.getEmail()));
					tv_url.setText(StringUtil.emptyProcess(coursesInfoBean
							.getWebsite()));
					tv_address.setText(StringUtil.emptyProcess(coursesInfoBean
							.getAddressInfo()));
				}
				break;
			case WHAT_FIALED:

				break;
			case WHAT_BAD_NET:

				break;
			default:
				break;
			}
		};
	};
	@Override
	public void onBackPressed() {
		if (tv_info.getVisibility()==View.VISIBLE) {
			layout_situation.setVisibility(View.VISIBLE);
			btn_back.setVisibility(View.GONE);
			tv_info.setVisibility(View.GONE);
		}
		else{
			super.onBackPressed();
		}
	};
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_courseInfo:
			layout_situation.setVisibility(View.GONE);
			// layout_info.setVisibility(View.VISIBLE);
			btn_back.setVisibility(View.VISIBLE);
			tv_info.setVisibility(View.VISIBLE);
			tv_info.setText(StringUtil.emptyProcess(coursesInfoBean
					.getIntroduction()));
			break;
		case R.id.btn_back:
			layout_situation.setVisibility(View.VISIBLE);
			btn_back.setVisibility(View.GONE);
			tv_info.setVisibility(View.GONE);
			// layout_info.setVisibility(View.GONE);
			break;
		default:
			break;
		}
	}
}
