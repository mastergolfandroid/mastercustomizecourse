package com.teewell.samsunggolfmate.activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.GeneralScoreBean;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.SubCoursesObject;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.PlayGameModel;

public class StartGameActivity extends ParentActivity implements
		OnClickListener {
	private final String TAG = StartGameActivity.class.getName();
	private Button btnBack, btnBegin, btn_courseInfo;
	private TextView tvCourseName;
	private RelativeLayout layoutSubcourse1, layoutSubcourse2, layoutHalfpace,
			layoutHole;
	private TextView tvSelect, tvSubcourse1, tvSubcourse2, tvHalfpace, tvHole;

	public static final int REQUESTCODE_SELECT = 0;

	private CoursesObject coursesObject;
	private PlayGameObject playGameObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.playerset);
		coursesObject = (CoursesObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_COURSESOBJECT);
		initView();
		initDate();
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
	}
	private void initPlayGameModel() {
		playGameObject = new PlayGameObject();
		playGameObject.setCoursesObject(coursesObject);
		List<SubCoursesObject> subCoursesObjects = coursesObject
				.getSubCourses();
		PlayGameModel.curHoleNumber = 0;
		for (SubCoursesObject subCoursesObject : subCoursesObjects) {
			if (subCoursesObject.getFairwayCount() >= PlayGameModel.FAIRWAY_COUNT) {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObject
						.getSubCoursesName();
				break;
			}
		}
		if (playGameObject.getSubCourseNameArray()[0] == null) {
			if (subCoursesObjects.size() == PlayGameModel.SUBCOURSE_COUNT) {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObjects
						.get(0).getId();
				playGameObject.getSubCourseIdArray()[1] = subCoursesObjects
						.get(0).getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObjects
						.get(0).getSubCoursesName();
				playGameObject.getSubCourseNameArray()[1] = subCoursesObjects
						.get(0).getSubCoursesName();
			} else {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObjects
						.get(0).getId();
				playGameObject.getSubCourseIdArray()[1] = subCoursesObjects
						.get(1).getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObjects
						.get(0).getSubCoursesName();
				playGameObject.getSubCourseNameArray()[1] = subCoursesObjects
						.get(1).getSubCoursesName();
			}
		}
		GeneralScoreBean generalScoreBean = new GeneralScoreBean();
		generalScoreBean.setCourseID(coursesObject.getCouseID());
		generalScoreBean.setCourse_Name(coursesObject.getCoursesName());
		generalScoreBean.setAbbreviation(coursesObject.getShortName());
		UserScoreCardObject userScoreCardObject = new UserScoreCardObject();
		userScoreCardObject.setGeneralScoreBean(generalScoreBean);
		playGameObject.setUserScoreCardObject(userScoreCardObject);
	}

	private void initDate() {

		tvCourseName.setText(coursesObject.getShortName());

		initPlayGameModel();
//		initBackButton(PlayGameModel.fromActivity);
		btnBack.setText(getString(R.string.return_).toString());

		initSetting();
	}

	private void initSetting() {
		tvHalfpace.setText(playGameObject.getStartTee());
		tvHole.setText((playGameObject.getStartHole() + 1) + "号洞");
		if (playGameObject.getSubCourseNameArray()[1] != null) {
			layoutSubcourse2.setVisibility(View.VISIBLE);
			
			tvSubcourse1.setText(playGameObject.getSubCourseNameArray()[0]);
			tvSubcourse2.setText(playGameObject.getSubCourseNameArray()[1]);
			tvSelect.setText(getString(R.string.select_frontnine_hole)
					.toString());
		} else {
			tvSelect.setText(getString(R.string.select_eighteen_hole)
					.toString());
			tvSubcourse1.setText(playGameObject.getSubCourseNameArray()[0]);
			layoutSubcourse2.setVisibility(View.GONE);
		}
		if (tvSubcourse1.getText().length() > 10) {
			tvSubcourse1.setTextSize(14);
			tvSubcourse2.setTextSize(14);
		}
		PlayGameModel.getInstance(this).asyncParAndAerialPhotoBeans(playGameObject);
	}

	private void initBackButton(int btnId) {
		switch (btnId) {
		case 0:
			btnBack.setText(getString(R.string.surrouding_courses).toString());
			break;
		case 1:
			btnBack.setText(getString(R.string.allcourses).toString());
			break;
		case 2:
			btnBack.setText(getString(R.string.mycourses).toString());
			break;
		case 3:
			btnBack.setText(getString(R.string.historyScoreCard).toString());
			break;
		case 4:
			btnBack.setText(getString(R.string.updatecourse_update).toString());
			break;
		case 5:
			btnBack.setText(getString(R.string.start_button).toString());
			break;
		}
	}

	private void initView() {
		tvCourseName = (TextView) findViewById(R.id.tv_titleName);
		layoutSubcourse1 = (RelativeLayout) findViewById(R.id.layout_subCourse1);
		layoutSubcourse2 = (RelativeLayout) findViewById(R.id.layout_subCourse2);
		tvSelect = (TextView) findViewById(R.id.tv_select);
		tvSubcourse1 = (TextView) findViewById(R.id.tv_subCourse1);
		tvSubcourse2 = (TextView) findViewById(R.id.tv_subCourse2);

		layoutHole = (RelativeLayout) findViewById(R.id.layout_hole);
		layoutHalfpace = (RelativeLayout) findViewById(R.id.layout_halfpace);
		tvHalfpace = (TextView) findViewById(R.id.tv_halfpace);
		tvHole = (TextView) findViewById(R.id.tv_hole);
		btnBegin = (Button) findViewById(R.id.btn_begin);
		btnBack = (Button) findViewById(R.id.btn_back);
		btn_courseInfo = (Button) findViewById(R.id.btn_rightTitle);
		btn_courseInfo.setText(getString(R.string.courseintro));

		btnBegin.setOnClickListener(this);
		btnBack.setOnClickListener(this);
		btn_courseInfo.setOnClickListener(this);
		layoutHalfpace.setOnClickListener(this);
		layoutHole.setOnClickListener(this);
		layoutSubcourse1.setOnClickListener(this);
		layoutSubcourse2.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_subCourse1:
			doSelect(SelectActivity.SELECT_SUB_I);
			break;
		case R.id.layout_subCourse2:
			doSelect(SelectActivity.SELECT_SUB_II);
			break;
		case R.id.layout_halfpace:
			doSelect(SelectActivity.SELECT_TEE);
			break;
		case R.id.layout_hole:
			doSelect(SelectActivity.SELECT_HOLE);
			break;
		case R.id.btn_back:
			finish();
			break;
		case R.id.btn_begin:
			startGame();
			break;
		case R.id.btn_rightTitle: {
			Intent intent = new Intent(this, CourseIntroActivity.class);
			intent.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
					playGameObject.getCoursesObject());
			startActivity(intent);
		}
			break;
		}
	}

	private void doSelect(String type) {
		Intent i = new Intent(this, SelectActivity.class);
		i.putExtra(SelectActivity.SELECT_TYPE, type);
		i.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT, playGameObject);
		startActivityForResult(i, REQUESTCODE_SELECT);

		// 通过以下方法设置动画，第一个参数设置启动另一个Activity时的动画，第二个参数设置退出另一个Activity时的动画。值为0时表示什么都不做

		// 参数为int型，即动画XML的ID

		overridePendingTransition(R.anim.fade, R.anim.hold);
	}

	private void startGame() {
		try {
			DateFormat df = new SimpleDateFormat(PlayGameModel.DATE_FORMAT);
			playGameObject.getUserScoreCardObject().setSaveTime(
					df.format(new Date()));
			playGameObject.getUserScoreCardObject().setChangeTime(
					df.format(new Date()));
			playGameObject.getUserScoreCardObject().getGeneralScoreBean()
					.setStartTee(playGameObject.getStartTee());
			playGameObject.getUserScoreCardObject().getGeneralScoreBean()
					.setStartHole((playGameObject.getStartHole() + 1) + "号洞");
			Intent intent = new Intent(this, PlayTabActivity.class);
			intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
					playGameObject);
			startActivity(intent);
			df = null;
			intent = null;
			finish();
		} catch (Throwable e) {
			Log.e(TAG, "", e);
			showToast(getString(R.string.select_t_text3));
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d(TAG, "onKeyDown.....");
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.d(TAG, "onKeyDown..... back is click");
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUESTCODE_SELECT && resultCode == RESULT_OK) {
			playGameObject = (PlayGameObject) data
					.getSerializableExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
			initSetting();
		}
	}
}
