package com.teewell.samsunggolfmate.activities;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TabHost;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.utils.ActivityManagerUtil;
import com.teewell.samsunggolfmate.utils.GpsSwitchUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;

/**
 * 功能主界面：包括周边球场、全国球场、我的球场、历史积分卡、设置
 * 
 * @Project GolfMate
 * @package com.teewell.golfmate.activities
 * @title TabHostActivity.java
 * @Description TODO
 * @author Administrator
 * @date 2013-1-15 14:32:52
 * @Copyright Copyright(C) 2013-1-15
 * @version 1.0.0
 */
public class TabHostActivity extends TabActivity implements
		OnCheckedChangeListener {
//	private final String TAG = TabHostActivity.class.getSimpleName();
	private TabHost tabHost;
	private Builder builder;
	/**
	 * tabhost标签按钮
	 */
	public static final String ONE = "surrounding";
	public static final String TWO = "country";
	public static final String THREE = "mycourses";
	public static final String FOUR = "scores";
	public static final String FIVE = "setting";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabhost);
		ActivityManagerUtil.getScreenManager().pushActivity(this);
		DataBaseSingle.getInstance(this.getApplicationContext());
//		CrashHandler crashHandler = CrashHandler.getInstance();
		// crashHandler.init(this);
		// ScreenUtil.setScreenOffTimeOut(this);
		this.tabHost = getTabHost();
		initRadios();
		setupIntent();
	}

	private void initRadios() {
		((RadioButton) findViewById(R.id.btn_surrounding))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.btn_country))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.btn_mycourses))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.btn_scores))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.btn_setting))
				.setOnCheckedChangeListener(this);
	}

	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			switch (buttonView.getId()) {
			case R.id.btn_surrounding:
				PlayGameModel.fromActivity = 0;
				this.tabHost.setCurrentTabByTag(ONE);
				break;
			case R.id.btn_country:
				PlayGameModel.fromActivity = 1;
				this.tabHost.setCurrentTabByTag(TWO);
				break;
			case R.id.btn_mycourses:
				PlayGameModel.fromActivity = 2;
				this.tabHost.setCurrentTabByTag(THREE);
				break;
			case R.id.btn_scores:
				PlayGameModel.fromActivity = 3;
				this.tabHost.setCurrentTabByTag(FOUR);
				break;
			case R.id.btn_setting:
				PlayGameModel.fromActivity = 4;
				this.tabHost.setCurrentTabByTag(FIVE);
				break;
			}
		}
	}

	private void setupIntent() {
		Intent intent_surrounding = new Intent(this, AroundActivity.class);
		Intent intent_country = new Intent(this, CountryCoursesActivity.class);
		Intent intent_mycourses = new Intent(this, MyCoursesActivity.class);
		Intent intent_scores = new Intent(this, HistoryScorecardActivity.class);
		Intent intent_setting = new Intent(this, SettingActivity.class);

		tabHost.addTab(buildTabSpec(ONE, R.string.surrouding_courses,
				R.drawable.btn_surrouding_select, intent_surrounding));

		tabHost.addTab(buildTabSpec(TWO, R.string.courselist,
				R.drawable.btn_country_select, intent_country));

		tabHost.addTab(buildTabSpec(THREE, R.string.mycourses,
				R.drawable.btn_mycourses_select, intent_mycourses));

		tabHost.addTab(buildTabSpec(FOUR, R.string.scores,
				R.drawable.btn_scores_select, intent_scores));

		tabHost.addTab(buildTabSpec(FIVE, R.string.update,
				R.drawable.btn_setting_select, intent_setting));

	}

	private void setStartIntent() {
		if (PlayGameModel.fromActivity == 4) {
			tabHost.setCurrentTabByTag(FIVE);
			((RadioButton) findViewById(R.id.btn_setting)).setChecked(true);
		} else if (PlayGameModel.fromActivity == 3) {
			tabHost.setCurrentTabByTag(FOUR);
			((RadioButton) findViewById(R.id.btn_scores)).setChecked(true);
		} else if (PlayGameModel.fromActivity == 2) {
			tabHost.setCurrentTabByTag(THREE);
			((RadioButton) findViewById(R.id.btn_mycourses)).setChecked(true);
		} else if (PlayGameModel.fromActivity == 1) {
			tabHost.setCurrentTabByTag(TWO);
			((RadioButton) findViewById(R.id.btn_country)).setChecked(true);
		} else {
			tabHost.setCurrentTabByTag(ONE);
			((RadioButton) findViewById(R.id.btn_surrounding)).setChecked(true);
		}
	}

	private TabHost.TabSpec buildTabSpec(String tag, int resLabel, int resIcon,
			final Intent content) {
		return this.tabHost
				.newTabSpec(tag)
				.setIndicator(getString(resLabel),
						getResources().getDrawable(resIcon))
				.setContent(content);
	}

	@Override
	protected void onResume() {
		super.onResume();
		setStartIntent();
	}

	/*
	 * TabHost中的返回键最好使用此方法监听返回键操作
	 */
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			this.showDialog(1);
			return false;
		}
		return super.dispatchKeyEvent(event);
	}

	protected Dialog onCreateDialog(int id) {
		if (builder == null) {
			builder = new Builder(TabHostActivity.this);
			builder.setMessage(getString(R.string.exit_system_tip));
			builder.setTitle(getString(R.string.exit_sys));
			builder.setPositiveButton(getString(R.string.exit_sure),
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							PlayGameModel.fromActivity = 0;
							LocationUtil.getInstance(
									TabHostActivity.this
											.getApplicationContext()).destroy();
							GpsSwitchUtil.restoreSwitch(TabHostActivity.this);
							// ScreenUtil.recoverScreenOffTimeOut(TabHostActivity.this);
							builder = null;
							finish();
						}
					});
			builder.setNegativeButton(getString(R.string.cancel),
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							builder = null;
						}
					});
		}
		return builder.create();
	}
}
