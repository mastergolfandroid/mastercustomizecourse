package com.teewell.samsunggolfmate.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.GolferObject;
import com.teewell.samsunggolfmate.beans.UserInfoObject;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.costants.KeyValuesConstant;
import com.teewell.samsunggolfmate.models.UserInfoModel;
import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.DialogUtil;
import com.teewell.samsunggolfmate.utils.Photos;
import com.teewell.samsunggolfmate.views.WhellYearMonthDayDialog;
import com.teewell.samsunggolfmate.views.YMDListener;

/**
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.activities
 * @title A.java
 * @Description TODO
 * @author Administrator
 * @date 2013-3-21 下午4:14:41
 * @Copyright Copyright(C) 2013-3-21
 * @version 1.0.0
 */
public class UserInfoActivity extends ParentActivity implements OnClickListener {

	private static final String TAG = UserInfoActivity.class.getName();

	public static final int MSG_GET_USER_INFO_START = 0x1;
	public static final int MSG_GET_USER_INFO_SUCCEED = 0x2;
	public static final int MSG_GET_USER_INFO_FAILED = 0x3;
	public static final int MSG_MODIFY_USER_INFO_SUCCEED = 0x4;
	public static final int MSG_MODIFY_USER_INFO_FAILED = 0x5;
	public static final int MSG_LOGINOUT_SUCCEED = 0x6;
	public static final int MSG_LOGINOUT_FAILED = 0x7;
	public static final int MSG_BAD_NETWORK = 0x8;

	public static final int MSG_RELOGIN_SUCCESSED = 0x9;

	private ImageView imageView_head;
	private TextView textView_userName;
	private EditText edit_nickName;
	private TextView textView_sex;
	private TextView textView_birthday;
	private EditText edit_otherPhone;
	private EditText edit_email;
	private EditText edit_sign;
	private Button btn_back;
	private Button btn_modifyUserInfo;
	private Button btn_loginOut;

	private UserInfoModel userInfoModel;

	private AlertDialog dialog;

	private String intent_fromActivity;

	private GolferObject golferObject;
	public static final String TAG_SEX = "sex";
	private static final int REQUEXT_CODE_SEX = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_info);

		findView();
		userInfoModel = UserInfoModel.getInstance(UserInfoActivity.this);
		intent_fromActivity = getIntent().getStringExtra(
				KeyValuesConstant.INTENT_FROM_ACTIVITY);
		btn_back.setText(intent_fromActivity);
		if (getString(R.string.single_scorecard).equals(intent_fromActivity)) {
			golferObject = (GolferObject) getIntent().getSerializableExtra(
					KeyValuesConstant.INTENT_GOLFEROBJECT);
			configUserInfo(golferObject.getUserInfoObject());
			btn_loginOut.setVisibility(View.GONE);
		} else {
			btn_back.setText(getString(R.string.setting));
			golferObject = new GolferObject();
			golferObject.setUserInfoObject(new UserInfoObject());
			handler.sendEmptyMessageDelayed(MSG_GET_USER_INFO_START, 200);
		}

		initPhoto();
	}

	private void initPhoto() {
		// TODO Auto-generated method stub
		String photoPath = golferObject.getPhotoPath();
		if (photoPath != null) {
			imageView_head.setImageBitmap(BitmapUtil
					.readBitMapByNative(photoPath));
		} else {
			imageView_head.setImageResource(R.drawable.defaultperson);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			// do something...
			cancelHandlerOperation();
			DialogUtil.setLoadingState(UserInfoActivity.this, false);
		}
		return super.onKeyDown(keyCode, event);
	}

	private void cancelHandlerOperation() {
		UserInfoModel userInfoModel = UserInfoModel.getInstance(this);
		userInfoModel.cancelHandlerOperation();
	}

	@SuppressWarnings("unused")
	private void finishWithHandler() {
		cancelHandlerOperation();
		this.finish();
	}

	/**
	 * 获取控件的句柄
	 */
	private void findView() {

		textView_userName = (TextView) findViewById(R.id.textView_userName);
		edit_nickName = (EditText) findViewById(R.id.edit_nickName);
		textView_sex = (TextView) findViewById(R.id.textView_sex);
		textView_birthday = (TextView) findViewById(R.id.textView_birthday);
		edit_otherPhone = (EditText) findViewById(R.id.edit_otherPhone);
		edit_email = (EditText) findViewById(R.id.edit_email);
		edit_sign = (EditText) findViewById(R.id.edit_sign);

		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(this);
		btn_loginOut = (Button) findViewById(R.id.btn_logout);
		btn_loginOut.setOnClickListener(this);
		btn_modifyUserInfo = (Button) findViewById(R.id.btn_modifyUserInfo);
		btn_modifyUserInfo.setOnClickListener(this);

		imageView_head = (ImageView) findViewById(R.id.imageView_head);
		imageView_head.setOnClickListener(this);

		LinearLayout layout_sex = (LinearLayout) findViewById(R.id.layout_sex);
		layout_sex.setOnClickListener(this);

		LinearLayout layout_birthday = (LinearLayout) findViewById(R.id.layout_birthday);
		layout_birthday.setOnClickListener(this);

		UserInfoModel userInfoModel = UserInfoModel.getInstance(this);
		textView_userName.setText(userInfoModel.getCurrentUserName());
	}

	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.btn_modifyUserInfo:
			
			setPhoto();
			break;
		case R.id.btn_logout:
			DialogUtil.setLoadingState(UserInfoActivity.this, true);
			userInfoModel.loginOut(handler);
			break;
		case R.id.layout_birthday:
			/**
			 * listener机制，完成时间信息获取
			 */
			WhellYearMonthDayDialog YMDdialog = new WhellYearMonthDayDialog(
					this, new YMDListener() {

						@Override
						public void refreshYMD(String YMDString) {
							// TODO Auto-generated method stub
							textView_birthday.setText(YMDString);
						}
					});
			YMDdialog.setShowDate(textView_birthday.getText().toString());
			YMDdialog.show();
			break;
		case R.id.layout_sex:
			Intent intent = new Intent(UserInfoActivity.this,
					SelectActivity.class);
			intent.putExtra(SelectActivity.SELECT_TYPE,
					SelectActivity.SELECT_SEX);
			intent.putExtra(TAG, textView_sex.getText().toString());
			startActivityForResult(intent, REQUEXT_CODE_SEX);
			break;
		case R.id.imageView_head:
			dialog = new AlertDialog.Builder(this).create();
			dialog.show();
			dialog.setContentView(R.layout.dialog_select_item);
			dialog.getWindow().setGravity(Gravity.BOTTOM);
			Button btn_select1 = (Button) dialog.findViewById(R.id.btn_select1);

			btn_select1.setOnClickListener(this);

			Button btn_select2 = (Button) dialog.findViewById(R.id.btn_select2);
			btn_select2.setOnClickListener(this);

			Button btn_select3 = (Button) dialog.findViewById(R.id.btn_select3);
			btn_select3.setOnClickListener(this);

			btn_select1.setText(getString(R.string.camera).toString());
			btn_select2.setText(getString(R.string.mapstore).toString());
			btn_select3.setText(getString(R.string.return_).toString());
			dialog.show();
			break;
		case R.id.btn_select1:
			dialog.dismiss();
			Photos.Photograph(this, Photos.PHOTOGRAPH_TEMP);
			break;
		case R.id.btn_select2:
			dialog.dismiss();
			Photos.Album(this);
			break;
		case R.id.btn_select3:
			dialog.dismiss();
			break;
		}
	}

	private UserInfoObject configUserInfo() {
		UserInfoObject object = new UserInfoObject();
		object.setUserName(edit_nickName.getText().toString());
		object.setUserSex(userInfoModel.getSexInteger(textView_sex.getText()
				.toString()));
		object.setUserBirthday(textView_birthday.getText().toString());
		object.setMobileNumber(edit_otherPhone.getText().toString());
		object.setUserEmail(edit_email.getText().toString());
		object.setUserSign(edit_sign.getText().toString());
		return object;
	}

	private void configUserInfo(UserInfoObject userInfoObject) {
		if (userInfoObject.getUserName() != null) {
			edit_nickName.setText(userInfoObject.getUserName());
		}

		if (userInfoObject.getUserSex() != null) {
			textView_sex.setText(userInfoModel.getSexString(userInfoObject
					.getUserSex().intValue()));
		}

		if (userInfoObject.getUserBirthday() != null) {
			textView_birthday.setText(userInfoObject.getUserBirthday());
		}

		if (userInfoObject.getMobileNumber() != null) {
			edit_otherPhone.setText(userInfoObject.getMobileNumber());
		}

		if (userInfoObject.getUserEmail() != null) {
			edit_email.setText(userInfoObject.getUserEmail());
		}

		if (userInfoObject.getUserSign() != null) {
			edit_sign.setText(userInfoObject.getUserSign());
		}
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {

		public void handleMessage(Message msg) {

			switch (msg.what) {
			case MSG_GET_USER_INFO_START: {
				DialogUtil.setLoadingState(UserInfoActivity.this, true);

				userInfoModel.startGetCurrentUserInfo(handler);
				break;
			}
			case MSG_GET_USER_INFO_SUCCEED: {
				// show the waitting dialog
				DialogUtil.setLoadingState(UserInfoActivity.this, false);
				UserInfoObject userInfoObject = (UserInfoObject) msg.obj;
				configUserInfo(userInfoObject);
				golferObject.setUserInfoObject(userInfoObject);
				break;
			}
			case MSG_GET_USER_INFO_FAILED: {
				// show the waitting dialog
				DialogUtil.setLoadingState(UserInfoActivity.this, false);
				showToast(getString(R.string.bad_network));
				configUserInfo(golferObject.getUserInfoObject());
				break;
			}
			case MSG_MODIFY_USER_INFO_SUCCEED:
				DialogUtil.setLoadingState(UserInfoActivity.this, false);
				showToast("修改资料成功!");

				golferObject.setUserInfoObject(configUserInfo());
				userInfoModel.saveGolferObject(golferObject);
				if (getString(R.string.single_scorecard).equals(
						intent_fromActivity)) {
					finish();
				}
				break;
			case MSG_MODIFY_USER_INFO_FAILED:
				DialogUtil.setLoadingState(UserInfoActivity.this, false);
				showToast(getString(R.string.bad_network));
				break;
			case MSG_RELOGIN_SUCCESSED:
				if (userInfoModel.hasLoggedIn()) {
					DialogUtil.setLoadingState(UserInfoActivity.this, true);
					userInfoModel.modifyUserInfo(handler, configUserInfo());
				} else {
					handler.sendEmptyMessage(MSG_MODIFY_USER_INFO_SUCCEED);
				}
				break;
			case MSG_LOGINOUT_SUCCEED:
				DialogUtil.setLoadingState(UserInfoActivity.this, false);
				finish();
				break;
			case MSG_LOGINOUT_FAILED:
				DialogUtil.setLoadingState(UserInfoActivity.this, false);
				showToast("注销失败!");
				break;
			case MSG_BAD_NETWORK:
				DialogUtil.setLoadingState(UserInfoActivity.this, false);
				showToast(getString(R.string.bad_network));
				break;
			default:
				Log.e(TAG, "invalid message id");
				break;
			}
		}
	};

	private void setPhoto() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				String photoPath = Contexts.USER_PATH
						+ userInfoModel.getCurrentUserName() + "/"
						+ userInfoModel.getCurrentUserName() + ".jpg";
				golferObject.setPhotoPath(photoPath);
				File file = new File(photoPath);
				File temp = new File(Photos.PHOTOGRAPH_TEMP);
				if (!file.getParentFile().exists()) {
					file.getParentFile().mkdirs();
				}
				try {
					FileOutputStream b = new FileOutputStream(file);
					FileInputStream in = new FileInputStream(temp);
					byte[] buf = new byte[in.available()];
					in.read(buf);
					b.write(buf);
					b.flush();
					b.close();
					in.close();
					buf=null;
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					handler.sendEmptyMessage(MSG_RELOGIN_SUCCESSED);
				}
				
			}
		}).start();
		
	}

	protected void onActivityResult(int requestCode, int resultCode,
			android.content.Intent data) {
		Log.e("requestCode", requestCode+"   "+resultCode);
		if (resultCode==0) {
			return;
		}
		if (REQUEXT_CODE_SEX == requestCode) {
			if (data != null) {
				textView_sex.setText(data.getStringExtra(TAG_SEX));
			}
		} else if (requestCode == Photos.PHOTOGRAPH
				|| requestCode == Photos.ALBLUM) {
			Photos.callbackZoom(this, requestCode, resultCode, data);
		} else if (requestCode == Photos.PHOTORESOULT) {
			if (data != null) {
				Log.d(TAG, "onActivityResult");
				Bitmap bitmap = Photos.callbackZoom(this, requestCode,
						resultCode, data);
				FileOutputStream b;
				File file = new File(Photos.PHOTOGRAPH_TEMP);
				if (!file.getParentFile().exists()) {
					file.getParentFile().mkdirs();
				}
				try {
					b = new FileOutputStream(file);
					System.out.println(bitmap+""+b);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);//
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				imageView_head.setImageBitmap(BitmapUtil.readBitMap(file));
//				setPhoto();
			}
		}

	};
}
