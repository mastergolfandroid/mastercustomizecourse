package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;

import org.json.JSONObject;

public class AerialPhotoBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int DOWNLOAD_SUCCESSED = 1;
	private Integer aerialPhotoID;
	/**
	 * 航拍图名字
	 */
	private String photoName;
	/**
	 * 航拍图左上经度
	 */
	private String luLongitude;
	/**
	 * 航拍图左手纬度
	 */
	private String luLatitude;
	/**
	 * 航拍图右下经度
	 */
	private String rdLongitude;
	/**
	 * 航拍图右下纬度
	 */
	private String rdLatitude;
	/**
	 * 航拍图存放的位置
	 */
	private String photoPath;
	/**
	 * 航拍图保存的格式
	 */
	private String photoMode;
	/**
	 * 航拍图旋转角度
	 */
	private Float rotationView;
	
	private int[] holeId = new int[2];
	
//	private List<RoadblockObject> roadblockList;
//	private List<FairwayObject> fairwayList;
	public Integer getAerialPhotoID() {
		return aerialPhotoID;
	}

	public void setAerialPhotoID(Integer aerialPhotoID) {
		this.aerialPhotoID = aerialPhotoID;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public String getLuLongitude() {
		return luLongitude;
	}

	public void setLuLongitude(String luLongitude) {
		this.luLongitude = luLongitude;
	}

	public String getLuLatitude() {
		return luLatitude;
	}

	public void setLuLatitude(String luLatitude) {
		this.luLatitude = luLatitude;
	}

	public String getRdLongitude() {
		return rdLongitude;
	}

	public void setRdLongitude(String rdLongitude) {
		this.rdLongitude = rdLongitude;
	}

	public String getRdLatitude() {
		return rdLatitude;
	}

	public void setRdLatitude(String rdLatitude) {
		this.rdLatitude = rdLatitude;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getPhotoMode() {
		return photoMode;
	}

	public void setPhotoMode(String photoMode) {
		this.photoMode = photoMode;
	}

	public Float getRotationView() {
		return rotationView;
	}

	public void setRotationView(Float rotationView) {
		this.rotationView = rotationView;
	}

//	public List<RoadblockObject> getRoadblockList() {
//		return roadblockList;
//	}
//
//	public void setRoadblockList(ArrayList<RoadblockObject> roadblockList) {
//		this.roadblockList = roadblockList;
//	}
//
//	public void setRoadblockList(List<RoadblockObject> roadblockList) {
//		this.roadblockList = roadblockList;
//	}
//
//	public List<FairwayObject> getFairwayList() {
//		return fairwayList;
//	}
//
//	public void setFairwayList(List<FairwayObject> fairwayList) {
//		this.fairwayList = fairwayList;
//	}

	public static AerialPhotoBean jsonAerialPhotoBean(JSONObject jsonObject) {
		AerialPhotoBean object = new AerialPhotoBean();
		object.setAerialPhotoID(jsonObject.optInt("aerialPhotoID"));
		object.setLuLatitude(jsonObject.optString("luLatitude"));
		object.setLuLongitude(jsonObject.optString("luLongitude"));
		object.setPhotoMode(jsonObject.optString("photoMode"));
		object.setPhotoName(jsonObject.optString("photoName"));
		object.setPhotoPath(jsonObject.optString("photoPath"));
		object.setRdLatitude(jsonObject.optString("rdLatitude"));
		object.setRdLongitude(jsonObject.optString("rdLongitude"));
		object.setRotationView((float)jsonObject.optDouble("rotationView"));
		
		return object;
	}

	public int[] getHoleId() {
		return holeId;
	}

	public void setHoleId(int[] holeId) {
		this.holeId = holeId;
	}
}
