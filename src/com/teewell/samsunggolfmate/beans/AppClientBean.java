package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;

public class AppClientBean implements Serializable{
    private static final long serialVersionUID = 1L;
    private String os;
    private String osVersion; // os�汾
    private String generatedTime;// ����ʱ��
    private String updateMemo; // ��������
    private String URL; // ������ĵ�ַ
    private String content; // ��ע
    private int level; // ���¼���  ��1-��ݸı䣬�ɸ��¡�2-��Կ�ı䣬������3-��ݸı䣬������� 4-��ݳ��׸��£���SERVER��ݽ�ȫ��ɾ��,��Կ���䣩��
    private int appVersion; // ����汾
    private int flag;
    private String key; 
    
    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag
     *            the flag to set
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getGeneratedTime() {
        return generatedTime;
    }

    public void setGeneratedTime(String generatedTime) {
        this.generatedTime = generatedTime;
    }

    public String getUpdateMemo() {
        return updateMemo;
    }

    public void setUpdateMemo(String updateMemo) {
        this.updateMemo = updateMemo;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String url) {
        URL = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(int appVersion) {
        this.appVersion = appVersion;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "[appVersion < " + this.appVersion + " >] [level <" + this.level
                + " > ] [<generatedTime " + this.generatedTime + " >] [<updateMemo " + this.updateMemo + " >]";
    }

}
