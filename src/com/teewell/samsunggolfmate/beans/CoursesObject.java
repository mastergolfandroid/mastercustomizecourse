/**
 * 
 */
package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;
import java.util.List;

import org.json.JSONObject;
/**
 * @Description
 * @Author 谭坚
 * @Title CoursesObject.java   
 * @Package cn.teewell.golf.data 
 * @Date 2012-5-8 上午9:34:14   
 * @Version 1.0  
 * Copyright: Copyright (c)  2012  
 * Company: teewell    
 */
public class CoursesObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int VALID = 1;
	private int coursesID;

	/**
	 * 球场名称
	 */
	private String coursesName;
	/**
	 * 球场简称
	 */
	private String shortName;

	/**
	 * 球场所在位置经度
	 */
	private String longitude;
	/**
	 * 球场所在位置纬度
	 */
	private String latitude;
	/*
	 * 球场版本号
	 */
	private String version = "1.0.0";
	/**
	 * 球场过期标志
	 */
	private Integer pastFlag = 0;

	/**
	 * 球场相关图片
	 */
	private String images;

	/**
	 * 记录最后修改时间
	 */
	private long modifyTime;

	/**
	 * 省份或直辖市名称
	 */
	private String provinceName;
	/**
	 * 省份或直辖市全拼
	 */
	private String provinceCode;
	/**
	 * 城市名称或直辖市区
	 */
	private String city; 

	/**
	 * 详细地址
	 */
	private String addressInfo;

	/**
	 * 
	 */
	private Float distance;
	
	private List<SubCoursesObject> subCourses;
	private String resourceURL;
	
	private Double evaluate;
	public Double getEvaluate() {
		return evaluate;
	}
	public void setEvaluate(Double evaluate) {
		this.evaluate = evaluate;
	}
	/**
	 * 返回球场id
	 * @return the id
	 */
	public int getCouseID() {
		return coursesID;
	}
	/**
	 * 设置球场id
	 * @param id
	 */
	public void setCouseID(int id) {
		this.coursesID = id;
	}
	
	public String getCoursesName() {
		return coursesName;
	}
	public void setCoursesName(String coursesName) {
		this.coursesName = coursesName;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Integer getPastFlag() {
		return pastFlag;
	}
	public void setPastFlag(Integer pastFlag) {
		this.pastFlag = pastFlag;
	}

	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}

	public long getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(long modifyTime) {
		this.modifyTime = modifyTime;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	public String getAddressInfo() {
		return addressInfo;
	}
	public void setAddressInfo(String addressInfo) {
		this.addressInfo = addressInfo;
	}
	
	public Float getDistance() {
		return distance;
	}
	public void setDistance(Float distance) {
		this.distance = distance;
	}
	
	
	public List<SubCoursesObject> getSubCourses() {
		return subCourses;
	}
	public void setSubCourses(List<SubCoursesObject> subCourses) {
		this.subCourses = subCourses;
	}
	public String getResourceURL() {
		return resourceURL;
	}
	public void setResourceURL(String resourceURL) {
		this.resourceURL = resourceURL;
	}
	public static CoursesObject getCoursesObject(JSONObject jsonObject){
		CoursesObject object = new CoursesObject();
		object.setAddressInfo(jsonObject.optString("addressInfo"));
		object.setCity(jsonObject.optString("city"));
		object.setCoursesName(jsonObject.optString("coursesName"));
		object.setCouseID(jsonObject.optInt("coursesID"));
		String[] image = jsonObject.optString("logo").split("/");
		object.setImages(image[image.length-1]);
		object.setLatitude(jsonObject.optString("latitude"));
		object.setLongitude(jsonObject.optString("longitude"));
		object.setProvinceCode(jsonObject.optString("provinceCode"));
		object.setProvinceName(jsonObject.optString("provinceName"));
		object.setModifyTime(jsonObject.optLong("modifyTime"));
		object.setPastFlag(jsonObject.optInt("pastFlag"));
		object.setShortName(jsonObject.optString("shortName"));
		object.setVersion(jsonObject.optString("version"));
		return object;
	}
}
