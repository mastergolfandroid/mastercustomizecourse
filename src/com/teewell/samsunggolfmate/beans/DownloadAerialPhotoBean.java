package com.teewell.samsunggolfmate.beans;

import java.util.List;

public class DownloadAerialPhotoBean {
	private CoursesObject coursesObject;
	private List<AerialPhotoBean> aerialPhotoBeanList;
//	private String resourceURL;
	private List<SubCoursesObject> subCourses;
	public List<AerialPhotoBean> getAerialPhotoBeanList() {
		return aerialPhotoBeanList;
	}
	public void setAerialPhotoBeanList(List<AerialPhotoBean> aerialPhotoBeanList) {
		this.aerialPhotoBeanList = aerialPhotoBeanList;
	}
	public CoursesObject getCoursesObject() {
		return coursesObject;
	}
	public void setCoursesObject(CoursesObject coursesObject) {
		this.coursesObject = coursesObject;
	}
//	public String getResourceURL() {
//		return resourceURL;
//	}
//	public void setResourceURL(String resourceURL) {
//		this.resourceURL = resourceURL;
//	}
	public List<SubCoursesObject> getSubCourses() {
		return subCourses;
	}
	public void setSubCourses(List<SubCoursesObject> subCourses) {
		this.subCourses = subCourses;
	} 
	
}
