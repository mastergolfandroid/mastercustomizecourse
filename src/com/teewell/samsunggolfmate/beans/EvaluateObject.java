/**
 * 
 */
package com.teewell.samsunggolfmate.beans;

import com.master.mastercourse.singlecourse.R;

/**  
 * @Project 用户评价实体表
 * @package cn.teewell.feedback.data
 * @title EvaluateObject.java 
 * @Description TODO
 * @author chengmingyan
 * @date 2012-9-26 下午3:10:46
 * @Copyright Copyright(C) 2012-9-26
 * @version 1.0.0
 */
public class EvaluateObject {
	public static final int[] RATINGBAR_ID = {R.drawable.rating_bg_0,R.drawable.rating_bg_1,R.drawable.rating_bg_2,
		R.drawable.rating_bg_3,R.drawable.rating_bg_4,R.drawable.rating_bg_5,R.drawable.rating_bg_6,
		R.drawable.rating_bg_7,R.drawable.rating_bg_8,R.drawable.rating_bg_9,R.drawable.rating_bg_10};
	private int id;
	private int number;
	private int courseId;
	private int evaluate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public int getEvaluate() {
		return evaluate;
	}
	public void setEvaluate(int evaluate) {
		this.evaluate = evaluate;
	}
}


