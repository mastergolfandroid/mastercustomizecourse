/**
 * 
 */
package com.teewell.samsunggolfmate.beans;
/**  
 * @Project 用户反馈信息实体表
 * @package cn.teewell.feedback.data
 * @title FeedbackInfoObject.java 
 * @Description TODO
 * @author chengmingyan
 * @date 2012-9-26 下午3:10:46
 * @Copyright Copyright(C) 2012-9-26
 * @version 1.0.0
 */
public class FeedbackInfoObject {
	private int id;
	private int userId;
	private int courseId;
	private int evaluate;
//	private UserInfoObject userInfoObject;
//	private UserObject userObject;
	private String message;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public int getEvaluate() {
		return evaluate;
	}
	public void setEvaluate(int evaluate) {
		this.evaluate = evaluate;
	}
	//	public UserInfoObject getUserInfoObject() {
//		return userInfoObject;
//	}
//	public void setUserInfoObject(UserInfoObject userInfoObject) {
//		this.userInfoObject = userInfoObject;
//	}
//	public UserObject getUserObject() {
//		return userObject;
//	}
//	public void setUserObject(UserObject userObject) {
//		this.userObject = userObject;
//	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}


