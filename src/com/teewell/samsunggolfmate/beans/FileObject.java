package com.teewell.samsunggolfmate.beans;
public class FileObject {
	private  String tag;
	
	/**fileType(1--上传；2---下载)。
	 * 
	 */
	private  Integer fileType;
	private  String  fileName;
	private  String filePath;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public Integer getFileType() {
		return fileType;
	}
	public void setFileType(Integer fileType) {
		this.fileType = fileType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
}
