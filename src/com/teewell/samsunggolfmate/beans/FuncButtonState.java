package com.teewell.samsunggolfmate.beans;

/**点击左下角按钮状态
 * @author chengmingyahn
 */
public enum FuncButtonState{
	cursor,
	addMark,
	removeMark
};
