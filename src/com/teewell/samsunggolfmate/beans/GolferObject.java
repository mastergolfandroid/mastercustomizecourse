package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;

public class GolferObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6995153745686419715L;
	private UserInfoObject userInfoObject;
	private String photoPath;
	public UserInfoObject getUserInfoObject() {
		return userInfoObject;
	}
	public void setUserInfoObject(UserInfoObject userInfoObject) {
		this.userInfoObject = userInfoObject;
	}
	public String getPhotoPath() {
		return photoPath;
	}
	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}
	
}
