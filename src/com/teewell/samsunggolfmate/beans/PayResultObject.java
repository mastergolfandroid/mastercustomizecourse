package com.teewell.samsunggolfmate.beans;

import com.teewell.samsunggolfmate.utils.MD5;

public class PayResultObject {
	private int CourseID;
	private String LeftDay;
	private String OrderId;
	private String Paycode;
	private String TradeID;
	private String OrderType;
	private String SaveTime;
	public int getCourseID() {
		return CourseID;
	}
	public void setCourseID(int courseID) {
		CourseID = courseID;
	}
	public String getLeftDay() {
		return LeftDay;
	}
	public void setLeftDay(String leftDay) {
		LeftDay = leftDay;
	}
	public String getOrderId() {
		return OrderId;
	}
	public void setOrderId(String orderId) {
		OrderId = orderId;
	}
	public String getPaycode() {
		return Paycode;
	}
	public void setPaycode(String paycode) {
		Paycode = paycode;
	}
	public String getTradeID() {
		return TradeID;
	}
	public void setTradeID(String tradeID) {
		TradeID = tradeID;
	}
	public String getOrderType() {
		return OrderType;
	}
	public void setOrderType(String orderType) {
		OrderType = orderType;
	}
	public String getSaveTime() {
		return SaveTime;
	}
	public void setSaveTime(String saveTime) {
		SaveTime = saveTime;
	}
	public static void main(String[] args) {
		System.out.println(MD5.getMD5Key("CourseID205"));
	}
}
