package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class PlayGameObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1619881728387031144L;
	private CoursesObject coursesObject;
	private int[] subCourseIdArray = new int[2];
	private String[] subCourseNameArray = new String[2];
	private String startTee = "蓝T";
//	private GolferBean golferBean;
	private int startHole;
	private UserScoreCardObject userScoreCardObject;
	private ArrayList<AerialPhotoBean> aerialPhotoBeanArray;
	public CoursesObject getCoursesObject() {
		return coursesObject;
	}
	public void setCoursesObject(CoursesObject coursesObject) {
		this.coursesObject = coursesObject;
	}
	public int[] getSubCourseIdArray() {
		return subCourseIdArray;
	}
	public void setSubCourseIdArray(int[] subCourseIdArray) {
		this.subCourseIdArray = subCourseIdArray;
	}
	public String[] getSubCourseNameArray() {
		return subCourseNameArray;
	}
	public void setSubCourseNameArray(String[] subCourseNameArray) {
		this.subCourseNameArray = subCourseNameArray;
	}
	public String getStartTee() {
		return startTee;
	}
	public void setStartTee(String startTee) {
		this.startTee = startTee;
	}
	public int getStartHole() {
		return startHole;
	}
	public void setStartHole(int startHole) {
		this.startHole = startHole;
	}
	public UserScoreCardObject getUserScoreCardObject() {
		return userScoreCardObject;
	}
	public void setUserScoreCardObject(UserScoreCardObject userScoreCardObject) {
		this.userScoreCardObject = userScoreCardObject;
	}
	public ArrayList<AerialPhotoBean> getAerialPhotoBeanArray() {
		return aerialPhotoBeanArray;
	}
	public void setAerialPhotoBeanArray(
			ArrayList<AerialPhotoBean> aerialPhotoBeanArray) {
		this.aerialPhotoBeanArray = aerialPhotoBeanArray;
	}
	
}
