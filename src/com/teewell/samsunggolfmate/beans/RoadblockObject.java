/**
 * 
 */
package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;

import org.json.JSONObject;

/**
 * @Description
 * @Author 谭坚
 * @Title RoadblockObject.java   
 * @Package cn.teewell.golf.data 
 * @Date 2012-5-21 下午5:21:12   
 * @Version 1.0  
 * Copyright: Copyright (c)  2012  
 * Company: teewell    
 */
public class RoadblockObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	
	public static int[] ROADBLOCKTYPE = {ONE,TWO,THREE};
	public static int[] DIRECTION = {ONE,TWO,THREE};
	/**
	 * 障碍物类型（1表示水障碍，2表示沙坑障碍，3表示树或其他障碍）
	 */
	private Integer roadblockType;
	/**
	 * 障碍物方向（1表示左边，2表示中间，3表示右边）
	 */
	private Integer direction;
	/**
	 * 障碍物类型（1表示水障碍，2表示沙坑障碍，3表示树或其他障碍）
	 */
	private String roadblockTypeString;
	/**
	 * 障碍物方向（1表示左边，2表示中间，3表示右边）
	 */
	private String directionString;
	/**
	 * 障碍物前沿经度
	 */
	private String frontLongitude;
	/**
	 * 障碍物前沿纬度
	 */
	private String frontLatitude;
	/**
	 * 障碍物后沿经度
	 */
	private String backLongitude;
	/**
	 * 障碍物后沿纬度
	 */
	private String backLatitude;
	/**
	 * 版本号
	 */
	private String version;
	private float length_backRoadblockToGreen;	//后沿到果岭中心距离
	
	public float getLength_backRoadblockToGreen() {
		return length_backRoadblockToGreen;
	}

	public void setLenth_backRoadblockToGreen(float length_backRoadblockToGreen) {
		this.length_backRoadblockToGreen = length_backRoadblockToGreen;
	}

	/**
	 * 返回障碍物id
	 * 
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置障碍物id
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 返回球道障碍物类型
	 * 
	 * @return
	 */
	public Integer getRoadblockType() {
		return roadblockType;
	}

	/**
	 * 设置球道障碍物类型
	 * 
	 * @param roadblockType
	 */
	public void setRoadblockType(Integer roadblockType) {
		this.roadblockType = roadblockType;
	}

	/**
	 * 返回球道障碍物方向
	 * 
	 * @return
	 */
	public Integer getDirection() {
		return direction;
	}

	/**
	 * 设置球道障碍物反向
	 * 
	 * @param direction
	 */
	public void setDirection(Integer direction) {
		this.direction = direction;
	}

	/**
	 * 返回障碍物前沿经度
	 * 
	 * @return
	 */
	public String getFrontLongitude() {
		return frontLongitude;
	}

	/**
	 * 设置球道障碍物前沿经度
	 * 
	 * @param frontLongitude
	 */
	public void setFrontLongitude(String frontLongitude) {
		this.frontLongitude = frontLongitude;
	}

	/**
	 * 返回求大障碍物前沿纬度
	 * 
	 * @return
	 */
	public String getFrontLatitude() {
		return frontLatitude;
	}

	/**
	 * 设置球道障碍物前沿纬度
	 * 
	 * @param frontLatitude
	 */
	public void setFrontLatitude(String frontLatitude) {
		this.frontLatitude = frontLatitude;
	}

	/**
	 * 返回球道障碍后延经度
	 * 
	 * @return
	 */
	public String getBackLongitude() {
		return backLongitude;
	}

	/**
	 * 设置球道障碍物后延经度
	 * 
	 * @param backLongitude
	 */
	public void setBackLongitude(String backLongitude) {
		this.backLongitude = backLongitude;
	}

	/**
	 * 返回求大障碍物后延纬度
	 * 
	 * @return
	 */
	public String getBackLatitude() {
		return backLatitude;
	}

	/**
	 * 设置球道障碍物后延纬度
	 * 
	 * @param backLatitude
	 */
	public void setBackLatitude(String backLatitude) {
		this.backLatitude = backLatitude;
	}

	/**
	 * 返回障碍物信息版本号
	 * 
	 * @return
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * 设置障碍物信息版本号
	 * 
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
	public static RoadblockObject jsonRoadblockObject(JSONObject jsonObject) {
		RoadblockObject object = new RoadblockObject();
		object.setBackLatitude(jsonObject.optString("backLatitude"));
		object.setBackLongitude(jsonObject.optString("backLongitude"));
		object.setFrontLatitude(jsonObject.optString("frontLatitude"));
		object.setFrontLongitude(jsonObject.optString("frontLongitude"));
		object.setRoadblockType(jsonObject.optInt("roadblockType"));
		object.setId(jsonObject.optInt("roadblockID"));
		object.setDirection(jsonObject.optInt("direction"));
		return object;
	}

public String getRoadblockTypeString() {
	return roadblockTypeString;
}

public void setRoadblockTypeString(String roadblockTypeString) {
	this.roadblockTypeString = roadblockTypeString;
}

public String getDirectionString() {
	return directionString;
}

public void setDirectionString(String directionString) {
	this.directionString = directionString;
}
}
