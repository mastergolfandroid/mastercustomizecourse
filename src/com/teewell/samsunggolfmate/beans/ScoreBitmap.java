package com.teewell.samsunggolfmate.beans;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.LinearLayout;

import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;

public class ScoreBitmap {
	
	public static Bitmap createShareBitmap(Context contexts,ArrayList<String> list_photoPaths){		
		Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint.setStyle(Paint.Style.STROKE);
		ArrayList<Bitmap> list_bitmaps = new ArrayList<Bitmap>();
		for (String path : list_photoPaths) {
//			File file = new File(path);
			list_bitmaps.add(BitmapUtil.getBitmapByScreenWidthOrHeight(path, ScreenUtil.getScreenWidth(contexts)/2, ScreenUtil.getScreenHeight(contexts)/3));
		}
		int maxWidth=0;
		int countHeight=0;
		for (Bitmap bitmap : list_bitmaps) {
			if (maxWidth>0) {
				if (bitmap.getWidth()>maxWidth) {
					maxWidth = bitmap.getWidth();
				}
			}else{
				maxWidth = bitmap.getWidth();
			}
			countHeight += bitmap.getHeight();
			
		}
			
		Bitmap b = Bitmap.createBitmap(maxWidth,countHeight ,Config.RGB_565);
		Canvas canvas=new Canvas(b);
		int tempHeight = 0;
		for (int i = 0; i < list_bitmaps.size(); i++) {
			canvas.drawBitmap(list_bitmaps.get(i), 0, tempHeight, mPaint); 
			tempHeight += list_bitmaps.get(i).getHeight();
			if(!list_bitmaps.get(i).isRecycled()){ 
				list_bitmaps.get(i).recycle();    
			}
		}
		return b;
	}
	public static Bitmap getBitmapWithScoreCard(Context contexts,Bitmap photoBitmap,LinearLayout  linearLayout){	
		Bitmap score_bitmap=getBitmapWithView(linearLayout);
		Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	    mPaint.setStyle(Paint.Style.STROKE);
	    Bitmap b ;
	    Bitmap reBitmap = null;
	    if (photoBitmap==null) {
        	//b=Bitmap.createBitmap(1470,290 ,Config.RGB_565);
        	b=Bitmap.createBitmap(score_bitmap.getWidth(),score_bitmap.getHeight() ,Config.RGB_565);
		}else {
			float size=(float)score_bitmap.getHeight()/(float)photoBitmap.getHeight();
			reBitmap=BitmapUtil.zoomBitmap(photoBitmap, size);			
			b=Bitmap.createBitmap(score_bitmap.getWidth()+reBitmap.getWidth(),score_bitmap.getHeight() ,Config.RGB_565);
		}
		Canvas canvas=new Canvas(b);
        //canvas.drawBitmap(bitmap, 0,0, mPaint);
        canvas.drawBitmap(score_bitmap, 0, 0, mPaint);        
        canvas.drawBitmap(reBitmap,score_bitmap.getWidth(), 0, mPaint);       
		return b;
	}
	 public static Bitmap getBitmapWithView( View view){
			view.setDrawingCacheEnabled(true); 
			view.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
	        		MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)); 
			view.layout(0, 0, view.getMeasuredWidth(),view.getMeasuredHeight());
			view.buildDrawingCache();
	        return view.getDrawingCache();     
		}
	 public static boolean viewToBitmap( View view,String photoPath){
		 boolean flag = false;
		 view.setDrawingCacheEnabled(true); 
		 view.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
				 MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)); 
		 view.layout(0, 0, view.getMeasuredWidth(),view.getMeasuredHeight());
		 view.buildDrawingCache();
		 Bitmap bitmap = view.getDrawingCache();
		 try {
			if (bitmap!=null) {
				String path = photoPath.substring(0,photoPath.lastIndexOf("/")+1);
				Log.i("photoPath", path);
				File filePath = new File(path);
				if (!filePath.exists()) {
					filePath.mkdirs();
				}else {
					filePath = new File(photoPath);
					if (filePath.exists()) {
						filePath.delete();
					}
				}
				FileOutputStream bos = new FileOutputStream(photoPath);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
				bitmap.isRecycled();
				bos.flush();
				bos.close();
				flag = true;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 return flag;
	 }
	 public static Bitmap getBitmapWithView( LinearLayout view,int screenwidth){
			view.setDrawingCacheEnabled(true); 
			view.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
	        		MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)); 
			view.layout(0, 0, screenwidth,view.getMeasuredHeight());
			view.buildDrawingCache();
	        return view.getDrawingCache();     
		}
}
