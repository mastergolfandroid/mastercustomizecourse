package com.teewell.samsunggolfmate.beans;

import java.io.Serializable;

import android.graphics.Bitmap;

public class SharePhotoBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5828510888714175974L;
	private Bitmap bitmap;
	private String bitmapPath;
	public Bitmap getBitmap() {
		return bitmap;
	}
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
	public String getBitmapPath() {
		return bitmapPath;
	}
	public void setBitmapPath(String bitmapPath) {
		this.bitmapPath = bitmapPath;
	}
}
