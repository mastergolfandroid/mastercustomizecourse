package com.teewell.samsunggolfmate.beans;

import org.json.JSONObject;

/**
 * @description 用户接口数据对象
 * @author 黄河
 * @title User.java   
 * @package cn.teewell.user.data 
 * @date 2012-4-2 下午10:51:28  
 * @version 1.0.0.1  
 * Copyright: Copyright (c)  2012    
 */
public class UserObject {
	/**
	 * 用户正常状态
	 */
	final static public int STATUS_NORMAL=1;
	/**
	 * 用户停用状态
	 */
	final static public int STATUS_STOP=0;
	/**
	 * 自动适应登录方式
	 */
	final public static char LOGIN_ANY='a';
	/**
	 * 手机登录方式
	 */
	final public static char LOGIN_MOBILE='m';
	/**
	 * 邮箱地址登录方式
	 */
	final public static char LOGIN_EMAIL='e';
	/**
	 * 用户账号登录方式
	 */
	final public static char LOGIN_USERNAME='u';
	/**
	 * 激活手机登录标志
	 */
	final static public char MOBILE='m';
	/**
	 * 激活email登录标志
	 */
	final static public char EMAIL='e';
	
	/**
	 * 网页登陆方式
	 */
	final static public String TYPE_WEB="web";
	
	private int userId;
	private String username;
	private String mobileNumber;
	private String email;
	private String nickname;
	private String name;
	private String loginMethod;
	private String dataKey;
	
	
	/**
	 * 返回用户ID
	 * @return the id
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * 设置用户Id
	 * @param id the id to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * 返回用户登录名
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * 设置用户登录名
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * 返回用户登录手机号码
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	/**
	 * 设置用户登录手机号码
	 * @param mobilenumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	/**
	 * 返回用户Email
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * 设置用户Email
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * 返回用户昵称
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * 设置用户昵称
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	/**
	 * 返回用户可显示的名称  优先级 nickname>username>email>mobileNumber
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置用户可显示的名称 优先级 nickname>username>email>mobileNumber
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 返回加密数据项
	 * @return the dataKey
	 */
	public String getDataKey() {
		return dataKey;
	}
	/**
	 * 设置加密数据项
	 * @param dataKey the dataKey to set
	 */
	public void setDataKey(String dataKey) {
		this.dataKey = dataKey;
	}
	
	/**
	 * 返回登录方式
	 * @return the loginMethod
	 */
	public String getLoginMethod() {
		return loginMethod;
	}
	/**
	 * 设置登录方式
	 * @param loginMethod the loginMethod to set
	 */
	public void setLoginMethod(String loginMethod) {
		this.loginMethod = loginMethod;
	}
	public static UserObject jsonUserObject(JSONObject jsonObject) {
		UserObject object = new UserObject();
		object.setUserId(jsonObject.optInt("userId"));
		object.setEmail(jsonObject.optString("email"));
		object.setUsername(jsonObject.optString("username"));
		object.setMobileNumber(jsonObject.optString("mobileNumber"));
		object.setNickname(jsonObject.optString("nickname"));
		object.setLoginMethod(jsonObject.optString("loginMethod"));
		object.setDataKey(jsonObject.optString("dataKey"));
		return object;
	}
}
