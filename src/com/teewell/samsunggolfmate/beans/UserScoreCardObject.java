package com.teewell.samsunggolfmate.beans;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @description 用户接口数据对象
 * @author 肖玉平
 * @title User.java   
 * @package cn.teewell.user.data 
 * @date 2012-7-1 下午10:51:28  
 * @version 1.0.0.1  
 * Copyright: Copyright (c)  2012    
 */
public class UserScoreCardObject implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * [state] 正常状态
	 */
	public static final int NORMAL_SCORECARD = 0;
	/**
	 * [state] 删除状态
	 */
	public static final int DLL_SCORECARD = -1;
	/**
	 * [state] 修改状态
	 */
	public static final int MODIFY_SCORECARD = 1;
	private Integer id;
	private Integer userID;
	private String scoreCard;
	private String photoName;
	private String saveTime;
	private String changeTime;
	private Integer version = 0;
	private Integer state = 0;		//0正常状态，-1删除状态，回传到客户端
	private GeneralScoreBean generalScoreBean;
	/**
	 * 向数据库插入数据时无需设置，其它操作时则必须设置
	 * @param id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 向数据库插入数据时无需设置，其它操作时则必须设置
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public String getScoreCard() {
		return scoreCard;
	}
	public void setScoreCard(String scoreCard) {
		this.scoreCard = scoreCard;
	}
	public String getPhotoName() {
		return photoName;
	}
	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}
	public String getSaveTime() {
		return saveTime;
	}
	public void setSaveTime(String saveTime) {
		this.saveTime = saveTime;
	}
	public String getChangeTime() {
		return changeTime;
	}
	public void setChangeTime(String changeTime) {
		this.changeTime = changeTime;
	}
	/**
	 * 只有获取数据的时候，需要设置此项，其它操作无需设置
	 * @return version
	 */
	public Integer getVersion() {
		return version;
	}
	/**
	 * 只有获取数据的时候，需要设置此项，其它操作无需设置
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}
	/**
	 * 只有获取数据的时候，需要设置此项，为了告知客户端，该数据的状态，是将数据添加到本地，或是删除本地上存在的数据
	 * @param state 0正常状态，-1删除状态
	 */
	public Integer getState() {
		return state;
	}
	/**
	 * 只有获取数据的时候，需要设置此项，为了告知客户端，该数据的状态，是将数据添加到本地，或是删除本地上存在的数据
	 * @param state 0正常状态，-1删除状态
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	public GeneralScoreBean getGeneralScoreBean() {
		return generalScoreBean;
	}
	public void setGeneralScoreBean(GeneralScoreBean generalScoreBean) {
		this.generalScoreBean = generalScoreBean;
	}
	public static UserScoreCardObject jsonUserScoreCardObject(JSONObject json){
		UserScoreCardObject object = new UserScoreCardObject();
		object.setId(json.optInt("id"));
		object.setState(json.optInt("state"));
		if (object.getState()==DLL_SCORECARD) {
			return object;
		}
//		object.setUserID(json.optInt("userID"));
		object.setScoreCard(json.optString("scoreCard"));
		object.setPhotoName(json.optString("photoName"));
		object.setSaveTime(json.optString("saveTime"));
		object.setChangeTime(json.optString("changeTime"));
		object.setVersion(json.optInt("version"));
		try {
			if (object.getScoreCard()!=null) {
				object.setGeneralScoreBean(GeneralScoreBean.jsonGeneralScoreBean(new JSONObject(object.getScoreCard())));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return object;
		
	}
}