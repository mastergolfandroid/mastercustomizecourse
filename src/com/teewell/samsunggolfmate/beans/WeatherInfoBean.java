package com.teewell.samsunggolfmate.beans;

import android.graphics.Bitmap;

/**  
 * @Project GolfMate04
 * @package com.teewell.golfmate.beans
 * @title WeatherInfobean.java 
 * @Description TODO
 * @author Administrator
 * @date 2013-5-3 上午11:38:40
 * @Copyright Copyright(C) 2013-5-3
 * @version 1.0.0
 */
public class WeatherInfoBean {
	private String city;
	private long cityid;
	private int temp;
	private String WD;//风向
	private String WS;//风力
	private String SD;
	private String WSE;
	private String time;
	private int isRadar;
	private String Radar;
	
	private String temp1;
	private String temp2;
	private String weather;
	private String img1;
	private String img2;
	private String ptime;
	
	private Bitmap bitmap;
	
	private long weatherImageTimes;
	private long weatherInfoTimes;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public long getCityid() {
		return cityid;
	}
	public void setCityid(long cityid) {
		this.cityid = cityid;
	}
	public int getTemp() {
		return temp;
	}
	public void setTemp(int temp) {
		this.temp = temp;
	}
	public String getWD() {
		return WD;
	}
	public void setWD(String wD) {
		WD = wD;
	}
	public String getWS() {
		return WS;
	}
	public void setWS(String wS) {
		WS = wS;
	}
	public String getSD() {
		return SD;
	}
	public void setSD(String sD) {
		SD = sD;
	}
	public String getWSE() {
		return WSE;
	}
	public void setWSE(String wSE) {
		WSE = wSE;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getIsRadar() {
		return isRadar;
	}
	public void setIsRadar(int isRadar) {
		this.isRadar = isRadar;
	}
	public String getRadar() {
		return Radar;
	}
	public void setRadar(String radar) {
		Radar = radar;
	}
	public String getTemp1() {
		return temp1;
	}
	public void setTemp1(String temp1) {
		this.temp1 = temp1;
	}
	public String getTemp2() {
		return temp2;
	}
	public void setTemp2(String temp2) {
		this.temp2 = temp2;
	}
	public String getWeather() {
		return weather;
	}
	public void setWeather(String weather) {
		this.weather = weather;
	}
	public String getImg1() {
		return img1;
	}
	public void setImg1(String img1) {
		this.img1 = img1;
	}
	public String getImg2() {
		return img2;
	}
	public void setImg2(String img2) {
		this.img2 = img2;
	}
	public String getPtime() {
		return ptime;
	}
	public void setPtime(String ptime) {
		this.ptime = ptime;
	}
	public Bitmap getBitmap() {
		return bitmap;
	}
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
	public long getWeatherImageTimes() {
		return weatherImageTimes;
	}
	public void setWeatherImageTimes(long uTimes) {
		this.weatherImageTimes = uTimes;
	}
	public long getWeatherInfoTimes() {
		return weatherInfoTimes;
	}
	public void setWeatherInfoTimes(long windInfoTimes) {
		this.weatherInfoTimes = windInfoTimes;
	}
	
	
}

