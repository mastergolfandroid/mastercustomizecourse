package com.teewell.samsunggolfmate.common;

import android.os.Environment;

/**
 * @Project GolfMate
 * @package com.teewell.golfmate.common
 * @title Contexts.java
 * @Description 服务器请求地址（根据模块划分的）
 * @author DarkWarlords
 * @date 2013-1-21 下午1:42:27
 * @Copyright Copyright(C) 2013-1-21
 * @version 1.0.0
 */
public class Contexts {
	/**
	 * 网络接口模块常量------start
	 * 
	 * @author lihe
	 */
	public static class ServerURL {
		public static final String SERVER_URL = "http://app.teewell.cn/term/";
		/**
		 * term 中的 iface
		 */
		public static final String MODEL_IFACE = SERVER_URL + "iface/";
		/**
		 * term 中的 dataSync
		 */
		public static final String MODEL_DATA_SYNC = SERVER_URL + "dataSync/";
		/**
		 * term 中的 feedback
		 */
		public static final String MODEL_FEEDBACK = SERVER_URL + "feedback/";
	}

	
	
	/**
	 * 网络接口模块常量------end
	 * 
	 * @author lihe
	 */
	/**
	 * 系统常量------start
	 * 
	 * @author chengmingyan
	 */
	public static final String PACKAGE_NAME = "com.master.mastercourse.singlecourse";
	/**
	 * SD卡根目录
	 */
	public static final String SD_PATH = Environment
			.getExternalStorageDirectory().getAbsoluteFile().getAbsolutePath() + "/";
	/**
	 * 手机数据库存储目录
	 */
	public static final String MOBIL_DATABASE_PATH = "/data/data/"+PACKAGE_NAME+"/databases/";
	public static final String APPLICATION_PATH = "masterCustomizeCourse/";
	public static final String WEATHER_PATH = SD_PATH + APPLICATION_PATH + "weather/";
	public static final String WEATHER_NAME = "weather";
	public static final String LOGO_PATH = SD_PATH + APPLICATION_PATH + "logo/";
	public static final String AERIALPHOTO_PATH = SD_PATH + APPLICATION_PATH + ".aerialPhoto/";
	public static final String GOLFERPHOTO_PATH = SD_PATH + APPLICATION_PATH +  ".golferPhoto/";
	public static final String USER_PATH = SD_PATH +  APPLICATION_PATH + "user/";
	public static final String SHAREPHOTO_PATH = SD_PATH +  APPLICATION_PATH + "sharePhoto/";
	public static final String SHARE_SCORECARD_PATH_NAME = SHAREPHOTO_PATH + "scorecard.jpg";
	public static final String SHARE_COURSE_PATH_NAME = SHAREPHOTO_PATH + "course.jpg";
	public static final String EXCEPTION_PATH = SD_PATH +  APPLICATION_PATH + "exception/";
	public static final String ALUMB_PATH_CONSTANTDS[] = {"dcim","DCIM","camera","Camera","CAMERA"};
	/**
	 * 系统常量------end
	 * 
	 * @author chengmingyan
	 */
	public static final int PHOTOHEIGHT = 200;
	public static float   mapRatio=(float)0.1;
	public static final String STRING_SEPARATOR = "<<!!!>>";//字符分隔符号
//	public static int[] distance ;
	//marke
    public static final int MARGEK_COUNT = 5;//一个洞默认五个marke
    public static final String SCORECARD_VIEW_ID ="scorecard_view_id" ;//0--好友积分卡/1-历史
    public static final String GOLFER_FILE_NAME = "golferList.txt";
	 public static class DataBase{
	    	// 数据库名称为data
		 	public static final String DB_PATHA_NAME = "database/";
		 	public static final String DB_PATH_DEFAULT = "defaultUser/";
	    	public static final String DB_USER_NAME = "score.db";
	        public static final int        DB_USER_VERSION = 1;
			public static final String DB_USERXMLNAME = "db_userupdate.sql.xml";
			public static final String MENU = "Sql";
			public static final String MENU_INDEX = "index";
			public static final String ITEM = "sql";
		}
	public static final class GOLFCOURSE {
		public static final String COURSETABLE = "GolfCourses";
		public static final String COURSEID = "CourseID";
		public static final String SAVETIME = "SaveTime";
		public static final String CHANGTIME = "ChangTime";
		public static final String COURSENAME = "Course_Name";
		public static final String COURSEPRO = "Course_pro";
		public static final String COURSEPROPY = "Course_proPy";
		public static final String COURSECITY = "Course_city";
		public static final String COURSELONGITUDE = "Course_longitude";
		public static final String COURSELATITUDE = "Course_latitude";
		public static final String UPDATENUM = "Update_num";
	}

	public static class GOLFER {
		public static final String GOLFERTABLE = "Golfer";
		public static final String GOLFERID = "ID";
		public static final String GOLFERNAME = "GolferName";
		public static final String EMAIL = "Email";
		public static final String GOLFERPHOTOPATH = "GolferPhotoPath";
		public static final String PHONE = "phone";
		public static final String SINA = "Sina";
	}

	public static final class GOLFMAPMARKE {
		public static final String GOLFMAPMARKETABLE = "GolfMapMarke";
		public static final String GOLFMAPMARKEID = "MarkeID";
		public static final String GOLFMAPMARKEHOLEID = "HoleID";
		public static final String GOLFMAPMARKE_ID = "_id";
		public static final String GOLFMAPMARKE_X = "Marke_X";
		public static final String GOLFMAPMARKE_Y = "Marke_Y";
		public static final String DISTANCE = "Distance";
	}
	public static final class SCORECARD{
		public static final String SCORECARDTABLE = "ScoreCard";
		public static final String ID = "id";
		public static final String USERID = "UserID";
		public static final String SCORECARD = "scoreCard";
		public static final String PHOTONAME = "photoName";
		public static final String SAVETIME = "saveTime";
		public static final String CHANGETIME = "changeTime";
		public static final String VERSION = "version";
		public static final String STATE = "state";
	}
	public static final class LOCALSCORECARD{
		public static final String  LOCALSCORECARDTABLE = "LocalScoreCard";
		public static final String LOCALID = "LocalID";
		public static final String SCORECARD = "ScoreCard";
		public static final String PHOTONAME = "photoName";
		public static final String SAVETIME = "SaveTime";
		public static final String CHANGETIME = "ChangeTime";
	}
	
	public static final String GOLFERCREATE = "CREATE TABLE IF NOT EXISTS "
			+ GOLFER.GOLFERTABLE + "(" + GOLFER.GOLFERID
			+ " INTEGER  primary key , " + GOLFER.GOLFERNAME
			+ " Varchar(64) , " + GOLFER.EMAIL + " Varchar(64)," + GOLFER.SINA
			+ "  Varchar(64)," + GOLFER.PHONE + "  Varchar(19),"
			+ GOLFER.GOLFERPHOTOPATH + " Varchar(299))";
	public static final String GOLFMAPMARKECREATE = "CREATE TABLE IF NOT EXISTS "
			+ GOLFMAPMARKE.GOLFMAPMARKETABLE
			+ " ("
			+ GOLFMAPMARKE.GOLFMAPMARKEID
			+ " INTEGER PRIMARY KEY,"
			+ GOLFMAPMARKE.GOLFMAPMARKEHOLEID
			+ " INTEGER,"
			+ GOLFMAPMARKE.GOLFMAPMARKE_ID
			+ " INTEGER,"
			+ GOLFMAPMARKE.GOLFMAPMARKE_X
			+ " FLOAT,"
			+ GOLFMAPMARKE.GOLFMAPMARKE_Y + " FLOAT );";
	public static final String SCORECARDCREATE = "CREATE TABLE IF NOT EXISTS "
			+ SCORECARD.SCORECARDTABLE
			+ " ("
			+ SCORECARD.ID
			+ " INTEGER PRIMARY KEY,"
			+ SCORECARD.USERID
			+ " INTEGER,"
			+ SCORECARD.SCORECARD
			+ " Varchar(1024),"
			+ SCORECARD.PHOTONAME
			+ " Varchar(32),"
			+ SCORECARD.SAVETIME
			+ " Varchar(32),"
			+ SCORECARD.CHANGETIME
			+ " Varchar(32),"
			+ SCORECARD.VERSION
			+ " INTEGER,"
			+ SCORECARD.STATE
			+ " INTEGER );";
	public static final String LOCALSCORECARDCREATE = "CREATE TABLE IF NOT EXISTS "
			+ LOCALSCORECARD. LOCALSCORECARDTABLE
			+ " ("
			+ LOCALSCORECARD.LOCALID
			+ " INTEGER PRIMARY KEY,"
			+ LOCALSCORECARD.SCORECARD
			+ " Varchar(1024),"
			+ LOCALSCORECARD.PHOTONAME
			+ " Varchar(32),"
			+ LOCALSCORECARD.SAVETIME
			+ " Varchar(32),"
			+ LOCALSCORECARD.CHANGETIME
			+ " Varchar(32) );";
}
