package com.teewell.samsunggolfmate.costants;

public class KeyValuesConstant {
	public static final String INTENT_FROM_ACTIVITY = "intent_from_activity";
	public static final String INTENT_USERSCORECARDOBJECT = "UserScoreCardObject";
	public static final String INTENT_COURSESOBJECT = "CoursesObject";
	public static final String INTENT_PLAYGAMEOBJECT = "PlayGameObject";
	public static final String INTENT_GOLFEROBJECT = "GolferObject";
//	public static final String INTENT_HISTORY_SCORECARD_ACTIVITY = "HistoryScorecardActivity";
//	public static final String INTENT_FEEDBACK_ACTIVITY = "FeedbackActivity";
}
