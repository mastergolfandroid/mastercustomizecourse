package com.teewell.samsunggolfmate.datebase;

import com.teewell.samsunggolfmate.common.Contexts.DataBase;
import com.teewell.samsunggolfmate.models.UserInfoModel;

import android.content.Context;

/**
 * @author huanghua
 *
 */
public class DataBaseSingle {
    public static DataBaseSingle instance = null;
    private MyDataBaseAdapter dataBaseAdapter;
    private Context context;
    public static DataBaseSingle getInstance(Context context){
    	if(instance == null){
    		instance = new DataBaseSingle(context);
    	}
    	return instance;
    }
    public DataBaseSingle(Context context) {
    	if(dataBaseAdapter == null){            
            dataBaseAdapter = new MyDataBaseAdapter(context);
        }
        modifyDataBase(context);
	}
    public void modifyDataBase(Context context){
    	if(dataBaseAdapter.isUserOpen()){   
        	try {
        		dataBaseAdapter.closeUser();
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        }
        if (UserInfoModel.getInstance(context).hasLoggedIn()) {
        	dataBaseAdapter.openUser(UserInfoModel.getInstance(context).getCurrentUserName()+"/");
        }else {
        	dataBaseAdapter.openUser(DataBase.DB_PATH_DEFAULT);
        }
    }
    public MyDataBaseAdapter getDataBase(){
    	if (dataBaseAdapter == null) {
    		instance = new DataBaseSingle(context);
		}
        return dataBaseAdapter;
    }
}
