package com.teewell.samsunggolfmate.datebase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import android.R.bool;
import android.R.integer;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.Contacts.People;
import android.util.Log;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.GeneralScoreBean;
import com.teewell.samsunggolfmate.beans.MapMarkeVo;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.common.Contexts.DataBase;
import com.teewell.samsunggolfmate.common.Contexts.LOCALSCORECARD;
import com.teewell.samsunggolfmate.common.Contexts.SCORECARD;
import com.teewell.samsunggolfmate.models.MapModel;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.UserInfoModel;
import com.teewell.samsunggolfmate.models.course.CourseInfoModel;
import com.teewell.samsunggolfmate.utils.ComparatorUserScoreCard;
import com.teewell.samsunggolfmate.utils.FileUtil;
import com.teewell.samsunggolfmate.utils.JsonProcessUtil;

@SuppressWarnings({ "unused", "deprecation" })
public class MyDataBaseAdapter {

    private static final String TAG = MyDataBaseAdapter.class.getSimpleName();
    public static Context mContext = null;
    public DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SQLiteDatabase mSQLiteUserDatabase = null;
    private DatabaseUserHelper mDatabaseUserHelper = null;
    private static class DatabaseUserHelper extends SQLiteOpenHelper {
    	public DatabaseUserHelper(Context context, String name,CursorFactory factory, int version){
        	super(context, name, factory, version);
//            super(context, Contexts.DataBase.DB_USER_NAME, null,
//                    Contexts.DataBase.DB_USER_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
//        	  db.execSQL(Contexts.GOLFERCREATE);
//              db.execSQL(Contexts.GOLFMAPMARKECREATE);
//              db.execSQL(Contexts.SCORECARDCREATE);
//              db.execSQL(Contexts.LOCALSCORECARDCREATE);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        	DataBaseUpdate dataBaseUpdate = new DataBaseUpdate();
        	dataBaseUpdate.update(db, oldVersion, mContext, Contexts.DataBase.DB_USERXMLNAME);
        }
       
    }
    public MyDataBaseAdapter(Context context) {
        mContext = context;
    }
    public void openUser(String path) throws SQLException {
//    	在SD卡上创建数据库方法如下：
        String dbPath = Contexts.USER_PATH+path+DataBase.DB_PATHA_NAME;
        File dbp=new File(dbPath);
        File dbf=new File(dbPath+DataBase.DB_USER_NAME);
        if(!dbp.exists()){
               dbp.mkdirs();
         }
        //数据库文件是否创建成功
         boolean isFileCreateSuccess=false; 
         if(!dbf.exists()){
        	 try {
				dbf.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
        	 isFileCreateSuccess=FileUtil.copyFileToSD(mContext, DataBase.DB_USER_NAME, dbPath+DataBase.DB_USER_NAME);
          }else{
                    isFileCreateSuccess=true;
          }
          if(isFileCreateSuccess){
        	  mDatabaseUserHelper = new DatabaseUserHelper(mContext, dbPath+DataBase.DB_USER_NAME, null, DataBase.DB_USER_VERSION);
        	  mSQLiteUserDatabase = mDatabaseUserHelper.getWritableDatabase();
          }
//        mSQLiteUserDatabase = SQLiteDatabase.openOrCreateDatabase(path, null);
    }
    public boolean isUserOpen() {
        if(mSQLiteUserDatabase == null){
        	System.out.println("mSQLiteUserDatabase is close");
            return false;
        }
        return mSQLiteUserDatabase.isOpen();
    }
    public void closeUser() {
        mDatabaseUserHelper.close();
    }
    public List<MapMarkeVo> selectMarkeById(int id) {
        List<MapMarkeVo> voList = new ArrayList<MapMarkeVo>();
        String[] strings = new String[] { Contexts.GOLFMAPMARKE.GOLFMAPMARKEID,
                Contexts.GOLFMAPMARKE.GOLFMAPMARKEHOLEID, Contexts.GOLFMAPMARKE.GOLFMAPMARKE_X,
                Contexts.GOLFMAPMARKE.GOLFMAPMARKE_Y };
        String sql = "select * from " + Contexts.GOLFMAPMARKE.GOLFMAPMARKETABLE
                + " where " + Contexts.GOLFMAPMARKE.GOLFMAPMARKEHOLEID + "=" + id;
        Cursor cursor=null;
        try {			
        	Log.d(TAG, "selectMarkeByHoleId sql = " + sql);
        	cursor = mSQLiteUserDatabase.rawQuery(sql, null);
        	if (cursor.getCount() > 0) {
//            cursor.moveToFirst();
        		while (cursor.moveToNext()) {
        			MapMarkeVo vo = new MapMarkeVo();
        			vo.setId(cursor.getInt(cursor.getColumnIndex(Contexts.GOLFMAPMARKE.GOLFMAPMARKEID)));
        			vo.setHoleId(cursor.getInt(cursor.getColumnIndex(Contexts.GOLFMAPMARKE.GOLFMAPMARKEHOLEID)));
        			vo.setX(cursor.getFloat(cursor.getColumnIndex(Contexts.GOLFMAPMARKE.GOLFMAPMARKE_X)));
        			vo.setY(cursor.getFloat(cursor.getColumnIndex(Contexts.GOLFMAPMARKE.GOLFMAPMARKE_Y)));
        			voList.add(vo);
        			vo = null;
        		}
        	}
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }

        return voList;
    }
    public MapMarkeVo selectMarkeByMarkeID(int markeId) {
        MapMarkeVo vo = new MapMarkeVo();
        String[] strings = new String[] { Contexts.GOLFMAPMARKE.GOLFMAPMARKEID,
                Contexts.GOLFMAPMARKE.GOLFMAPMARKEHOLEID, Contexts.GOLFMAPMARKE.GOLFMAPMARKE_X,
                Contexts.GOLFMAPMARKE.GOLFMAPMARKE_Y };
        String sql = "select * from " + Contexts.GOLFMAPMARKE.GOLFMAPMARKETABLE
                + " where " + Contexts.GOLFMAPMARKE.GOLFMAPMARKEID + "=" + markeId;
        Log.d(TAG, "selectMarkeById sql = " + sql);
        Cursor cursor=null;
        try {
        	cursor = mSQLiteUserDatabase.rawQuery(sql, null);
        	if (cursor.getCount() > 0) {
        		cursor.moveToFirst();
        		vo.setId(cursor.getInt(cursor.getColumnIndex(Contexts.GOLFMAPMARKE.GOLFMAPMARKEID)));
        		vo.setHoleId(cursor.getInt(cursor.getColumnIndex(Contexts.GOLFMAPMARKE.GOLFMAPMARKEHOLEID)));
        		vo.setX(cursor.getFloat(cursor.getColumnIndex(Contexts.GOLFMAPMARKE.GOLFMAPMARKE_X)));
        		vo.setY(cursor.getFloat(cursor.getColumnIndex(Contexts.GOLFMAPMARKE.GOLFMAPMARKE_Y)));
        	}			
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return vo;
    }

    public void updateMarke(int id, float x, float y) {
        Boolean flag = false;
        ContentValues args = new ContentValues();
        args.put(Contexts.GOLFMAPMARKE.GOLFMAPMARKE_X, x);
        args.put(Contexts.GOLFMAPMARKE.GOLFMAPMARKE_Y, y);
        mSQLiteUserDatabase.update(Contexts.GOLFMAPMARKE.GOLFMAPMARKETABLE, args,
                Contexts.GOLFMAPMARKE.GOLFMAPMARKEID + "=" + id, null);
    }

    public boolean deleteMapMarkeById(int id) {
        Boolean flag = false;
        flag = mSQLiteUserDatabase.delete(Contexts.GOLFMAPMARKE.GOLFMAPMARKETABLE,
                Contexts.GOLFMAPMARKE.GOLFMAPMARKEID + "=" + id, null) > 0;
        return flag;
    }
    
    
    
    public boolean deleteMapMarkeByHoleID(int holeId) {
         Boolean flag = false;
         flag = mSQLiteUserDatabase.delete(Contexts.GOLFMAPMARKE.GOLFMAPMARKETABLE,
                 Contexts.GOLFMAPMARKE.GOLFMAPMARKEHOLEID + "=" + holeId, null) > 0;
         return flag;
     }
    
    
    public Boolean insertMapMarke(int holeId, float x, float y) {
    	Boolean flag = false;
    	String dd = df.format(new Date());
    	String[] marke = new String[] { Contexts.GOLFMAPMARKE.GOLFMAPMARKEHOLEID,
    			Contexts.GOLFMAPMARKE.GOLFMAPMARKE_X, Contexts.GOLFMAPMARKE.GOLFMAPMARKE_Y };
    	ContentValues initialValues = new ContentValues();
    	initialValues.put(Contexts.GOLFMAPMARKE.GOLFMAPMARKEHOLEID, holeId);
    	initialValues.put(Contexts.GOLFMAPMARKE.GOLFMAPMARKE_X, x);
    	initialValues.put(Contexts.GOLFMAPMARKE.GOLFMAPMARKE_Y, y);
    	long number = mSQLiteUserDatabase.insert(
    			Contexts.GOLFMAPMARKE.GOLFMAPMARKETABLE, null, initialValues);
    	if (number != -1)            flag = true;
    	return flag;
    }
    public boolean deleteScoreCard(UserScoreCardObject object){
    	boolean flag = false;
    	if (object.getVersion()!=0) {
    		flag = deleteHistoryScoreCard(object.getId());
		}else {
			flag = deleteLocalScoreCard(object.getId());
		}
    	return flag;
    }
    public void deleteAllLocalScoreCard(){
    	mSQLiteUserDatabase.beginTransaction();
  	   mSQLiteUserDatabase.setTransactionSuccessful();
  	   int count = mSQLiteUserDatabase.delete(LOCALSCORECARD.LOCALSCORECARDTABLE, null, null);
  	   mSQLiteUserDatabase.endTransaction();
    }
	public boolean deleteLocalScoreCard(int id){
	
	   mSQLiteUserDatabase.beginTransaction();
	   mSQLiteUserDatabase.setTransactionSuccessful();
	   boolean flag = false;
	   int count = mSQLiteUserDatabase.delete(LOCALSCORECARD.LOCALSCORECARDTABLE, LOCALSCORECARD.LOCALID+"="+id, null);
	   if (count>0) {
		flag = true;
	   }
	   mSQLiteUserDatabase.endTransaction();
	   return flag;
   }
	public boolean deleteHistoryScoreCard(int id){
		
		mSQLiteUserDatabase.beginTransaction();
		mSQLiteUserDatabase.setTransactionSuccessful();
		boolean flag = false;
		ContentValues values = new ContentValues();
		values.put(SCORECARD.STATE, UserScoreCardObject.DLL_SCORECARD);
		values.put(SCORECARD.CHANGETIME, df.format(new Date()));
		int count = mSQLiteUserDatabase.update(SCORECARD.SCORECARDTABLE, values, SCORECARD.ID+"="+id, null);
		if (count>0) {
			flag = true;
		}
		mSQLiteUserDatabase.endTransaction();
		return flag;
	}
	public boolean updateScoreCard(UserScoreCardObject object){
		boolean flag = false;
//		object.getGeneralScoreBean().setMySelfScore(calculateMyScore(object));
//		String scoreCardString = JsonProcessUtil.toJSON(object.getGeneralScoreBean());
//		object.setScoreCard(scoreCardString);
		if (object.getId()==null||object.getId().intValue()==0) {
			int id = selectLocalScoreCardMaxLocalId();
			object.setId(id);
			return saveGeneralScoreBean(mContext, object);
		}
		if (object.getVersion()!=0) {
			flag = updateHistoryScoreCard(object);
		}else {
			flag = updateLocalScoreCard(object);
		}
		return flag;
	}
	public boolean UserScoreCardObject(Context context,UserScoreCardObject scoreBean) {
//		scoreBean.getGeneralScoreBean().setMySelfScore(calculateMyScore(scoreBean));
		String scoreCard = JsonProcessUtil.toJSON(scoreBean.getGeneralScoreBean());
		scoreBean.setScoreCard(scoreCard);
		boolean flag=false;
		try {
			ContentValues values = new ContentValues();
			values.put(SCORECARD.CHANGETIME, scoreBean.getSaveTime());
			values.put(SCORECARD.PHOTONAME, "");
			values.put(SCORECARD.SAVETIME, scoreBean.getSaveTime());
			values.put(SCORECARD.SCORECARD, scoreBean.getScoreCard());
			values.put(SCORECARD.STATE, UserScoreCardObject.MODIFY_SCORECARD);
			values.put(SCORECARD.VERSION, UserScoreCardObject.MODIFY_SCORECARD);
			String sql = "delete from "+LOCALSCORECARD.LOCALSCORECARDTABLE;
			mSQLiteUserDatabase.execSQL(sql);
			flag=mSQLiteUserDatabase.insert(SCORECARD.SCORECARDTABLE, null, values)>0?true:false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return flag;
	}
	private boolean updateLocalScoreCard(UserScoreCardObject object){
		
		mSQLiteUserDatabase.beginTransaction();
		mSQLiteUserDatabase.setTransactionSuccessful();
		boolean flag = false;
		ContentValues values = new ContentValues();
		values.put(LOCALSCORECARD.SCORECARD, object.getScoreCard());
		values.put(LOCALSCORECARD.SAVETIME, object.getSaveTime());
		values.put(LOCALSCORECARD.CHANGETIME, df.format(new Date()));
		values.put(LOCALSCORECARD.PHOTONAME, object.getPhotoName());
		int count = mSQLiteUserDatabase.update(LOCALSCORECARD.LOCALSCORECARDTABLE,values, LOCALSCORECARD.LOCALID+"="+object.getId(), null);
		if (count>0) {
			flag = true;
		}
		mSQLiteUserDatabase.endTransaction();
		return flag;
	}
	private boolean updateHistoryScoreCard(UserScoreCardObject object){
		
		mSQLiteUserDatabase.beginTransaction();
		mSQLiteUserDatabase.setTransactionSuccessful();
		boolean flag = false;
		ContentValues values = new ContentValues();
		values.put(SCORECARD.SCORECARD, object.getScoreCard());
		values.put(SCORECARD.STATE, UserScoreCardObject.MODIFY_SCORECARD);
		values.put(LOCALSCORECARD.SAVETIME, object.getSaveTime());
		values.put(LOCALSCORECARD.PHOTONAME, object.getPhotoName());
		values.put(SCORECARD.CHANGETIME, df.format(new Date()));
		int count = mSQLiteUserDatabase.update(SCORECARD.SCORECARDTABLE,values, SCORECARD.ID+"="+object.getId(), null);
		if (count>0) {
			flag = true;
		}
		mSQLiteUserDatabase.endTransaction();
		return flag;
	}

	private boolean excuteSqlList(List<String> sqlList,SQLiteDatabase sqLiteDatabase){
		boolean flag=false;	
		try {			
			beginTransaction(sqLiteDatabase);
			if (sqlList!=null) {			
				for (int i = 0; i < sqlList.size(); i++) {
					 String excusql = (String) sqlList.get(i);
					 sqLiteDatabase.execSQL(excusql);
				}
			}
			setTransactionSuccessful(sqLiteDatabase);
			flag=true;
		} catch (Exception e) {
			Log.d(TAG, "excuteSqlList()--"+e);
			// TODO: handle exception
		}finally{
			sqLiteDatabase.endTransaction();
		}
		return flag;
	} 
	
	private void beginTransaction(SQLiteDatabase sqLiteDatabase)throws Exception{
		sqLiteDatabase.beginTransaction();		
	}
	
	private void setTransactionSuccessful(SQLiteDatabase sqLiteDatabase)throws Exception{
		sqLiteDatabase.setTransactionSuccessful();		
	}
	
	private void endTransaction(SQLiteDatabase sqLiteDatabase)throws Exception{
		sqLiteDatabase.endTransaction();		
	}
//	public int calculateMyScore(UserScoreCardObject scoreBean){
//		int count = 0;
//		for (Integer integer : scoreBean.getGeneralScoreBean().getScores()) {
//			count += integer;
//		}
//		return count;
//	}
	public boolean saveGeneralScoreBean(Context context,UserScoreCardObject scoreBean) {
//		scoreBean.getGeneralScoreBean().setMySelfScore(calculateMyScore(scoreBean));
		String scoreCard = JsonProcessUtil.toJSON(scoreBean.getGeneralScoreBean());
		scoreBean.setScoreCard(scoreCard);
		boolean flag=false;
		try {
			ContentValues values = new ContentValues();
			values.put(LOCALSCORECARD.CHANGETIME, scoreBean.getSaveTime());
			values.put(LOCALSCORECARD.PHOTONAME, "");
			values.put(LOCALSCORECARD.SAVETIME, scoreBean.getSaveTime());
			values.put(LOCALSCORECARD.SCORECARD, scoreBean.getScoreCard());
			flag=mSQLiteUserDatabase.insert(LOCALSCORECARD.LOCALSCORECARDTABLE, null, values)>0?true:false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return flag;
	}
	public List<UserScoreCardObject> selectScoreCardObjects(String key){
		List<UserScoreCardObject> scoreCardObjects = new ArrayList<UserScoreCardObject>();
		List<UserScoreCardObject> list = new ArrayList<UserScoreCardObject>();
		UserScoreCardObject bean = null;
		Cursor c = null;
		String name = null; 
		String[] selectionFromLocal = {LOCALSCORECARD.CHANGETIME,LOCALSCORECARD.PHOTONAME,LOCALSCORECARD.SAVETIME,
				LOCALSCORECARD.SCORECARD,LOCALSCORECARD.LOCALID};
		String[] selection = {SCORECARD.CHANGETIME,SCORECARD.PHOTONAME,SCORECARD.SAVETIME,
				SCORECARD.SCORECARD,SCORECARD.ID,SCORECARD.STATE,SCORECARD.USERID,SCORECARD.VERSION};
		try {
			/*c = mSQLiteUserDatabase.query(LOCALSCORECARD.LOCALSCORECARDTABLE, selectionFromLocal, null,null, null, null, null);
			if (c.moveToFirst()) {
			    do {
			    	bean = new UserScoreCardObject();
			    	bean.setId(c.getInt(c.getColumnIndexOrThrow(LOCALSCORECARD.LOCALID)));
			    	bean.setChangeTime(c.getString(c.getColumnIndexOrThrow(LOCALSCORECARD.CHANGETIME)));
			    	bean.setPhotoName(c.getString(c.getColumnIndexOrThrow(LOCALSCORECARD.PHOTONAME)));
			    	bean.setSaveTime(c.getString(c.getColumnIndexOrThrow(LOCALSCORECARD.SAVETIME)));
			    	bean.setScoreCard(c.getString(c.getColumnIndexOrThrow(LOCALSCORECARD.SCORECARD)));
			    	bean.setGeneralScoreBean((GeneralScoreBean)JsonProcessUtil.fromJSON(bean.getScoreCard(), GeneralScoreBean.class));
			    	scoreCardObjects.add(bean);
			   } while (c.moveToNext());
			 }*/
			c = mSQLiteUserDatabase.query(SCORECARD.SCORECARDTABLE, selection, SCORECARD.STATE+"!="+UserScoreCardObject.DLL_SCORECARD,null, null, null, null);
			if (c.moveToFirst()) {
				do {
					bean = new UserScoreCardObject();
					bean.setId(c.getInt(c.getColumnIndexOrThrow(SCORECARD.ID)));
					bean.setUserID(c.getInt(c.getColumnIndexOrThrow(SCORECARD.USERID)));
					bean.setState(c.getInt(c.getColumnIndexOrThrow(SCORECARD.STATE)));
					bean.setVersion(c.getInt(c.getColumnIndexOrThrow(SCORECARD.VERSION)));
					bean.setChangeTime(c.getString(c.getColumnIndexOrThrow(SCORECARD.CHANGETIME)));
					bean.setPhotoName(c.getString(c.getColumnIndexOrThrow(SCORECARD.PHOTONAME)));
					bean.setSaveTime(c.getString(c.getColumnIndexOrThrow(SCORECARD.SAVETIME)));
					bean.setScoreCard(c.getString(c.getColumnIndexOrThrow(SCORECARD.SCORECARD)));
					bean.setGeneralScoreBean((GeneralScoreBean)JsonProcessUtil.fromJSON(bean.getScoreCard(), GeneralScoreBean.class));
					scoreCardObjects.add(bean);
				} while (c.moveToNext());
			}
			if (scoreCardObjects.size()>0) {
				
				if (!"".equals(key)) {
					for (int i = 0; i < scoreCardObjects.size(); i++) {
						bean = scoreCardObjects.get(i);
						name = bean.getGeneralScoreBean().getCourse_Name();
						if(name.indexOf(key.toUpperCase())>=0){
							list.add(bean);
						}
					}
				}else {
					list = scoreCardObjects;
				}
				ComparatorUserScoreCard comparator = new ComparatorUserScoreCard();
				Collections.sort(list, comparator);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (c != null) {
				c.close();
			}
		}
		
		return list;
	}
	public boolean synchScoreCard(List<UserScoreCardObject> userScoreCardObjects){
		boolean flag = false;
		UserScoreCardObject bean = null;
		Cursor c = null;
		String name = null; 
		String[] selection = {SCORECARD.STATE};
		String whereString = null;
		String sql = null;
		List<String> sqlList = new ArrayList<String>();
		try {
			for (UserScoreCardObject object:userScoreCardObjects) {
				whereString = SCORECARD.ID+"="+object.getId();
				c = mSQLiteUserDatabase.query(SCORECARD.SCORECARDTABLE, selection, whereString,null, null, null, null);
				if (c.moveToFirst()) {
					int state = c.getInt(c.getColumnIndexOrThrow(SCORECARD.STATE));
					if (state==UserScoreCardObject.DLL_SCORECARD) {
						sql = "delete from "+SCORECARD.SCORECARDTABLE+" where "+whereString;
//						sql = "update "+SCORECARD.SCORECARDTABLE+" set "+SCORECARD.STATE+"="+UserScoreCardObject.DLL_SCORECARD+","+SCORECARD.VERSION+"="
//								+object.getVersion()+" "+whereString;
					}else {
						sql = "update "+SCORECARD.SCORECARDTABLE+" set "+SCORECARD.STATE+"="+UserScoreCardObject.NORMAL_SCORECARD+","+SCORECARD.VERSION+"="
								+object.getVersion()+" where "+whereString;
					}
					sqlList.add(sql);
				}else if(object.getState()!=UserScoreCardObject.DLL_SCORECARD){
					object.setUserID(UserInfoModel.getInstance(mContext).getUserId());
					sql = "insert into "+SCORECARD.SCORECARDTABLE+"("
							+SCORECARD.ID+","+SCORECARD.USERID+","+SCORECARD.SCORECARD+","+SCORECARD.PHOTONAME+","+SCORECARD.SAVETIME
							+","+SCORECARD.CHANGETIME+","+SCORECARD.STATE+","+SCORECARD.VERSION+") values("
							+object.getId()+","+object.getUserID()+",'"+object.getScoreCard()+"','"+object.getPhotoName()+"','"+object.getSaveTime()
							+"','"+object.getChangeTime()+"',"+object.getState()+","+object.getVersion()+")";
					sqlList.add(sql);
				}
			}
			flag = excuteSqlList(sqlList, mSQLiteUserDatabase);
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (c != null) {
				c.close();
			}
		}
		
		return flag;
	}
	public int selectScoreCardMaxVersion(){
		int version = 0;
		Cursor c = null;
		String name = null; 
		String[] selection = {"max("+SCORECARD.VERSION+")"};
		try {
			c = mSQLiteUserDatabase.query(SCORECARD.SCORECARDTABLE, selection, null,null, null, null, null);
			if (c.moveToFirst()) {
					version = c.getInt(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (c != null) {
				c.close();
			}
		}
		
		return version;
	}
	public List<UserScoreCardObject> selectModifyScoreCardObjects(){
		List<UserScoreCardObject> scoreCardObjects = new ArrayList<UserScoreCardObject>();
		UserScoreCardObject bean = null;
		Cursor c = null;
		String name = null; 
		String[] selection = {SCORECARD.CHANGETIME,SCORECARD.PHOTONAME,SCORECARD.SAVETIME,
				SCORECARD.SCORECARD,SCORECARD.ID,SCORECARD.STATE,SCORECARD.USERID,SCORECARD.VERSION};
		String whereString = SCORECARD.STATE+"="+UserScoreCardObject.MODIFY_SCORECARD;
		try {
			c = mSQLiteUserDatabase.query(SCORECARD.SCORECARDTABLE, selection, whereString,null, null, null, null);
			if (c.moveToFirst()) {
				do {
					bean = new UserScoreCardObject();
					bean.setId(c.getInt(c.getColumnIndexOrThrow(SCORECARD.ID)));
//					bean.setUserID(c.getInt(c.getColumnIndexOrThrow(SCORECARD.USERID)));
//					bean.setState(c.getInt(c.getColumnIndexOrThrow(SCORECARD.STATE)));
//					bean.setVersion(c.getInt(c.getColumnIndexOrThrow(SCORECARD.VERSION)));
					bean.setChangeTime(c.getString(c.getColumnIndexOrThrow(SCORECARD.CHANGETIME)));
//					bean.setPhotoName(c.getString(c.getColumnIndexOrThrow(SCORECARD.PHOTONAME)));
//					bean.setSaveTime(c.getString(c.getColumnIndexOrThrow(SCORECARD.SAVETIME)));
					bean.setScoreCard(c.getString(c.getColumnIndexOrThrow(SCORECARD.SCORECARD)));
					scoreCardObjects.add(bean);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (c != null) {
				c.close();
			}
		}
		
		return scoreCardObjects;
	}
	public List<UserScoreCardObject> selectDeleteScoreCardObjects(){
		List<UserScoreCardObject> scoreCardObjects = new ArrayList<UserScoreCardObject>();
		UserScoreCardObject bean = null;
		Cursor c = null;
		String name = null; 
		String[] selection = {SCORECARD.CHANGETIME,SCORECARD.PHOTONAME,SCORECARD.SAVETIME,
				SCORECARD.SCORECARD,SCORECARD.ID,SCORECARD.STATE,SCORECARD.USERID,SCORECARD.VERSION};
		String whereString = SCORECARD.STATE+"="+UserScoreCardObject.DLL_SCORECARD;
		try {
			c = mSQLiteUserDatabase.query(SCORECARD.SCORECARDTABLE, selection, whereString,null, null, null, null);
			if (c.moveToFirst()) {
				do {
					bean = new UserScoreCardObject();
					bean.setId(c.getInt(c.getColumnIndexOrThrow(SCORECARD.ID)));
//					bean.setUserID(c.getInt(c.getColumnIndexOrThrow(SCORECARD.USERID)));
//					bean.setState(c.getInt(c.getColumnIndexOrThrow(SCORECARD.STATE)));
//					bean.setVersion(c.getInt(c.getColumnIndexOrThrow(SCORECARD.VERSION)));
					bean.setChangeTime(c.getString(c.getColumnIndexOrThrow(SCORECARD.CHANGETIME)));
//					bean.setPhotoName(c.getString(c.getColumnIndexOrThrow(SCORECARD.PHOTONAME)));
//					bean.setSaveTime(c.getString(c.getColumnIndexOrThrow(SCORECARD.SAVETIME)));
//					bean.setScoreCard(c.getString(c.getColumnIndexOrThrow(SCORECARD.SCORECARD)));
					scoreCardObjects.add(bean);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (c != null) {
				c.close();
			}
		}
		
		return scoreCardObjects;
	}
	public List<UserScoreCardObject> selectLocalScoreCardObjects(){
		List<UserScoreCardObject> scoreCardObjects = new ArrayList<UserScoreCardObject>();
		UserScoreCardObject bean = null;
		Cursor c = null;
		String name = null; 
		String[] selectionFromLocal = {LOCALSCORECARD.CHANGETIME,LOCALSCORECARD.PHOTONAME,LOCALSCORECARD.SAVETIME,
				LOCALSCORECARD.SCORECARD,LOCALSCORECARD.LOCALID};
		try {
			c = mSQLiteUserDatabase.query(LOCALSCORECARD.LOCALSCORECARDTABLE, selectionFromLocal, null,null, null, null, null);
			if (c.moveToFirst()) {
				do {
					bean = new UserScoreCardObject();
					bean.setId(c.getInt(c.getColumnIndexOrThrow(LOCALSCORECARD.LOCALID)));
					bean.setChangeTime(c.getString(c.getColumnIndexOrThrow(LOCALSCORECARD.CHANGETIME)));
					bean.setPhotoName(c.getString(c.getColumnIndexOrThrow(LOCALSCORECARD.PHOTONAME)));
					bean.setSaveTime(c.getString(c.getColumnIndexOrThrow(LOCALSCORECARD.SAVETIME)));
					bean.setScoreCard(c.getString(c.getColumnIndexOrThrow(LOCALSCORECARD.SCORECARD)));
					scoreCardObjects.add(bean);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (c != null) {
				c.close();
			}
		}
		
		return scoreCardObjects;
	}
	public int selectLocalScoreCardMaxLocalId(){
		int id = 1;
		Cursor c = null;
		String name = null; 
		String[] selection = {"max("+LOCALSCORECARD.LOCALID+")"};
		try {
			c = mSQLiteUserDatabase.query(LOCALSCORECARD.LOCALSCORECARDTABLE, selection, null,null, null, null, null);
			if (c.moveToFirst()) {
					id = c.getInt(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (c != null) {
				c.close();
			}
		}
		
		return id;
	}
}
