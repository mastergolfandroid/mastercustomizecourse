package com.teewell.samsunggolfmate.models;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.teewell.samsunggolfmate.beans.Point;
import com.teewell.samsunggolfmate.utils.CoordinateUtil;
import com.teewell.samsunggolfmate.utils.DistanceUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;
import com.teewell.samsunggolfmate.views.MapView;

/**
 * 航拍图的数据结构
 * @author Antony
 *
 */
public class MapModel {
	private static final String TAG = "NavigationMapModel";
    private Point pointMe; // 人的位置
    private Point pointTee; // Tee的位置
    private List<Point> pointGreens; // 果岭的位置
    private Point touchPoint; // 点击的位置
    private float rotateAngle; // 旋转的角度
    private CoordinateUtil coordinateUtil;
    private Tee teeType;//tee台类型
    private Context mContext;
    private Bitmap rotatedCourseBmp;
    private Handler handler;
    
    public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public Point getPointMe() {
        return this.pointMe;
    }

    public void setPointMe(Point pointMe) {
        this.pointMe = pointMe;
    }

    public void setPointMe(double lon, double lat) {
        this.pointMe = coordinateUtil.convertLonLatToOldXY(lon, lat);
        this.pointMe.lon = lon;
        this.pointMe.lat = lat;
    }

    public Point getPointTee() {
        return this.pointTee;
    }

    public void setPointTee(Point pointTee) {
        this.pointTee = pointTee;
    }

    public void setPointTee(double lon, double lat) {
        this.pointTee = coordinateUtil.convertLonLatToOldXY(lon, lat);
        this.pointTee.lon = lon;
        this.pointTee.lat = lat;
    }

    public List<Point> getPointGreens() {
        return this.pointGreens;
    }

    public void addPointGreen(double lon, double lat) {
        Point green = coordinateUtil.convertLonLatToOldXY(lon, lat);
        this.pointGreens.add(green);
    }

    public void setPointGreens(List<Point> pointGreens) {
        this.pointGreens = pointGreens;
    }

    public Point getTouchPoint() {
        return this.touchPoint;
    }

    public void setTouchPoint(Point touchPoint) {
        System.out.println("setTouchPoint(Point):");
        this.touchPoint = touchPoint;
        double[] result = coordinateUtil.getLonLatByTouch(touchPoint.x,
                touchPoint.y);
        this.touchPoint.lon = result[0];
        this.touchPoint.lat = result[1];
        System.out.println("touchPoint.x:" + touchPoint.x + ",touchPoint.y"
                + touchPoint.y + "??");
        System.out.println("touchPoint.lon:" + touchPoint.lon
                + ",touchPoint.lat" + touchPoint.lat + "??");
    }

    public void setTouchPoint(double lon, double lat) {
        System.out.println("setTouchPoint(double,double):");
        this.touchPoint = coordinateUtil.convertLonLatToOldXY(lon, lat);
        System.out.println("touchPoint.x:" + touchPoint.x + ",touchPoint.y"
                + touchPoint.y + "??");
        System.out.println("touchPoint.lon:" + touchPoint.lon
                + ",touchPoint.lat" + touchPoint.lat + "??");
    }

    public float getRotateAngle() {
        return this.rotateAngle;
    }

    public void setRotateAngle(float rotateAngle) {
        this.rotateAngle = rotateAngle;
    }

    public CoordinateUtil getCoordinateUtil() {
        return this.coordinateUtil;
    }

    public void setCoordinateUtil(CoordinateUtil coordinateUtil) {
        this.coordinateUtil = coordinateUtil;
    }



    public MapModel(Context context, Bitmap rebitmapOrg, CoordinateUtil coordinateUtil) {
		mContext = context;
//		pointMe = new Point(ScreenUtil.getScreenWidth(mContext)/2, ScreenUtil.getScreenHeight(mContext)-MapView.cursor_touch_offset_Y);
        pointTee = new Point(-100, -100);
        pointGreens = new ArrayList<Point>();
        touchPoint = new Point(-100, -100);
        this.coordinateUtil = coordinateUtil;
        rotatedCourseBmp = rebitmapOrg;
        setRotateAngle(coordinateUtil.getRotateAngle());
	}
    public void setRotatedCourseBmp(Bitmap rotatedCourseBmp) {
        this.rotatedCourseBmp = rotatedCourseBmp;
    }

    public Bitmap getRotatedCourseBmp() {
        return rotatedCourseBmp;
    }

    /**
     * 计算点击的点到分别到果岭和人的距离
     * 
     * @param green 果岭的坐标
     * @return 返回的数组中，元素1为到果岭的距离，元素2为到人的距离
     */
    public String[] getDistanceToGreenAndMe(Point green,int index) {
        String[] distances = new String[2];
        float tempUtil = 1f;
        if (index == DistanceUtil.YARD) {
			tempUtil = 0.9144f;
		}
        distances[0] = Math.round((coordinateUtil
                .getDistanceBettwen(green, touchPoint)) / tempUtil)+"";
        distances[1] = Math.round((coordinateUtil
                .getDistanceBettwen(pointMe, touchPoint)) / tempUtil)+"";
        return distances;
    }
    
//    private float tempUtil = 1;
    
    /**
     * 计算人到果林的距离
     * 
     * @param green 果岭的坐标
     * @return 返回的数组中，元素1为到果岭的距离，元素2为到人的距离
     */
    public int getDistanceToGreen(Point green,int distanceUnit) {
    	float tempUtil = 1;
    	if (DistanceUtil.YARD==distanceUnit) {
    		tempUtil = 0.9144f;
		}
        int len = Math.round((coordinateUtil
                .getDistanceBettwen(getPointMe(), green))/tempUtil );
        return len;
    }
    public float getDistancePersonToGreen(Point green,int distanceUnit) {
    	float tempUtil = 1;
    	if (DistanceUtil.YARD==distanceUnit) {
    		tempUtil = 0.9144f;
    	}
    	float len = Math.abs((coordinateUtil
    			.getDistanceBettwen(getPointMe(), green))/tempUtil );
    	return len;
    }
    public float getRealDistancePersonToGreen(){
    	LocationUtil locationUtil = LocationUtil.getInstance(mContext);
    	Point p = getPointGreens().get(0);
    	double[] touchLonLat = coordinateUtil.getLonLatByTouch(p.x, p.y);
    	return DistanceUtil.distanceBetween(locationUtil.latitude, locationUtil.longitude, touchLonLat[1], touchLonLat[0]);
    }
    
    /**
     * @param teeType
     *            the teeType to set
     */
    public void setTeeType(Tee teeType) {
        this.teeType = teeType;
    }

    /**
     * @return the teeType
     */
    public Tee getTeeType() {
        return teeType;
    }

    public enum Tee {
        BLUE, RED, WHITE, BLACK, GOLD
    }
}
