package com.teewell.samsunggolfmate.models;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.teewell.samsunggolfmate.beans.SharePhotoBean;
import com.teewell.samsunggolfmate.common.Contexts;

import android.content.Context;

public class PhotoModel {
	private static PhotoModel instance;
	private List<SharePhotoBean> list_uploadPhotoBean = null;
	public static PhotoModel getInstance(Context context){
		if (instance == null) {
			instance = new PhotoModel(context.getApplicationContext());
		}
		return instance;
	}
	public PhotoModel(Context context) {
	}
	public List<SharePhotoBean> getList_uploadPhotoBean() {
		if (list_uploadPhotoBean == null) {
			list_uploadPhotoBean = new ArrayList<SharePhotoBean>();
		}
		return list_uploadPhotoBean;
	}
	public void setList_uploadPhotoBean(List<SharePhotoBean> list_uploadPhotoBean) {
		this.list_uploadPhotoBean = list_uploadPhotoBean;
	}
	public void addList_uploadPhotoBean(SharePhotoBean bean){
		list_uploadPhotoBean.add(bean);
	}
	public void removeBeanOfList_uploadPhotoBean(int position){
		list_uploadPhotoBean.remove(position);
	}
	
	 /* 
     * 获取系统时间 
     * 格式：IMG_xxxx年xx月xx日_xx时xx分xx秒 
     */  
    public String getUploadPhotoName(){  
        Calendar c = Calendar.getInstance();  
          
        String str = String.format("IMG_%04d%02d%02d_%02d%02d%02d",  
                c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE),  
                c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE),c.get(Calendar.SECOND));  
          
        return str;  
    }  
      
    /* 
     * 获取完整路径 
     */  
    public String getPhotoFullPath(){  
        StringBuilder sb = new StringBuilder();  
        sb.delete(0, sb.length()); 
        String photoPath = Contexts.SHAREPHOTO_PATH;
        File file = new File(photoPath);
        if (!file.exists()) {
			file.mkdirs();
		}
        sb.append(photoPath);  
        sb.append(getUploadPhotoName());  
        sb.append(".jpg");  
          
        return sb.toString();  
    }  
}
