package com.teewell.samsunggolfmate.models;

import java.util.ArrayList;

import com.teewell.samsunggolfmate.activities.PlayTabActivity;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;
import com.teewell.samsunggolfmate.beans.FairwayObject;
import com.teewell.samsunggolfmate.beans.GeneralScoreBean;
import com.teewell.samsunggolfmate.beans.PlayGameObject;
import com.teewell.samsunggolfmate.beans.SubCoursesObject;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;
import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.utils.ActivityManagerUtil;
import com.teewell.samsunggolfmate.utils.DistanceUtil;
import com.teewell.samsunggolfmate.utils.JsonProcessUtil;
import com.teewell.samsunggolfmate.utils.SharedPreferencesUtils;

import android.content.Context;

public class PlayGameModel {
	private static PlayGameModel instance;
	private PlayGameObject playGameObject;
	public static final String DISTANCEARC="distanceArc";
	public static final int FAIRWAY_COUNT = 18;
	public static final int SUBCOURSE_COUNT = 1;
	/**
	 * 选择的球洞
	 */
	public static int   curHoleNumber;       /*current hole number*/
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
	public static int fromActivity;
	public static int shortDistanceIndex = DistanceUtil.YARD;
	public static int longDistanceIndex = DistanceUtil.KILOMETER;
	public static boolean isChangeSubCourse;
	public PlayGameModel(Context context) {
		// TODO Auto-generated constructor stub
	}
	public static PlayGameModel getInstance(Context context){
		if (instance == null) {
			instance = new PlayGameModel(context);
		}
		return instance;
	}
	public PlayGameObject getPlayGameObject() {
		return playGameObject;
	}
	public void setPlayGameObject(PlayGameObject playGameObject) {
		this.playGameObject = playGameObject;
	}
	public ArrayList<FairwayObject> getFairwayObjectArray(PlayGameObject playGameObject){
		ArrayList<FairwayObject> list = new ArrayList<FairwayObject>();
		for (int i = 0; i < playGameObject.getSubCourseIdArray().length; i++) {
			int id = playGameObject.getSubCourseIdArray()[i];
			if (id>0) {
				for (SubCoursesObject subCoursesObject : playGameObject.getCoursesObject().getSubCourses()) {
					if (id==subCoursesObject.getId()) {
						list.addAll(subCoursesObject.getFairwayList());
					}
				}
			}
		}
		return list;
	}
	public void asyncParAndAerialPhotoBeans(PlayGameObject playGameObject){
		ArrayList<FairwayObject> list = getFairwayObjectArray(playGameObject);
		int pars[] = new int[18];
		ArrayList<AerialPhotoBean> aerialPhotoBeans = new ArrayList<AerialPhotoBean>();
		if (list.size()>18) {
			for (int i = 0; i < list.size(); i++) {
				FairwayObject fairwayObject = list.get(i);
				AerialPhotoBean aerialPhotoBeanTemp = fairwayObject.getAerialPhotoBean();
				if (aerialPhotoBeans.size()==0) {
					aerialPhotoBeans.add(aerialPhotoBeanTemp);
					pars[0] = fairwayObject.getPoleNumber();
				}
				boolean isExist = false;
				for (int j = 0; j < aerialPhotoBeans.size(); j++) {
					AerialPhotoBean aerialPhotoBean = aerialPhotoBeans.get(j);
					if (aerialPhotoBean.getAerialPhotoID().intValue()==fairwayObject.getAerialPhotoBean().getAerialPhotoID().intValue()) {
						aerialPhotoBean.getHoleId()[1] = fairwayObject.getId();
						isExist = true;
						break;
					}
					aerialPhotoBean.getHoleId()[0] = fairwayObject.getId();
				}
				if (!isExist) {
					aerialPhotoBeans.add(aerialPhotoBeanTemp);
					pars[aerialPhotoBeans.size()-1] = fairwayObject.getPoleNumber();
				}
			}
		}else {
			for (int i = 0; i < list.size(); i++) {
				FairwayObject fairwayObject = list.get(i);
				AerialPhotoBean aerialPhotoBeanTemp = fairwayObject.getAerialPhotoBean();
				aerialPhotoBeans.add(aerialPhotoBeanTemp);
				pars[i] = fairwayObject.getPoleNumber();
			}
		}
		playGameObject.setAerialPhotoBeanArray(aerialPhotoBeans);
		playGameObject.getUserScoreCardObject().getGeneralScoreBean().setPars(pars);
	}
	public ArrayList<AerialPhotoBean> getAerialPhotoBeanArray(ArrayList<FairwayObject> list){
		ArrayList<AerialPhotoBean> aerialPhotoBeans = new ArrayList<AerialPhotoBean>();
		if (list.size()>18) {
			for (int i = 0; i < list.size(); i++) {
				FairwayObject fairwayObject = list.get(i);
				AerialPhotoBean aerialPhotoBeanTemp = fairwayObject.getAerialPhotoBean();
				if (aerialPhotoBeans.size()==0) {
					aerialPhotoBeans.add(aerialPhotoBeanTemp);
				}
				boolean isExist = false;
				for (int j = 0; j < aerialPhotoBeans.size(); j++) {
					AerialPhotoBean aerialPhotoBean = aerialPhotoBeans.get(j);
					if (aerialPhotoBean.getAerialPhotoID().intValue()==fairwayObject.getAerialPhotoBean().getAerialPhotoID().intValue()) {
						aerialPhotoBean.getHoleId()[1] = fairwayObject.getId();
						isExist = true;
						break;
					}
					aerialPhotoBean.getHoleId()[0] = fairwayObject.getId();
				}
				if (!isExist) {
					aerialPhotoBeans.add(aerialPhotoBeanTemp);
				}
			}
		}else {
			for (int i = 0; i < list.size(); i++) {
				FairwayObject fairwayObject = list.get(i);
				AerialPhotoBean aerialPhotoBeanTemp = fairwayObject.getAerialPhotoBean();
				aerialPhotoBeans.add(aerialPhotoBeanTemp);
			}
		}
		return aerialPhotoBeans;
	}
	public void cleanCacheDate(Context context,UserScoreCardObject scoreBean){
		DataBaseSingle.getInstance(context).getDataBase().UserScoreCardObject(context, jsonUserScoreCardObject(scoreBean));
		setPlayGameObject( null);
		ActivityManagerUtil.getScreenManager().popActivityToSomeActivity(PlayTabActivity.class);
	}
	private UserScoreCardObject jsonUserScoreCardObject(UserScoreCardObject scoreBean){
		GeneralScoreBean generalScoreBean = scoreBean.getGeneralScoreBean();
		int mySelfScore = 0;
		for (int i = 0; i < generalScoreBean.getScores().length; i++) {
			mySelfScore += generalScoreBean.getScores()[i];
		}
		generalScoreBean.setMySelfScore(mySelfScore);
		scoreBean.setScoreCard(JsonProcessUtil.toJSON(generalScoreBean));
		return scoreBean;
	}
	public UserScoreCardObject updateUserScoreCard(Context context,UserScoreCardObject scoreBean){
		DataBaseSingle.getInstance(context).getDataBase().updateScoreCard(jsonUserScoreCardObject(scoreBean));
		return scoreBean;
	}
	
}
