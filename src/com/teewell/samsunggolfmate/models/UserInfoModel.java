package com.teewell.samsunggolfmate.models;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.activities.RegisterLoginActivity;
import com.teewell.samsunggolfmate.activities.UserInfoActivity;
import com.teewell.samsunggolfmate.beans.GolferObject;
import com.teewell.samsunggolfmate.beans.UserInfoObject;
import com.teewell.samsunggolfmate.beans.UserObject;
import com.teewell.samsunggolfmate.common.Contexts.ServerURL;
import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
import com.teewell.samsunggolfmate.exceptions.UserException;
import com.teewell.samsunggolfmate.utils.Base64;
import com.teewell.samsunggolfmate.utils.JsonProcessUtil;
import com.teewell.samsunggolfmate.utils.MD5;
import com.teewell.samsunggolfmate.utils.SharedPreferencesUtils;
import com.teewell.samsunggolfmate.utils.VolleyUitl;

/**
 * @Project GolfMate
 * @package com.teewell.golfmate.models
 * @title UserInfoModel.java
 * @Description 用户登录、修改密码、获取用户信息、注销时调用此类。 该类主要负责生成HTTP请求对象，执行异步请求
 *              ，并通过回调对象解析服务器返回的数据。
 * @author DarkWarlords
 * @date 2013-1-21 下午3:11:36
 * @Copyright Copyright(C) 2013-1-21
 * @version 1.0.0
 */
@SuppressLint("SimpleDateFormat")
public class UserInfoModel {

	private static final String TAG = UserInfoModel.class.getName();

	private static final String SEND_VERIFY_CODE_NAME = "name";
	private static final String SEND_VERIFY_CODE_METHOD = "method";
	public static final String SEND_VERIFY_CODE_VERIFY_USER = "verifyUser";

	private static final String REGISTER_NAME = "name";
	private static final String REGISTER_PASSWORD = "password";
	private static final String REGISTER_VERIFY_CODE = "verifyCode";
	private static final String REGISTER_TIME_LENGTH = "timeLength";
	private static final long TIME_LENGTH = 90 * 1000 * 10;

	private static final String REGISTER_COMMON_PASSWORD = "8888";

	private static final String REQUEST_LABEL = "request";

	private static final String RESULT_LABEL_DATA = "data";
	private static final String RESULT_LABEL_RETURNCODE = "returnCode";
	private static final String RESULT_LABEL_USERID = "userId";
	private static final String RESULT_LABEL_USERNAME = "userName";
	private static final String RESULT_LABEL_USEREMAIL = "userEmail";
	private static final String RESULT_LABEL_USERMOBILENUMBER = "mobileNumber";
	private static final String RESULT_LABEL_USERBIRTHAY = "userBirthday";
	private static final String RESULT_LABEL_USERSEX = "userSex";
	private static final String RESULT_LABEL_USERNSIGN = "userSign";

	private static final int SUCCESS_RETURNCODE = 1;

	Handler uiHandler;

	public static final String USERINFO_KEY = "userInfo";
	private static final String USERINFO_LOGGEDIN_KEY = "loggedIn";
	private static final String USERINFO_CREDENTIAL_KEY = "credential";
	private static final String USERINFO_DATAKEY = "dataKey";
	private static final String USERINFO_USERNAME = "userName";

	private static final String USERINFO_DATEKEY_AND_HASH_CODE = "%1$s?t=%2$s&hash=%3$s";

	private boolean loggedIn = false;
	private String credential;
	private String dataKey;
	private String userName;
	/**
	 * 登录名
	 */
	public static final String REQUEST_LABEL_LOGIN_NAME = "loginName";
	/**
	 * 登录密码
	 */
	public static final String REQUEST_LABEL_PASSWORD = "password";
	/**
	 * 登录方式（可根据enum LoginMethod赋值）
	 */
	public static final String REQUEST_LABEL_LOGIN_METHOD = "loginMethod";
	/**
	 * 访问方式（可根据enum ClientType赋值）
	 */
	public static final String REQUEST_LABEL_CLIENT_TYPE = "clientType";

	public static final String REQUEST_LABEL_LOGIN_CREDENTIAL = "credential";

	/**
	 * 用户登录
	 */
	public static final String ACTION_LOGIN = ServerURL.MODEL_IFACE
			+ "login.action";
	public static final String ACTION_LOGINOUT = ServerURL.MODEL_IFACE
			+ "logout.action";
	public static final String ACTION_LOGIN_CREDENTIAL = ServerURL.MODEL_IFACE
			+ "loginCredential.action";
	public static final String ACTION_SEND_VERIFY_CODE = ServerURL.MODEL_IFACE
			+ "sendVerifyCode.action";
	public static final String ACTION_REGISTER_WITH_VERIFY_CODE = ServerURL.MODEL_IFACE
			+ "registerWithVerifyCode.action";
	public static final String ACTION_GET_USER_INFO = ServerURL.MODEL_IFACE
			+ "getUserInfo.action";
	public static final String ACTION_MODIFY_USER_INFO = ServerURL.MODEL_IFACE
			+ "modifyUserInfo.action";

	private int succeed_message_id;
	private int failed_message_id;

	private static UserInfoModel INSTANCE = null;

	Context applicationContext = null;

	private UserInfoModel(Context activity) {
		applicationContext = activity.getApplicationContext();

		reloadUserInfo();
	}

	public boolean hasLoggedIn() {
		return loggedIn;
	}

	private void reloadUserInfo() {
		SharedPreferencesUtils shareUtils = new SharedPreferencesUtils(
				applicationContext);
		String key = applicationContext.getSharedPreferences(
				SharedPreferenceConstant.SHAREDPREFERENCE_NAME, 0).getString(
				SharedPreferenceConstant.SHAREDPREFERENCE_NAME,
				SharedPreferenceConstant.DEFAULT_USER_SHAREDPREFERENCE_NAME);
		if (!SharedPreferenceConstant.DEFAULT_USER_SHAREDPREFERENCE_NAME
				.equals(key)) {
			loggedIn = shareUtils.getBoolean(USERINFO_LOGGEDIN_KEY, false);
			credential = shareUtils.getString(USERINFO_CREDENTIAL_KEY, "");
			dataKey = shareUtils.getString(USERINFO_DATAKEY, "");
			userName = shareUtils.getString(USERINFO_USERNAME, "");
			userId = shareUtils.getInt(RESULT_LABEL_USERID, 0);
		}
	}

	private void saveUserInfo() {
		SharedPreferencesUtils shareUtils = new SharedPreferencesUtils(
				applicationContext);
		shareUtils.commitBoolean(USERINFO_LOGGEDIN_KEY, loggedIn);
		shareUtils.commitString(USERINFO_CREDENTIAL_KEY, credential);
		shareUtils.commitString(USERINFO_DATAKEY, dataKey);
		shareUtils.commitString(USERINFO_USERNAME, userName);
		shareUtils.commitInt(RESULT_LABEL_USERID, userId);
	}

	public String appendUrlWithDateKeyAndHashCode(String urlString,
			String postString) {

		String newUrl = null;
		// build timeStamp
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		String timeStamp = dateFormat.format(new Date());
		try {
			StringBuffer postBuffer = new StringBuffer();

			String postStringBase64 = Base64.encode(postString
					.getBytes("UTF-8"));
			postBuffer.append(postStringBase64);

			if (loggedIn) {
				postBuffer.append(dataKey);
			}
			postBuffer.append(timeStamp);

			String hashMD5 = MD5.Encode16(postBuffer.toString());

			newUrl = String.format(USERINFO_DATEKEY_AND_HASH_CODE, urlString,
					timeStamp, hashMD5);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace(System.err);
		}

		return newUrl;
	}

	public String appendUrlWithNoDateKeyAndHashCode(String urlString,
			String postString) {

		String newUrl = null;
		// build timeStamp
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		String timeStamp = dateFormat.format(new Date());
		try {
			StringBuffer postBuffer = new StringBuffer();

			// only for post request
			String postStringBase64 = Base64.encode(postString
					.getBytes("UTF-8"));
			postBuffer.append(postStringBase64);

			postBuffer.append(timeStamp);
			String hashMD5 = MD5.Encode16(postBuffer.toString());

			newUrl = String.format(USERINFO_DATEKEY_AND_HASH_CODE, urlString,
					timeStamp, hashMD5);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace(System.err);
		}

		return newUrl;
	}

	public static UserInfoModel getInstance(Context activity) {
		if (null == INSTANCE) {
			INSTANCE = new UserInfoModel(activity);
		}
		return INSTANCE;
	}

	public void cancelHandlerOperation() {
		if (uiHandler != null) {
			VolleyUitl.getInstence(applicationContext).cancelAll(uiHandler);
			uiHandler = null;
		}
	}

	// send verify code
	// ==================================================================================================================================
	private HashMap<String, String> params = new HashMap<String, String>();
	public void sendVerifyCode(Handler handler, String phoneNumber) {
	
		cancelHandlerOperation();
	
		// save the uiHandler
		uiHandler = handler;
	
		JSONObject object = new JSONObject();
		try {
			object.put(SEND_VERIFY_CODE_NAME, phoneNumber);
			object.put(SEND_VERIFY_CODE_METHOD, UserObject.MOBILE);

			String urlString = appendUrlWithDateKeyAndHashCode(
					ACTION_SEND_VERIFY_CODE, object.toString());

			System.out.println(urlString + object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, sendVerifyCodeListener, sendverifyCodeErrorListener, uiHandler);

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private Listener<String> sendVerifyCodeListener = new Listener<String>() {



		@Override
		public void onResponse(String response) {
			// TODO Auto-generated method stub
			try {
				int returnCode = new JSONObject(response)
						.optInt(RESULT_LABEL_RETURNCODE);
				switch (returnCode) {
				case SUCCESS_RETURNCODE:
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(RegisterLoginActivity.MSG_SEND_VERIFY_CODE_SUCCEED);
					break;
				default:
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(RegisterLoginActivity.MSG_SEND_VERIFY_CODE_FAILED);
					System.out.println(response);
					break;
				}
			} catch (JSONException e) {
				if (uiHandler != null)
					uiHandler
							.sendEmptyMessage(RegisterLoginActivity.MSG_SEND_VERIFY_CODE_FAILED);
				e.printStackTrace();
				System.out.println(response);
			}
		}

	};
	private ErrorListener sendverifyCodeErrorListener = new ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError error) {
			// TODO Auto-generated method stub
			if (uiHandler != null)
				uiHandler
						.sendEmptyMessage(RegisterLoginActivity.MSG_SEND_VERIFY_CODE_BAD_NETWORK);
		}
	};
	// register
	// ==================================================================================================================================

	public void registerWithVerifyCode(Handler handler, String phoneNumber,
			String verifyCode) {
	
		cancelHandlerOperation();
	
		// save the uiHandler
		uiHandler = handler;
	
		JSONObject object = new JSONObject();
		try {
			object.put(REGISTER_NAME, phoneNumber);
			object.put(REGISTER_VERIFY_CODE, verifyCode);
			object.put(REGISTER_PASSWORD, REGISTER_COMMON_PASSWORD);
			object.put(REGISTER_TIME_LENGTH, TIME_LENGTH);

			String urlString = appendUrlWithDateKeyAndHashCode(
					ACTION_REGISTER_WITH_VERIFY_CODE, object.toString());

			System.out.println(urlString + object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, registerWithVerifyListener, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(RegisterLoginActivity.MSG_REGISTER_LOGIN_BAD_NETWORK);
				}
			}, uiHandler);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


	private Listener<String> registerWithVerifyListener = new Listener<String>() {

		


		@Override
		public void onResponse(String response) {
			// TODO Auto-generated method stub
			try {
				int returnCode = new JSONObject(response)
						.optInt(RESULT_LABEL_RETURNCODE);
				switch (returnCode) {
				case SUCCESS_RETURNCODE:
				case UserException.USERNAME_IS_EXIST:
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(RegisterLoginActivity.MSG_LOGIN_START);
					break;
				default:
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(RegisterLoginActivity.MSG_REGISTER_LOGIN_FAILED);
					break;
				}
			} catch (JSONException e) {
				if (uiHandler != null)
					uiHandler
							.sendEmptyMessage(RegisterLoginActivity.MSG_REGISTER_LOGIN_FAILED);
				Log.e(TAG, response);
			}
		}
	};

	// login credential
	// ==================================================================================================================================

	private Listener<String> loginCredentialListener = new Listener<String>() {



		@Override
		public void onResponse(String response) {
			// TODO Auto-generated method stub
			try {
				JSONObject jsonObject = new JSONObject(response);
				int returnCode = jsonObject.optInt(RESULT_LABEL_RETURNCODE);
				if (returnCode == SUCCESS_RETURNCODE) {
					jsonObject = jsonObject.optJSONObject(RESULT_LABEL_DATA);

					dataKey = jsonObject.optString(USERINFO_DATAKEY);
					saveUserInfo();

					if (uiHandler != null)
						uiHandler.sendEmptyMessage(succeed_message_id);
				} else {
					if (uiHandler != null)
						uiHandler.sendEmptyMessage(failed_message_id);
					System.out.println(response);
				}
			} catch (JSONException e) {
				if (uiHandler != null)
					uiHandler.sendEmptyMessage(failed_message_id);
				e.printStackTrace();
			}
		}

	};

	public boolean loginCredential(Handler handler, Integer succeed_msg,
			Integer failed_msg) {

		if (!loggedIn) {
			return false;
		}

		if (null == handler) {
			return false;
		}
		cancelHandlerOperation();

		// save the uiHandler
		uiHandler = handler;

		// save message
		succeed_message_id = succeed_msg;
		failed_message_id = failed_msg;

		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_LOGIN_NAME, userName);
			object.put(REQUEST_LABEL_LOGIN_CREDENTIAL, credential);
			object.put(REQUEST_LABEL_LOGIN_METHOD, UserObject.LOGIN_ANY);
			object.put(REQUEST_LABEL_CLIENT_TYPE, UserObject.LOGIN_ANY);

			String urlString = appendUrlWithNoDateKeyAndHashCode(
					ACTION_LOGIN_CREDENTIAL, object.toString());

			System.out.println(urlString + object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, loginCredentialListener, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(RegisterLoginActivity.MSG_REGISTER_LOGIN_BAD_NETWORK);
				}
			}, uiHandler);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return true;
	}

	protected int userId;

	// login
	// ==================================================================================================================================

	private Listener<String> loginListener = new Listener<String>()  {

		@Override
		public void onResponse(String response) {
			// TODO Auto-generated method stub
			try {
				JSONObject jsonObject = new JSONObject(response);
				int returnCode = jsonObject.optInt(RESULT_LABEL_RETURNCODE);
				if (returnCode == SUCCESS_RETURNCODE) {
					jsonObject = jsonObject.optJSONObject(RESULT_LABEL_DATA);
					userId = jsonObject.optInt(RESULT_LABEL_USERID);
					credential = jsonObject.optString(USERINFO_CREDENTIAL_KEY);
					dataKey = jsonObject.optString(USERINFO_DATAKEY);
					loggedIn = true;
					applicationContext
							.getSharedPreferences(
									SharedPreferenceConstant.SHAREDPREFERENCE_NAME,
									0)
							.edit()
							.putString(
									SharedPreferenceConstant.SHAREDPREFERENCE_NAME,
									userId + "").commit();

					saveUserInfo();
					reloadUserInfo();
					UserScoreCardModel.getInstance(applicationContext)
							.changeUserDataBase();

					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(RegisterLoginActivity.MSG_REGISTER_LOGIN_SUCCEED);
				} else {
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(RegisterLoginActivity.MSG_REGISTER_LOGIN_FAILED);
					System.out.println(response);
				}
			} catch (JSONException e) {
				if (uiHandler != null)
					uiHandler
							.sendEmptyMessage(RegisterLoginActivity.MSG_REGISTER_LOGIN_BAD_NETWORK);
				e.printStackTrace();
			}
		}

	};

	public void login(Handler handler, String phoneNumber) {
		cancelHandlerOperation();
		// save the uiHandler

		uiHandler = handler;
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_LOGIN_NAME, phoneNumber);
			object.put(REQUEST_LABEL_PASSWORD, REGISTER_COMMON_PASSWORD);
			object.put(REQUEST_LABEL_LOGIN_METHOD, UserObject.LOGIN_ANY);
			object.put(REQUEST_LABEL_CLIENT_TYPE, UserObject.LOGIN_ANY);

			String urlString = appendUrlWithDateKeyAndHashCode(ACTION_LOGIN,
					object.toString());

			System.out.println(urlString);
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, loginListener, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(RegisterLoginActivity.MSG_REGISTER_LOGIN_BAD_NETWORK);
				}
			}, uiHandler);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// save the phone number
		userName = phoneNumber;
	}

	// loginout
	// ==================================================================================================================================

	private Listener<String> loginOutListener = new Listener<String>() {

		@Override
		public void onResponse(String response) {
			// TODO Auto-generated method stub
			try {
				JSONObject jsonObject = new JSONObject(response);
				int returnCode = jsonObject.optInt(RESULT_LABEL_RETURNCODE);
				if (returnCode == SUCCESS_RETURNCODE) {
					// userId = jsonObject.optInt(RESULT_LABEL_USERID);
					credential = "";
					dataKey = "";
					loggedIn = false;

					saveUserInfo();

					UserScoreCardModel.getInstance(applicationContext)
							.changeUserDataBase();
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(UserInfoActivity.MSG_LOGINOUT_SUCCEED);
				} else {
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(UserInfoActivity.MSG_LOGINOUT_FAILED);
					System.out.println(response);
				}
			} catch (JSONException e) {
				if (uiHandler != null)
					uiHandler
							.sendEmptyMessage(UserInfoActivity.MSG_BAD_NETWORK);
				e.printStackTrace();
			}
		}

	};

	public void loginOut(Handler handler) {
		cancelHandlerOperation();
		// save the uiHandler
		uiHandler = handler;
		JSONObject object = new JSONObject();
		String urlString = appendUrlWithDateKeyAndHashCode(ACTION_LOGINOUT,
				object.toString());
		System.out.println(urlString);
		params.clear();
		params.put(REQUEST_LABEL, object.toString());
		VolleyUitl.getInstence(applicationContext).Post(urlString, params, loginOutListener, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				if (uiHandler != null)
					uiHandler
							.sendEmptyMessage(UserInfoActivity.MSG_BAD_NETWORK);
			}
		}, uiHandler);
	}


	private Listener<String> getUserInfoListener = new Listener<String>() {


		@Override
		public void onResponse(String response) {
			// TODO Auto-generated method stub
			try {
				Log.i(TAG, response);
				JSONObject retJsonObject = new JSONObject(response);
				int returnCode = retJsonObject.optInt(RESULT_LABEL_RETURNCODE);
				switch (returnCode) {
				case SUCCESS_RETURNCODE:
					Message msg = new Message();
					msg.what = UserInfoActivity.MSG_GET_USER_INFO_SUCCEED;
					String dataString = retJsonObject
							.optString(RESULT_LABEL_DATA);
					UserInfoObject userInfoObject = (UserInfoObject) JsonProcessUtil
							.fromJSON(dataString, UserInfoObject.class);
					msg.obj = userInfoObject;
					SharedPreferencesUtils shareUtils = new SharedPreferencesUtils(
							applicationContext);
					String infoString = shareUtils
							.getString(
									SharedPreferenceConstant.SHAREDPREFERENCE_GOLFEROBJECT,
									null);
					GolferObject golferObject = null;
					if (infoString != null && !"null".equals(infoString)) {
						golferObject = (GolferObject) JsonProcessUtil.fromJSON(
								infoString, GolferObject.class);
					}
					if (golferObject == null) {
						golferObject = new GolferObject();
						golferObject.setUserInfoObject(userInfoObject);
					}
					shareUtils
							.commitString(
									SharedPreferenceConstant.SHAREDPREFERENCE_GOLFEROBJECT,
									JsonProcessUtil.toJSON(golferObject));
					if (uiHandler != null) {
						uiHandler.sendMessage(msg);
					}
					break;
				case UserException.USER_OFFLINE: {
					if (!loginCredential(uiHandler,
							UserInfoActivity.MSG_GET_USER_INFO_START,
							UserInfoActivity.MSG_GET_USER_INFO_FAILED)) {
						if (uiHandler != null) {
							uiHandler
									.sendEmptyMessage(UserInfoActivity.MSG_GET_USER_INFO_FAILED);
						}
					}

				}
					break;
				default:
					if (uiHandler != null) {
						uiHandler
								.sendEmptyMessage(UserInfoActivity.MSG_GET_USER_INFO_FAILED);
					}
					Log.i(TAG, response);
					break;
				}
			} catch (JSONException e) {
				if (uiHandler != null) {
					uiHandler
							.sendEmptyMessage(UserInfoActivity.MSG_GET_USER_INFO_FAILED);
				}
				e.printStackTrace();
				System.out.println(response);
			}
		}
	};

	// get current user info
	// ==================================================================================================================================
	public boolean startGetCurrentUserInfo(Handler handler) {

		if (!loggedIn) {
			return false;
		}
		cancelHandlerOperation();

		// save the uiHandler
		uiHandler = handler;
		int userId = getUserId();
		JSONObject object = new JSONObject();

		try {
			object.put(RESULT_LABEL_USERID, userId);
			String urlString = appendUrlWithDateKeyAndHashCode(
					ACTION_GET_USER_INFO, object.toString());
			System.out.println(urlString + object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, getUserInfoListener, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(UserInfoActivity.MSG_GET_USER_INFO_FAILED);
				}
			}, uiHandler);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}


	private Listener<String> modifyUserInfoListener = new Listener<String>() {


		@Override
		public void onResponse(String response) {
			// TODO Auto-generated method stub
			try {
				JSONObject retJsonObject = new JSONObject(response);
				int returnCode = retJsonObject.optInt(RESULT_LABEL_RETURNCODE);
				switch (returnCode) {
				case SUCCESS_RETURNCODE:
					Message msg = new Message();
					msg.what = UserInfoActivity.MSG_MODIFY_USER_INFO_SUCCEED;
					if (uiHandler != null) {
						uiHandler.sendMessage(msg);
					}
					break;
				case UserException.USER_OFFLINE: {
					if (!loginCredential(uiHandler,
							UserInfoActivity.MSG_RELOGIN_SUCCESSED,
							UserInfoActivity.MSG_MODIFY_USER_INFO_FAILED)) {
						if (uiHandler != null) {
							uiHandler
									.sendEmptyMessage(UserInfoActivity.MSG_MODIFY_USER_INFO_FAILED);
						}
					}

				}
					break;
				default:
					if (uiHandler != null) {
						uiHandler
								.sendEmptyMessage(UserInfoActivity.MSG_MODIFY_USER_INFO_FAILED);
					}
					Log.i(TAG, response);
					break;
				}
			} catch (JSONException e) {
				if (uiHandler != null) {
					uiHandler
							.sendEmptyMessage(UserInfoActivity.MSG_MODIFY_USER_INFO_FAILED);
				}
				e.printStackTrace();
				System.out.println(response);
			}
		}
	};

	// modify userInfo
	// ==================================================================================================================================
	public boolean modifyUserInfo(Handler handler, UserInfoObject userInfoObject) {

		if (!loggedIn) {
			return false;
		}
		cancelHandlerOperation();

		// save the uiHandler
		uiHandler = handler;
		JSONObject object = new JSONObject();

		try {
			object.put(RESULT_LABEL_USERID, userId);
			object.put(RESULT_LABEL_USERNAME, userInfoObject.getUserName());
			object.put(RESULT_LABEL_USERSEX, userInfoObject.getUserSex());
			object.put(RESULT_LABEL_USERBIRTHAY,
					userInfoObject.getUserBirthday());
			object.put(RESULT_LABEL_USERMOBILENUMBER,
					userInfoObject.getMobileNumber());
			object.put(RESULT_LABEL_USEREMAIL, userInfoObject.getUserEmail());
			object.put(RESULT_LABEL_USERNSIGN, userInfoObject.getUserSign());
			String urlString = appendUrlWithDateKeyAndHashCode(
					ACTION_MODIFY_USER_INFO, object.toString());
			System.out.println(urlString + object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, modifyUserInfoListener, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					if (uiHandler != null)
						uiHandler
								.sendEmptyMessage(UserInfoActivity.MSG_MODIFY_USER_INFO_FAILED);
				}
			}, uiHandler);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public String getCurrentUserName() {
		if (loggedIn) {
			return userName;
		} else {
			return null;
		}
	}

	public int getUserId() {
		if (loggedIn) {
			return userId;
		}
		return 0;
	}

	public GolferObject getGolferObject() {
		SharedPreferencesUtils shareUtils = new SharedPreferencesUtils(
				applicationContext);
		String infoString = shareUtils.getString(
				SharedPreferenceConstant.SHAREDPREFERENCE_GOLFEROBJECT, null);
		GolferObject golferObject = null;
		if (infoString != null && !"null".equals(infoString)) {

			golferObject = (GolferObject) JsonProcessUtil.fromJSON(infoString,
					GolferObject.class);
		}
		if (golferObject == null) {
			golferObject = new GolferObject();
			UserInfoObject userInfoObject = new UserInfoObject();
			userInfoObject.setUserName(applicationContext
					.getString(R.string.self));
			golferObject.setUserInfoObject(userInfoObject);
			shareUtils.commitString(
					SharedPreferenceConstant.SHAREDPREFERENCE_GOLFEROBJECT,
					JsonProcessUtil.toJSON(golferObject));
		}
		return golferObject;
	}

	public void saveGolferObject(GolferObject golferObject) {
		SharedPreferencesUtils shareUtils = new SharedPreferencesUtils(
				applicationContext);
		shareUtils.commitString(
				SharedPreferenceConstant.SHAREDPREFERENCE_GOLFEROBJECT,
				JsonProcessUtil.toJSON(golferObject));
	}

	public static final String[] sexStrings = { "男", "女" };

	public String getSexString(int userSex) {
		if (userSex >= sexStrings.length) {
			userSex = 0;
		}
		return sexStrings[userSex];
	}

	public Integer getSexInteger(String sexString) {
		if (!"".equals(sexString)) {
			if (sexStrings[0].equals(sexString)) {
				return 0;
			}
			return 1;
		}
		return null;
	}
}
