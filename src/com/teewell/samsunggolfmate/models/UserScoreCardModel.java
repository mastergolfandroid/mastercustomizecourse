package com.teewell.samsunggolfmate.models;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.teewell.samsunggolfmate.activities.HistoryScorecardActivity;
import com.teewell.samsunggolfmate.activities.UserInfoActivity;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;
import com.teewell.samsunggolfmate.common.Contexts.ServerURL;
import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
import com.teewell.samsunggolfmate.datebase.DataBaseSingle;
import com.teewell.samsunggolfmate.datebase.MyDataBaseAdapter;
import com.teewell.samsunggolfmate.exceptions.UserException;
import com.teewell.samsunggolfmate.utils.SharedPreferencesUtils;
import com.teewell.samsunggolfmate.utils.VolleyUitl;


/**  
 * @Project GolfMate
 * @package com.teewell.golfmate.models
 * @title UserScoreCardModel.java 
 * @Description TODO
 * @author DarkWarlords
 * @date 2013-1-21 上午10:30:50
 * @Copyright Copyright(C) 2013-1-21
 * @version 1.0.0
 */
@SuppressLint("SimpleDateFormat")
public class UserScoreCardModel {
	private SimpleDateFormat df = new SimpleDateFormat("MM-dd HH:mm");
	@SuppressWarnings("unused")
	private static final String TAG = UserScoreCardModel.class.getName();
	private static final String REQUEST_LABEL = "request";
	private static final String REQUEST_LABEL_SCORECARD = "scoreCard";
	private static final String REQUEST_LABEL_SAVETIME = "saveTime";
	private static final String REQUEST_LABEL_CHANGETIME = "changeTime";
	private static final String REQUEST_LABEL_ID = "id";
	private static final String REQUEST_LABEL_VERSION = "version";
	private static final String REQUEST_LABEL_TAG = "tag";
	private static final String RESULT_LABEL_DATA = "data";
	private static final String RESULT_LABEL_USERSCORECARDS = "userScoreCards";
	private static final String RESULT_LABEL_RETURNCODE = "returnCode";
	private static final int SUCCESS_RETURNCODE = 1;
	/**
	 * 上传记分卡
	 */
	public static final String ACTION_ADD_SCORECARD = ServerURL.MODEL_DATA_SYNC+ "addScoreCard.action";
	public static final String ACTION_OBTIAN_SCORECARD = ServerURL.MODEL_DATA_SYNC+ "obtianScorecard.action";
	public static final String ACTION_DOWNLOAD_FILE = ServerURL.MODEL_DATA_SYNC+ "downloadFile.action";
	public static final String ACTION_MODIFY_SCORECARD = ServerURL.MODEL_DATA_SYNC+ "modifyScoreCard.action";
	public static final String ACTION_DEL_SCORECARD = ServerURL.MODEL_DATA_SYNC+ "delScoreCard.action";
	private Context applicationContext = null;
	private static UserScoreCardModel INSTANCE;
//	private SharedPreferencesUtils utils;
	private Handler scoreCardHandler;
	private HashMap<String, String> params =new HashMap<String,String>();
//	=======================================================================================
	private UserScoreCardModel(Context context){
		applicationContext = context.getApplicationContext();
//		utils = new SharedPreferencesUtils(context);
	}
	public static UserScoreCardModel getInstance(Context activity){
		if (null == INSTANCE) {
			INSTANCE = new UserScoreCardModel(activity);
		}
		return INSTANCE;
	}
	private void cancelHandlerOperation(){
		if (scoreCardHandler != null) {
			VolleyUitl.getInstence(applicationContext).cancelAll(scoreCardHandler);
		}
	}
//	======================================================================================
	public void addScoreCard(Handler handler,final UserScoreCardObject object){
		cancelHandlerOperation();
		scoreCardHandler = handler;
		JSONObject jsobject = new JSONObject();
		try {
			jsobject.put(REQUEST_LABEL_SCORECARD, object.getScoreCard());
			jsobject.put(REQUEST_LABEL_SAVETIME, object.getSaveTime());
			jsobject.put(REQUEST_LABEL_CHANGETIME, object.getChangeTime());
			params.clear();
			params.put(REQUEST_LABEL, jsobject.toString());
			String urlString = UserInfoModel.getInstance(applicationContext).appendUrlWithDateKeyAndHashCode(ACTION_ADD_SCORECARD, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, new Listener<String>() {

				@Override
				public void onResponse(String response) {
					// TODO Auto-generated method stub
					try {
						JSONObject jsonObject = new JSONObject(response);
						int returnCode = jsonObject.optInt(RESULT_LABEL_RETURNCODE);
						if (returnCode == SUCCESS_RETURNCODE) {
							Message message = new Message();
							message.what = HistoryScorecardActivity.WHAT_ADDSCORECARD_SUCCEED;
							message.arg1 = object.getId();
							scoreCardHandler.sendMessage(message);
						}else if (returnCode == UserException.USER_OFFLINE) {
							if (!UserInfoModel.getInstance(applicationContext).loginCredential(scoreCardHandler, UserInfoActivity.MSG_GET_USER_INFO_START, UserInfoActivity.MSG_GET_USER_INFO_FAILED)) {
								if (scoreCardHandler != null) {
									scoreCardHandler.sendEmptyMessage(UserInfoActivity.MSG_GET_USER_INFO_FAILED);
								}
							}
						}else {
							scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_SYNCHRONIZATION_FAILED);
							System.out.println(response);
						}
					} catch (JSONException e) {
						scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_SYNCHRONIZATION_FAILED);
						e.printStackTrace();
					}
				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_BAD_NETWORK);
				}
			}, scoreCardHandler);
			
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}
//	======================================================================================
	public void modifyScoreCard(Handler handler,String scoreCard,int id,String changeTime){
		cancelHandlerOperation();
		scoreCardHandler = handler;
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_SCORECARD, scoreCard);
			object.put(REQUEST_LABEL_ID, id);
			object.put(REQUEST_LABEL_CHANGETIME, changeTime);
			String urlString = UserInfoModel.getInstance(applicationContext).appendUrlWithDateKeyAndHashCode(ACTION_MODIFY_SCORECARD, object.toString());
			System.out.println(urlString+object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, new Listener<String>() {

				@Override
				public void onResponse(String response) {
					// TODO Auto-generated method stub
					try {
						JSONObject jsonObject = new JSONObject(response);
						int returnCode = jsonObject.optInt(RESULT_LABEL_RETURNCODE);
						if (returnCode == SUCCESS_RETURNCODE) {
							scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_MODIFYSCORECARD_SUCCEED);
						}else if (returnCode == UserException.USER_OFFLINE) {
							if (!UserInfoModel.getInstance(applicationContext).loginCredential(scoreCardHandler, UserInfoActivity.MSG_GET_USER_INFO_START, UserInfoActivity.MSG_GET_USER_INFO_FAILED)) {
								if (scoreCardHandler != null) {
									scoreCardHandler.sendEmptyMessage(UserInfoActivity.MSG_GET_USER_INFO_FAILED);
								}
							}
						}else {
							scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_SYNCHRONIZATION_FAILED);
							System.out.println(response);
						}
					} catch (JSONException e) {
						scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_SYNCHRONIZATION_FAILED);
						e.printStackTrace();
					}
				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_BAD_NETWORK);
				}
			}, scoreCardHandler);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}
//	======================================================================================
	public void deleteScoreCard(Handler handler,final int id,String changeTime){
		cancelHandlerOperation();
		scoreCardHandler = handler;
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_ID, id);
			object.put(REQUEST_LABEL_CHANGETIME, changeTime);
			String urlString = UserInfoModel.getInstance(applicationContext).appendUrlWithDateKeyAndHashCode(ACTION_DEL_SCORECARD, object.toString());
			System.out.println(urlString+object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, new Listener<String>() {

				@Override
				public void onResponse(String response) {
					// TODO Auto-generated method stub
					try {
						JSONObject jsonObject = new JSONObject(response);
						int returnCode = jsonObject.optInt(RESULT_LABEL_RETURNCODE);
						if (returnCode == SUCCESS_RETURNCODE) {
							Message message = new Message();
							message.what = HistoryScorecardActivity.WHAT_DELETESCORECARD_SUCCEED;
							message.arg1 = id;
							scoreCardHandler.sendMessage(message);
						}else if (returnCode == UserException.USER_OFFLINE) {
							if (!UserInfoModel.getInstance(applicationContext).loginCredential(scoreCardHandler, UserInfoActivity.MSG_GET_USER_INFO_START, UserInfoActivity.MSG_GET_USER_INFO_FAILED)) {
								if (scoreCardHandler != null) {
									scoreCardHandler.sendEmptyMessage(UserInfoActivity.MSG_GET_USER_INFO_FAILED);
								}
							}
						}else {
							scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_SYNCHRONIZATION_FAILED);
							System.out.println(response);
						}
					} catch (JSONException e) {
						scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_SYNCHRONIZATION_FAILED);
						e.printStackTrace();
					}
				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_BAD_NETWORK);
				}
			}, scoreCardHandler);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}
//	======================================================================================
	public void obtianScoreCard(Handler handler,int version){
		cancelHandlerOperation();
		scoreCardHandler = handler;
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_VERSION, version);
			String urlString = UserInfoModel.getInstance(applicationContext).appendUrlWithDateKeyAndHashCode(ACTION_OBTIAN_SCORECARD, object.toString());
			System.out.println(urlString+object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params, new Listener<String>() {

				@Override
				public void onResponse(String response) {
					// TODO Auto-generated method stub
					try {
						System.out.println(response);
						JSONObject json = new JSONObject(response);
						int returnCode = json.optInt(RESULT_LABEL_RETURNCODE);
						if (returnCode == SUCCESS_RETURNCODE) {
							JSONObject jsonObject = new JSONObject(json.optString(RESULT_LABEL_DATA));
//							JSONObject jsonObject = new JSONObject(response).optJSONObject(RESULT_LABEL_DATA);
//							tags = (List<String>) jsonObject.optJSONArray(RESULT_LABEL_TAGS);
							JSONArray jsonArray = jsonObject.optJSONArray(RESULT_LABEL_USERSCORECARDS);
							List<UserScoreCardObject>	userScoreCardObjects = new ArrayList<UserScoreCardObject>();
							for (int i = 0; i < jsonArray.length(); i++) {
								userScoreCardObjects.add(UserScoreCardObject.jsonUserScoreCardObject(jsonArray.optJSONObject(i))) ;
							}
							new SharedPreferencesUtils(applicationContext).commitString(SharedPreferenceConstant.SCORECARD_LAST_UPDATE_TIME, df.format(new Date()));
							Message message = new Message();
							message.what = HistoryScorecardActivity.WHAT_OBTIANSCORECARD_SUCCEED;
							message.obj = userScoreCardObjects;
							scoreCardHandler.sendMessage(message);
						}else if (returnCode == UserException.USER_OFFLINE) {
							if (!UserInfoModel.getInstance(applicationContext).loginCredential(scoreCardHandler, UserInfoActivity.MSG_GET_USER_INFO_START, UserInfoActivity.MSG_GET_USER_INFO_FAILED)) {
								if (scoreCardHandler != null) {
									scoreCardHandler.sendEmptyMessage(UserInfoActivity.MSG_GET_USER_INFO_FAILED);
								}
							}
						}else {
							scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_SYNCHRONIZATION_FAILED);
							System.out.println(response);
						}
					} catch (JSONException e) {
						scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_SYNCHRONIZATION_FAILED);
						e.printStackTrace();
					}
				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					// TODO Auto-generated method stub
					scoreCardHandler.sendEmptyMessage(HistoryScorecardActivity.WHAT_BAD_NETWORK);
				}
			}, scoreCardHandler);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}
	public static List<String> tags;
//	======================================================================================
	public synchronized void synchScoreCard(){
		
	}
	public static List<UserScoreCardObject> userScoreCardObjects ;
	public List<UserScoreCardObject> getScoreCardObjects(MyDataBaseAdapter mdb){
		userScoreCardObjects=mdb.selectScoreCardObjects("");
		return userScoreCardObjects;
	}
	public List<UserScoreCardObject> getScoreCardObjectsByKey(MyDataBaseAdapter mdb,String key){
		return mdb.selectScoreCardObjects(key);
	}
	/**
	 * 改变用户数据库
	 */
	public void changeUserDataBase(){
		DataBaseSingle.getInstance(applicationContext).modifyDataBase(applicationContext);
	}
}
