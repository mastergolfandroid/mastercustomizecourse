package com.teewell.samsunggolfmate.models.course;

import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;

public class AerialPhotoResponseListener implements Listener<Bitmap>,ErrorListener{
	@SuppressWarnings("unused")
	private static final String TAG = AerialPhotoResponseListener.class.getSimpleName();
	private String photoPathUrl;
	private Handler handler;
	private Message message;
	public AerialPhotoResponseListener(String photoPathUrl,Handler handler,Message message){
		this.handler = handler; 
		this.message = message; 
		this.photoPathUrl = photoPathUrl;
	}
	
	@Override
	public void onResponse(Bitmap response) {
		// TODO Auto-generated method stub
		try {
			if (response!=null) {
				
				FileOutputStream bos = new FileOutputStream(photoPathUrl);
				response.compress(Bitmap.CompressFormat.JPEG, 100, bos);
				response.isRecycled();
				bos.flush();
				bos.close();
				message.arg2 = AerialPhotoBean.DOWNLOAD_SUCCESSED;
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			handler.sendMessage(message);
		}
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		// TODO Auto-generated method stub
		handler.sendMessage(message);
	}


}
