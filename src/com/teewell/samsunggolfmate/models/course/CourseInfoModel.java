package com.teewell.samsunggolfmate.models.course;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.teewell.samsunggolfmate.activities.EvaluateActivity;
import com.teewell.samsunggolfmate.activities.FeedbackActivity;
import com.teewell.samsunggolfmate.activities.SituationActivity;
import com.teewell.samsunggolfmate.activities.UserInfoActivity;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;
import com.teewell.samsunggolfmate.beans.CoursesInfoBean;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.DownloadAerialPhotoBean;
import com.teewell.samsunggolfmate.beans.EvaluateObject;
import com.teewell.samsunggolfmate.beans.FairwayObject;
import com.teewell.samsunggolfmate.beans.ProvinceBean;
import com.teewell.samsunggolfmate.beans.SubCoursesObject;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.common.Contexts.ServerURL;
import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
import com.teewell.samsunggolfmate.datebase.MyDataBaseAdapter;
import com.teewell.samsunggolfmate.exceptions.UserException;
import com.teewell.samsunggolfmate.models.ComparatorProvinceCity;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.UserInfoModel;
import com.teewell.samsunggolfmate.models.UserScoreCardModel;
import com.teewell.samsunggolfmate.models.weather.WeatherImageResponseListener;
import com.teewell.samsunggolfmate.models.weather.WeatherInfoModel;
import com.teewell.samsunggolfmate.models.weather.WeatherInfoResponseListener;
import com.teewell.samsunggolfmate.utils.ComparatorCourses;
import com.teewell.samsunggolfmate.utils.FileUtil;
import com.teewell.samsunggolfmate.utils.JsonProcessUtil;
import com.teewell.samsunggolfmate.utils.SharedPreferencesUtils;
import com.teewell.samsunggolfmate.utils.VolleyUitl;
import com.teewell.samsunggolfmate.views.CourseListAdapter;
import com.teewell.samsunggolfmate.views.CourseListCellWrapper;
import com.teewell.samsunggolfmate.views.XListView;

/**
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.models
 * @title CourseInfoModel.java
 * @Description 球场数据模块 功能：周边球场列表、全国球场列表、球场logo、球场天气
 * @author Administrator
 * @date 2013-1-24 下午5:11:27
 * @Copyright Copyright(C) 2013-1-24
 * @version 1.0.0
 */
@SuppressLint("SimpleDateFormat")
public class CourseInfoModel {
	private static final String TAG = CourseInfoModel.class.getName();
	private static final String REQUEST_LABEL = "request";
	private static final String REQUEST_LABEL_COURSEID = "courseID";
	private static final String REQUEST_LABEL_FEED_BACKCOURSEID = "courseId";
	private static final String REQUEST_LABEL_EVALUATE = "evaluate";
	private static final String REQUEST_LABEL_DATETIME = "dateTime";
	private static final String RESULT_LABEL_DATA = "data";
	private static final String RESULT_LABEL_LOGOURL = "logoURL";
	private static final String RESULT_LABEL_COURSES = "courses";
	private static final String RESULT_LABEL_COURSESINFO = "coursesInfo";
	private static final String RESULT_LABEL_RETURNCODE = "returnCode";
	private static final int SUCCESS_RETURNCODE = 1;
	private final static String REQUEST_LABEL_USERID = "userId";
	private final static String REQUEST_LABEL_MESSAGE = "message";
	private static final long MODIFYTIME_MIN = 0L;
	private static final String ACTION_COURSES = ServerURL.MODEL_IFACE
			+ "obtianCourses.action";
	private static final String ACTION_SUBCOURSE = ServerURL.MODEL_IFACE
			+ "obtianSubCourses.action";
	public static final String ACTION_COURSEIMAGES = ServerURL.MODEL_IFACE
			+ "obtainCourseImagesByID.action";
	private static final String ACTION_COURSEINFO = ServerURL.MODEL_IFACE
			+ "obtainCoursesInfoByID.action";
	private static final String ACTION_FEEDBACK = ServerURL.MODEL_FEEDBACK
			+ "saveFeedbackInfo.action";
	private static final String ACTION_SAVEEVALUATE = ServerURL.MODEL_FEEDBACK
			+ "saveEvaluateInfo.action";
	private static final String ACTION_OBTAINEVALUATE = ServerURL.MODEL_FEEDBACK
			+ "obtianEvaluateInfo.action";

	private List<CoursesObject> courseList = null;

	public List<ProvinceBean> provincesBeans;

	// public static CourseInfoModel courseInfoModel = new CourseInfoModel();
	private Context applicationContext = null;
	private static CourseInfoModel INSTANCE;
	private Handler courseHandler;
	private SimpleDateFormat df = new SimpleDateFormat("MM-dd HH:mm");

	public static final int WHAT_SYNCDATA_SUCCEED = 201;
	public static final int WHAT_SYNCDATA_FAILED = -201;

	public static final int WHAT_GETSUBCOURSE_SUCCEED = 202;
	public static final int WHAT_GETSUBCOURSEINFO_SUCCEED = 203;
	public static final int WHAT_PROGRESS = 0x203;
	public static final int WHAT_FILD = -202;

	public static final int AROUND_COUNT = 20;

	private SharedPreferencesUtils sharedPreferencesUtils;
	private HashMap<String, String> params = new HashMap<String, String>();

	private SharedPreferencesUtils getSharedPreferencesUtils() {
		return sharedPreferencesUtils;
	}

	private CourseInfoModel(Context context) {
		applicationContext = context.getApplicationContext();
		sharedPreferencesUtils = new SharedPreferencesUtils(applicationContext,
				SharedPreferenceConstant.SHAREDPREFERENCE_NAME);
	}

	public static CourseInfoModel getInstance(Context activity) {
		if (null == INSTANCE) {
			INSTANCE = new CourseInfoModel(activity);
		}
		return INSTANCE;
	}

	private void cancelHandlerOperation() {
		if (courseHandler != null) {
			VolleyUitl.getInstence(applicationContext).cancelAll(courseHandler);
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoursesObject> getCourseList() {
		if (courseList == null) {
			String response = getSharedPreferencesUtils().getString(
					SharedPreferenceConstant.COURSESINFO, null);
			if (response != null) {
				courseList = ((List<CoursesObject>) FileUtil
						.getBase64Object(response));
			} else {
				return null;
			}
		}

		return courseList;

	}

	// ===================================================================================================
	public void startSyncCourseList(Handler handler) {
		cancelHandlerOperation();
		courseHandler = handler;
		long dateTime = getSharedPreferencesUtils().getLong(
				SharedPreferenceConstant.COURSE_MODIFYTIME, MODIFYTIME_MIN);
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_DATETIME, dateTime);
			String urlString = UserInfoModel.getInstance(applicationContext)
					.appendUrlWithNoDateKeyAndHashCode(ACTION_COURSES,
							object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params,
					new Listener<String>() {

						@SuppressWarnings("unchecked")
						@Override
						public void onResponse(String response) {
							// TODO Auto-generated method stub
							System.err.println(response);
							
							try {
								int returnCode = new JSONObject(response)
										.optInt(RESULT_LABEL_RETURNCODE);
								if (returnCode == SUCCESS_RETURNCODE) {
									JSONObject jsonObject;
									List<CoursesObject> coursesObjects = new ArrayList<CoursesObject>();
									long maxChangTime = getSharedPreferencesUtils()
											.getLong(
													SharedPreferenceConstant.COURSE_MODIFYTIME,
													MODIFYTIME_MIN);
									jsonObject = new JSONObject(response)
											.getJSONObject(RESULT_LABEL_DATA);
									String logoURL = jsonObject
											.optString(RESULT_LABEL_LOGOURL);
									JSONArray josArray = jsonObject
											.getJSONArray(RESULT_LABEL_COURSES);
									for (int i = 0; i < josArray.length(); i++) {
										CoursesObject object = CoursesObject
												.getCoursesObject((JSONObject) josArray
														.opt(i));
//										if (object.getPastFlag() != CoursesObject.VALID
//												&& maxChangTime == 0) {
//											continue;
//										}
										if (object.getCoursesName().indexOf("常平")>=0 || object.getCoursesName().indexOf("海逸")>=0 || object.getCoursesName().indexOf("峰景")>=0) {
											
											coursesObjects.add(object);
										}
									}
									// 此处主要用于首次及后期球场更新操作（球场列表同步）
									if (coursesObjects.size() > 0) {
										maxChangTime = coursesObjects.get(0)
												.getModifyTime();
										for (int j = 0; j < coursesObjects
												.size(); j++) {
											long temp = coursesObjects.get(j)
													.getModifyTime();
											if (maxChangTime <= temp) {
												maxChangTime = temp;
											}
										}
										List<CoursesObject> historyCoursesObjects = null;
										String info = getSharedPreferencesUtils()
												.getString(
														SharedPreferenceConstant.COURSESINFO,
														null);
										if (info != null) {
											historyCoursesObjects = (List<CoursesObject>) FileUtil
													.getBase64Object(info);
											for (int i = 0; i < coursesObjects
													.size(); i++) {
												CoursesObject newCoursesObject = coursesObjects
														.get(i);
												boolean isExist = false;
												for (int j = 0; j < historyCoursesObjects
														.size(); j++) {
													CoursesObject oldCoursesObject = coursesObjects
															.get(j);
													if (newCoursesObject
															.getCouseID() == oldCoursesObject
															.getCouseID()) {
														if (newCoursesObject
																.getPastFlag() == CoursesObject.VALID) {
															historyCoursesObjects
																	.set(j,
																			newCoursesObject);
														} else {
															historyCoursesObjects
																	.remove(j);
														}
														FileUtil.deleteFolderFile(
																Contexts.AERIALPHOTO_PATH
																		+ oldCoursesObject
																				.getCouseID(),
																true);
														isExist = true;
														break;
													}
												}
												if (!isExist) {
													coursesObjects
															.add(newCoursesObject);
													historyCoursesObjects
															.add(newCoursesObject);
												}
											}
										} else {
											historyCoursesObjects = coursesObjects;
										}
										coursesObjects = historyCoursesObjects;
										courseList = historyCoursesObjects;
										getSharedPreferencesUtils()
												.commitString(
														SharedPreferenceConstant.LOGOURL,
														logoURL);
										getSharedPreferencesUtils()
												.commitLong(
														SharedPreferenceConstant.COURSE_MODIFYTIME,
														maxChangTime);
										String courseInfo = FileUtil
												.setBase64Object(historyCoursesObjects);
										Log.d(TAG, courseInfo);
										getSharedPreferencesUtils()
												.commitString(
														SharedPreferenceConstant.COURSESINFO,
														courseInfo);
									}
									getSharedPreferencesUtils()
											.commitString(
													SharedPreferenceConstant.COURSE_LAST_UPDATE_TIME,
													df.format(new Date()));
									courseHandler
											.sendEmptyMessage(WHAT_SYNCDATA_SUCCEED);
								} else {
									courseHandler
											.sendEmptyMessage(WHAT_SYNCDATA_FAILED);
								}
							} catch (JSONException e) {
								courseHandler
										.sendEmptyMessage(WHAT_SYNCDATA_FAILED);
								// e.printStackTrace();
							} catch (IOException e) {
								courseHandler
										.sendEmptyMessage(WHAT_SYNCDATA_FAILED);
								// e.printStackTrace();
							}
						}
					}, new ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub
							Log.e(TAG, error.getMessage());
							courseHandler
									.sendEmptyMessage(WHAT_SYNCDATA_FAILED);
						}
					}, courseHandler);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}

	// ===================================================================================================
	public void getSubCourseInfo(final CoursesObject coursesObject,
			Handler handler) {
		cancelHandlerOperation();
		courseHandler = handler;
		if (coursesObject.getSubCourses() == null) {
			JSONObject object = new JSONObject();
			try {
				object.put(REQUEST_LABEL_COURSEID, coursesObject.getCouseID());
				params.clear();
				params.put(REQUEST_LABEL, object.toString());
				String urlString = UserInfoModel
						.getInstance(applicationContext)
						.appendUrlWithNoDateKeyAndHashCode(ACTION_SUBCOURSE,
								object.toString());
				VolleyUitl.getInstence(applicationContext).Post(urlString,
						params, new Listener<String>() {

							@Override
							public void onResponse(String response) {
								System.err.println(response);
								try {
									long timestamp = System.currentTimeMillis();
									String fileName = "crash-" + timestamp + ".txt";
									if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
										File dir = new File(Contexts.EXCEPTION_PATH);
										if (!dir.exists()) {
											dir.mkdirs();
										}
										FileOutputStream fos = new FileOutputStream(Contexts.EXCEPTION_PATH + fileName);
										fos.write(response.getBytes());
										fos.close();
									}
								} catch (Exception e) {
									Log.e(TAG, "an error occured while writing file...", e);
								}
								try {
									final JSONObject jsonObject = new JSONObject(
											response);
									int returnCode = jsonObject
											.optInt(RESULT_LABEL_RETURNCODE);
									if (returnCode == SUCCESS_RETURNCODE) {
										JSONObject data = jsonObject
												.getJSONObject(RESULT_LABEL_DATA);
										// String subCourseInfo =
										// data.optString("subCourses");
										String resourceURL = data
												.optString("resourceURL");
										coursesObject
												.setResourceURL(resourceURL);
										ArrayList<SubCoursesObject> subCoursesObjects = new ArrayList<SubCoursesObject>();
										ArrayList<FairwayObject> fairwayObjects = new ArrayList<FairwayObject>();
										JSONArray subCourses = data
												.getJSONArray("subCourses");
										for (int i = 0; i < subCourses.length(); i++) {
											SubCoursesObject subCoursesObject = SubCoursesObject
													.jsonSubCourseObject(subCourses
															.optJSONObject(i));
											subCoursesObjects
													.add(subCoursesObject);
											fairwayObjects
													.addAll(subCoursesObject
															.getFairwayList());
										}

										List<AerialPhotoBean> list = PlayGameModel.getInstance(applicationContext).getAerialPhotoBeanArray(fairwayObjects);
										DownloadAerialPhotoBean bean = new DownloadAerialPhotoBean();
										bean.setAerialPhotoBeanList(list);
										bean.setCoursesObject(coursesObject);
//										bean.setResourceURL(resourceURL);
										bean.setSubCourses(subCoursesObjects);

										Message msg = new Message();
										msg.what = WHAT_GETSUBCOURSEINFO_SUCCEED;
										msg.obj = bean;
										msg.arg1 = 0;
										courseHandler.sendMessage(msg);

									} else {
										courseHandler
												.sendEmptyMessage(WHAT_FILD);
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}, new ErrorListener() {

							@Override
							public void onErrorResponse(VolleyError error) {
								// TODO Auto-generated method stub
								Log.e(TAG, error.getMessage());
								courseHandler.sendEmptyMessage(WHAT_FILD);
							}
						}, courseHandler);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}else {
			ArrayList<FairwayObject> fairwayObjects = new ArrayList<FairwayObject>();
			for (int i = 0; i < coursesObject.getSubCourses().size(); i++) {
				SubCoursesObject subCoursesObject = coursesObject.getSubCourses().get(i);
				fairwayObjects
						.addAll(subCoursesObject
								.getFairwayList());
			}

			List<AerialPhotoBean> list = PlayGameModel.getInstance(applicationContext).getAerialPhotoBeanArray(fairwayObjects);
			DownloadAerialPhotoBean bean = new DownloadAerialPhotoBean();
			bean.setAerialPhotoBeanList(list);
			bean.setCoursesObject(coursesObject);
//			bean.setResourceURL(resourceURL);
			bean.setSubCourses(coursesObject.getSubCourses());

			Message msg = new Message();
			msg.what = WHAT_GETSUBCOURSEINFO_SUCCEED;
			msg.obj = bean;
			msg.arg1 = 0;
			courseHandler.sendMessage(msg);
		}

	}

	public void downloadAerialPhoto(Handler handler,
			DownloadAerialPhotoBean bean, int position) {
		courseHandler = handler;
		AerialPhotoBean aerialPhotoBean = bean.getAerialPhotoBeanList().get(
				position);
		String photoPath = Contexts.AERIALPHOTO_PATH
				+ bean.getCoursesObject().getCouseID() + "/";
		String photoPathUrl = photoPath + aerialPhotoBean.getPhotoName() + "."
				+ aerialPhotoBean.getPhotoMode();

		File fileUrl = new File(photoPath);
		if (!fileUrl.exists()) {
			fileUrl.mkdirs();
		}
		File file = new File(photoPathUrl);
		if (!file.exists()) {
			String url = bean.getCoursesObject().getResourceURL() + aerialPhotoBean.getPhotoPath()
					+ aerialPhotoBean.getPhotoName() + "."
					+ aerialPhotoBean.getPhotoMode();
			Message msg = new Message();
			msg.what = WHAT_PROGRESS;
			msg.arg1 = position + 1;
			// msg.obj = bean;
			AerialPhotoResponseListener listener = new AerialPhotoResponseListener(
					photoPathUrl, courseHandler, msg);
			VolleyUitl.getInstence(applicationContext).getImage(url, listener,
					0, 0, Bitmap.Config.ARGB_8888, listener, courseHandler);
		} else {

			Message msg = new Message();
			msg.what = WHAT_PROGRESS;
			msg.arg1 = position + 1;
			msg.arg2 = AerialPhotoBean.DOWNLOAD_SUCCESSED;
			// msg.obj = bean;
			courseHandler.sendMessage(msg);
			// Log.i(TAG, photoPathUrl);
		}
	}

	// ===================================================================================================
	public void saveFeedbackInfo(Handler handler, int userID, int courseID,
			String message) {
		cancelHandlerOperation();
		courseHandler = handler;
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_USERID, userID);
			object.put(REQUEST_LABEL_FEED_BACKCOURSEID, courseID);
			object.put(REQUEST_LABEL_MESSAGE, message);
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			String urlString = UserInfoModel.getInstance(applicationContext)
					.appendUrlWithDateKeyAndHashCode(ACTION_FEEDBACK,
							object.toString());
			System.out.println(urlString + object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params,
					new Listener<String>() {

						@Override
						public void onResponse(String response) {
							// TODO Auto-generated method stub
							JSONObject jsonObject;
							try {
								jsonObject = new JSONObject(response);
								int returnCode = jsonObject
										.optInt(RESULT_LABEL_RETURNCODE);
								if (returnCode == SUCCESS_RETURNCODE) {
									courseHandler
											.sendEmptyMessage(FeedbackActivity.WHAT_SAVEFEEDBACKINFO);
								} else if (returnCode == UserException.USER_OFFLINE) {
									if (!UserInfoModel
											.getInstance(applicationContext)
											.loginCredential(
													courseHandler,
													UserInfoActivity.MSG_GET_USER_INFO_START,
													UserInfoActivity.MSG_GET_USER_INFO_FAILED)) {
										if (courseHandler != null) {
											courseHandler
													.sendEmptyMessage(UserInfoActivity.MSG_GET_USER_INFO_FAILED);
										}
									}
								} else {
									courseHandler.sendEmptyMessage(WHAT_FILD);
									System.out.println(response);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}, new ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub

						}
					}, courseHandler);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}

	// ===================================================================================================
	public static CoursesInfoBean coursesInfoBean;

	public void getCoursesInfo(Handler handler, int courseID) {
		cancelHandlerOperation();
		courseHandler = handler;
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_COURSEID, courseID);
			String urlString = UserInfoModel.getInstance(applicationContext)
					.appendUrlWithNoDateKeyAndHashCode(ACTION_COURSEINFO,
							object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params,
					new Listener<String>() {
						@Override
						public void onResponse(String response) {
							// TODO Auto-generated method stub
							JSONObject jsonObject;
							System.out.println(response);
							try {
								jsonObject = new JSONObject(response);
								int returnCode = jsonObject
										.optInt(RESULT_LABEL_RETURNCODE);
								if (returnCode == SUCCESS_RETURNCODE) {
									jsonObject = jsonObject
											.optJSONObject(RESULT_LABEL_DATA);
									CoursesInfoBean bean = (CoursesInfoBean) JsonProcessUtil.fromJSON(
											jsonObject
													.optString(RESULT_LABEL_COURSESINFO),
											CoursesInfoBean.class);
									Message message = new Message();
									message.what = SituationActivity.WHAT_SUCCEED;
									coursesInfoBean = bean;
									message.obj = bean;
									courseHandler.sendMessage(message);
								} else {
									courseHandler
											.sendEmptyMessage(SituationActivity.WHAT_FIALED);
									System.out.println(response);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}, new ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub
							courseHandler
									.sendEmptyMessage(SituationActivity.WHAT_BAD_NET);
						}
					}, courseHandler);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}

	// ===================================================================================================
	public void saveEvaluateInfo(Handler handler, final int courseID,
			int evaluate) {
		cancelHandlerOperation();
		courseHandler = handler;
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_FEED_BACKCOURSEID, courseID);
			object.put(REQUEST_LABEL_EVALUATE, evaluate);
			String urlString = UserInfoModel.getInstance(applicationContext)
					.appendUrlWithNoDateKeyAndHashCode(ACTION_SAVEEVALUATE,
							object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params,
					new Listener<String>() {

						@Override
						public void onResponse(String response) {
							// TODO Auto-generated method stub
							JSONObject jsonObject;
							System.out.println(response);
							try {
								jsonObject = new JSONObject(response);
								int returnCode = jsonObject
										.optInt(RESULT_LABEL_RETURNCODE);
								if (returnCode == SUCCESS_RETURNCODE) {
									courseHandler
											.sendEmptyMessage(EvaluateActivity.WHAT_SAVE_SUCCEED);
								} else {
									courseHandler
											.sendEmptyMessage(EvaluateActivity.WHAT_FIALED);
									System.out.println(response);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}, new ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub
							courseHandler
									.sendEmptyMessage(EvaluateActivity.WHAT_BAD_NET);

						}
					}, courseHandler);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}

	public void obtainEvaluateInfo(Handler handler, final int courseID,
			final CoursesObject course, final CourseListCellWrapper cellView) {
		cancelHandlerOperation();
		if (null != handler) {
			courseHandler = handler;
		}
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_FEED_BACKCOURSEID, courseID);
			String urlString = UserInfoModel.getInstance(applicationContext)
					.appendUrlWithNoDateKeyAndHashCode(ACTION_OBTAINEVALUATE,
							object.toString());
			System.out.println(urlString + object.toString());
			params.clear();
			params.put(REQUEST_LABEL, object.toString());
			VolleyUitl.getInstence(applicationContext).Post(urlString, params,
					new Listener<String>() {
						@Override
						public void onResponse(String response) {
							// TODO Auto-generated method stub
							JSONObject jsonObject;
							System.out.println(response);
							try {
								jsonObject = new JSONObject(response);
								int returnCode = jsonObject
										.optInt(RESULT_LABEL_RETURNCODE);
								if (returnCode == SUCCESS_RETURNCODE) {
									Message msg = new Message();
									if (null != course || null != cellView) {
										double evaluate = jsonObject.optDouble(
												RESULT_LABEL_DATA, 0);
										course.setEvaluate(jsonObject
												.optDouble(RESULT_LABEL_DATA, 0));
										cellView.courseRating
												.setVisibility(View.VISIBLE);
										cellView.courseRating
												.setBackgroundResource(EvaluateObject.RATINGBAR_ID[(int) (evaluate * 2)]);
									}

									msg.what = EvaluateActivity.WHAT_OBTAIN_SUCCEED;
									msg.obj = jsonObject.optDouble(
											RESULT_LABEL_DATA, 0);
									courseHandler.sendMessage(msg);
								} else {
									courseHandler
											.sendEmptyMessage(EvaluateActivity.WHAT_FIALED);
									System.out.println(response);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}, new ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub
							System.out
									.println("---------------------------------------------------");
							courseHandler
									.sendEmptyMessage(EvaluateActivity.WHAT_BAD_NET);
						}
					}, courseHandler);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}

	// ===================================================================================================
	public List<CoursesObject> searchAllCourseByProvinceCity(String key,
			List<ProvinceBean> provinceList) {
		List<CoursesObject> srcCourseObjects = getCourseList();

		if (srcCourseObjects == null) {
			return srcCourseObjects;
		}
		// construct the course object list
		List<CoursesObject> retCourseObjects = null;
		if ("".equals(key.trim())) {
			retCourseObjects = new ArrayList<CoursesObject>(srcCourseObjects);
		} else {
			retCourseObjects = new ArrayList<CoursesObject>();

			Iterator<CoursesObject> srcIt = srcCourseObjects.iterator();
			while (srcIt.hasNext()) {
				CoursesObject course = (CoursesObject) srcIt.next();
				if (course.getCoursesName().indexOf(key.trim()) >= 0) {
					retCourseObjects.add(course);
				}
			}
		}

		// sort by province and city
		Collections.sort(retCourseObjects, new ComparatorProvinceCity());

		// construct the map
		provinceList.clear();
		Iterator<CoursesObject> srcIt = retCourseObjects.iterator();
		int pos = 0;
		String province = "";
		while (srcIt.hasNext()) {
			CoursesObject course = (CoursesObject) srcIt.next();
			if (!course.getProvinceName().equals(province)) {
				province = course.getProvinceName();
				ProvinceBean bean = new ProvinceBean();
				bean.setPos(pos);
				bean.setProvinceName(province);

				provinceList.add(bean);
			}
			pos++;

		}

		return retCourseObjects;
	}

	// ===================================================================================================
	public List<CoursesObject> getMyCoursesData(MyDataBaseAdapter mdb) {
		List<CoursesObject> coursesObjects = new ArrayList<CoursesObject>();
		List<CoursesObject> courseList = getCourseList();
		List<Integer> courseIDs = new ArrayList<Integer>();
		if (courseList != null) {

			if (UserScoreCardModel.userScoreCardObjects == null) {
				UserScoreCardModel userScoreCardModel = UserScoreCardModel
						.getInstance(applicationContext);
				userScoreCardModel.getScoreCardObjects(mdb);
			}
			for (int i = 0; i < UserScoreCardModel.userScoreCardObjects.size(); i++) {
				int id = UserScoreCardModel.userScoreCardObjects.get(i)
						.getGeneralScoreBean().getCourseID();
				boolean isExist = false;
				for (int j = 0; j < courseIDs.size(); j++) {
					if (id == courseIDs.get(j)) {
						isExist = true;
						break;
					}
				}
				if (!isExist) {
					courseIDs.add(id);
				}
			}
			for (int i = 0; i < courseIDs.size(); i++) {
				for (int j = 0; j < courseList.size(); j++) {
					if (courseIDs.get(i) == courseList.get(j).getCouseID()) {
						coursesObjects.add(courseList.get(j));
						break;
					}
				}
			}
			if (courseIDs.size() > 2) {

				// to fixed me in furture
				ComparatorCourses comparator = new ComparatorCourses(0, 0);

				Collections.sort(coursesObjects, comparator);
			}
		}
		return coursesObjects;
	}

	// 周边球场常用==================================================================================
	/**
	 * 本地是否存储有球场列表信息
	 * 
	 * @return true 有 false 无
	 */
	public boolean checkData() {
		boolean isExist = false;
		String courseInfo = getSharedPreferencesUtils().getString(
				SharedPreferenceConstant.COURSESINFO, null);
		if (courseInfo != null && !"".equals(courseInfo)) {
			isExist = true;
		}
		return isExist;
	}

	/**
	 * 根据Gps位置，由近到远排序，取前20个
	 * 
	 * @return 20个最近的球场列表
	 */
	public List<CoursesObject> searchAroundCourseList(double lon, double lat,
			String key) {

		List<CoursesObject> srcList = getCourseList();
		if (null == srcList) {
			return null;
		}

		ComparatorCourses comparator = new ComparatorCourses(lon, lat);

		List<CoursesObject> listAll = null;
		if ("".equals(key.trim())) {
			listAll = srcList;
		} else {
			listAll = new ArrayList<CoursesObject>();

			Iterator<CoursesObject> srcIt = srcList.iterator();
			while (srcIt.hasNext()) {
				CoursesObject course = (CoursesObject) srcIt.next();
				if (course.getCoursesName().indexOf(key.trim()) >= 0) {
					listAll.add(course);
				}
			}
		}

		Collections.sort(listAll, comparator);
		List<CoursesObject> retList = null;
		if (listAll.size() > AROUND_COUNT) {
			retList = listAll.subList(0, AROUND_COUNT);
		} else {
			retList = new ArrayList<CoursesObject>(listAll);
		}
		return retList;
	}

	// laod the network data
	public synchronized void loadNetworkData(int firstVisibleItem,
			int visibleItemCount, CourseListAdapter adapter, XListView listView) {
		// check weather is synchronizing the course list
		// cancel all of the useless request
		WeatherInfoModel model = WeatherInfoModel
				.getInstance(applicationContext);
		final String weatherImageWildCard = "W:WE";
		final String weatherInfoWildCard = "W:WI";

		for (int pos = firstVisibleItem; pos < firstVisibleItem
				+ visibleItemCount; pos++) {

			// there is one step charge
			CoursesObject course = adapter.getItem(pos - 1);
			if (null == course) {
				continue;
			}

			View cellView = listView.getChildAt(pos - firstVisibleItem);
			Log.i(TAG, "pos:" + pos);
			if (null == cellView) {
				Log.i(TAG, "pos:" + pos + " is null");
				continue;
			}

			CourseListCellWrapper cellWrapper = (CourseListCellWrapper) cellView
					.getTag();
			if (null == cellWrapper) {
				continue;
			}

			if (!model.checkWeatherImageValid(course)) {

				// FIX: the view should be saved to tag
				HttpGet request = model.getWeatherImageGet(course);
				WeatherImageResponseListener listener = new WeatherImageResponseListener(
						course.getCity(), cellWrapper);
				String key = weatherImageWildCard + course.getCouseID();
				VolleyUitl.getInstence(applicationContext).Get(request.getURI().toString(), listener, null, key);
			}
			if (!model.checkWeatherInfoValid(course)) {

				// FIX: the view should be saved to tag
				HttpGet request = model.getWeatherInfoGet(course);
				WeatherInfoResponseListener listener = new WeatherInfoResponseListener(
						course.getCity(), cellWrapper);
				String key = weatherInfoWildCard + course.getCouseID();
				VolleyUitl.getInstence(applicationContext).Get(request.getURI().toString(), listener, null, key);
			}
			Double evaluate = course.getEvaluate();
			if (evaluate == null) {
				obtainEvaluateInfo(null, course.getCouseID(), course,
						cellWrapper);
				// EvaluateImageResponseListener listener = new
				// EvaluateImageResponseListener(
				// course, cellWrapper);
				// String key = evaluateWildCard + course.getCouseID();
				// AsyncHttpClient.sendRequest(key, request, listener);
			}

			String logoName = course.getImages();
			if (logoName != null && !"".equals(logoName)) {
				File file = new File(Contexts.LOGO_PATH + logoName);
				if (!file.exists()) {
					CourseLogoImageResponseListener listener = new CourseLogoImageResponseListener(
							Contexts.LOGO_PATH + logoName, cellWrapper);
					getCourseImage(logoName, listener);
				}
			}
		}
	}

	// ===================================================================================================
	public String getCourseImage(String logoName, Listener<Bitmap> listener) {
		String urlString = null;
		urlString = getSharedPreferencesUtils().getString(SharedPreferenceConstant.LOGOURL, "")+"logo/"+logoName;
		VolleyUitl.getInstence(applicationContext).getImage(urlString,
				listener, 0, 0, Bitmap.Config.ARGB_8888, null, courseHandler);
		return urlString;
	}
}
