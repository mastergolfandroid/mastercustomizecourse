package com.teewell.samsunggolfmate.models.course;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpEntity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.teewell.samsunggolfmate.views.CourseListCellWrapper;

public class CourseLogoImageResponseListener implements Listener<Bitmap>{
	private CourseListCellWrapper cellView;
	private String photoPathUrl;
	public CourseLogoImageResponseListener(String photoPathUrl,CourseListCellWrapper cellView){
		this.cellView = cellView; // default type
		this.photoPathUrl = photoPathUrl;
	}

	@Override
	public void onResponse(Bitmap response) {
		// TODO Auto-generated method stub
		InputStream is;
		try {
			if (response!=null) {
				FileOutputStream bos = new FileOutputStream(photoPathUrl);
				response.compress(Bitmap.CompressFormat.JPEG, 100, bos);
				cellView.logo.setImageBitmap(response);
				bos.flush();
				bos.close();
			}
			
		} catch (IllegalStateException e) {
//			e.printStackTrace();
		} catch (IOException e) {
//			e.printStackTrace();
		}
	}


}
