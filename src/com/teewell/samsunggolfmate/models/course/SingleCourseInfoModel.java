package com.teewell.samsunggolfmate.models.course;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.baidu.platform.comapi.map.l;
import com.teewell.samsunggolfmate.activities.EvaluateActivity;
import com.teewell.samsunggolfmate.activities.FeedbackActivity;
import com.teewell.samsunggolfmate.activities.SituationActivity;
import com.teewell.samsunggolfmate.activities.UserInfoActivity;
import com.teewell.samsunggolfmate.beans.AerialPhotoBean;
import com.teewell.samsunggolfmate.beans.CoursesInfoBean;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.DownloadAerialPhotoBean;
import com.teewell.samsunggolfmate.beans.EvaluateObject;
import com.teewell.samsunggolfmate.beans.FairwayObject;
import com.teewell.samsunggolfmate.beans.ProvinceBean;
import com.teewell.samsunggolfmate.beans.SubCoursesObject;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.common.Contexts.ServerURL;
import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
import com.teewell.samsunggolfmate.datebase.MyDataBaseAdapter;
import com.teewell.samsunggolfmate.exceptions.UserException;
import com.teewell.samsunggolfmate.models.ComparatorProvinceCity;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.UserInfoModel;
import com.teewell.samsunggolfmate.models.UserScoreCardModel;
import com.teewell.samsunggolfmate.models.weather.WeatherImageResponseListener;
import com.teewell.samsunggolfmate.models.weather.WeatherInfoModel;
import com.teewell.samsunggolfmate.models.weather.WeatherInfoResponseListener;
import com.teewell.samsunggolfmate.utils.ComparatorCourses;
import com.teewell.samsunggolfmate.utils.FileUtil;
import com.teewell.samsunggolfmate.utils.JsonProcessUtil;
import com.teewell.samsunggolfmate.utils.SharedPreferencesUtils;
import com.teewell.samsunggolfmate.utils.VolleyUitl;
import com.teewell.samsunggolfmate.views.CourseListAdapter;
import com.teewell.samsunggolfmate.views.CourseListCellWrapper;
import com.teewell.samsunggolfmate.views.XListView;

/**
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.models
 * @title CourseInfoModel.java
 * @Description 球场数据模块 功能：周边球场列表、全国球场列表、球场logo、球场天气
 * @author Administrator
 * @date 2013-1-24 下午5:11:27
 * @Copyright Copyright(C) 2013-1-24
 * @version 1.0.0
 */
@SuppressLint("SimpleDateFormat")
public class SingleCourseInfoModel {
	private static final String TAG = SingleCourseInfoModel.class.getName();
	private static final String REQUEST_LABEL = "request";
	private static final String REQUEST_LABEL_COURSEID = "courseID";
	private static final String REQUEST_LABEL_FEED_BACKCOURSEID = "courseId";
	private static final String REQUEST_LABEL_EVALUATE = "evaluate";
	private static final String REQUEST_LABEL_DATETIME = "dateTime";
	private static final String RESULT_LABEL_DATA = "data";
	private static final String RESULT_LABEL_LOGOURL = "logoURL";
	private static final String RESULT_LABEL_COURSES = "courses";
	private static final String RESULT_LABEL_COURSESINFO = "coursesInfo";
	private static final String RESULT_LABEL_RETURNCODE = "returnCode";
	private static final int SUCCESS_RETURNCODE = 1;
	private final static String REQUEST_LABEL_USERID = "userId";
	private final static String REQUEST_LABEL_MESSAGE = "message";
	private static final long MODIFYTIME_MIN = 0L;

	private List<CoursesObject> courseList = null;
	// public static CourseInfoModel courseInfoModel = new CourseInfoModel();
	private Context applicationContext = null;
	private static SingleCourseInfoModel INSTANCE;
	private SimpleDateFormat df = new SimpleDateFormat("MM-dd HH:mm");

	public static final int WHAT_SYNCDATA_SUCCEED = 201;
	public static final int WHAT_SYNCDATA_FAILED = -201;

	public static final int WHAT_GETSUBCOURSE_SUCCEED = 202;
	public static final int WHAT_GETSUBCOURSEINFO_SUCCEED = 203;
	public static final int WHAT_PROGRESS = 0x203;
	public static final int WHAT_FILD = -202;

	public static final int AROUND_COUNT = 20;

	private SharedPreferencesUtils sharedPreferencesUtils;
	private HashMap<String, String> params = new HashMap<String, String>();

	public SharedPreferencesUtils getSharedPreferencesUtils() {
		return sharedPreferencesUtils;
	}

	private SingleCourseInfoModel(Context context) {
		applicationContext = context.getApplicationContext();
		sharedPreferencesUtils = new SharedPreferencesUtils(applicationContext,
				SharedPreferenceConstant.SHAREDPREFERENCE_NAME);
	}

	public static SingleCourseInfoModel getInstance(Context activity) {
		if (null == INSTANCE) {
			INSTANCE = new SingleCourseInfoModel(activity);
		}
		return INSTANCE;
	}

	@SuppressWarnings("unchecked")
	public List<CoursesObject> getCourseList(Context context ) {
		if (courseList == null) {
			try {
				InputStream is = context.getAssets().open("courses.txt");
				BufferedReader br=new BufferedReader(new InputStreamReader(is,"UTF-8")); 
				String response = reader2String(br);
				is.close();
				courseList = ((List<CoursesObject>) FileUtil
							.getBase64Object(response));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
		return courseList;
	}
	public static String reader2String(BufferedReader rd) throws IOException {
		StringBuffer baos = new StringBuffer();
		String str = null;
		while ((str = rd.readLine())!= null) {
			baos.append(str);
		}
		return baos.toString();
	}
	// ===================================================================================================
	public List<CoursesObject> courseList2String(Context context) {
		List<CoursesObject> coursesObjects = new ArrayList<CoursesObject>();
		try {
			InputStream is = context.getAssets().open("course.txt");
			BufferedReader br=new BufferedReader(new InputStreamReader(is,"UTF-8")); 
			String response = reader2String(br);
			is.close();
			int returnCode = new JSONObject(response).optInt(RESULT_LABEL_RETURNCODE);
			if (returnCode == SUCCESS_RETURNCODE) {
				JSONObject jsonObject;
				jsonObject = new JSONObject(response)
						.getJSONObject(RESULT_LABEL_DATA);
				String logoURL = jsonObject.optString(RESULT_LABEL_LOGOURL);
				JSONArray josArray = jsonObject
						.getJSONArray(RESULT_LABEL_COURSES);
				for (int i = 0; i < josArray.length(); i++) {
					CoursesObject object = CoursesObject
							.getCoursesObject((JSONObject) josArray.opt(i));
					coursesObjects.add(object);
				}
					getSharedPreferencesUtils().commitString(
							SharedPreferenceConstant.LOGOURL, logoURL);
//					String courseInfo = FileUtil
//							.setBase64Object(historyCoursesObjects);
//					Log.d(TAG, courseInfo);
//					getSharedPreferencesUtils().commitString(
//							SharedPreferenceConstant.COURSESINFO, courseInfo);
				}
		} catch (JSONException e) {
		} catch (IOException e) {
		}
		return coursesObjects;
	}

	// ===================================================================================================
	public DownloadAerialPhotoBean subCourseInfo2String(Context context,final CoursesObject coursesObject) {
		DownloadAerialPhotoBean bean = new DownloadAerialPhotoBean();
		try {
			InputStream is = context.getAssets().open("subCourse.txt");
			BufferedReader br=new BufferedReader(new InputStreamReader(is,"UTF-8")); 
			String response = reader2String(br);
			is.close();
			final JSONObject jsonObject = new JSONObject(response);
				JSONObject data = jsonObject.getJSONObject(RESULT_LABEL_DATA);
				String resourceURL = data.optString("resourceURL");
				coursesObject.setResourceURL(resourceURL);
				ArrayList<SubCoursesObject> subCoursesObjects = new ArrayList<SubCoursesObject>();
				ArrayList<FairwayObject> fairwayObjects = new ArrayList<FairwayObject>();
				JSONArray subCourses = data.getJSONArray("subCourses");
				for (int i = 0; i < subCourses.length(); i++) {
					SubCoursesObject subCoursesObject = SubCoursesObject
							.jsonSubCourseObject(subCourses.optJSONObject(i));
					subCoursesObjects.add(subCoursesObject);
					fairwayObjects.addAll(subCoursesObject.getFairwayList());
				}

				List<AerialPhotoBean> list = PlayGameModel.getInstance(
						applicationContext).getAerialPhotoBeanArray(
						fairwayObjects);
				
				bean.setAerialPhotoBeanList(list);
				bean.setCoursesObject(coursesObject);
				// bean.setResourceURL(resourceURL);
				bean.setSubCourses(subCoursesObjects);

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bean;
	}
}
