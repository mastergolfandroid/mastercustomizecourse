package com.teewell.samsunggolfmate.models.weather;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.Response.Listener;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.WeatherInfoBean;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.VolleyUitl;

public class WeatherInfoModel {
	private static final String TAG = WeatherInfoModel.class.getSimpleName();
	private static WeatherInfoModel INSTANCE = null;
	private final long VALID_INTERVAL = 15 * 60 * 1000;
	
	private HashMap<String, WeatherInfoBean> weatherMap;
	
	private static final String WEATHER_INFO = "http://www.weather.com.cn/data/sk/";
	private static final String WEATHER_IMAGE = "http://www.weather.com.cn/data/cityinfo/";
	private static final String WEATHERPIC = "http://www.weather.com.cn/m/i/weatherpic/29x20/";
	private static final String WEB_TYPE = ".html";
	private static final String RESULT_LABEL = "weatherinfo";
	private static final String REQUEST_LABEL_TEMP = "temp";
	private static final String REQUEST_LABEL_WD = "WD";
	private static final String REQUEST_LABEL_WS = "WS";
	private static final String REQUEST_LABEL_IMG = "img1";
	
	//this should not be used directly
	private Map<String,String> cityCodeMap;

	Context context;

	
	private WeatherInfoModel(){
		weatherMap = new HashMap<String, WeatherInfoBean>();
	}
	
	public static WeatherInfoModel getInstance(Context context){
		if (null == INSTANCE) {
			INSTANCE = new WeatherInfoModel();
			INSTANCE.context = context.getApplicationContext();
		}
		return INSTANCE;
	}
	
	public boolean checkWeatherImageValid(CoursesObject object)
	{
		long curTimes = System.currentTimeMillis();
		WeatherInfoBean weatherBean = weatherMap.get(object.getCity());
		
		if(weatherBean != null){
			if(weatherBean.getWeatherImageTimes() + VALID_INTERVAL > curTimes){
				return true;
			}
		}
		return false;
	}
	

	public boolean checkWeatherInfoValid(CoursesObject object)
	{
		long curTimes = System.currentTimeMillis();
		WeatherInfoBean weatherBean = weatherMap.get(object.getCity());
		
		if(weatherBean != null){
			if(weatherBean.getWeatherImageTimes() + VALID_INTERVAL > curTimes){
				return true;
			}
		}
		return false;
	}
	
	public HttpGet getWeatherInfoGet(CoursesObject object)
	{
		Map<String,String> cityMap = getCityCodeMap();
		String cityCode = cityMap.get(object.getCity());
		if(null == cityCode){
			Log.e(TAG, "Invalid city name");
			return null;
		}
		HttpGet request = new HttpGet(WEATHER_INFO + cityCode + WEB_TYPE);
		return request;
	}
	public HttpGet getWeatherImageGet(CoursesObject object)
	{
		Map<String,String> cityMap = getCityCodeMap();
		String cityCode = cityMap.get(object.getCity());
		if(null == cityCode){
			Log.e(TAG, "Invalid city name");
			return null;
		}
		HttpGet request = new HttpGet(WEATHER_IMAGE + cityCode + WEB_TYPE);
		return request;
	}
	
	private Map<String,String> readCityCode(InputStream inStream)throws Exception {    
        SAXParserFactory spf = SAXParserFactory.newInstance(); // 初始化sax解析器    
        SAXParser sp = spf.newSAXParser(); // 创建sax解析器    
        //XMLReader xr = sp.getXMLReader();// 创建xml解析器    
        PlistHandler handler = new PlistHandler();    
        sp.parse(inStream, handler);    
        return handler.getMapResult();
    }  
	
	private Map<String,String> getCityCodeMap()
	{
		if (cityCodeMap == null) {
			try {
				InputStream inStream = context.getAssets().open("CityWeatherAddress.plist");
				cityCodeMap = readCityCode(inStream);
				inStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return cityCodeMap;
	}

	public WeatherInfoBean getWeatherInfoBean(String city)
	{
		if(null == city){
			return null;
		}
		
		WeatherInfoBean bean = weatherMap.get(city);
		return bean;
	}

	public boolean submitWeatherImage(String city, String response)
	{
		 File teFile = new File(getWeatherPath(context));
		 if (!teFile.exists()) {
			return false;
		}
		if(null == city){
			return false;
		}
		
		if(null == response){
			return false;
		}
		
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(response);
			jsonObject = jsonObject.optJSONObject(RESULT_LABEL);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		WeatherInfoBean bean = weatherMap.get(city);
		if(null == bean){
			bean = new WeatherInfoBean();
		}
		
		bean.setImg1(jsonObject.optString(REQUEST_LABEL_IMG));

		// 天气图标
		final File file = new File(getWeatherPath(context) + bean.getImg1());
		Bitmap bitmap;
		if (file.isFile() && file.exists()) {
			bitmap = BitmapUtil.readBitMapByNative(getWeatherPath(context) + bean.getImg1());
		} else {
			VolleyUitl.getInstence(context).getImage(WEATHERPIC+bean.getImg1(), new Listener<Bitmap>() {

				@Override
				public void onResponse(Bitmap response) {
					// TODO Auto-generated method stub
					if (null!=response) {
						try {
							BitmapUtil.saveBitmap2file(response, file);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}, 0, 0, Bitmap.Config.ARGB_8888, null,null);
			return false;
		}
		
		if(null == bitmap){
			Log.e(TAG, "Failed to read weather image!");
			return false;
		}
		
		bean.setBitmap(bitmap);
		weatherMap.put(city, bean);
		
		return true;
	}
	public boolean submitWeatherInfo(String city, String response)
	{
		if(null == city){
			return false;
		}
		
		if(null == response){
			return false;
		}
		
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(response);
			jsonObject = jsonObject.optJSONObject(RESULT_LABEL);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		WeatherInfoBean bean = weatherMap.get(city);
		if(null == bean){
			bean = new WeatherInfoBean();
		}
		
		// 得到温度
		bean.setTemp(jsonObject.optInt(REQUEST_LABEL_TEMP));
		// 得到风向
		bean.setWD(jsonObject.optString(REQUEST_LABEL_WD));
		// 得到风力
		bean.setWS(jsonObject.optString(REQUEST_LABEL_WS));
		
		weatherMap.put(city, bean);
		
		return true;
	}
	public static String getWeatherPath(Context context) {
		return Contexts.WEATHER_PATH+getWeatherName(context)+"/";
	}
	public static String getWeatherName(Context context) {
		String weatherName = Contexts.WEATHER_NAME;
		try {
			// Get the package info
			PackageManager pm = context.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(Contexts.PACKAGE_NAME, 0);
			int versionName = pi.versionCode;
			weatherName += versionName;
		} catch (Exception e) {
		}
		return weatherName;
	}

}
