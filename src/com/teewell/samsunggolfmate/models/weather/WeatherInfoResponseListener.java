package com.teewell.samsunggolfmate.models.weather;

import org.apache.http.ParseException;

import com.android.volley.Response.Listener;
import com.teewell.samsunggolfmate.beans.WeatherInfoBean;
import com.teewell.samsunggolfmate.views.CourseListCellWrapper;

public class WeatherInfoResponseListener implements Listener<String>{
	@SuppressWarnings("unused")
	private static final String TAG = WeatherInfoResponseListener.class.getSimpleName();
	private static final String TEMP_UNIT = "℃";
	private CourseListCellWrapper cellView;
	private String city;
	
	public WeatherInfoResponseListener(String cityString, CourseListCellWrapper cellView){
		this.cellView = cellView; // default type
		this.city = cityString;
	}
	

	@Override
	public void onResponse(String response) {
		// TODO Auto-generated method stub
		try {
			WeatherInfoModel model = WeatherInfoModel.getInstance(null);
			model.submitWeatherInfo(city, response);
			
			//change the cellview
			WeatherInfoBean bean = model.getWeatherInfoBean(city);
			if(null == bean){
				return;
			}
			cellView.wind.setText(bean.getWD());
			cellView.windLevel.setText(bean.getWS());
			cellView.temperature.setText(bean.getTemp()+TEMP_UNIT);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}	
	}


}
