package com.teewell.samsunggolfmate.utils;

import java.util.Stack;

import android.app.Activity;

public class ActivityManagerUtil {
	  	private static Stack<Activity> activityStack;
	    private static ActivityManagerUtil instance;
	    private ActivityManagerUtil() {
	    }
	    public static ActivityManagerUtil getScreenManager() {
	        if (instance == null) {
	            instance = new ActivityManagerUtil();
	        }
	        return instance;
	    }
	    
	    public static Stack<Activity> getActivityStack() {
			return activityStack;
		}
		//退出栈顶Activity
	    public void popActivity(Activity activity) {
	        if (activity != null) {
	           //在从自定义集合中取出当前Activity时，也进行了Activity的关闭操作
	            activity.finish();
	            activityStack.remove(activity);
	            activity = null;
	        }
	    }
	    //获得当前栈顶Activity
	    public Activity currentActivity() {
	        Activity activity = null;
	       if(!activityStack.empty())
	         activity= activityStack.lastElement();
	        return activity;
	    }
	    //将当前Activity推入栈中
	    public void pushActivity(Activity activity) {
	        if (activityStack == null) {
	            activityStack = new Stack<Activity>();
	        }
	        activityStack.add(activity);
	    }
	    //退出栈中所有Activity
	    public void popAllActivityExceptOne(Class<?> cls) {
	        while (true) {
	            Activity activity = currentActivity();
	            if (activity == null) {
	                break;
	            }
	            if (activity.getClass().equals(cls)) {
	                break;
	            }
	            popActivity(activity);
	        }
	    }
	    /**
	    //退到栈中某一个Activity后，停止退出
	     * @param cls
	     */
	    public void popActivityToSomeActivity(Class<?> cls) {
	    	while (true) {
	    		Activity activity = currentActivity();
	    		if (activity == null) {
	    			break;
	    		}
	    		if (activity.getClass().equals(cls)) {
	    			popActivity(activity);
	    			break;
	    		}
	    		popActivity(activity);
	    	}
	    }
}
