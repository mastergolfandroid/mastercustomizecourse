package com.teewell.samsunggolfmate.utils;

import java.util.HashMap;
import java.util.Map;
import android.app.Activity;

/**  
 * @Project com.vehiclesApp
 * @package com.vehiclesApp.utils
 * @title ActivityStackControlUtil.java 
 * @Description TODO
 * 在复杂的Activity调用栈中，这个类静态类可以辅助代码完成全部Activity的正确关闭，
 * @author teewell
 * @date Nov 26, 2012 11:21:18 AM
 * @Copyright Copyright(C) Nov 26, 2012
 * @version 1.0.0
 */
public class ActivityStackControlUtil {
	
	private static Map<String, Activity> activityMap = new HashMap<String, Activity>();

    
    public static void remove(String activityKey){ 
    	activityMap.remove(activityKey);
    } 

    public static void add(String key, Activity activity){ 
    	activityMap.put(key, activity);
    } 

     

    public static void finishActivity(String key) { 
    	Activity activity = activityMap.get(key);
    	if (null != activity) {
			activity.finish();
			activityMap.remove(key);
		}
    } 
}
