package com.teewell.samsunggolfmate.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

/**
 * bitmap的一些常用操作：读图片文件、图片缩放和旋转、将图片二进制转化为bitmap，以及图片加密，解密
 */
public class BitmapUtil {

	private static String TAG = BitmapUtil.class.getName();

	/**
	 * 图片二进制转为Bitmap
	 * 
	 * @param byte[] bytes 图片二进制
	 * @return Bitmap 位图
	 */
	public static Bitmap getPicFromBytes(byte[] bytes,
			BitmapFactory.Options opts) {

		if (bytes != null)
			if (opts != null)
				return BitmapFactory.decodeByteArray(bytes, 0, bytes.length,
						opts);
			else
				return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
		return null;
	}

	/**
	 * 加密
	 * 
	 * @param byteContent
	 *            待加密二进制
	 * @param password
	 *            密码
	 * @return byte[] 加密后的二进制
	 */
	public static byte[] encrypt(byte[] byteContent, String password) {
		byte[] result = null;
		try {
			KeyGenerator kgen = KeyGenerator.getInstance("AES");
			kgen.init(128, new SecureRandom(password.getBytes()));
			SecretKey secretKey = kgen.generateKey();
			byte[] enCodeFormat = secretKey.getEncoded();
			SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
			Cipher cipher = Cipher.getInstance("AES");// 创建密码器
			// byte[] byteContent = content.getBytes("utf-8");
			cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
			result = cipher.doFinal(byteContent);
			return result; // 加密
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 解密
	 * 
	 * @param content
	 *            待解密二进制
	 * @param password
	 *            密码
	 * @return byte [] 解密后的二进制
	 */
	public static byte[] decrypt(byte[] content, String password) {
		byte[] result = null;
		try {
			KeyGenerator kgen = KeyGenerator.getInstance("AES");
			kgen.init(128, new SecureRandom(password.getBytes()));
			SecretKey secretKey = kgen.generateKey();
			byte[] enCodeFormat = secretKey.getEncoded();
			SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
			Cipher cipher = Cipher.getInstance("AES");// 创建密码器
			cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
			result = cipher.doFinal(content);
			return result; // 加密 Svn中文网 nm
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Drawable 转 bitmap
	 * 
	 * @param drawable
	 * @return
	 */
	public static Bitmap drawable2Bitmap(Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		} else if (drawable instanceof NinePatchDrawable) {
			Bitmap bitmap = Bitmap
					.createBitmap(
							drawable.getIntrinsicWidth(),
							drawable.getIntrinsicHeight(),
							drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
									: Bitmap.Config.RGB_565);
			Canvas canvas = new Canvas(bitmap);
			drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
					drawable.getIntrinsicHeight());
			drawable.draw(canvas);
			return bitmap;
		} else {
			return null;
		}
	}

	/**
	 * 以最省内存的方式读取本地资源的图片
	 * 
	 * @param context
	 * @param resId
	 * @return
	 */
	public static Bitmap readBitMapByNative(String filePath) {
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.RGB_565;
		opt.inPurgeable = true;
		opt.inInputShareable = true;
		// 缩放的比例，缩放是很难按准备的比例进行缩放的，其值表明缩放的倍数，SDK中建议其值是2的指数值,值越大会导致图片不清晰
		opt.inSampleSize = 1;// 图片宽高都为原来的二分之一，即图片为原来的四分之一

		// 获取资源图片
		FileInputStream is = null;
		try {
			is = new FileInputStream(filePath);
			// is = context.openFileInput(path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return BitmapFactory.decodeStream(is, null, opt);
	}

	/**
	 * 以最省内存的方式读取本地资源的图片
	 * 
	 * @param context
	 * @param resId
	 * @return
	 */
	public static Bitmap readBitMap(File file) {
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.RGB_565;
		opt.inPurgeable = true;
		opt.inInputShareable = true;
		// 缩放的比例，缩放是很难按准备的比例进行缩放的，其值表明缩放的倍数，SDK中建议其值是2的指数值,值越大会导致图片不清晰
		opt.inSampleSize = 2;// 图片宽高都为原来的二分之一，即图片为原来的四分之一

		// 获取资源图片
		FileInputStream is = null;
		try {
			is = new FileInputStream(file);
			// is = context.openFileInput(path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return BitmapFactory.decodeStream(is, null, opt);
	}

	public static boolean saveBitmap2file(Bitmap bmp, File file) {
		CompressFormat format = Bitmap.CompressFormat.PNG;
		int quality = 100;
		OutputStream stream = null;
		try {
			stream = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bmp.compress(format, quality, stream);
	}

	/**
	 * 以最省内存的方式读取本地资源的图片(8倍缩放)
	 * 
	 * @param context
	 * @param path
	 * @return
	 */
	public static Bitmap readBitMap(String path) {
		Bitmap bitmap = null;
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.RGB_565;
		opt.inPurgeable = true;
		opt.inInputShareable = true;
		// 缩放的比例，缩放是很难按准备的比例进行缩放的，其值表明缩放的倍数，SDK中建议其值是2的指数值,值越大会导致图片不清晰
		opt.inSampleSize = 8;// 图片宽高都为原来的二分之一，即图片为原来的四分之一

		// 获取资源图片
		FileInputStream is = null;
		try {
			is = new FileInputStream(path);
			bitmap = BitmapFactory.decodeStream(is, null, opt);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return bitmap;
	}

	/**
	 * 以最省内存的方式读取本地资源的图片
	 * 
	 * @param context
	 * @param resId
	 * @return
	 */
	public static Bitmap readBitMap(Context context, int resId) {
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.RGB_565;
		opt.inPurgeable = true;
		opt.inInputShareable = true;
		// 获取资源图片
		InputStream is = context.getResources().openRawResource(resId);
		return BitmapFactory.decodeStream(is, null, opt);
	}

	/**
	 * 获取带黑边的图片有效区域
	 * 
	 * @param bitmapOrg
	 *            图片
	 * @return 上下左右的截取位置坐标，像素点
	 */
	public static int[] getValidArea(Bitmap bitmapOrg) {
		int h = bitmapOrg.getHeight();
		int w = bitmapOrg.getWidth();
		int[] pixels = new int[w * h];
		bitmapOrg.getPixels(pixels, 0, bitmapOrg.getWidth(), 0, 0,
				bitmapOrg.getWidth(), bitmapOrg.getHeight());

		int x1 = w;
		int y1 = h;
		int x2 = 0;
		int y2 = 0;
		for (int i = 0; i < h; i += 5) {
			for (int j = 0; j < w; j += 5) {
				int index = i * w + j;
				int r = (pixels[index] >> 16) & 0xff;
				int g = (pixels[index] >> 8) & 0xff;
				int b = pixels[index] & 0xff;
				if (r > 45 || g > 45 || b > 45) {
					if (j < x1) {
						x1 = j;
					}
					if (j > x2) {
						x2 = j;
					}

					if (i < y1) {
						y1 = i;
					}
					if (i > y2) {
						y2 = i;
					}
				}
			}
		}
		x2 += 1;
		y2 += 1;
		Log.d(TAG, "x1 = " + x1 + " y1 = " + y1 + " x2 = " + x2 + " y2 = " + y2);

		return new int[] { x1, y1, x2, y2 };
	}

	/**
	 * 等比例缩放图片
	 * 
	 * @param orgBitmap
	 * @param ratio
	 *            缩放比
	 * @return
	 */
	public static Bitmap zoomBitmap(Bitmap orgBitmap, float ratio) {
		if (ratio == 1.0) {
			return orgBitmap;
		}
		// 获取这个图片的宽和高
		int width = orgBitmap.getWidth();
		int height = orgBitmap.getHeight();

		// 创建操作图片用的matrix对象
		Matrix matrix = new Matrix();
		// 缩放图片动作
		matrix.postScale(ratio, ratio);
		Log.d(TAG, "width=" + width + ",height=" + height + " ratio=" + ratio);
		Bitmap bitmap = Bitmap.createBitmap(orgBitmap, 0, 0, width, height,
				matrix, true);
		Log.d(TAG, "bitmap.width=" + bitmap.getWidth() + ",bitmap.height="
				+ bitmap.getWidth());
		if (orgBitmap != null && !orgBitmap.isRecycled()) {// 先判断图片是否已释放了
			orgBitmap.recycle();
		}
		// 创建新的图片
		return bitmap;
	}

	/**
	 * 按指定角度旋转图片
	 * 
	 * @param orgBitmap
	 * @param angle
	 *            旋转角度
	 * @return
	 */
	public static Bitmap rotateBitmap(Bitmap orgBitmap, double angle) {
		// 获取这个图片的宽和高
		int width = orgBitmap.getWidth();
		System.out.println("bitmap width:" + width);
		int height = orgBitmap.getHeight();
		System.out.println("bitmap height:" + height);

		Matrix matrix = new Matrix();
		// 旋转图片 动作
		matrix.setRotate((float) angle);

		// 创建新的图片
		Bitmap bitmap = Bitmap.createBitmap(orgBitmap, 0, 0, width, height,
				matrix, true);
		// 释放资源，防止内存溢出
		// if(orgBitmap != null && !orgBitmap.isRecycled()){//先判断图片是否已释放了
		// orgBitmap.recycle();
		// }
		return bitmap;
	}

	/**
	 * 按指定大小缩放图片
	 * 
	 * @param orgBitmap
	 * @param ratio
	 *            缩放比
	 * @return
	 */
	public static Bitmap zoomPhotoBitmap(Bitmap orgBitmap, int width, int height) {

		// 获取这个图片的宽和高
		int height_bitmap = orgBitmap.getHeight();
		int width_bitmap = orgBitmap.getWidth();

		// 计算缩放率，新尺寸除原始尺寸
		float ratioWidth = ((float) width) / width_bitmap;
		float ratioHeight = ((float) height) / height_bitmap;
		// float tempRatio = ratioWidth>=ratioHeight?ratioHeight:ratioWidth;
		// 创建操作图片用的matrix对象
		Matrix matrix = new Matrix();
		// 缩放图片动作
		matrix.postScale(ratioWidth, ratioHeight);
		Bitmap bitmap = Bitmap.createBitmap(orgBitmap, 0, 0, width_bitmap,
				height_bitmap, matrix, true);
		// 释放资源，防止内存溢出
		if (orgBitmap != bitmap && !orgBitmap.isRecycled()) {// 先判断图片是否已释放了
			orgBitmap.recycle();
		}
		// 创建新的图片
		return bitmap;
	}

	/**
	 * 质量压缩方法
	 * 
	 * @param image
	 * @return
	 */
	public static Bitmap compressImage(Bitmap image) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 100, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
		int options = 100;
		while (baos.toByteArray().length / 1024 > 100) { // 循环判断如果压缩后图片是否大于500kb,大于继续压缩
			baos.reset();// 重置baos即清空baos
			options -= 10;// 每次都减少10
			image.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// 把ByteArrayInputStream数据生成图片
		if (image != null && !image.isRecycled()) {
			image.recycle();
		}
		return bitmap;
	}

	/**
	 * 图片按比例大小压缩方法（根据路径获取图片并压缩）
	 * 
	 * @param srcPath
	 * @return
	 */
	@SuppressWarnings("unused")
	private Bitmap getimage(String srcPath) {
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		// 开始读入图片，此时把options.inJustDecodeBounds 设回true了
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);// 此时返回bm为空

		newOpts.inJustDecodeBounds = false;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;
		// 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
		float hh = 800f;// 这里设置高度为800f
		float ww = 480f;// 这里设置宽度为480f
		// 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
		int be = 1;// be=1表示不缩放
		if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
			be = (int) (newOpts.outWidth / ww);
		} else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
			be = (int) (newOpts.outHeight / hh);
		}
		if (be <= 0)
			be = 1;
		newOpts.inSampleSize = be;// 设置缩放比例
		// 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
		bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
		return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
	}

	/**
	 * 图片按比例大小压缩方法（根据Bitmap图片压缩）
	 * 
	 * @param image
	 * @return
	 */
	@SuppressWarnings("unused")
	private Bitmap comp(Bitmap image) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		if (baos.toByteArray().length / 1024 > 1024) {// 判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
			baos.reset();// 重置baos即清空baos
			image.compress(Bitmap.CompressFormat.JPEG, 50, baos);// 这里压缩50%，把压缩后的数据存放到baos中
		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		// 开始读入图片，此时把options.inJustDecodeBounds 设回true了
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
		newOpts.inJustDecodeBounds = false;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;
		// 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
		float hh = 800f;// 这里设置高度为800f
		float ww = 480f;// 这里设置宽度为480f
		// 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
		int be = 1;// be=1表示不缩放
		if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
			be = (int) (newOpts.outWidth / ww);
		} else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
			be = (int) (newOpts.outHeight / hh);
		}
		if (be <= 0)
			be = 1;
		newOpts.inSampleSize = be;// 设置缩放比例
		// 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
		isBm = new ByteArrayInputStream(baos.toByteArray());
		bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
		return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
	}

	/**
	 * 图片按手机屏幕大小缩放方法（根据路径获取图片并压缩）
	 * 
	 * @param srcPath
	 * @return
	 */
	public static Bitmap getBitmapByScreenWidthOrHeight(String srcPath,
			int screenWidth, int screenHeight) {
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		// 开始读入图片，此时把options.inJustDecodeBounds 设回true了
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);// 此时返回bm为空

		newOpts.inJustDecodeBounds = false;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;

		// 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
		int be = 1;// be=1表示不缩放
		if (w > h && w > screenWidth) {// 如果宽度大的话根据宽度固定大小缩放
			be = (int) (newOpts.outWidth / screenWidth);
		} else if (w < h && h > screenHeight) {// 如果高度高的话根据宽度固定大小缩放
			be = (int) (newOpts.outHeight / screenHeight);
		}
		if (be <= 0)
			be = 1;
		newOpts.inSampleSize = be;// 设置缩放比例
		// 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
		bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
		return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
	}

	/**
	 * 图片按固定狂赌缩放方法（根据路径获取图片并压缩）
	 * 
	 * @param srcPath
	 * @return
	 */
	public static Bitmap getBitmapByWidthAndHeight(String srcPath, int width,
			int height) {
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		// 开始读入图片，此时把options.inJustDecodeBounds 设回true了
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);// 此时返回bm为空

		newOpts.inJustDecodeBounds = false;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;

		// 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
		int be = 1;// be=1表示不缩放
		if (w > h && w > width) {// 如果宽度大的话根据宽度固定大小缩放
			be = (int) (newOpts.outWidth / width);
		} else if (w < h && h > height) {// 如果高度高的话根据宽度固定大小缩放
			be = (int) (newOpts.outHeight / height);
		}
		if (be <= 0)
			be = 8;
		newOpts.inSampleSize = be;// 设置缩放比例
		// 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
		bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
		return bitmap;// 压缩好比例大小后再进行质量压缩
	}

	/**
	 * 将本地图片加载到Drawable 将文件生成位图
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public BitmapDrawable getImageDrawable(String path) throws IOException {
		// 打开文件
		File file = new File(path);
		if (!file.exists()) {
			return null;
		}

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] bt = new byte[1024];

		// 得到文件的输入流
		@SuppressWarnings("resource")
		InputStream in = new FileInputStream(file);

		// 将文件读出到输出流中
		int readLength = in.read(bt);
		while (readLength != -1) {
			outStream.write(bt, 0, readLength);
			readLength = in.read(bt);
		}

		// 转换成byte 后 再格式化成位图
		byte[] data = outStream.toByteArray();
		Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);// 生成位图
		BitmapDrawable bd = new BitmapDrawable(bitmap);

		return bd;
	}

	/**
	 * 将本地图片加载到Drawable 将文件生成位图
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static BitmapDrawable getImageDrawable(File file) throws IOException {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] bt = new byte[1024];

		// 得到文件的输入流
		@SuppressWarnings("resource")
		InputStream in = new FileInputStream(file);

		// 将文件读出到输出流中
		int readLength = in.read(bt);
		while (readLength != -1) {
			outStream.write(bt, 0, readLength);
			readLength = in.read(bt);
		}

		// 转换成byte 后 再格式化成位图
		byte[] data = outStream.toByteArray();
		Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);// 生成位图
		BitmapDrawable bd = new BitmapDrawable(bitmap);

		return bd;
	}

	public static Uri convertBitmap2Uri(Activity activity, Bitmap bitmap) {
		if (bitmap != null) {
			return Uri.parse(MediaStore.Images.Media.insertImage(
					activity.getContentResolver(), bitmap, null, null));
		} else {
			return null;
		}
	}
}
