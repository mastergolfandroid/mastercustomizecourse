package com.teewell.samsunggolfmate.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.utils.ScalingUtil.ScalingLogic;

public class CameraHead {
	/**
	 * 调用相册的请求代码
	 */
	public static final int ALBLUM = 0x2804;
	
	/**
	 * 调用拍照的请求代码
	 */
	public static final int PHOTOGRAPH = 0x2805;
	
	/**
	 * 取景的的请求代码
	 */
	public static final int PHOTORESOULT = 0x2806;
	
	/**
	 * 照片文件类型
	 */
	private static final String IMAGE_UNSPECIFIED = "image/*";
	
	/**
	 * 拍照时，临时存储一个副本到根目录下的temp.jpg
	 */
	public static final String PHOTOGRAPH_TEMP = Environment.getExternalStorageDirectory() + "/temp.jpg";
	
	/**
	 * 选择图像获取方式
	 */
	public static void selectModeDialog(final Activity activity) {
		Builder dialog = new AlertDialog.Builder(activity);
		String[] items = new String[] {
			activity.getString(R.string.camear),
			activity.getString(R.string.album),
			activity.getString(R.string.cancel)
		};
		dialog.setItems(items, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case 0:
						Photograph(activity);
						break;
					case 1:
						Album(activity);
						break;
					case 2:
						dialog.dismiss();
						break;
					default:
						break;
				}
			}
		});
		dialog.create().show();
	}
	
	public static void selectModeDialog(final Fragment fragment) {
		Builder dialog = new AlertDialog.Builder(fragment.getActivity());
		String[] items = new String[] {
				fragment.getString(R.string.camear),
				fragment.getString(R.string.album),
				fragment.getString(R.string.cancel)
		};
		dialog.setItems(items, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case 0:
						Photograph(fragment);
						break;
					case 1:
						Album(fragment);
						break;
					case 2:
						dialog.dismiss();
						break;
					default:
						break;
				}
			}
		});
		dialog.create().show();
	}
	
	/**
	 * 调用相册
	 * 调用该方法需要在指定的activity页面中添加onActivityResult回调方法
	 * @param activity Activity 传递指定的上下文环境
	 */
	public static void Album(Activity activity)
	{
        activity.startActivityForResult(getAlbumIntent(), ALBLUM);
	}
	
	public static void Album(Fragment fragment) {
		fragment.startActivityForResult(getAlbumIntent(), ALBLUM);
	}
	
	private static Intent getAlbumIntent(){
		Intent intent = new Intent(Intent.ACTION_PICK);  
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
        return intent;
	}
	
	/**
	 * 调用拍照
	 * 调用该方法需要在指定的activity页面中添加onActivityResult回调方法
	 * @param activity Activity 传递指定的上下文环境
	 */
	public static void Photograph (Activity activity)
	{
        activity.startActivityForResult(getPhotographIntent(), PHOTOGRAPH);
	}
	
	public static void Photograph(Fragment fragment){
		fragment.startActivityForResult(getPhotographIntent(), PHOTOGRAPH);
	}
	
	private static Intent getPhotographIntent(){
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); 
		//保存一个临时的照片副本
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(PHOTOGRAPH_TEMP)));  
        return intent;
	}
	
	/**
	 * 拍照和相册回调处理方法（调用缩放取景功能）
	 * @param activity Activity 传递应用上下文环境
	 * @param requestCode int 请求代码
	 * @param resultCode int 返回的代码
	 * @param data
	 * @return Bitmap
	 */
	public static Bitmap callbackZoom(Activity activity,int requestCode, int resultCode, Intent data)
	{
		Bitmap bitmap = null;
		//resultCode = 0 表示没有或不存在
		if ( resultCode == 0 )
		{
			return null;
		}
		
		int mDstWidth = ScreenUtil.getScreenWidth(activity);
        int mDstHeight = ScreenUtil.getScreenHeight(activity);
        Bitmap unscaledBitmap = null;	//同比缩放后的Bitmap
		switch(requestCode)
		{
			//调用相册后的处理
			case ALBLUM :
				//将URI转换为文件路径
				String filePath = convertUri2FilePath(activity, data.getData());
				//将照片进行同比缩放
				unscaledBitmap = ScalingUtil.decodeResource(filePath, mDstWidth, mDstHeight, ScalingLogic.CROP);
				//进行裁剪
				startPhotoZoom(activity,convertBitmap2Uri(activity, unscaledBitmap),1,1,320,320);
				break;
			//调用拍照后的处理
			case PHOTOGRAPH :
				//将照片进行同比缩放
				unscaledBitmap = ScalingUtil.decodeResource(PHOTOGRAPH_TEMP, mDstWidth, mDstHeight, ScalingLogic.CROP);
				//再进行裁剪
	            startPhotoZoom(activity,convertBitmap2Uri(activity, unscaledBitmap),1,1,320,320);
				break;
			//调用取景后的处理
			case PHOTORESOULT : 
				Bundle extras = data.getExtras();
	            if (extras != null) {  
	            	bitmap = extras.getParcelable("data");
	            	ByteArrayOutputStream stream = new ByteArrayOutputStream(); 
	                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);// (0 - 100)压缩文件   
	            } 
				break;
		}
		return bitmap;
	}
	
	public static Bitmap callbackZoom(Fragment fragment,int requestCode, int resultCode, Intent data) {
		Bitmap bitmap = null;
		//resultCode = 0 表示没有或不存在
		if ( resultCode == 0 || data == null )
		{
			return null;
		}
		
		int mDstWidth = ScreenUtil.getScreenWidth(fragment.getActivity());
        int mDstHeight = ScreenUtil.getScreenHeight(fragment.getActivity());
        Bitmap unscaledBitmap = null;	//同比缩放后的Bitmap
		switch(requestCode)
		{
			//调用相册后的处理
			case ALBLUM :
				//将URI转换为文件路径
				String filePath = convertUri2FilePath(fragment.getActivity(), data.getData());
				//将照片进行同比缩放
				unscaledBitmap = ScalingUtil.decodeResource(filePath, mDstWidth, mDstHeight, ScalingLogic.CROP);
				//进行裁剪
				startPhotoZoom(fragment,convertBitmap2Uri(fragment.getActivity(), unscaledBitmap),1,1,320,320);
				break;
			//调用拍照后的处理
			case PHOTOGRAPH :
				//将照片进行同比缩放
				unscaledBitmap = ScalingUtil.decodeResource(PHOTOGRAPH_TEMP, mDstWidth, mDstHeight, ScalingLogic.CROP);
				//再进行裁剪
	            startPhotoZoom(fragment,convertBitmap2Uri(fragment.getActivity(), unscaledBitmap),1,1,320,320);
				break;
			//调用取景后的处理
			case PHOTORESOULT : 
				Bundle extras = data.getExtras();
	            if (extras != null) {  
	            	bitmap = extras.getParcelable("data");
	            	ByteArrayOutputStream stream = new ByteArrayOutputStream(); 
	                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);// (0 - 100)压缩文件   
	            } 
				break;
		}
		return bitmap;
	}
	
	/**
	 * 将大图片缩放到屏幕大小
	 * @param activity 调用该方法需要在指定的activity页面中添加onActivityResult回调方法
	 * @param uri
	 * @param aspectX,aspectY  是宽高的比例 ，当其中一个为零时，则表示可自由调整高度
	 * @param outputX,outputY  是裁剪图片宽高  ，当其中一个为零时，则不设置裁剪宽高
	 */
	public static void startPhotoZoom(Activity activity,Uri uri,int aspectX ,int aspectY ,int outputX ,int outputY) {  
		activity.startActivityForResult(getPhotoZoomIntent(uri,aspectX,aspectY,outputX,outputY), PHOTORESOULT);
    } 
	
	public static void startPhotoZoom(Fragment fragment,Uri uri,int aspectX ,int aspectY ,int outputX ,int outputY) {  
        fragment.startActivityForResult(getPhotoZoomIntent(uri,aspectX,aspectY,outputX,outputY), PHOTORESOULT);
    } 
	
	private static Intent getPhotoZoomIntent(Uri uri,int aspectX ,int aspectY ,int outputX ,int outputY){
		Intent intent = new Intent("com.android.camera.action.CROP");  
        intent.setDataAndType(uri, IMAGE_UNSPECIFIED); 
        intent.putExtra("crop", "true");  	
        if( aspectX!=0 && aspectY!=0  )
        {
        	intent.putExtra("aspectX", aspectX);  
    		intent.putExtra("aspectY", aspectY);
        }
        if( outputX!=0 && outputY!=0  )
        {
        	//判断裁剪图片的高度，防止裁剪时内存溢出
	        if( outputY>800 )
	        {
	        	outputX = outputX/2;
	        	outputY = outputY/2;
	        }
	        intent.putExtra("outputX", outputX);  
	        intent.putExtra("outputY", outputY);  
        }
        intent.putExtra("return-data", true); 
        return intent;
	}
	
	public static String convertUri2FilePath(Activity activity,Uri uri)
	{
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor actualimagecursor = activity.getContentResolver().query(uri,proj,null,null,null);
		int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		actualimagecursor.moveToFirst();
		return actualimagecursor.getString(actual_image_column_index);
	}
	
	public static Bitmap convertUri2Bitmap(Activity activity,Uri uri)
	{
		byte [] mContent = null;
		try {
			//将图片内容解析成字节数组 
			InputStream inputStream = activity.getContentResolver().openInputStream(Uri.parse(uri.toString()));
			mContent= readStream(inputStream); 
			inputStream.close();
		} catch (Exception e) {
		}
        //将字节数组转换为ImageView可调用的Bitmap对象 
        return getPicFromBytes(mContent, null); 
	}
	
	public static byte[] readStream(InputStream inStream) throws Exception { 
        byte[] buffer = new byte[1024]; 
        int len = -1; 
        ByteArrayOutputStream outStream = new ByteArrayOutputStream(); 
        while ((len = inStream.read(buffer)) != -1) { 
                 outStream.write(buffer, 0, len); 
        } 
        byte[] data = outStream.toByteArray(); 
        outStream.close();
        return data; 
   }
	
	public static Bitmap getPicFromBytes(byte[] bytes, BitmapFactory.Options opts) { 
		if (bytes != null) {
            if (opts != null) 
                return BitmapFactory.decodeByteArray(bytes, 0, bytes.length,opts); 
            else 
                return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
		}else{
			return null;
		}
    }
	
	 public static Uri convertBitmap2Uri( Activity activity, Bitmap bitmap ){
	    	if( bitmap!=null )
	    	{
	    		return Uri.parse(MediaStore.Images.Media.insertImage(activity.getContentResolver(), bitmap, null,null));
	    	}else{
	    		return null;
	    	}
	    }
	
}
