package com.teewell.samsunggolfmate.utils;

import java.text.CollationKey;
import java.text.Collator;
import java.util.Comparator;

import com.teewell.samsunggolfmate.beans.ProvinceBean;

/**  
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.utils
 * @title ComparatorProvince.java 
 * @Description TODO
 * @author Administrator
 * @date 2013-2-21 下午5:44:13
 * @Copyright Copyright(C) 2013-2-21
 * @version 1.0.0
 */
public class ComparatorProvince implements Comparator<ProvinceBean> {
	//关于Collator。  
	   private Collator collator = Collator.getInstance();//点击查看中文api详解  
	@Override
	public int compare(ProvinceBean object1, ProvinceBean object2) {
		//把字符串转换为一系列比特，它们可以以比特形式与 CollationKeys 相比较  
	    CollationKey key1=collator.getCollationKey(object1.getProvinceName().toString());//要想不区分大小写进行比较用o1.toString().toLowerCase()  
	    CollationKey key2=collator.getCollationKey(object2.getProvinceName().toString());  
	     return key1.compareTo(key2);//返回的分别为1,0,-1 分别代表大于，等于，小于。要想按照字母降序排序的话 加个“-”号  
	}

}