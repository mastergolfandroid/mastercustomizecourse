package com.teewell.samsunggolfmate.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import android.annotation.SuppressLint;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;


public class ComparatorUserScoreCard  implements Comparator<UserScoreCardObject>{
	@SuppressLint("SimpleDateFormat")
	private static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	@Override
	public int compare(UserScoreCardObject object1, UserScoreCardObject object2) {
		int flag = 0;
		try {
			flag = df.parse(object1.getSaveTime()).after(df.parse(object2.getSaveTime()))?-1:1;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return flag;
	}
}

