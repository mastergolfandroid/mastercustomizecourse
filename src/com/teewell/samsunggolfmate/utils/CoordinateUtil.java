package com.teewell.samsunggolfmate.utils;
import com.teewell.samsunggolfmate.beans.Point;

import android.util.Log;

/**
 * 经纬度和屏幕坐标转换的工具类。原理是通过图片的左上角、右下角经纬度，
 * 结合坐标系旋转、平移公式。图片绕中心点旋转前后，坐标不变。
 * @author Antony
 * 
 */
public class CoordinateUtil {
    private float rotateAngle; // 旋转角度
    private double latLTop; //图片左上角纬度
    private double lonLTop; //图片左上角经度
    private double latRBottom;//图片左下角纬度
    private double lonRBottom;//图片左下角经度
    private float offsetX; //航拍图左上角横坐标到屏幕左上角横坐标的偏移量
    private float offsetY; //航拍图左上角纵坐标到屏幕左上角纵坐标的偏移量
    private float xO;// 新坐标系的原点在旧坐标系的x坐标
    private float yO;// 新坐标系的原点在旧坐标系的y坐标
    private float midpointX; // 图片中心点X坐标
    private float midpointY; // 图片中心点Y坐标
    private float offsetH;
    private float offsetW;
    /**
     * @return the offsetX
     */
    public float getOffsetX() {
        return offsetX;
    }
    /**
     * @param offsetX the offsetX to set
     */
    public void setOffsetX(float offsetX) {
        this.offsetX = offsetX;
    }
    /**
     * @return the offsetY
     */
    public float getOffsetY() {
        return offsetY;
    }

    /**
     * @param offsetY the offsetY to set
     */
    public void setOffsetY(float offsetY) {
        this.offsetY = offsetY;
    }
    
    /**
     * @return the rotateAngle
     */
    public float getRotateAngle() {
        return rotateAngle;
    }

    /**
     * @return the latLTop
     */
    public double getLatLTop() {
        return latLTop;
    }

    /**
     * @param latLTop the latLTop to set
     */
    public void setLatLTop(double latLTop) {
        this.latLTop = latLTop;
    }

    /**
     * @return the lonLTop
     */
    public double getLonLTop() {
        return lonLTop;
    }

    /**
     * @param lonLTop the lonLTop to set
     */
    public void setLonLTop(double lonLTop) {
        this.lonLTop = lonLTop;
    }

    /**
     * @return the latRBottom
     */
    public double getLatRBottom() {
        return latRBottom;
    }

    /**
     * @param latRBottom the latRBottom to set
     */
    public void setLatRBottom(double latRBottom) {
        this.latRBottom = latRBottom;
    }

    /**
     * @return the lonRBottom
     */
    public double getLonRBottom() {
        return lonRBottom;
    }

    /**
     * @param lonRBottom the lonRBottom to set
     */
    public void setLonRBottom(double lonRBottom) {
        this.lonRBottom = lonRBottom;
    }

    /**
     * @param rotateAngle the rotateAngle to set
     */
    public void setRotateAngle(float rotateAngle) {
        this.rotateAngle = rotateAngle;
    }

    /**
     * @return the bmpWidth
     */
    public float getBmpWidth() {
        return bmpWidth;
    }

    /**
     * @param bmpWidth the bmpWidth to set
     */
    public void setBmpWidth(float bmpWidth) {
        this.bmpWidth = bmpWidth;
    }

    /**
     * @return the bmpHeight
     */
    public float getBmpHeight() {
        return bmpHeight;
    }

    /**
     * @param bmpHeight the bmpHeight to set
     */
    public void setBmpHeight(float bmpHeight) {
        this.bmpHeight = bmpHeight;
    }

    public float bmpWidth;
    public float bmpHeight;

    private float viewWidth;
    private float viewHeight;

    private double onePxToLat;
    private double onePxToLon;

    private double cost;
    private double sint;
    
    private int[] validArea;
    private float density;
    private float ratio;

    public float getViewWidth(){
        return viewWidth;
    }
    public float getViewHeight(){
        return viewHeight;
    }
    
    /**
     * @param density 屏幕密度
     * @param validArea 有效区域的x1 y1, x2 y2坐标
     * @param offsetH 图片在原图放大或缩小后的Y坐标位置
     * @param rotateAngle
     *            旋转的角度
     * @param width
     *            图片的宽
     * @param height
     *            图片的高
     * @param lonLTop
     *            图片左上角的经度
     * @param latLTop
     *            图片左上角的纬度
     * @param lonRBottom
     *            图片右下角的经度
     * @param latRBottom
     *            图片右下角的纬度

     */
    public CoordinateUtil(float ratio,float density, int[] validArea, float offsetH, float offsetW, float rotateAngle, float bmpWidth, float bmpHeight,
            float viewWidth, float viewHeight, double lonLTop, double latLTop,
            double lonRBottom, double latRBottom) {
        this.ratio = ratio;
        this.density = density;
        this.validArea = validArea;
        this.offsetH = offsetH;
        this.offsetW = offsetW;
        
        this.rotateAngle = rotateAngle;
        this.bmpWidth = bmpWidth;
        this.bmpHeight = bmpHeight;
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
        this.lonLTop = lonLTop;
        this.latLTop = latLTop;
        this.lonRBottom = lonRBottom;
        this.latRBottom = latRBottom;
        
        reset();

    }
    
    /**
     * 重新计算屏幕偏移量、终点坐标等
     */
    public void reset(){
        computeOffset(bmpWidth, bmpHeight, viewWidth, viewHeight);
        
        onePxToLon = Math.abs((lonRBottom - lonLTop) / bmpWidth);
        onePxToLat = Math.abs((latRBottom - latLTop) / bmpHeight);
        
        cost = Math.cos(rotateAngle * Math.PI / 180);
        sint = Math.sin(rotateAngle * Math.PI / 180);
        calculateMidpoint();
        calculateOrigin();
    }


    /**
     * 计算图片的中心点坐标
     */
    private void calculateMidpoint() {
        midpointX = bmpWidth / 2;
        midpointY = bmpHeight / 2;
    }

    /**
     * 计算新的原点在旧坐标系的坐标
     */
    private void calculateOrigin() {
        xO = (float) (midpointX * (1 - cost) + midpointY * sint);
        yO = (float) (midpointY * (1 - cost) - midpointX * sint);
    }

    /**
     * 计算屏幕上的一点在绕中心点旋转以后的坐标系的坐标 根据公式推导:x1=x2cost-y2sint+xo1 y1=x2sint+y2cost+yo1
     * x = x1
     * 
     * @param touchX
     *            点击的点在屏幕上的X坐标
     * @param touchY
     *            点击的点在屏幕上的Y坐标
     * @return
     */
    public Point getNewXYByTouch(float touchX, float touchY) {
        Point point = new Point();
        point.x = (float) ((touchX - offsetX) * cost + (touchY - offsetY)
                * sint - yO * sint - xO * cost);
        point.y = (float) ((-(touchX - offsetX)) * sint + (touchY - offsetY)
                * cost + xO * sint - yO * cost);
        return point;
    }

    /**
     * 旋转后点击的点的坐标转换为经纬度
     * 
     * @param touchX
     *            点击的点在屏幕上的X坐标
     * @param touchY
     *            点击的点在屏幕上的Y坐标
     * @return
     */
    public double[] getLonLatByTouch(float touchX, float touchY) {
        Point point = getNewXYByTouch(touchX, touchY);
        double[] temp = new double[2];
        temp[0] = point.x * onePxToLon + lonLTop;
        temp[1] = latLTop - point.y * onePxToLat;
        return temp;
    }

    /**
     * 将经纬度装换为屏幕上的点坐标
     * 
     * @param lon
     *            经度
     * @param lat
     *            纬度
     * @return Point对象
     */
    public Point convertLonLatToOldXY(double lon, double lat) {
        Point point = new Point();

        if (lon>0) {
        	double x1 = (lon - lonLTop) / onePxToLon;
            double y1 = (latLTop - lat) / onePxToLat;

            System.out.println("x1=" + x1);
            System.out.println("y1=" + y1);
            point.x = (float) (x1 * cost - y1 * sint + xO) + offsetX;
            point.y = (float) (x1 * sint + y1 * cost + yO) + offsetY;
            System.out.println("x=" + point.x);
            System.out.println("y=" + point.y);
		}else {
			point.x = Short.MAX_VALUE;
			point.y = Short.MAX_VALUE;
		}
        
        
        point.lon = lon;
        point.lat = lat;
        return point;
    }

   

   
    /**
     * 计算偏移量
     * @param bmpWidth 图片的宽
     * @param bmpHeight 图片的高
     * @param viewWidth 控件的宽
     * @param viewHeight 控件的高
     */
    private void computeOffset(float bmpWidth, float bmpHeight,
            float viewWidth, float viewHeight) {
        int x1 = validArea[0];
        int y1 = validArea[1];
        int x2 = validArea[2];
        int y2 = validArea[3];
        
        if((y2 - y1) < 0 || (x2 - x1) <= 0){            
            offsetX = Math.abs((bmpWidth - viewWidth) / 2);
            offsetY = Math.abs((bmpHeight - viewHeight) / 2);
            if (bmpWidth > viewWidth) {
                offsetX = -offsetX;
            }
            if (bmpHeight > viewHeight) {
                offsetY = -offsetY;
            }
        }else {            
            //默认把有效地图放到最大时，用这个offset
            offsetX = offsetW - Math.abs((viewWidth - (x2 -x1)*ratio) / 2);
            /*if(offsetX < 0){
                offsetX = Math.abs((x2 -x1 - viewWidth) / 2) - 1;
//                offsetX = Math.abs(offsetW) * -1;
            }else{
            }*/
            offsetX = - offsetX;
            offsetY = Math.abs(offsetH) * -1;
        }
         
        
        Log.d("test", "x1 = " + x1 + " x2 = " + x2 + " offsetw = " + offsetW + " offsetX = " + offsetX + " offsetY = " + offsetY + " density = " + density);
    }

    /**
     * 计算两点之间的大地距离
     * @see com.util.Coordinates#getDistanceBettwen(com.dna.map.Point, com.dna.map.Point)
     */
    public float getDistanceBettwen(Point pointA, Point pointB) {
        double[] touchLonLatA = getLonLatByTouch(pointA.x, pointA.y);
        double[] touchLonLatB = getLonLatByTouch(pointB.x, pointB.y);
        return DistanceUtil.distanceBetween(touchLonLatA[1], touchLonLatA[0],
                touchLonLatB[1], touchLonLatB[0]);
    }
}
