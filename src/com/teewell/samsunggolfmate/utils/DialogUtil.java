package com.teewell.samsunggolfmate.utils;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface.OnClickListener;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.views.FullDialog;

/**  
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.utils
 * @title DialogUtil.java 
 * @Description TODO
 * @author Administrator
 * @date 2013-2-20 上午11:03:17
 * @Copyright Copyright(C) 2013-2-20
 * @version 1.0.0
 */
public class DialogUtil{
	public static FullDialog fullScreenDialog;
	/**设置全屏刷新数据Dialog
	 * @param activity dialog所在页面
	 * @param loading 是否显示
	 */
	public static void setLoadingState(Activity activity,boolean loading){
		if (loading) {
			if (null == DialogUtil.fullScreenDialog) {
				DialogUtil.fullScreenDialog = new FullDialog(activity,R.style.tip_dialog);
			}
			DialogUtil.fullScreenDialog.show();
		}else{
			if (DialogUtil.fullScreenDialog!=null && DialogUtil.fullScreenDialog.isShowing()) {
				DialogUtil.fullScreenDialog.dismiss();
				DialogUtil.fullScreenDialog = null;
			}
		}
	}
	public static void showAskDialog(Activity activity,String title,String msg, OnClickListener oklistener){
		Builder builder = new Builder(activity).setCancelable(true)
				.setMessage(msg)
				.setTitle(title)
				.setNegativeButton(R.string.ok, oklistener)
				.setPositiveButton(R.string.cancel, null);
		builder.create().show();

	}
}

