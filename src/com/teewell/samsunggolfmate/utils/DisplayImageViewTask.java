package com.teewell.samsunggolfmate.utils;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Displays bitmap in {@link ImageView}. Must be called on UI thread.
 * 
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 * @see ImageLoadingListener
 */
final class DisplayImageViewTask implements Runnable {

	private final Bitmap bitmap;
	private final ImageView imageView;

	public DisplayImageViewTask(Bitmap bitmap, ImageView imageView) {
		this.bitmap = bitmap;
		this.imageView = imageView;
	}

	public void run() {
		imageView.setImageBitmap(bitmap);
	}
}
