package com.teewell.samsunggolfmate.utils;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.master.mastercourse.singlecourse.R;

import android.content.Context;
import android.location.Location;
/** 距离换算工具
 * （包括屏幕像素、地球实际距离、距离换算单位转换等）
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.utils
 * @title DistanceUtil.java 
 * @Description TODO
 * @author Administrator
 * @date 2013-1-24 下午3:34:14
 * @Copyright Copyright(C) 2013-1-24
 * @version 1.0.0
 */
public class DistanceUtil {
	/**
	 * 码
	 */
	public static final int YARD = 0; 
	/**
	 * 米
	 */
	public static final int METER = 1; 
	/**
	 * 英里
	 */
	public static final int MILE = 2; 
	/**
	 * 公里
	 */
	public static final int KILOMETER = 3;
	/**
	 *米和码的转换比率
	 */
	public static final float RATE_YARD = 0.9144f;
	/**
	 * //一英里等于多少米
	 */
	public static final float RATE_MILE = 1609.344f;
	/**
	 * //一公里里等于多少米
	 */
	public static final float RATE_KM = 1000.0f;

	/**
	 * 根据两点的经纬度计算大地距离,返回码值
	 * @param startLatitude
	 * @param startLongitude
	 * @param endLatitude
	 * @param endLongitude
	 * @return 码code
	 * @see Location#distanceBetween(double, double, double, double, float[])
	 */
	public static int getDistanceByYard(double startLatitude,
			double startLongitude, double endLatitude, double endLongitude) {
		int ret = Math.round(
				distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude) / RATE_YARD);
		return ret;
	}
	/**
	 * 根据两点的经纬度计算大地距离,返回米值
	 * @param startLatitude
	 * @param startLongitude
	 * @param endLatitude
	 * @param endLongitude
	 * @return meter米
	 * @see Location#distanceBetween(double, double, double, double, float[])
	 */
	public static int getDistanceByMeter(double startLatitude,
			double startLongitude, double endLatitude, double endLongitude) {
		int ret = Math.round(distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude));
		return ret;
	}
	/**
	 * 根据两点的经纬度计算大地距离,返回英里值
	 * @param startLatitude
	 * @param startLongitude
	 * @param endLatitude
	 * @param endLongitude
	 * @return mile英里
	 * @see Location#distanceBetween(double, double, double, double, float[])
	 */
	public static float getDistanceByMile(double startLatitude,
			double startLongitude, double endLatitude, double endLongitude) {
		float ret = Math.round(distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude)) /
				RATE_MILE;
		return ret;
	}
	/**
	 * 根据两点的经纬度计算大地距离,返回公里值
	 * @param startLatitude
	 * @param startLongitude
	 * @param endLatitude
	 * @param endLongitude
	 * @return kilometer公里
	 * @see Location#distanceBetween(double, double, double, double, float[])
	 */
	public static float getDistanceByKilometer(double startLatitude,
			double startLongitude, double endLatitude, double endLongitude) {
		float ret = Math.round(distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude)) /
				RATE_KM;
		return ret;
	}
	

	/**
	 * 根据两点的经纬度计算大地距离
	 * @param startLatitude
	 * @param startLongitude
	 * @param endLatitude
	 * @param endLongitude
	 * @return kilometer公里
	 * @see Location#distanceBetween(double, double, double, double, float[])
	 */
	public static String getDistance(double startLatitude,
			double startLongitude, double endLatitude, double endLongitude,int distanceUnit) {
		String ret;
		switch (distanceUnit) {
		case YARD:
		default:
			ret = getDistanceByYard(startLatitude, startLongitude, endLatitude, endLongitude)+"";
			break;
		case METER:
			ret = getDistanceByMeter(startLatitude, startLongitude, endLatitude, endLongitude)+"";
			break;
		case MILE:{
			float distance = getDistanceByMile(startLatitude, startLongitude, endLatitude, endLongitude);
			DecimalFormat df = new DecimalFormat();
			df.applyPattern("#####.00");
			ret = df.format(distance);
		}
			break;
		case KILOMETER:{
			float distance = getDistanceByKilometer(startLatitude, startLongitude, endLatitude, endLongitude);
			DecimalFormat df = new DecimalFormat();
			df.applyPattern("#####.00");
			ret = df.format(distance);
			break;
		}
		}
		return ret;
	}
	
	 /**
     * 根据经纬度计算大地距离
     * @param startLatitude
     * @param startLongitude
     * @param endLatitude
     * @param endLongitude
     * @return 米meter
     * @see Location#distanceBetween(double, double, double, double, float[])
     */
    public static float distanceBetween(double startLatitude,
            double startLongitude, double endLatitude, double endLongitude) {
    	
        float[] results = new float[3];
        Location.distanceBetween(startLatitude, startLongitude, endLatitude,
                endLongitude, results);
        //the first value is the distance
        return results[0];
    }
    
    /**
     * 将度分秒格式的经纬度转换为小数格式
     * 
     * @param str
     *            度分秒字符串
     * @return 小数格式的经纬度
     * @throws IllegalArgumentException
     *             如果度分秒格式的经纬度格式不是XX°XX′XX″[ESNW],则抛出该异常
     */
    public static double converterToDecimal(String str) {
        double result = 0.0;
        String regex = "(\\d{1,3})°(\\d{1,2})′?([0-9.]{1,6})″?[ESNW]";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        String degree = null;
        String minute = null;
        String second = null;
        if (matcher.find()) {
            int groupCount = matcher.groupCount();
            String[] values = new String[groupCount + 1];

            for (int i = 0; i <= groupCount; i++) {
                values[i] = matcher.group(i);
            }

            degree = values[1];
            minute = values[3];
            second = values[5];

            if (degree != null) {
                result = Double.valueOf(degree);
                if (minute != null) {
                    result += Double.valueOf(minute) / 60;
                }
                if (second != null) {
                    result += Double.valueOf(second) / 3600;
                }
            }
            return result;
        }
        throw new IllegalArgumentException("请输入正确的数值");
    }
    public static String getDistanceUnit(Context context,int select){
		String unit = "";
		switch (select) {
		case YARD:
		default:
			unit = context.getString(R.string.yard);
			break;
		case METER:
			unit = context.getString(R.string.meter);
			break;
		case MILE:
			unit = context.getString(R.string.mile);
			break;
		case KILOMETER:
			unit = context.getString(R.string.kilometer);
			break;
		}
		return unit;
	}
    public static String getDistanceUnit(Context context,float distance,int select){
		String unit = "";
		DecimalFormat df = new DecimalFormat();
		switch (select) {
		case YARD:
		default:
			df.applyPattern("#####");
			unit = df.format(distance)+context.getString(R.string.yard);
			break;
		case METER:
			df.applyPattern("#####");
			unit = df.format(distance)+context.getString(R.string.meter);
			break;
		case MILE:
			df.applyPattern("#####.00");
			unit = df.format(distance)+context.getString(R.string.mile);
			break;
		case KILOMETER:
			df.applyPattern("#####.00");
			unit = df.format(distance)+context.getString(R.string.kilometer);
			break;
		}
		return unit;
	}
}

