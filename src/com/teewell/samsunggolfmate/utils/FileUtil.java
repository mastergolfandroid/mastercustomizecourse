package com.teewell.samsunggolfmate.utils;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StreamCorruptedException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EncodingUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

public class FileUtil {
	 /**
	  * 该类负责对文件所有的操作
	  */
	   
	 private Context context;
	 
	 public FileUtil(Context context){
	  this.context=context;
	 }
	 
	 
	 /**
	  * 对文件进行读取 
	  * 返回二进制数组
	  */
	 public byte[] readFile(String fileName) throws Exception{
	  FileInputStream fis=context.openFileInput(fileName);
	  byte buffer[]=new byte[1024];
	  ByteArrayOutputStream bos=new ByteArrayOutputStream();
	  int len;
	  while((len=fis.read(buffer))!=-1){
	   bos.write(buffer, 0, len);
	  }
	  fis.close();
	  bos.close();
	  return bos.toByteArray();
	 }
	 
	 /**
	  * 将信息写到文件中去
	  * 手机内存中
	  */
	 public void save(String fileName,String content)throws Exception{
	  //私有
	  FileOutputStream fos=(FileOutputStream)context.openFileOutput(fileName, Context.MODE_PRIVATE);
	  fos.write(content.getBytes());
	  fos.close();
	 }
	 /**
	  * 将信息写到sd卡中
	  */
	 public void saveMemory(String fileName,String content) throws Exception{
	  if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
	  //在存储卡上建立fileName的文件
	  File f=new File(Environment.getExternalStorageDirectory(),fileName);
	  //如果文件不存在 则自动创建 所以不用再文件不存在的情况下自己手动调用创建
	  FileOutputStream fos=new FileOutputStream(f);
	        fos.write(content.getBytes());
	        fos.close();
	  }
	 }
	 
	 /**
	  * 将信息追加到手机内存中的文件中
	  */
	 public void saveAppend(String fileName,String content)throws Exception{
	  FileOutputStream fos=(FileOutputStream)context.openFileOutput(fileName, Context.MODE_APPEND);
	  fos.write(content.getBytes());
	  fos.close();
	 }
	 /**
	  * 将信息追加到sd卡中
	  */
	 public void saveMemoryAppend(String fileName,String content) throws Exception{
	  File f=new File(Environment.getExternalStorageDirectory(),fileName);
	  FileOutputStream fos=new FileOutputStream(f);
	  BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(fos));
	  bw.append(content);
	  bw.close();
	  fos.close();
	 }
	 /**
	  * 获取手机中文件输入流
	  */
	 public InputStream getPhoneInputStream(String fileName)throws Exception{
	  File f=new File(fileName);
	  InputStream is=new FileInputStream(f);
	  return is;
	 }
	 /**
	  * 获取输入流
	  */
	 public InputStream getInputStream(String fileName)throws Exception{
	  File f=new File(Environment.getExternalStorageDirectory(),fileName);
	  InputStream is=new FileInputStream(f);
	  return is;
	 }
	 /**
	  * 将输入流转变成字节数组
	  */
	 public byte[] convertByte(InputStream in)throws Exception{
	  ByteArrayOutputStream bos=new ByteArrayOutputStream();
	     byte[] buffer=new byte[100];
	     int len;
	     while((len=in.read(buffer))!=-1){
	      bos.write(buffer, 0, len);
	     }
	        return bos.toByteArray();  
	 }
	/**
	 * 将文件读为2进制数组
	 * 
	 * @param file
	 * @return 返回文件的2进制数组
	 * @throws IOException
	 */
	public static byte[] getHex2FromFile(File file) throws IOException {
		FileInputStream input = new FileInputStream(file);
		byte[] inputTemp = new byte[input.available()];
		input.read(inputTemp);
		input.close();
		return inputTemp;
	}

	/**
	 * 将二进制数组存储到文件
	 * 
	 * @param hex2
	 *            二进制字符数组
	 * @param filePath
	 *            要存储文件的路径
	 * @throws IOException
	 */
	public static void writeHex2ToFile(byte[] hex2, String filePath)
			throws Exception {
		File file = new File(filePath);
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			file.createNewFile();
		}
		InputStream input = new ByteArrayInputStream(hex2);
		FileOutputStream output = new FileOutputStream(filePath);
		byte[] b = new byte[1024 * 5];
		int len;
		while ((len = input.read(b)) != -1) {
			output.write(b, 0, len);
		}
		output.flush();
		output.close();
		input.close();

	}

	/**从网络上下载图片到本地，并存储在文件下
	 * @param url 网络地址
	 * @param photoPathUrl 本地路径
	 */
	public synchronized static boolean downloadAerialPhoto(String url, String photoPathUrl) {
		boolean flag = false;
		// String photoHost = "http://192.168.1.220:8080/res/img/aerialPhoto/";
		if (!url.contains("http") || photoPathUrl == null) {
			return flag;
		}
		Log.i("PhotoPath", url);
		// httpGet连接对象
		HttpGet httpRequest = new HttpGet(url);
		// 取得HttpClient 对象
		HttpClient httpclient = new DefaultHttpClient();
		try {
			// 请求httpClient ，取得HttpRestponse
			HttpResponse httpResponse = httpclient.execute(httpRequest);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// 取得相关信息 取得HttpEntiy
				HttpEntity httpEntity = httpResponse.getEntity();
				// 获得一个输入流
				InputStream is = httpEntity.getContent();
				Bitmap bitmap = BitmapFactory.decodeStream(is);
				FileOutputStream bos = new FileOutputStream(photoPathUrl);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
				bitmap.isRecycled();
				bos.flush();
				bos.close();
				is.close();
				flag = true;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings("unused")
	public void downFile(String url, String path, String filename)
			throws IOException {
		// 下载函数
		// filename = url.substring(url.lastIndexOf("/") + 1);
		// 获取文件名
		URL myURL = new URL(url);
		URLConnection conn = myURL.openConnection();
		conn.connect();
		InputStream is = conn.getInputStream();
		int fileSize = conn.getContentLength();// 根据响应获取文件大小
		if (fileSize <= 0)
			throw new RuntimeException(" unknow file size ");
		if (is == null)
			throw new RuntimeException(" inputstream is null ");
		FileOutputStream fos = new FileOutputStream(path + filename);
		// 把数据存入路径+文件名
		byte buf[] = new byte[1024];
		int downloadFileSize = 0;
		// sendData(0);
		do {
			// 循环读取
			int numread = is.read(buf);
			if (numread == -1) {
				break;
			}
			fos.write(buf, 0, numread);
			// downloadFileSize += numread;
			downloadFileSize += numread;

			// sendData(1);// 更新进度条
		} while (true);
		// sendData(2);// 通知下载完成
		if (is != null) {
			is.close();
		}
		if (fos != null) {
			fos.close();
		}

	}

	/**
	 * 判断SD卡是否存在
	 */
	public static boolean isExternalStorageState() {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			return true;
		}
		return false;
	}

	// public static void downloadAerialPhoto(String url,String photoPathUrl){
	// // String photoHost = "http://192.168.1.220:8080/res/img/aerialPhoto/";
	// HttpGet get = new HttpGet(url);
	// // httpGet连接对象
	// // 取得HttpClient 对象
	// HttpClient httpclient = new DefaultHttpClient();
	// try {
	// // 请求httpClient ，取得HttpRestponse
	// HttpResponse httpResponse = httpclient.execute(get);
	// if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
	// // 取得相关信息 取得HttpEntiy
	// HttpEntity httpEntity = httpResponse.getEntity();
	// // 获得一个输入流
	// InputStream is = httpEntity.getContent();
	// File file = new File(photoPathUrl);
	// if (!file.exists()) {
	// file.createNewFile();
	// }
	// FileOutputStream fos = new FileOutputStream(photoPathUrl);
	// int fileSize = is.read();// 根据响应获取文件大小
	// byte buf[] = new byte[fileSize];
	// fos.write(buf, 0, fileSize);
	// if (is != null)
	// {
	// is.close();
	// }
	// if (fos != null)
	// {
	// fos.close();
	// }
	// }
	// } catch (ClientProtocolException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }finally{
	//
	// }
	// }
	public static String setBase64Object(Object object) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(outputStream);
			objectOutputStream.writeObject(object);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String courseInfo = new String(
				Base64.encode(outputStream.toByteArray()));
		return courseInfo;
	}

	public static Object getBase64Object(String info) {
		Object object = null;
		byte[] byte64Bytes = Base64.decode(info);
		ByteArrayInputStream inputStream = new ByteArrayInputStream(byte64Bytes);
		ObjectInputStream objectInputStream;
		try {
			objectInputStream = new ObjectInputStream(inputStream);
			object = objectInputStream.readObject();
		} catch (StreamCorruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return object;
	}

	public static boolean copyFileToSD(Context context, String fromPath,
			String toPath) {
		boolean flag = false;
		InputStream myInput;
		OutputStream myOutput;
		try {
			myOutput = new FileOutputStream(toPath);
			myInput = context.getAssets().open(fromPath);
			byte[] buffer = new byte[myInput.available()];
			int count = 0;
			while ((count = myInput.read(buffer)) > 0) {
				myOutput.write(buffer, 0, count);
			}
			myOutput.flush();
			myInput.close();
			myOutput.close();
			flag = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return flag;
	}

	/*
	 * 写在/mnt/sdcard/目录下面的文件
	 */
	public static void writeFileSdcard(File fileName, String message) {

		try {

			// FileOutputStream fout = openFileOutput(fileName, MODE_PRIVATE);

			FileOutputStream fout = new FileOutputStream(fileName);

			byte[] bytes = message.getBytes();

			fout.write(bytes);

			fout.close();

		}

		catch (Exception e) {

			e.printStackTrace();

		}

	}

	/*
	 *  读在/mnt/sdcard/目录下面的文件
	 */
	public static String readFileSdcard(File fileName) {

		String res = "";

		try {

			FileInputStream fin = new FileInputStream(fileName);

			int length = fin.available();

			byte[] buffer = new byte[length];

			fin.read(buffer);

			res = EncodingUtils.getString(buffer, "UTF-8");

			fin.close();

		}

		catch (Exception e) {

			e.printStackTrace();

		}

		return res;

	}
	/**
	 * 删除文件夹所有内容
	 *
	 */
	public static void deleteFile(File file) {

		if (file.exists()) { // 判断文件是否存在
			if (file.isFile()) { // 判断是否是文件
				file.delete(); // delete()方法 你应该知道 是删除的意思;
			} else if (file.isDirectory()) { // 否则如果它是一个目录
				File files[] = file.listFiles(); // 声明目录下所有的文件 files[];
				for (int i = 0; i < files.length; i++) { // 遍历目录下所有的文件
					deleteFile(files[i]); // 把每个文件 用这个方法进行迭代
				}
			}
			file.delete();
		} else {
			//
		}
	}
	/**
	 * 删除文件夹所有内容
	 *
	 */
	public static void deleteFile(String path) {
		File file = new File(path);
		if (file.exists()) { // 判断文件是否存在
			if (file.isFile()) { // 判断是否是文件
				file.delete(); // delete()方法 你应该知道 是删除的意思;
			} else if (file.isDirectory()) { // 否则如果它是一个目录
				File files[] = file.listFiles(); // 声明目录下所有的文件 files[];
				for (int i = 0; i < files.length; i++) { // 遍历目录下所有的文件
					deleteFile(files[i]); // 把每个文件 用这个方法进行迭代
				}
			}
			file.delete();
		} else {
			//
		}
	}
	/** 
     * 获取文件夹大小 
     * @param file File实例 
     * @return long 单位为M 
     * @throws Exception 
     */  
    public static long getFolderSize(java.io.File file)throws Exception{  
        long size = 0;  
        java.io.File[] fileList = file.listFiles();  
        for (int i = 0; i < fileList.length; i++)  
        {  
            if (fileList[i].isDirectory())  
            {  
                size = size + getFolderSize(fileList[i]);  
            } else  
            {  
                size = size + fileList[i].length();  
            }  
        }  
        return size/1048576;  
    }  

/** 
     * 删除指定目录下文件及目录 
     *  
     * @param deleteThisPath 
     * @param filepath 
     * @return 
     */  
    public static void deleteFolderFile(String filePath, boolean deleteThisPath)  
            throws IOException {  
        if (!TextUtils.isEmpty(filePath)) {  
            File file = new File(filePath);  
  
            if (file.isDirectory()) {// 处理目录  
                File files[] = file.listFiles();  
                for (int i = 0; i < files.length; i++) {  
                    deleteFolderFile(files[i].getAbsolutePath(), true);  
                } 
            }  
            if (deleteThisPath) {  
                if (!file.isDirectory()) {// 如果是文件，删除  
                    file.delete();  
                } else {// 目录  
                    if (file.listFiles().length == 0) {// 目录下没有文件或者目录，删除  
                        file.delete();  
                    }  
                }  
            }  
        }  
    }  
}
