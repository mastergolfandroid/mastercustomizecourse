package com.teewell.samsunggolfmate.utils;


import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;

import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;


/**  
 * @Project GolfMate
 * @package com.teewell.golfmate.utils
 * @title GpsUtil.java 
 * @Description TODO
 * @author Administrator
 * @date 2013-1-22 上午10:46:11
 * @Copyright Copyright(C) 2013-1-22
 * @version 1.0.0
 */
public class GpsSwitchUtil {
	/**
     * 确保GPS状态开启
     * @param mContext
     * @return boolean 开启返回true/否则返回false
     */
    public static boolean ensureOpenGPS(Context mContext) {
        try {
            LocationManager alm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            boolean flag = alm.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
            if(!flag){
            	trigerSwitch(mContext);
            }
            SharedPreferencesUtils util = new SharedPreferencesUtils(mContext);
            util.commitBoolean(SharedPreferenceConstant.SETTING_GPS_SWITCH, flag);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }   

    /**
     * 切换GPS开关
     * @param mContext
     * @return 无返回值
     */
    private static boolean trigerSwitch(Context mContext) {
    	Intent gpsIntent = new Intent();
		gpsIntent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
		gpsIntent.addCategory("android.intent.category.ALTERNATIVE");
		gpsIntent.setData(Uri.parse("custom:3"));
		try {
		    PendingIntent.getBroadcast(mContext, 0, gpsIntent, 0).send();
		} catch (CanceledException e) {
		    e.printStackTrace();
		    return false;
		}
		return true;
    }

    /**
     * 恢复GPS开关
     * @param mContext
     * @return 无返回值
     */
    public static void restoreSwitch(Context mContext) {
        SharedPreferencesUtils util = new SharedPreferencesUtils(mContext);
        boolean flag = util.getBoolean(SharedPreferenceConstant.SETTING_GPS_SWITCH, true);
        if(!flag){
        	trigerSwitch(mContext);
        }

    }

}

