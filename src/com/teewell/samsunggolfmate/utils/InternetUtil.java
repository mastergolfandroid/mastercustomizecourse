package com.teewell.samsunggolfmate.utils;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

/**
 * 检测互联网是否连接，已经连接方式。wife是否打开，以及打开wife，关闭wife
 * @author 程明炎
 *
 */
public class InternetUtil {  
    
	/**
     * 检查是否联网
     * @param mContext
     * @return  string[]  mum[0]是否联网，如果联网num[1]为联网的方式
     */
    public static String []   getInternetConnect(Context mContext) {
        String []num=new String[2];
        ConnectivityManager cwjManager1=(ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo =cwjManager1.getActiveNetworkInfo();       
        
        num[0]=Boolean.FALSE.toString();
        num[1]="";
        if ( networkInfo != null && networkInfo.isConnected()) {
            num[0]=Boolean.TRUE.toString();
            num[1]=networkInfo.getTypeName();

        }
        return num;
    }
  
    
    public static boolean  checkConnect(Context mContext) {
        boolean isConnect = false;
        ConnectivityManager cwjManager1=(ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo =cwjManager1.getActiveNetworkInfo();       
        
        if ( networkInfo != null && networkInfo.isConnected()) {
            isConnect = Boolean.TRUE;
        }
        return isConnect;
    }

    public WifiManager wManager;
    /**
     * 取wifi状态是否开启
     * @param mContext
     * @return boolean true--wife开启/false--wife关闭
     */
    public static boolean checkWIFEStatu(Context mContext) {
        Boolean flag=false;
        try {
            WifiManager wManager  = (WifiManager) mContext
                    .getSystemService(Context.WIFI_SERVICE);            
            if (wManager != null ){
                flag=wManager.isWifiEnabled()?true:false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return flag;
    }
    /**
     * 打开wifi
     * @param mContext
     * @return
     */
    public static void  openWIFE(Context mContext) {
        try {
            WifiManager wManager  = (WifiManager) mContext
                    .getSystemService(Context.WIFI_SERVICE);            
           
            if (wManager.isWifiEnabled()) {
             
                wManager.setWifiEnabled(true);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 关闭wifi
     * @param mContext
     * @return
     */
    public static void  closeWIFE(Context mContext) {
        try {
            WifiManager wManager  = (WifiManager) mContext
                    .getSystemService(Context.WIFI_SERVICE);            
            
            if (!wManager.isWifiEnabled()) {
             
                wManager.setWifiEnabled(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void setWirless(Context mContext){
       Intent wirless = new Intent("android.settings.WIRELESS_SETTINGS");
       wirless.addCategory(Intent.CATEGORY_DEFAULT);
       mContext.startActivity(wirless);
   }

    //判断数据连接是否激活，如果激活则进一步判断是不是CMWAP连接，我的联网只用CMWAP，你可以根据自己的需要具体去改写。 

    public static boolean isNetworkCMWAPAvailable(Context context) {

               ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE); 
               if (connectivity != null) {
                   NetworkInfo[] info = connectivity.getAllNetworkInfo(); 
                   if (info != null) {
                     //System.out.println("NETWORK active info is well     "); 
                       for (int i = 0; i < info.length; i++) {
                           info[i].getTypeName(); 
                           String extraInfo = info[i].getExtraInfo(); 
                          
                         //System.out.println("NETWORK active info state      "  + info.getState() + "   "  + typeName + "   " + extraInfo); 
                           if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                                info[i].getSubtypeName(); 
                                if ( extraInfo!=null && extraInfo.contains("cmwap") ) { 
                                    //connectivity.stopUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "wap"); 
                                      
                                    return true; 
                                }
                           }
                       }
                   }
               }
              return false;
            }
}
