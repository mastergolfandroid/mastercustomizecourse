package com.teewell.samsunggolfmate.utils;

import com.baidu.location.*;

import android.content.Context;
import android.util.Log;
import android.os.Handler;

public class LocationUtil {

	public static String TAG = LocationUtil.class.getName();
	private static final String COORTYPE = "gcj02";
	
	private LocationClient locationClient;

	private Handler locationHandler;
	private int locationSignal;
	
	public double latitude;
	public double longitude;
	
	public static final int LOC_PERIOD = 0;
	public static final int LOC_ONCE = 1;

	private static final int TIME_ONCE = 100;
	private static final int TIME_PEROID = 1000;

	public static LocationUtil gLocationUtil = null;
	public BDLocationListener myListener = new MyLocationListenner();
	
	public static synchronized LocationUtil getInstance(Context context){
		if(null == gLocationUtil){
			gLocationUtil = new LocationUtil(context); 
		}
		return gLocationUtil;
	}
	
	private LocationUtil(Context context){
		locationClient = new LocationClient(context);
		locationClient.registerLocationListener(myListener);
	}
				
	/**
	 * 监听函数，有新位置的时候，刷新数据
	 */
	public class MyLocationListenner implements BDLocationListener {
		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null)
				return ;
			double newLatitude = location.getLatitude();
			double newLongitude = location.getLongitude();
			longitude = newLongitude;
			latitude = newLatitude;
			Log.i(TAG, "get new location");
			if (locationHandler != null) {
				locationHandler.sendEmptyMessage(locationSignal);
			}
		}
		
		/* 获取到详细地址时
		 * @see com.baidu.location.BDLocationListener#onReceivePoi(com.baidu.location.BDLocation)
		 */
		public void onReceivePoi(BDLocation poiLocation) {
		}
	}
	
	public void setHandlerInfo(Handler myHandler,int what_Gps, int accuracy){

		locationHandler = myHandler;
		locationSignal = what_Gps;

		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);			//打开gps
		option.setCoorType(COORTYPE);		//设置坐标类型
		option.setServiceName("com.baidu.location.service_v2.9");
		option.setPoiNumber(3);
		option.disableCache(true);		
		
		
		switch(accuracy){
			case LOC_PERIOD:
				option.setScanSpan(TIME_PEROID);
				option.setPriority(LocationClientOption.GpsFirst);        //不设置，默认是gps优先
				break;
			case LOC_ONCE:
				//设置定位模式，小于1秒则一次定位;大于等于1秒则定时定位，定位时间
				option.setScanSpan(TIME_ONCE);
				option.setPriority(LocationClientOption.NetWorkFirst);      //设置网络优先
				break;
		}
		locationClient.setLocOption(option);
		locationClient.start();
	}

	public boolean refreshLocation()
	{
		if(locationClient != null && locationClient.isStarted()){
			locationClient.requestLocation();
			return true;
		}else{
			return false;
		}
	}

	public void resetHandlerInfo(Handler myHandler,int what_Gps){

		locationHandler = myHandler;
		locationSignal = what_Gps;
		
	}
	
	public void clearHandlerInfo(){
		locationHandler = null;
		locationClient.stop();
	}

	public void destroy(){
		locationHandler = null;

		locationClient.stop();
		locationClient.unRegisterLocationListener(myListener);
		locationClient = null;
		
		gLocationUtil = null;
	}
}