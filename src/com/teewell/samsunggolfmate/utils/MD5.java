package com.teewell.samsunggolfmate.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**  
 * @Project GolfMate
 * @package com.teewell.golfmate.utils
 * @title MD5.java 
 * @Description MD5加密
 * @author DarkWarlords
 * @date 2013-1-21 下午3:29:54
 * @Copyright Copyright(C) 2013-1-21
 * @version 1.0.0
 */
public class MD5 {
	/*  
	* MD5加密  
	*/  
	private static final char HEX_DIGITS[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',  
    'a', 'b', 'c', 'd', 'e', 'f' };  
	public static String toHexString(byte[] b) {  
	    //String to  byte  
	    StringBuilder sb = new StringBuilder(b.length * 2);    
	    for (int i = 0; i < b.length; i++) {    
	        sb.append(HEX_DIGITS[(b[i] & 0xf0) >>> 4]);    
	        sb.append(HEX_DIGITS[b[i] & 0x0f]);    
	    }    
	    return sb.toString();    
	}  
	/*  
	* MD5加密  
	*/
	public static String getMD5Key(String s) {  
	    try {  
	        // Create MD5 Hash  
	        MessageDigest digest = java.security.MessageDigest.getInstance("MD5");  
	        digest.update(s.getBytes());  
	        byte messageDigest[] = digest.digest();  
	                                  
	        return toHexString(messageDigest);  
	    } catch (NoSuchAlgorithmException e) {  
	        e.printStackTrace();  
	    }  
	    return "";  
	}
	private final static String[] hexDigits = {
	      "0", "1", "2", "3", "4", "5", "6", "7",
	      "8", "9", "a", "b", "c", "d", "e", "f"};
	/**
	 * 将字符串算出16进制MD5串，字符串按系统编码进行编码
	 * @param origin 字符串
	 * @return 16进制MD5字符串
	 */
	public static String Encode16(String origin)throws Exception
	{
		String resultString = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString =byteArrayToHexString(md.digest((new String(origin)).getBytes()));
		}
		catch (Exception e) {
			throw new Exception(e.getLocalizedMessage());
		}
		return resultString;
	}
	/**
	 * 转换字节为10进制字符串
	 * @param b字节
	 * @return 10进制字符串
	 */
	private static String byteToHexString(byte b) {
		int n = b;
		if (n < 0) {
		  n = 256 + n;
		}
		int d1 = n / 16;
		int d2 = n % 16;
		return (hexDigits[d1] + hexDigits[d2]);
	}
	/**
	 * 转换字节数组为10进制字串
	 * @param b 字节数组
	 * @return 16进制字串
	 */
	private static String byteArrayToHexString(byte[] b) 
	{
		StringBuffer resultSb = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
		  resultSb.append(byteToHexString(b[i]));//若使用本函数转换则可得到加密结果的16进制表示，即数字字母混合的形式
		}
		return resultSb.toString();
	}
}
