package com.teewell.samsunggolfmate.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.teewell.samsunggolfmate.common.Contexts;

public class Photos {
	/**
	 * 调用相册的请求代码
	 */
	public static final int ALBLUM = 10;
	
	/**
	 * 调用拍照的请求代码
	 */
	public static final int PHOTOGRAPH = 20;
	
	/**
	 * 取景的的请求代码
	 */
	public static final int PHOTORESOULT = 30;
	
	/**
	 * 照片文件类型
	 */
	private static final String IMAGE_UNSPECIFIED = "image/*";
	
	/**
	 * 拍照时，临时存储一个副本到根目录下的temp.jpg
	 */
	public static final String PHOTOGRAPH_TEMP = Environment.getExternalStorageDirectory() + "/temp.png";
	
	/**
	 * 调用相册
	 * 调用该方法需要在指定的activity页面中添加onActivityResult回调方法
	 * @param activity Activity 传递指定的上下文环境
	 */
	public static void Album(Activity activity,String photoPath)
	{

		Intent intent = new Intent(Intent.ACTION_PICK);  
		intent.putExtra("crop", "false");
		intent.putExtra("return-data", true);
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED); 
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(photoPath)));  
        activity.startActivityForResult(intent, ALBLUM);
	}
	/**
	 * 调用相册
	 * 调用该方法需要在指定的activity页面中添加onActivityResult回调方法
	 * @param activity Activity 传递指定的上下文环境
	 */
	public static void Album(Activity activity)
	{
		Intent intent = new Intent(Intent.ACTION_PICK);  
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED); 
        activity.startActivityForResult(intent, ALBLUM);
	}
	/**
	 * 调用拍照
	 * 调用该方法需要在指定的activity页面中添加onActivityResult回调方法
	 * @param activity Activity 传递指定的上下文环境
	 */
	public static void Photograph (Activity activity,String photoPath){ 
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); 
		File file = new File(photoPath);
		//保存一个临时的照片副本
		if (file.exists()) {
			file.delete();
		}
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(photoPath)));  
        activity.startActivityForResult(intent, PHOTOGRAPH);
	}
	/**
	 * 调用拍照
	 * 调用该方法需要在指定的activity页面中添加onActivityResult回调方法
	 * @param activity Activity 传递指定的上下文环境
	 */
	public static void Photograph (Activity activity)
	{ System.out.println("111111111");
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); 
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(photoPath)));  
        activity.startActivityForResult(intent, PHOTOGRAPH);
	}
	/**
	 * 拍照和相册回调处理方法（调用缩放取景功能）
	 * @param activity Activity 传递应用上下文环境
	 * @param requestCode int 请求代码
	 * @param resultCode int 返回的代码
	 * @param data
	 */
	public static Bitmap callbackZoom(Activity activity,int requestCode, int resultCode, Intent data)
	{
		Bitmap bitmap = null;
		//resultCode = 0 表示没有或不存在
		if ( requestCode == 0)
		{
			return null;
		}
		switch(requestCode)
		{
		//调用相册后的处理
			case ALBLUM :
				if (data.getData()==null) {
					return null;
				}
				startPhotoZoom(activity,data.getData());
				break;
				//调用拍照后的处理
			case PHOTOGRAPH :
				//加载副本文件，进行缩放			
				System.out.println("22222222222");
	            File picture = new File(PHOTOGRAPH_TEMP);
	            startPhotoZoom(activity,Uri.fromFile(picture)); 
				break;
				//调用取景后的处理
			case PHOTORESOULT : 
				System.out.println("444444444444");
				Bundle extras = data.getExtras();
	            if (extras != null) {  
	            	System.out.println("yyyyyyyyy");
	            	bitmap = extras.getParcelable("data");
//	            	if (bitmap == null) {
//	            		Uri uri = data.getData();   
//		            	String[] proj = { MediaStore.Images.Media.DATA };   
//		            	Cursor actualimagecursor = activity.managedQuery(uri,proj,null,null,null);   
//		            	int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);   
//		            	actualimagecursor.moveToFirst();   
//		            	String img_path = actualimagecursor.getString(actual_image_column_index); 
//		            	File file = new File(img_path);
//		            	bitmap = PhotoUtil.readBitMap(file);
//					}
	            	Log.e("22222", "-----width"+bitmap.getWidth()+"----height"+bitmap.getHeight());
	                ByteArrayOutputStream stream = new ByteArrayOutputStream();  
	                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);//(0 - 100)压缩文件    
	            } 
				break;
		}
		return bitmap;
	}
	/**
	 * 拍照和相册回调处理方法
	 * @param activity Activity 传递应用上下文环境
	 * @param requestCode int 请求代码
	 * @param resultCode int 返回的代码
	 * @param data
	 */
	public static Bitmap callback(Activity activity,int requestCode, int resultCode, Intent data,String path)
	{
		Bitmap bitmap = null;
		Bitmap bitmapTemp = null;
		//resultCode = 0 表示没有或不存在
		if ( requestCode == 0 )
		{
			return null;
		}
		switch(requestCode)
		{
		//调用相册后的处理
			case ALBLUM :
//				photoZoom(activity,data.getData());
//	            break;
			case PHOTOGRAPH :
			case PHOTORESOULT : 
				System.out.println("444444444444");
				Bundle extras1 = data.getExtras();
	            if (extras1 != null) {  
	            	System.out.println("yyyyyyyyy");
	            	bitmapTemp = extras1.getParcelable("data");
	            	if (bitmapTemp == null) {
	            		Uri uri = data.getData();   
		            	String[] proj = { MediaStore.Images.Media.DATA };   
		            	Cursor actualimagecursor = activity.managedQuery(uri,proj,null,null,null);   
		            	int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);   
		            	actualimagecursor.moveToFirst();   
		            	String img_path = actualimagecursor.getString(actual_image_column_index); 
		            	File file = new File(img_path);
		            	bitmapTemp = BitmapUtil.readBitMap(file);
	            	}
	            }else{
	            	Uri uri = data.getData();   
	            	String[] proj = { MediaStore.Images.Media.DATA };   
	            	Cursor actualimagecursor = activity.managedQuery(uri,proj,null,null,null);   
	            	int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);   
	            	actualimagecursor.moveToFirst();   
	            	String img_path = actualimagecursor.getString(actual_image_column_index); 
	            	File file = new File(img_path);
	            	bitmapTemp = BitmapUtil.readBitMap(file);
	            }
	            	if(bitmapTemp.getHeight()<=Contexts.PHOTOHEIGHT){
	            		bitmap = bitmapTemp;
	            	}else {
	            		bitmap = BitmapUtil.zoomPhotoBitmap(bitmapTemp, Contexts.PHOTOHEIGHT, Contexts.PHOTOHEIGHT);
					}
	            	Log.d("111", "-----width"+bitmap.getWidth()+"----height"+bitmap.getHeight());
					File file = new File(path);
					if (file.exists()) {
						file.delete();
					}
						try {
							FileOutputStream bos = new FileOutputStream(path);
							bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
							Log.d("111", "---111--width"+bitmap.getWidth()+"----height"+bitmap.getHeight());
							bos.flush();
							bos.close();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
				break;
		}
		return bitmap;
	}
	/**
	 * 将大图片缩放到屏幕大小
	 * @param activity 调用该方法需要在指定的activity页面中添加onActivityResult回调方法
	 * @param uri
	 */
	public static void startPhotoZoom(Activity activity,Uri uri) {  
        Intent intent = new Intent("com.android.camera.action.CROP");  
        intent.setDataAndType(uri, IMAGE_UNSPECIFIED); 
        intent.putExtra("crop", "true");  
//		aspectX aspectY 是宽高的比例 ，默认的话可自由调整高度
		intent.putExtra("aspectX", 1);  
		intent.putExtra("aspectY", 1);  
        System.out.println("3333333333333");
       /* DisplayMetrics dm = new DisplayMetrics();		//屏幕分辨率容器
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);  
        float density  = dm.density;      				// 屏幕密度（像素比例：0.75/1.0/1.5/2.0）    
        int screenWidth  = (int)(dm.widthPixels * density + 0.5f);      // 屏幕宽（px，如：480px）    
        int screenHeight = (int)(dm.heightPixels * density + 0.5f);     // 屏幕高（px，如：800px）    
        
        // outputX outputY 是裁剪图片宽高  
        intent.putExtra("outputX", screenWidth);  
        intent.putExtra("outputY", screenHeight);  */
        // outputX outputY 是裁剪图片宽高  
        
//        intent.putExtra("outputX", 360);  
//        intent.putExtra("outputY", 360);
        intent.putExtra("return-data", true);  
        activity.startActivityForResult(intent, PHOTORESOULT);  
    } 
	/**
	 * 将大图片缩放到屏幕大小
	 * @param activity 调用该方法需要在指定的activity页面中添加onActivityResult回调方法
	 * @param uri
	 */
	@SuppressWarnings("unused")
	private void photoZoom(Activity activity,Uri uri) {  
        Intent intent = new Intent("com.android.camera.action.CROP");  
        intent.setDataAndType(uri, IMAGE_UNSPECIFIED); 
        intent.putExtra("crop", "true");  
 
//		DisplayMetrics dm = new DisplayMetrics();		//屏幕分辨率容器
//        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);  
//		int screenwidth = dm.widthPixels;
//		int screenheight = dm.heightPixels-100;
//		int temp = screenheight/screenwidth;
//		aspectX aspectY 是宽高的比例 ，默认的话可自由调整高度
//		intent.putExtra("aspectX", 1);  
//		intent.putExtra("aspectY", temp); 
        // outputX outputY 是裁剪图片宽高  
//        intent.putExtra("outputX", screenwidth);  
//        intent.putExtra("outputY", screenheight);  
        // outputX outputY 是裁剪图片宽高  
        intent.putExtra("outputX", 100);  
        intent.putExtra("outputY", 200);
        intent.putExtra("return-data", true);  
        activity.startActivityForResult(intent, PHOTORESOULT);  
    } 
	public static String getAlbumResultPhotoPath(Context context,Intent intent){
		String photoPath = null;
		if (intent!=null) {
			Uri uri = intent.getData(); 
			Cursor cursor = context.getContentResolver().query(uri, null, 
					null, null, null); 
			cursor.moveToFirst(); 
//			String imgNo = cursor.getString(0); // 图片编号 
			photoPath = cursor.getString(1); // 图片文件路径 
//			String imgSize = cursor.getString(2); // 图片大小 
//			String imgName = cursor.getString(3); // 图片文件名 
			cursor.close(); 
		}
		return photoPath;
	}

}
