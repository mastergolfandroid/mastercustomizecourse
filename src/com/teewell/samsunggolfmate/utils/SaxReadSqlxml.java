package com.teewell.samsunggolfmate.utils;
import java.io.InputStream;    
import java.util.ArrayList;
import java.util.List;    
   
   


import javax.xml.parsers.SAXParser;    
import javax.xml.parsers.SAXParserFactory;    

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.teewell.samsunggolfmate.common.Contexts;
   
public class SaxReadSqlxml {    
	 public  List<String> readXml(InputStream inStream,int verson)throws Exception {    
	        SAXParserFactory spf = SAXParserFactory.newInstance(); // ��ʼ��sax������    
	        SAXParser sp = spf.newSAXParser(); // ����sax������    
	        //XMLReader xr = sp.getXMLReader();// ����xml������    
	        XMLContentHandler handler = new XMLContentHandler(verson);    
	        sp.parse(inStream, handler);    
	        return handler.getSqlString();    
	    }  
	    class XMLContentHandler extends DefaultHandler {
	    	private int verson;
	    	private String tempString;
	    	private String sqlString = "";
	    	private List<String> sqlList;
	    	private int id;
	    	private String title = "";
	    	public XMLContentHandler(int verson) {
	    		this.verson = verson;
	    	}

	    	public List<String> getSqlString() {
	    		return sqlList;
	    	}

	    	/**
	    	 * sax �����ļ�������䣬ֻ��ȡ ��xml�ļ��е� �ⲿ�ָ���Ȥ
	    	 * ��ô�ⲿ�ְ���person���ڵ�Ԫ�صĿ�ʼ�ͽ�β����ô��Ҫ��startElement��endElement
	    	 * ��}���������text�Ľڵ�Ԫ�أ���������ڵ�֮��Ļس��Ϳո��б�ǩ�е���������������ֶ����ı��ڵ� Ԫ��
	    	 * ��text�ڵ�Ԫ�أ���ôҪ�����ı��ڵ�Ԫ�ؾ�Ҫ�õ�characters ����4��ȡ��Ϣ����
	    	 * ���ַ�ʽ������һ�ֻ����¼����api����}��֣���������¼���������������XMLReader�ӿڣ�
	    	 * �����ȡXML�ĵ��������¼����������¼�(Ҳ���¼�Դ)���¼�������ContentHandler�ӿڣ� ����Է��͵��¼���Ӧ�ͽ���XML�ĵ����?
	    	 */
	    	/**
	    	 * Ҫ��ʼ��ȡxml�ļ���ʱ����õķ��� ��ʼ��persons
	    	 */
	    	@Override
	    	public void startDocument() throws SAXException {
	    		// ������list�ĳ�ʼ������
	    		sqlList = new ArrayList<String>();
	    	}

	    	/**
	    	 * sax ��ȡ���ı��ڵ��ʱ�����������
	    	 * * �������н����������ĳ���ǩ�����ݲ�ֹһ�У�Ҫ�����Ǽ���4
	    	 */
	    	@Override
	    	public void characters(char[] ch, int start, int length)
	    			throws SAXException {

	    		if (id > verson && Contexts.DataBase.ITEM.equals(title)) {
	    			tempString = new String(ch, start, length);
	    			sqlString += tempString;
	    			// if ("sql".equals(title)) {
	    			// tempString = new String(ch, start, length);
	    			// }
	    		}
	    	}

	    	/**
	    	 * sax ��ȡ��Ԫ�ؽڵ��ʱ���õ�������
	    	 */
	    	@Override
	    	public void startElement(String uri, String localName, String name,
	    			Attributes attributes) throws SAXException {
	    		// ���ж϶u���Ԫ���Ƿ���Sql
	    		System.out.println("localName==" + localName);
	    		if (Contexts.DataBase.MENU.equals(localName)) {
	    			// ���u�����Sql���Ԫ�� ��Ҫ������4�����������Ǵ�����
	    			// attributes�����ԡ�
	    			id = Integer.valueOf(attributes.getValue(Contexts.DataBase.MENU_INDEX)).intValue();
	    			System.out.println("id==" + id);
	    		}
	    		title = localName;
	    	}

	    	/**
	    	 * ������ÿ�������ı�ǩ����ִ�е� ������ֻ�����Ľ�β�ŵ��� ��ȡ�����sql'�Ľ�β
	    	 * �ͽ���װ�õ�һ��string���浽list�� �������string����
	    	 */
	    	@Override
	    	public void endElement(String uri, String localName, String name)
	    			throws SAXException {
	    		if (Contexts.DataBase.ITEM.equals(localName) && tempString != null && !"".equals(sqlString)) {
	    			sqlList.add(sqlString);
	    		}
	    		tempString = null;
	    		sqlString = "";
	    	}
	    	/**
	    	 * ʹ��SAX����XML�Ĳ��裺 1��ʵ��һ���SAXParserFactory 2��ʵ��SAXPraser���󣬴���XMLReader ������
	    	 * 3��ʵ��handler��������4��������ע��һ���¼� 4����ȡ�ļ���5�������ļ�
	    	 */

	    }
	}   
	//sp.parse(inStream, handler); 
	//�����Կ���xml�ļ����������ʽ����4��,����handler���ǻص����ʵ��,��sax���������xml�ļ���ʱ������xml�����ݾͻ�ȥhandle���е��ö�Ӧ�������ᵽ�ķ���.�����ǽ�b���XMLContentHandler ��Ҫ�������Ҫʵ��ContentHandler()���ӿ�,�������ӿ����кܶ���Ҫʵ�ֵķ���.sax�ṩ��һ��DefaultHandler�ӿ� ֻҪʵ�����ӿ��������Ȥ�ķ����Ϳ�����.
