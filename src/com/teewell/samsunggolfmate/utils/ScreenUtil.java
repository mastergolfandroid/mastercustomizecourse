package com.teewell.samsunggolfmate.utils;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.DisplayMetrics;

import com.teewell.samsunggolfmate.costants.SharedPreferenceConstant;
public class ScreenUtil {
	
	private static DisplayMetrics dm=null;
	private static int screenWidth=0; //��Ļ���
	private static int screenHeight=0;//��Ļ�߶�
	private static float density=0;//��Ļ�ܶ�
	private static boolean isSettingScreenTimeOut;
	
	public  static int getScreenWidth(Context context){
		 if (dm==null) {			
			 dm = new DisplayMetrics();        
			 ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);    	         
			 int nowWidth = dm.widthPixels; //��ǰ�ֱ��� ���
			 int nowHeight=dm.heightPixels;
			 float nowDensity=dm.density;
			 screenWidth=nowWidth;
			 screenHeight=nowHeight;
			 density=nowDensity;
		 }
		 return screenWidth;
	   }
	
	public  static int getScreenHeight(Context context){
		 if (dm==null) {			
			 dm = new DisplayMetrics();        
			 ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);    	         
			 int nowWidth = dm.widthPixels; //��ǰ�ֱ��� ���
			 int nowHeight=dm.heightPixels;
			 float nowDensity=dm.density;
			 screenWidth=nowWidth;
			 screenHeight=nowHeight;
			 density=nowDensity;
		 }
		 return screenHeight;
	   }
	
	public  static float getScreenDensity(Context context){
		 if (dm==null) {			
			 dm = new DisplayMetrics();        
			 ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);    	         
			 int nowWidth = dm.widthPixels; //��ǰ�ֱ��� ���
			 int nowHeight=dm.heightPixels;
			 float nowDensity=dm.density;
			 screenWidth=nowWidth;
			 screenHeight=nowHeight;
			 density=nowDensity;
		 }
		 return density;
	   }
	/**获取及更改屏幕待机时间
	 * @param context
	 */
	public static void setScreenOffTimeOut(Context context){
		 try {
			 if (!isSettingScreenTimeOut) {
				 int result  = Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);
				 if (result<2*60*1000) {
					result = 2*60*1000;
				}
				 new SharedPreferencesUtils(context).commitInt(SharedPreferenceConstant.SCREEN_OFF_TIMEOUT, result);
//				 Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, -1);
				 isSettingScreenTimeOut = true;
			 }
         } catch (SettingNotFoundException e) {
             e.printStackTrace();
         }
	}
	/**恢复屏幕待机时间
	 * @param context
	 */
	public static void recoverScreenOffTimeOut(Context context){
		int result  = new SharedPreferencesUtils(context).getInt(SharedPreferenceConstant.SCREEN_OFF_TIMEOUT, 2*60*1000);
	    Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, result);
	    isSettingScreenTimeOut = false;
	}
}
