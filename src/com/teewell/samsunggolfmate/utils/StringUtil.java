package com.teewell.samsunggolfmate.utils;

public class StringUtil {
	public static String emptyProcess(Object string){
		if (string == null) {
			return "暂未提供数据";
		}
		return String.valueOf(string);
	}
	/**
	 * 字符数组合并
	 * 
	 * @param strings
	 *            数组
	 * @param expression
	 *            特殊字符
	 * @return
	 *         String
	 */
	public static String addString ( String[] strings, String expression )
	{
		StringBuffer string = new StringBuffer();
		for (int i = 0; i < strings.length - 1; i++)
		{
			string.append(strings[i] + expression);
		}
		string.append(strings[strings.length - 1]);
		return string.toString();
	}
}
