/**
 * 更新app
 * 注意修改filename及urlStr
 */
package com.teewell.samsunggolfmate.utils;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.common.Contexts;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.webkit.MimeTypeMap;


@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class UpdateApp {
	protected static final int HASUPDATE = 0;
	private static final String path = Contexts.SD_PATH + Contexts.APPLICATION_PATH+"apk/";// 下载的apk在sdcard中的位置
	private String fileName;// 下载后apk的名字
	private String filename = "masterCustomizeCourse";// 程序名
	private String urlStr = "http://182.92.79.93/";// 下载的url后接文件名
	protected static final int DOWNLOADCOMPLETE = 1;
	protected static final int INSTALAPKFILE = 2;
	private Activity acticity;
	private DownloadManager dm;
	private File file;
	private long id;
	private Handler updatehandler = new Handler(){
		public void handleMessage(final Message msg) {
			switch (msg.what) {
			case HASUPDATE:
				DialogUtil.showAskDialog(acticity, "", acticity.getString(R.string.update_has_update_ask), new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
//						if (msg.obj!=null) {
//							
//							Download(msg.obj.toString());
//						}else {
//						}
						Download(urlStr+fileName);
					}
				});
				break;
			case DOWNLOADCOMPLETE:
				DialogUtil.showAskDialog(acticity, "", acticity.getString(R.string.update_download_complete_ask), new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						instalApk();
					}
				});
				break;
			case INSTALAPKFILE:
				DialogUtil.showAskDialog(acticity, "", acticity.getString(R.string.update_apk_file_exists), new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						instalApk();
					}
				});
				break;
			default:
				break;
			}
		};
	};
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Message msg = updatehandler.obtainMessage();
			msg.what = DOWNLOADCOMPLETE;
			// 这里可以取得下载的id，这样就可以知道哪个文件下载完成了。适用与多个下载任务的监听
			updatehandler.sendMessage(msg);
		}
	};
	

	/** 
	 * 判断是否有更新
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public boolean checkUpdate(URL url) throws IOException {
		URLConnection urlConnection = url.openConnection();
		return ((HttpURLConnection) urlConnection).getResponseCode() == 200;
	}
 
	private String getfileName(String fileName) {
//		return fileName + ".apk";
		
		return fileName + String.format("%1$.1f", getNextVersion()) + ".apk";
	}
	/**
	 * 获取下个版本VersionName
	 * @return versionName
	 */
	public float getNextVersion() {
		try {
			// 获取packagemanager的实例
			PackageManager packageManager = acticity.getPackageManager();
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageInfo packInfo = packageManager.getPackageInfo(
					acticity.getPackageName(), 0);
			String version = packInfo.versionName;
			float v = Float.parseFloat(version);
			v += 0.1f;
			return v;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public void update() {
		

		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (fileName == null) {
					fileName = getfileName(filename);
				}
				FileUtils utils = new FileUtils();
				file = utils.getSDFile(path + fileName);
				Log.e("--", fileName+"--"+file.getAbsolutePath()+"--"+file.getName()+"--"+file.getPath());
				URL url;
				try {
					if (file.exists()) {
//					if (FileUtil.existFolderFile(file.getAbsolutePath(),fileName)) {
						updatehandler.sendEmptyMessage(INSTALAPKFILE);
					}else {
						url = new URL(urlStr + fileName);
						Log.e("update", url.toString());
						dm = (DownloadManager) acticity
								.getSystemService(Context.DOWNLOAD_SERVICE);
						if (checkUpdate(url)) {
							if (!file.exists()) {
								if (file.getParentFile().exists()
										&& file.getParentFile().listFiles().length > 0) {
									Log.e("--","111");
									while (file.getParentFile().listFiles().length > 0) {
										file.getParentFile().listFiles()[0].delete();
										Log.e("--","222");
									}
								}else{
									file.getParentFile().mkdirs();
								}
								Message msg = updatehandler.obtainMessage();
								msg.what = HASUPDATE;
								msg.obj = urlStr + fileName;
								updatehandler.sendMessage(msg);

							} else {
								updatehandler.sendEmptyMessage(INSTALAPKFILE);
							}
					}
				}} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				

				}
			}
		).start();
	}
	public void instalApk(){
		Intent intent = new Intent();
		intent.setAction(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file),
				"application/vnd.android.package-archive");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		acticity.startActivity(intent);
	}
	public UpdateApp(Activity acticity) {
		super();
		this.acticity = acticity;
	}
	/**
	 * 调用系统DownloadManager下载文件
	 * @param url
	 */
	private void Download(String url) {
		// TODO Auto-generated method stub
		System.err.println(url);
		DownloadManager.Request request = new DownloadManager.Request(
				Uri.parse(url.toString()));
		request.setAllowedNetworkTypes(Request.NETWORK_MOBILE
				| Request.NETWORK_WIFI);
		request.setAllowedOverRoaming(false);
		// 设置文件类型
		MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
		String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap
				.getFileExtensionFromUrl(url));
		request.setMimeType(mimeString);
		// 在通知栏中显示
		request.setShowRunningNotification(true);
		request.setVisibleInDownloadsUi(true);
		request.setTitle(fileName);
		// sdcard的目录下的download文件夹
		request.setDestinationInExternalPublicDir(path, fileName);
		request.setTitle(fileName);
		id = dm.enqueue(request);
		acticity.registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
	}
}
