package com.teewell.samsunggolfmate.views;

import com.master.mastercourse.singlecourse.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AroundSearchAdapter extends BaseAdapter {
	private static final int[] IMAGE_ID ={R.drawable.icon_class_meishi,R.drawable.icon_class_hotel,R.drawable.icon_class_bank
		,R.drawable.icon_class_cinema,R.drawable.icon_class_ktv,R.drawable.icon_class_bus_station};
	private static final String[] NAME ={"美食","酒店",  "银行", "电影院 ","KTV", "公交车 "};
	private LayoutInflater inflater;
	class AroundSearchView{
		public ImageView image;
		public TextView tv_name;
		public RelativeLayout layout_item;
	}
	public AroundSearchAdapter(Context context) {
		inflater = LayoutInflater.from(context);
	}
	
	public static String[] getName() {
		return NAME;
	}
	@Override
	public int getCount() {
		return getName().length;
	}
	@Override
	public String getItem(int position) {
		return getName()[position];
	}
	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		AroundSearchView view = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.around_search_item, null); 
			view = new AroundSearchView();
			view.image = (ImageView) convertView.findViewById(R.id.image);
			view.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
			view.layout_item = (RelativeLayout) convertView.findViewById(R.id.layout_item);
			convertView.setTag(view);
		}else {
			view = (AroundSearchView) convertView.getTag();
		}        
		view.image.setImageResource(IMAGE_ID[position]);
		view.tv_name.setText(getItem(position));
		if (position==0) {
			view.layout_item.setBackgroundResource(R.drawable.bg_above_wane_nor);
		}else if (position==getCount()-1) {
			view.layout_item.setBackgroundResource(R.drawable.bg_below_wane_nor);
		}else {
			view.layout_item.setBackgroundResource(R.drawable.bg_middle_wane_nor);
		}
		return convertView;
	}

}
