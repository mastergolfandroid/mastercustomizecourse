package com.teewell.samsunggolfmate.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class BorderLinearLayout extends LinearLayout
{


	@Override
	protected void onDraw ( Canvas canvas )
	{
		super.onDraw(canvas);
		Paint paint = new Paint();		
		paint.setColor(android.graphics.Color.BLACK);
		paint.setStrokeWidth(0.2f);
		drawLine(canvas, paint);
	}

	public BorderLinearLayout ( Context context,AttributeSet attrs )
	{
		super(context, attrs);
	}

	public BorderLinearLayout ( Context context )
	{
		super(context);
	}

	private void drawLine ( Canvas canvas, Paint paint )
	{
	
		canvas.drawLine(0, 0, this.getWidth() - 1, 0, paint);// ��
		canvas.drawLine(0, 0, 0, this.getHeight() - 1, paint);
		canvas.drawLine(this.getWidth() - 1, 0, this.getWidth() - 1,this.getHeight() - 1, paint);
		canvas.drawLine(0, this.getHeight() - 1, this.getWidth() - 1,this.getHeight() - 1, paint);// ��			
		

	}

}
