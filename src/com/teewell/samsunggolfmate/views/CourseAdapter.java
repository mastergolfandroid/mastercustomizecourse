/**
 * 
 */ 
package com.teewell.samsunggolfmate.views; 

import java.util.List;

import com.teewell.samsunggolfmate.beans.CoursesObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/** 
 * @author 程明炎 E-mail: 345021109@qq.com
 * @version 创建时间：2014年7月14日 上午11:34:16 
 * 类说明 
 */
/**
 * @author sxx
 *
 */
public class CourseAdapter extends BaseAdapter {
	private List<CoursesObject> list;
	private LayoutInflater inflater;
	/**
	 * 
	 */
	public CourseAdapter(Context context,List<CoursesObject> list) {
		// TODO Auto-generated constructor stub
		inflater = LayoutInflater.from(context);
		this.list = list;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public CoursesObject getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
 