package com.teewell.samsunggolfmate.views;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.CoursesObject;
import com.teewell.samsunggolfmate.beans.WeatherInfoBean;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.models.weather.WeatherInfoModel;
import com.teewell.samsunggolfmate.utils.BitmapUtil;
import com.teewell.samsunggolfmate.utils.DistanceUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;
/**
 * 
 * @author chengmingyan
 * 
 */
public class CourseListAdapter extends BaseAdapter{
	private static final String TAG = CourseListAdapter.class.getName();

	private LayoutInflater inflater;
	private Activity activity;
	private CourseListCellWrapper wrap;
	private List<CoursesObject> courseList;
	
	private Bitmap logoBitmap;
	private String logoName;
	private final String TEMP_UNIT = "℃";
	private LocationUtil location;

	private static final int MIN_COUNT = 0;
	
	public void setCoursesList(List<CoursesObject> coursesList) {
		this.courseList = coursesList;
	}

	public CourseListAdapter ( Activity activity)
	{
		this.activity = activity;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		location = LocationUtil.getInstance(activity.getApplicationContext());
	}

	public int getCount ()
	{
		if(null == courseList || courseList.size() < MIN_COUNT){
			return MIN_COUNT;
		}
		
		return courseList.size();
	}
	
	public CoursesObject getItem ( int position )
	{
		if(null == courseList){
			return null;
		}
		if(position > courseList.size() - 1 || position < 0){
			return null;
		}
		return courseList.get(position);
	}
	
	public long getItemId ( int position )
	{
		return position;
	}

	public View getView ( int position, View convertView, ViewGroup parent )
	{
		if (convertView == null)
		{
			convertView = inflater.inflate(R.layout.course_item, null);
			wrap = new CourseListCellWrapper();
			wrap.logo = (ImageView) convertView.findViewById(R.id.image_logo);
			wrap.courseName = (TextView) convertView.findViewById(R.id.tv_coursename);
			wrap.courseRating = (ImageView) convertView.findViewById(R.id.image_rating);
			wrap.temperature = (TextView) convertView.findViewById(R.id.tv_temperature);
			wrap.wind = (TextView) convertView.findViewById(R.id.tv_wind);
			wrap.windLevel = (TextView) convertView.findViewById(R.id.tv_windLevel);
			wrap.courseWeather = (ImageView) convertView.findViewById(R.id.image_courseWeather);
			wrap.courseRange = (TextView) convertView.findViewById(R.id.tv_courseRange);
			convertView.setTag(wrap);
		}
		else
		{
			wrap = (CourseListCellWrapper) convertView.getTag();
		}
		
		CoursesObject courseBean = getItem(position);
		if(null ==  courseBean){
			convertView.setVisibility(View.INVISIBLE);
			return convertView;
		}else{
			convertView.setVisibility(View.VISIBLE);
		}
		
		logoName = courseBean.getImages();
		if (logoName != null && !"".equals(logoName)) {
			File file = new File(Contexts.LOGO_PATH + logoName);
			if (file.exists()) {
				logoBitmap = BitmapUtil.readBitMapByNative(Contexts.LOGO_PATH + logoName);
				wrap.logo.setImageBitmap(logoBitmap);
			}
		}
		
		wrap.courseName.setText(courseBean.getCoursesName() + activity.getString(R.string.golfcourse).toString());
		
		WeatherInfoBean weatherBean = WeatherInfoModel.getInstance(activity).getWeatherInfoBean(courseBean.getCity());
		if(null != weatherBean ){
			if (weatherBean.getBitmap() != null) {
				wrap.courseWeather.setImageBitmap(weatherBean.getBitmap());
			}else{
				wrap.courseWeather.setImageBitmap(null);
			}
			if (weatherBean.getWD() != null) {
				wrap.wind.setText(weatherBean.getWD());
				wrap.windLevel.setText(weatherBean.getWS());
				wrap.temperature.setText(weatherBean.getTemp()+TEMP_UNIT);
			}else{
				wrap.wind.setText("");
				wrap.windLevel.setText("");
				wrap.temperature.setText("");
			}
		}else{
			wrap.courseWeather.setImageBitmap(null);
			wrap.wind.setText("");
			wrap.windLevel.setText("");
			wrap.temperature.setText("");
		}
		
		float distance;
		if (PlayGameModel.longDistanceIndex == DistanceUtil.MILE) {
			distance = DistanceUtil.getDistanceByMile(Double.parseDouble(courseBean.getLatitude()), Double.parseDouble(courseBean.getLongitude()), location.latitude, location.longitude);
		}else {
			distance = DistanceUtil.getDistanceByKilometer(Double.parseDouble(courseBean.getLatitude()), Double.parseDouble(courseBean.getLongitude()), location.latitude, location.longitude);
		}
		
		String typeUnit = DistanceUtil.getDistanceUnit(activity, distance,PlayGameModel.longDistanceIndex);
		wrap.courseRange.setText(typeUnit);
		
		return convertView;
	}

	public static String getTag() {
		return TAG;
	}
	
}



