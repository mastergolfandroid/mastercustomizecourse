package com.teewell.samsunggolfmate.views;

import android.widget.ImageView;
import android.widget.TextView;

public class CourseListCellWrapper{
	public TextView courseName;//
	public ImageView courseRating;//
	public TextView temperature;//
	public TextView wind;//
	public TextView windLevel;//
	public ImageView courseWeather;//
	public ImageView logo;//
	public TextView courseRange;//
}
