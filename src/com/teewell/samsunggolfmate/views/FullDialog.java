package com.teewell.samsunggolfmate.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.master.mastercourse.singlecourse.R;

/**  自定义的Dialog在使用时，要注意：先执行dialog.show()方法，在设置显示的内容及按钮事件，否则会报null异常
 * 
 * 或者：Window window = alertDialog.getWindow();
		window.setBackgroundDrawable(new BitmapDrawable());
		用dialog先获取屏幕，在设置显示的内容及按钮事件，在执行dialog.show()方法
 * @Project GolfMate04
 * @package com.teewell.golfmate.views
 * @title MyDialog.java 
 * @Description TODO
 * @author Administrator
 * @date 2013-4-26 下午1:42:49
 * @Copyright Copyright(C) 2013-4-26
 * @version 1.0.0
 */
public class FullDialog extends Dialog {
	public FullDialog(Context context, int theme) {
		super(context, theme);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.xml.full_dialog);
	}
}

