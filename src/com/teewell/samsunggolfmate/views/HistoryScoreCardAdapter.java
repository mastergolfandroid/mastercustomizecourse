package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.teewell.samsunggolfmate.activities.HistoryScorecardActivity;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.UserScoreCardObject;

/**  
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.views
 * @title s.java 
 * @Description TODO
 * @author Administrator
 * @date 2013-3-15 下午3:59:37
 * @Copyright Copyright(C) 2013-3-15
 * @version 1.0.0
 */
/**
 * 历史记分卡数据适配器
 * @author Antony
 *
 */
public class HistoryScoreCardAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context mContext;
    private List<UserScoreCardObject> list;
    private static final int MIN_COUNT = 0;
    public HistoryScoreCardAdapter(Context context) {
        mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    public List<UserScoreCardObject> getList() {
    	if (list == null) {
			list = new ArrayList<UserScoreCardObject>();
		}
		return list;
	}

	public void setList(List<UserScoreCardObject> list) {
		this.list = list;
	}

	public int getCount() {
		if(getList().size() < MIN_COUNT){
			return MIN_COUNT;
		}
        return getList().size();
    }
    public UserScoreCardObject getItem(int position) {
    	
		if(position > getList().size() - 1 || position < 0){
			return null;
		}
        return getList().get(position);
    }
    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.history_scorecard_item,null);
            ViewWarp warp = new ViewWarp();
            warp.delete = (ImageView) convertView.findViewById(R.id.btn_delete);
            warp.courseName = (TextView) convertView .findViewById(R.id.courseName);
            warp.playDate = (TextView) convertView.findViewById(R.id.saveDate);
            warp.selfScore=(TextView) convertView.findViewById(R.id.selfScore);
            convertView.setTag(warp);
        }
        ViewWarp warp = (ViewWarp) convertView.getTag();
        UserScoreCardObject bean = getItem(position);
        if(null ==  bean){
			convertView.setVisibility(View.INVISIBLE);
			return convertView;
		}else{
			convertView.setVisibility(View.VISIBLE);
		}
        warp.courseName.setText(bean.getGeneralScoreBean().getCourse_Name()+mContext.getString(R.string.golfcourse).toString());
        warp.playDate.setText(bean.getSaveTime());
        warp.selfScore.setText(Integer.toString(bean.getGeneralScoreBean().getMySelfScore()));
        if (HistoryScorecardActivity.isDelete) {
			warp.delete.setVisibility(View.VISIBLE);
		}else {
			warp.delete.setVisibility(View.GONE);
		}
        return convertView;
    }
    class ViewWarp {
        public TextView courseName;
        public TextView playDate;
        public TextView selfScore;
        public ImageView delete;
    }
}
