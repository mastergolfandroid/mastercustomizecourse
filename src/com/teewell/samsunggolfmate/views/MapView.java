package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;
import java.util.List;

import com.teewell.samsunggolfmate.activities.MapActivity;
import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.FuncButtonState;
import com.teewell.samsunggolfmate.beans.MapMarkeVo;
import com.teewell.samsunggolfmate.beans.Point;
import com.teewell.samsunggolfmate.common.Contexts;
import com.teewell.samsunggolfmate.datebase.MyDataBaseAdapter;
import com.teewell.samsunggolfmate.models.MapModel;
import com.teewell.samsunggolfmate.models.PlayGameModel;
import com.teewell.samsunggolfmate.utils.CoordinateUtil;
import com.teewell.samsunggolfmate.utils.DistanceDataUtil;
import com.teewell.samsunggolfmate.utils.DistanceUtil;
import com.teewell.samsunggolfmate.utils.LocationUtil;
import com.teewell.samsunggolfmate.utils.RightTriangleUtil;
import com.teewell.samsunggolfmate.utils.ScreenUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.text.method.MovementMethod;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
/**
 * 航拍图控件
 * @author chengmingyan
 */
public class MapView extends View {

    private static final String TAG = MapView.class.getName();
    private static  float RADIUS;
//    private static Context context;
    
        
    private FuncButtonState funcButtonState = FuncButtonState.cursor;
    
    public FuncButtonState getFuncButtonState() {
		return funcButtonState;
	}
	public void setFuncButtonState(FuncButtonState funcButtonState) {
		this.funcButtonState = funcButtonState;
	}

	/**
     * 左边图片的左起点坐标
     */
    private final int LEFT_BITMAP_LEFT = 10;
    /**
     * 左边果岭旗子的上起点坐标
     */
    private final int LEFT_FlAG_TOP = 10;
    
    private Paint defaultPaint;
    private Paint greenFontPaint;
    private Paint arcLineFontPaint;
    private Paint marketPaint;
    private Paint cursorLinePaint;
    private Paint cursorFontPaint;

    /**
     * 果岭旗子
     */
    private Bitmap flagBmp;
    private Bitmap flagBmp1;
    private Bitmap flagBmp2;
    /**
     * 人图标
     */
    private Bitmap personBmp;
    /**
     * 游标图片
     */
    private Bitmap cursorBmp;
    /**
     * 球场图片
     */
    private Bitmap courseBitmap;


    private Bitmap markBitmap;
    private Bitmap markBitmapDelete;
    private Bitmap markBitmapMoving;

    private MapMarkeVo movingMarkeVo;//当前拖动的marke对象

    @SuppressWarnings("unused")
	private Point touchPoint;
    /**
     * cursor在y轴上的偏移量
     */
    public static float cursor_touch_offset_Y = 100;
    /**
     * 触摸时，marke扩大区域,在Y轴上的偏移量
     */
    public static float marke_touch_offset = 150;
    
    /**
     * 按钮控件的偏移区域
     */
    private static float image_offset = 40;
    private MapModel navigationMapModel;
    private static List<MapMarkeVo> voList ;
    private Paint paint;
    private static float buttonY=60;
    /**
     * 弧线值集合
     */
    private int[] distanceArc;
    /**
     * 当前洞的号
     */
    private int currIndex;//当前洞的index
    /**
     * 一码对应多少像素
     */
    private float scale;//一码对应多少像素
    private static float cursor_textSize;//弧线字体大小 480的20  720的22
    private static float arc_textSize;//弧线字体大小 480的20  720的22
    private static float mark_textSize;//弧线字体大小 480的20  720的22
    private static float green_textSize;//果岭字体大小 480的20  720的22
    private static float image_bottom_offset=30;//480的30  720的60
	private Bitmap personBmp_;
	private Bitmap personBmp_direction;
	private CoordinateUtil util;
	private int index;
	private String distanceUnit_near;
	
	public boolean state_direction;
	private float density;
	private float screenWidth;
	private float screenHeight;
	private LocationUtil location;
    public boolean isState_direction() {
		return state_direction;
	}
	public void setState_direction(boolean state_direction) {
		this.state_direction = state_direction;
	}
	
	public Bitmap getPersonBmp_direction() {
		if (personBmp_direction == null) {
			personBmp_direction = BitmapFactory.decodeResource(getResources(),R.drawable.personwithdirection);
		}
		return personBmp_direction;
	}
	public void setPersonBmp_direction(Bitmap personBmp_direction) {
		this.personBmp_direction = personBmp_direction;
	}
	public MapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }
    public MapView(Context context, AttributeSet attrs) {
    	super(context, attrs);
    	init(context);
    }
    /**
     * 更新marke
     * @param mdb
     */
    public static void changeMarkeVo(MyDataBaseAdapter mdb,int lastId,int nowId){
    	if (lastId>=0) {			
    		mdb.deleteMapMarkeByHoleID(lastId);    	
    		for (int i = 0; i < voList.size(); i++) {
    			MapMarkeVo vo = voList.get(i);
    			if (vo.getState()!=3) {					
    				mdb.insertMapMarke(lastId, vo.getX(), vo.getY());
				}
    		}
    		voList.clear();
		}
    	List<MapMarkeVo> list=mdb.selectMarkeById(nowId);
    	voList=list;
    }
    private void initBitmap(){
    	markBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.mark);
		markBitmapDelete= BitmapFactory.decodeResource(getResources(),R.drawable.mark_move);
		markBitmapMoving = BitmapFactory.decodeResource(getResources(),R.drawable.mark_remove);
		
		cursorBmp = BitmapFactory.decodeResource(getResources(),R.drawable.cursor);
				
		// 指示果岭的图片
        flagBmp = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
        flagBmp1 = BitmapFactory.decodeResource(getResources(), R.drawable.flag1);
        flagBmp2 = BitmapFactory.decodeResource(getResources(), R.drawable.flag2);
        // 指示人的位置的图片
        personBmp = BitmapFactory.decodeResource(getResources(),R.drawable.person_begin);
        personBmp_ = BitmapFactory.decodeResource(getResources(),R.drawable.person_end);
    }
    private void initPaint(){
    	paint = new Paint();
        paint.setStrokeWidth(2f);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);

        touchPoint = new Point();
        cursorLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        cursorLinePaint.setStyle(Paint.Style.STROKE);
        cursorLinePaint.setColor(Color.WHITE);
        cursorLinePaint.setStrokeWidth(3f);

        marketPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        marketPaint.setStyle(Paint.Style.STROKE);
        marketPaint.setColor(Color.RED);
        marketPaint.setTextSize(mark_textSize);

        defaultPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        defaultPaint.setStyle(Paint.Style.STROKE);

        greenFontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        greenFontPaint.setStyle(Paint.Style.STROKE);
        greenFontPaint.setColor(Color.WHITE);
        greenFontPaint.setTextSize(green_textSize);
        
        cursorFontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        cursorFontPaint.setStyle(Paint.Style.STROKE);
        cursorFontPaint.setColor(Color.WHITE);
        cursorFontPaint.setTextSize(cursor_textSize);
        
        arcLineFontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        arcLineFontPaint.setColor(Color.WHITE);
        arcLineFontPaint.setTextSize(arc_textSize);
    }
    /**
     * 初始化用到的各种图标和画笔
     */
	private void init(Context contexts) {
		 density = ScreenUtil.getScreenDensity(contexts);
		 screenHeight = ScreenUtil.getScreenHeight(contexts);
		 screenWidth = ScreenUtil.getScreenWidth(contexts);
		 location = LocationUtil.getInstance(contexts);
    	initBitmap();
    	
    	initDistance(contexts);
		
		marke_touch_offset = contexts.getResources().getDimension(R.dimen.marke_touch_offset)/density;
		cursor_touch_offset_Y = contexts.getResources().getDimension(R.dimen.cursor_touch_offset_Y)/density;
		image_offset = contexts.getResources().getDimension(R.dimen.image_offset)/density;
		RADIUS = contexts.getResources().getDimension(R.dimen.RADIUS)/density;
		
    	image_bottom_offset = contexts.getResources().getDimension(R.dimen.image_bottom_offset)/density;
		
    	cursor_textSize = contexts.getResources().getDimension(R.dimen.cursor_textSize);
    	mark_textSize = contexts.getResources().getDimension(R.dimen.mark_textSize);
		arc_textSize = contexts.getResources().getDimension(R.dimen.arc_textSize);
		green_textSize = contexts.getResources().getDimension(R.dimen.green_textSize);
    	buttonY = markBitmap.getHeight()+10+image_bottom_offset;
  		
        initPaint();
        
    }
	 /**
	 * 初始化距离单位
	 */
	public  void initDistance(Context contexts){
		index = PlayGameModel.shortDistanceIndex;
		distanceUnit_near = DistanceUtil.getDistanceUnit(contexts, index);
    	distanceArc = DistanceDataUtil.getDistanceArc(contexts);
	 }
    public void setNavigationMapModel(MapModel navigationMapModel) {
        this.navigationMapModel = navigationMapModel;
        courseBitmap = navigationMapModel.getRotatedCourseBmp();
        util = (CoordinateUtil) navigationMapModel.getCoordinateUtil();
        //计算左上右下两点的像素点
//        dis_screen = Math.sqrt((Math.pow(util.bmpWidth, 2) + Math.pow(util.bmpHeight, 2)));
    }
    public MapModel getNavigationMapModel() {
        return navigationMapModel;
    }
    /**
     * @param context
     */
    public MapView(Context context) {
        super(context);
        init(context);
    }
    public void setIndex(int currIndex){
        this.currIndex = currIndex;
    }
    public int getIndex(){
        return currIndex;
    }
    public void setMapMarkeVoList(List<MapMarkeVo> voList){
        this.voList = voList;
        if(this.voList == null){
            this.voList = new ArrayList<MapMarkeVo>();
        }
        
        this.invalidate();
    }
    public List<MapMarkeVo> getMapMarkeVoList(){
        List<MapMarkeVo> mapMarkeVos = new ArrayList<MapMarkeVo>();
        for (MapMarkeVo mapMarkeVo : voList) {
            //把新增的、移动过的 和删除掉的，加入新list。并返回
            if(mapMarkeVo.getId() < 0 || mapMarkeVo.getState() == MapMarkeVo.STATE_2 || mapMarkeVo.getState() == MapMarkeVo.STATE_3){
                mapMarkeVos.add(mapMarkeVo);
            }
        }
        return mapMarkeVos;
    }
    
    boolean state;
	
    public void setPersonBmpState(boolean state){
        this.state = state;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (navigationMapModel == null) {
            canvas.drawColor(Color.BLACK);
            return;
        }
        //画航拍图
        canvas.drawBitmap(courseBitmap, ((CoordinateUtil) navigationMapModel.getCoordinateUtil()).getOffsetX(),((CoordinateUtil) navigationMapModel.getCoordinateUtil())
                        .getOffsetY(), defaultPaint);
          // 画小人
        float left_c = 0;
        float top_c = 0;
    	if(navigationMapModel.getPointMe()==null){//top_c>this.getMeasuredHeight()
        	 left_c = (float)this.getMeasuredWidth() / 2;
        	 top_c = (float)this.getMeasuredHeight() - 60;
        	 navigationMapModel.setPointMe(new Point(left_c, top_c));
        }else if(isPersonPosition()){
        	navigationMapModel.setPointMe(navigationMapModel.getCoordinateUtil().convertLonLatToOldXY(location.longitude, location.latitude));
		}
    	left_c = navigationMapModel.getPointMe().x;
        top_c = navigationMapModel.getPointMe().y;
    	
        // 画果岭小旗
        if(navigationMapModel.getPointGreens().size()==1){
        	Point p = navigationMapModel.getPointGreens().get(0);
            canvas.drawBitmap(flagBmp, p.x, p.y- flagBmp.getHeight(), defaultPaint);
            String myLenToGreen=navigationMapModel.getDistanceToGreen(p,index)+distanceUnit_near;
            float lenToGreen = navigationMapModel.getDistancePersonToGreen(p,index);
            float dis_screen = FloatMath.sqrt((left_c - p.x)*(left_c - p.x) + (top_c - p.y)*(top_c - p.y));
            scale = dis_screen/lenToGreen;
        	canvas.drawBitmap(flagBmp, LEFT_BITMAP_LEFT, LEFT_FlAG_TOP, defaultPaint);
        	canvas.drawText(myLenToGreen, LEFT_BITMAP_LEFT+flagBmp.getWidth(), LEFT_FlAG_TOP+flagBmp.getHeight(), greenFontPaint);
        }else {
        	int green_count=0;
            for (Point p : navigationMapModel.getPointGreens()) {
            	green_count++;
            	 String myLenToGreen=navigationMapModel.getDistanceToGreen(p,index)+distanceUnit_near;
                if (green_count==1) {
                	canvas.drawBitmap(flagBmp1, p.x , p.y- flagBmp1.getHeight(), defaultPaint);
                	canvas.drawBitmap(flagBmp1, LEFT_BITMAP_LEFT, LEFT_FlAG_TOP, defaultPaint);
                	canvas.drawText(myLenToGreen, LEFT_BITMAP_LEFT*2+flagBmp1.getWidth(), LEFT_FlAG_TOP+flagBmp1.getHeight(), greenFontPaint);
    			}else {
    				canvas.drawBitmap(flagBmp2, p.x , p.y- flagBmp2.getHeight(), defaultPaint);
    				canvas.drawBitmap(flagBmp2, LEFT_BITMAP_LEFT, LEFT_FlAG_TOP+flagBmp2.getHeight(), defaultPaint);
                	canvas.drawText(myLenToGreen, LEFT_BITMAP_LEFT*2+flagBmp2.getWidth(), LEFT_FlAG_TOP+flagBmp2.getHeight()*2, greenFontPaint);
    			}
            }
		}
        
      //在航拍图上画弧线，及到每条弧线的长度
        if (distanceArc!=null) {
        	drawArcAndDistance(canvas, left_c, top_c);
		}

        //draw cursor
        switch(funcButtonState){
        case cursor:
            drawTouch(canvas);
        	break;
        case addMark:
        	for (int i = 0;i < voList.size();i++) {
        		MapMarkeVo vo = voList.get(i);
        		drawMarke(canvas, vo, markBitmap);
			}
        	
        	if (movingMarkeVo != null) {
        		drawMarke(canvas, movingMarkeVo, markBitmapDelete);
			}
        	break;
        case removeMark:
        	for (int i = 0;i < voList.size();i++) {
        		MapMarkeVo vo = voList.get(i);
        		drawMarke(canvas, vo, markBitmapMoving);
			}
        	break;
        default:
        	break;
        }
        
        if (state_direction) {
    		canvas.drawBitmap(getPersonBmp_direction(), left_c - personBmp_direction.getWidth() / 2, top_c - personBmp_direction.getHeight() / 2, defaultPaint);
		}else if (isTouchMe) {
    		  canvas.drawBitmap(personBmp_, left_c - personBmp.getWidth() / 2, top_c - personBmp.getHeight() / 2, defaultPaint);
		}else {
			  canvas.drawBitmap(personBmp, left_c - personBmp.getWidth() / 2, top_c - personBmp.getHeight() / 2, defaultPaint);
		}
    }

    private boolean isPersonPosition(){
    	return true;
//    	return navigationMapModel.getRealDistancePersonToGreen()<1000?true:false;
    }
    
    /**在航拍图上画弧线，及到每条弧线的长度
     * @param canvas
     * @param left_c
     * @param top_c
     */
    private void drawArcAndDistance(Canvas canvas, float left_c, float top_c){
        //以小人位置为基点，显示“距离信息”时的偏移角度(使用时需要转换为弧度：*Math.PI/180)
        final int defaultSweepAngle = 90;
        final int defaultStartAngle = 225;
        final int textWidth = 120;
        
        float textSweepAngle = defaultStartAngle - 180;
        
        Path path = new Path();
        //计算航拍图左上右下两点间的大地距离
//        float distance =  Float.parseFloat(DistanceUtil.getDistance(util.getLatLTop(),util.getLonLTop() , util.getLatRBottom(), util.getLonRBottom(),index));
//        scale = (float)dis_screen / distance;
        //第一道弧线和距离信息
        float maxRadius = distanceArc[distanceArc.length - 1] * scale;
//        paint.setTextSize(green_textSize);
        
        float marginToRight = RightTriangleUtil.getNearSideLenth(maxRadius, textSweepAngle);			//临角边边长
        
        //第一次获取“距离信息”在屏幕中弧线上的点位，判断是否超出屏幕范围，如果超出，则修改默认的60度为实际需要的大小
    	Log.v(TAG, "maxRadius" +  maxRadius);
    	Log.v(TAG, "marginToRight:" + marginToRight);
    	Log.v(TAG, "width" +  ScreenUtil.getScreenWidth(this.getContext()));
    	Log.v(TAG, "left_c" +  left_c);
        if( marginToRight + textWidth + left_c > ScreenUtil.getScreenWidth(this.getContext()) )
        {
        	marginToRight = ScreenUtil.getScreenWidth(this.getContext()) - left_c - textWidth;
        	textSweepAngle = RightTriangleUtil.getAcuteAngle(marginToRight, maxRadius);				//反算出所需要的角度
        	Log.v(TAG, "angle:" + textSweepAngle);
        }
        //用于计算出外切于圆弧的直线上的两个点的辅助线
        float auxiliaryLineY = RightTriangleUtil.getOppositeSideLenth(textWidth, 90 - textSweepAngle );		//辅助线对边边长
        float auxiliaryLineX = RightTriangleUtil.getNearSideLenth(textWidth, 90 - textSweepAngle);			//辅助线临角边边长
        
        for( int i=0; i<distanceArc.length ;i++ )
        {
        	float radius = distanceArc[i] * scale;
        	if( radius < top_c )
            {
        		RectF oval3 = new RectF(left_c - radius, top_c - radius,  left_c + radius, top_c + radius);
                paint.setColor(this.getResources().getColor(R.color.arc));
                canvas.drawArc(oval3, defaultStartAngle, defaultSweepAngle, false, paint);	
                
                //画弧
                float oppositeSide = RightTriangleUtil.getOppositeSideLenth(radius, textSweepAngle);	//对边边长
                float adjacentSide = RightTriangleUtil.getNearSideLenth(radius, textSweepAngle);		//临角边边长
                float distanceTextX = left_c + adjacentSide;
                float distanceTextY = top_c - oppositeSide ;
                
                //虚拟一条穿过这两个点同时外切该圆弧的直线（用于使显示的文字倾斜）
                path.moveTo( distanceTextX, distanceTextY);
                path.lineTo( distanceTextX + auxiliaryLineX, distanceTextY + auxiliaryLineY );
                path.close();
                
                paint.setColor(this.getResources().getColor(R.color.white));
                canvas.drawTextOnPath(distanceArc[i] + "", path, 6,6, arcLineFontPaint);				//沿着这条斜线画“距离信息”
                path.reset();															//重置路径
            }
        }
    }
    
    /**
     * true 触摸人
     */
    private boolean isTouchMe = false;
//    private boolean isTouchCursor = false;
    private boolean isTouchingCursor = false;
//    private boolean isTouchMark = false;
    /**
     * 手指状态：true触摸中，false离开屏幕
     */
    private boolean isTouch = false;
    private boolean checkMarkMoveValid(float vx, float vy)
    {
    	return checkMoveValid(vx, vy - (float)marke_touch_offset);
    }
    
    private boolean checkMoveValid(float vx, float vy)
    {
    	
    	float x = vx;
    	float y = vy;
		if (x < image_offset  ||
				y < image_offset || 
				x > (screenWidth - image_offset) ||	
				y > (screenHeight - image_offset)  
				) {
			/*check the outside of valid rectangle for view*/
			return false;
		}
		
		if((x < (cursorBmp.getWidth()*3/2) && 
				y >cursorBmp.getHeight()*3 + buttonY) ){
			/*check the inside of three function button*/
			return false;
		}
		
		return true;
    }
    
    private boolean checkTouchMe(MotionEvent event)
    {
    	float x = event.getX();
    	float y = event.getY();
    	
        if (!isPersonPosition() &&
        		(x< navigationMapModel.getPointMe().x+image_offset) && 
        		(x>navigationMapModel.getPointMe().x-image_offset) && 
        		(y>navigationMapModel.getPointMe().y-image_offset) && 
        		(y<navigationMapModel.getPointMe().y+image_offset)) {
    		return true;
    	}
        return false;
    }

    private boolean onActionDown(MotionEvent event)
    {
    	/*check pressed me or not*/
        isTouchMe = checkTouchMe(event);
        if(isTouchMe){
        	return true;
        }

        float x, y;
        x = event.getX();
        y = event.getY();
        
        /*check function button*/
        if( y < this.getMeasuredHeight() && 
        		y > this.getMeasuredHeight()- 10 - image_bottom_offset - cursorBmp.getHeight() - cursorBmp.getHeight()
        		 - cursorBmp.getHeight() &&
        		 x > 0 &&
        		 x < 10+cursorBmp.getWidth()+ 20){
        		return false;
        	/*isTouchingCursor = false;
        	isTouchCursor = false;
        	isTouchMark = false;
        	isTouchMe = false;

        	if(y < this.getMeasuredHeight() && 
        			y > this.getMeasuredHeight()-10-image_bottom_offset-cursorBmp.getHeight()){
            	funcButtonState = FuncButtonState.removeMark;
            	
            	return true;
        	}else if(y < this.getMeasuredHeight()-10-image_bottom_offset-cursorBmp.getHeight() && 
        			y>this.getMeasuredHeight()-10-image_bottom_offset-cursorBmp.getHeight()-cursorBmp.getHeight()){
            	funcButtonState = FuncButtonState.addMark;
            	
            	return true;
        	}else{
        		funcButtonState = FuncButtonState.cursor;
        		return true;
        	}*/
        }
        isTouchingCursor = false;
        switch(funcButtonState){
        	case cursor:
    			navigationMapModel.setTouchPoint(new Point(x, y	- cursor_touch_offset_Y));
//        		isTouchCursor = true;
        		isTouchingCursor = true;
        		break;
        	case addMark:
                /*check whether pressed mark or not*/
           		for (MapMarkeVo mapMarkeVo : voList) {
           			if(x > mapMarkeVo.getX() - image_offset && x < mapMarkeVo.getX() + image_offset && y > mapMarkeVo.getY() - image_offset && y < mapMarkeVo.getY() + image_offset){
           				voList.remove(mapMarkeVo);
           				mapMarkeVo.setX(x);
           				mapMarkeVo.setY(y-marke_touch_offset);
           				mapMarkeVo.setState(MapMarkeVo.STATE_4);
           				
           				this.movingMarkeVo = mapMarkeVo;
//           				isTouchMark = true;
           				return true;
           			}
           		}
           		
           		/*add new mark*/
    			if(voList.size() >= Contexts.MARGEK_COUNT){
    				return false;
    			}
    			
    			MapMarkeVo vo = new MapMarkeVo();
    			vo.setState(MapMarkeVo.STATE_4);
    			vo.setX(x);
    			vo.setY(y-marke_touch_offset);
    			
//   				isTouchMark = true;
    			this.movingMarkeVo = vo;
    	        return true;
        	case removeMark:
 				for(int n=0 ;n<voList.size();n++) {
 					MapMarkeVo mapMarkeVo =voList.get(n);			
 					if(x > mapMarkeVo.getX() - image_offset 
 							&& x < mapMarkeVo.getX() + image_offset 
 							&& y > mapMarkeVo.getY() - image_offset 
 							&& y < mapMarkeVo.getY() + image_offset + markBitmap.getHeight()){
 						voList.remove(n);
 						return true;
 					}					
 				}
        		break;
        	default:
        		break;
        }
        return true;
    }
    

    private boolean onActionMove(MotionEvent event)
    {
        if (navigationMapModel != null ){
        	if (isTouchMe) {
        		navigationMapModel.setPointMe(new Point(event.getX(), event.getY()));
        		return true;
        	}
        }
    	
    	switch(funcButtonState){
    	case cursor:
            if(!checkMoveValid(event.getX(), event.getY())){
            	return false;
            }
			navigationMapModel.setTouchPoint(new Point(event.getX(), event.getY()
					- cursor_touch_offset_Y));
			return true;
    	case addMark:
//    		if(isTouchMark){
                if(!checkMarkMoveValid(event.getX(), event.getY())){
                	return false;
                }
                if (movingMarkeVo!=null) {
                	this.movingMarkeVo.setX(event.getX());
                	this.movingMarkeVo.setY(event.getY() - marke_touch_offset);
                	return true;
				}
//    		}
//    		break;
    	case removeMark:
    		
    		break;
    	default:
    		break;
    	}
    	
    	return false;
    }

    private boolean onActionUp(MotionEvent event)
    {
    	isTouchingCursor = false;
    	if(isTouchMe){
    		isTouchMe = false;
    		return true;
    	}
    	
//    	if(isTouchCursor){
//    		isTouchingCursor = false;
//    		return true;
//    	}
    	
    	if(movingMarkeVo != null && funcButtonState == FuncButtonState.addMark){
//    	if(isTouchMark){
			
    		movingMarkeVo.setState(MapMarkeVo.STATE_1);
    		movingMarkeVo.setTemp_holeId(currIndex);
    		
    		/*check the mark position alidation*/
    		if(movingMarkeVo.getY() < 0){
    			movingMarkeVo.setY(markBitmap.getHeight());
    		}
    		
    		voList.add(movingMarkeVo);
    		movingMarkeVo = null;
    		
//    		isTouchMark = false;
    		return true;
    	}
    	
    	return false;
    }

	/* 处理Touch事件(加减marke）
     * @see android.view.View#onTouchEvent(android.view.MotionEvent)
     */
    @Override
     public boolean onTouchEvent(MotionEvent event) {
        Log.d(TAG, "x = " + event.getX() + "  y = " + event.getY());
        switch(event.getAction()){
        	
        case MotionEvent.ACTION_DOWN:
        	if(onActionDown(event)){
        		this.invalidate();
        	}
        	break;
        case MotionEvent.ACTION_UP:
        	if(onActionUp(event)){
        		this.invalidate();
        	}
        	break;
        case MotionEvent.ACTION_MOVE:
        	if(onActionMove(event)){
        		this.invalidate();
        	}
        	break;
        default:
        	break;
        }

        return true;
    }

    /**
     *     
     * 用户在地图上打标记
     * @param x
     * @param y
     */
    private void drawMarke(Canvas canvas, MapMarkeVo vo, Bitmap marketBitmap){

    	float x,y;
    	x = vo.getX();
    	y = vo.getY();
    	Point markPoint = new Point(x,y);
    	
    	x = x - marketBitmap.getWidth()/2;
    	y = y - marketBitmap.getHeight();
    	
        //到人的距离
        String distance = "";
        
        Point pointMe = navigationMapModel.getPointMe();
        
    	float distance1 = ((CoordinateUtil) navigationMapModel.getCoordinateUtil()).getDistanceBettwen(pointMe, markPoint);
    	if ("米".equals(distanceUnit_near)) {
      		distance = (int)distance1+"米";
  		}else {
  			distance = ((int)(distance1/0.9144) + 1)+"码";
  		}
        marketPaint.setColor(this.getResources().getColor(R.color.white));
        canvas.drawText(String.valueOf(distance) , x, y - 10, marketPaint);
        
        canvas.drawBitmap(marketBitmap, x, y, marketPaint);
        markPoint = null;
    }
    
     public interface MapViewInterface{
        public void onclick();
    }
    
    private MapViewInterface mapViewInterface;
    public void setMapViewInterface(MapViewInterface mapViewInterface){
        this.mapViewInterface = mapViewInterface;
    }
    
    private String[] distances;

    /**
     * 绘制当前用户点击的点
     * @param canvas
     */
    public void drawTouch(Canvas canvas) {
    	if (funcButtonState == FuncButtonState.cursor && navigationMapModel.getTouchPoint().x > 0) {
    		Point crossoverPointA;// 点击点到果岭的点所在的直线与点击的点的圆的交点
    	    Point crossoverPointB;// 点击点到Tee台的点所在的直线与点击的点的圆的交点
    		Point touchPoint = navigationMapModel.getTouchPoint();
//    		果岭距离
    		if (navigationMapModel.getPointGreens().size()==1) {
					Point p = navigationMapModel.getPointGreens().get(0);
					crossoverPointA = calculateIntersectionPoint(p, touchPoint);
					crossoverPointB = calculateIntersectionPoint(navigationMapModel.getPointMe(), touchPoint);
					
					distances = navigationMapModel.getDistanceToGreenAndMe(p,index);
					
					Point pointF=getMidpoint(p,navigationMapModel.getTouchPoint());
					
					if (Integer.valueOf(distances[0]).intValue()>10) {
						drawLineWithShadow(canvas, p.x, p.y, crossoverPointA.x,crossoverPointA.y);
						canvas.drawBitmap(cursorBmp, navigationMapModel.getTouchPoint().x- cursorBmp.getWidth() / 2, navigationMapModel.getTouchPoint().y- cursorBmp.getHeight() / 2, defaultPaint);
		    			drawTextWithShadow(canvas, distances[0], pointF);	
						if (isTouchingCursor) {
							drawTextWithShadow(canvas, distances[1],new Point(navigationMapModel.getTouchPoint().x+50.0f,navigationMapModel.getTouchPoint().y));
						}else {
							drawTextWithShadow(canvas, distances[1],getMidpoint(navigationMapModel.getTouchPoint(), navigationMapModel.getPointMe()));
						}
					}else {
	    				canvas.drawBitmap(cursorBmp, p.x-cursorBmp.getWidth() / 2 , p.y-cursorBmp.getHeight() / 2, defaultPaint);
	    				crossoverPointB = calculateIntersectionPoint(navigationMapModel.getPointMe(), p);
	    				
					}
	    			drawLineWithShadow(canvas, navigationMapModel.getPointMe().x,navigationMapModel.getPointMe().y, crossoverPointB.x,crossoverPointB.y);
	    		}else if (navigationMapModel.getPointGreens().size()==2) {
	    			distances = navigationMapModel.getDistanceToGreenAndMe(navigationMapModel.getPointGreens().get(0),index);
	    			int distance1 = Integer.valueOf(distances[0]).intValue();//触摸点到果岭一距离
	    			int distance2 = Integer.valueOf(navigationMapModel.getDistanceToGreenAndMe(navigationMapModel.getPointGreens().get(1),index)[0]).intValue();//触摸点到果岭二距离
	    			Point a = navigationMapModel.getPointGreens().get(0);//果岭一
	    			Point b = navigationMapModel.getPointGreens().get(1);//果岭二
	    			if (distance1>=10 && distance2>=10) {
//	    				distances = navigationMapModel.getDistanceToGreenAndMe(navigationMapModel.getPointGreens().get(0),index);
		    			
//	    				画果岭一到人的距离及线
	    				crossoverPointA = calculateIntersectionPoint(a, navigationMapModel.getTouchPoint());
	    				drawLineWithShadow(canvas, a.x, a.y, crossoverPointA.x,crossoverPointA.y);
	    				Point pointA=getMidpoint(getMidpoint(a,touchPoint),a);
	    				drawTextWithShadow(canvas, distances[0]+"", pointA);	
//	    				画果岭二到人的距离及线		
	    				crossoverPointA = calculateIntersectionPoint(b, navigationMapModel.getTouchPoint());
	    				drawLineWithShadow(canvas, b.x, b.y, crossoverPointA.x,crossoverPointA.y);
	    				Point pointB=getMidpoint(getMidpoint(b,touchPoint),touchPoint);
//	    				Point point2=new Point(pointB.x-index*60,pointB.y);
	    				drawTextWithShadow(canvas, distances[1]+"", pointB);
//	    				画触摸点
        				canvas.drawBitmap(cursorBmp, navigationMapModel.getTouchPoint().x- cursorBmp.getWidth() / 2, navigationMapModel.getTouchPoint().y- cursorBmp.getHeight() / 2, defaultPaint);
//        				画人到触摸点的线 及距离
        				crossoverPointB = calculateIntersectionPoint(navigationMapModel.getPointMe(), navigationMapModel.getTouchPoint());
        				drawLineWithShadow(canvas, navigationMapModel.getPointMe().x,navigationMapModel.getPointMe().y, crossoverPointB.x,crossoverPointB.y);
						if (isTouchingCursor) {
							drawTextWithShadow(canvas, distances[1],new Point(touchPoint.x+50.0f,touchPoint.y));
						}else {
							drawTextWithShadow(canvas, distances[1],getMidpoint(touchPoint, navigationMapModel.getPointMe()));
						}
	    				
					}else if (distance1<10 && distance2>=10) {
						drawGreenShadow(canvas, a);
					}else if (distance1>=10 && distance2<10) {
						drawGreenShadow(canvas, b);
					}else if (distance1<10 && distance2<10) {
						if (distance1>=distance2) {
							drawGreenShadow(canvas, b);
						}else {
							drawGreenShadow(canvas, a);
						}
				} 
			}
		}
    }

    private void drawGreenShadow(Canvas canvas,Point p){
    	canvas.drawBitmap(cursorBmp, p.x- cursorBmp.getWidth() / 2, p.y- cursorBmp.getHeight() / 2, defaultPaint);
		drawLineWithShadow(canvas, navigationMapModel.getPointMe().x,navigationMapModel.getPointMe().y,p.x,p.y);
    }
    /**
     * 绘制一条带阴影的线。先绘制一条比较宽的黑线，然后在之上绘制一条白线
     * @param canvas
     * @param startX 开始的X坐标
     * @param startY 开始的Y坐标
     * @param stopX 结束的X坐标
     * @param stopY 结束的Y坐标
     */
    private void drawLineWithShadow(Canvas canvas, float startX, float startY,
            float stopX, float stopY) {
        canvas.drawLine(startX, startY, stopX, stopY, cursorLinePaint);
    }

    /**
     * 绘制一条带阴影的字符串，原理同drawLineWithShadow
     * @param canvas
     * @param text 要绘制的字符串
     * @param startPoint 开始的点
     */
    private void drawTextWithShadow(Canvas canvas, String text, Point startPoint) {
//        canvas.drawText(text, startPoint.x, startPoint.y, mPaint3);
        canvas.drawText(text, startPoint.x, startPoint.y, cursorFontPaint);
    }
    
    

    private Point tmpPoint;

    /**
     * 计算两点所在直线的中点
     * @param a 点A
     * @param b 点B
     * @return 点A与点B连线的中点
     */
    private Point getMidpoint(Point a, Point b) {
        tmpPoint = new Point();
        if (a.x > b.x) {
            tmpPoint.x = a.x - (a.x - b.x) / 2;
        } else {
            tmpPoint.x = a.x + (b.x - a.x) / 2;
        }

        if (a.y > b.y) {
            tmpPoint.y = a.y - (a.y - b.y) / 2;
        } else {
            tmpPoint.y = a.y + (b.y - a.y) / 2;
        }

        return tmpPoint;
    }

    /**
     * 计算两个点A与 以点B为圆心，半径为RADIUS的圆的交点
     * 
     * @param A
     *            点A
     * @param B
     *            点B
     * @return 点A与 以点B为圆心，半径为RADIUS的圆的交点
     */
    private Point calculateIntersectionPoint(Point A, Point B) {
        tmpPoint = new Point();
        float a = 0;
        float b = 0;
        double angle = 0;
        if (A.y > B.y && A.x <= B.x) {
            angle = Math.atan((B.x - A.x) / (A.y - B.y));
            a = (float) (Math.sin(angle) * RADIUS);
            b = (float) (Math.cos(angle) * RADIUS);
            tmpPoint.x = B.x - a;
            tmpPoint.y = B.y + b;
        } else if (A.y >= B.y && A.x > B.x) {
            angle = Math.atan((A.y - B.y) / (A.x - B.x));
            a = (float) (Math.cos(angle) * RADIUS);
            b = (float) (Math.sin(angle) * RADIUS);
            tmpPoint.x = B.x + a;
            tmpPoint.y = B.y + b;
        } else if (B.y > A.y && B.x <= A.x) {
            angle = Math.atan((A.x - B.x) / (B.y - A.y));
            a = (float) (Math.sin(angle) * RADIUS);
            b = (float) (Math.cos(angle) * RADIUS);
            tmpPoint.x = B.x + a;
            tmpPoint.y = B.y - b;
        } else if (B.y >= A.y && B.x > A.x) {
            angle = Math.atan((B.y - A.y) / (B.x - A.x));
            a = (float) (Math.cos(angle) * RADIUS);
            b = (float) (Math.sin(angle) * RADIUS);
            tmpPoint.x = B.x - a;
            tmpPoint.y = B.y - b;
        }
        return tmpPoint;
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int measuredHeight = measureHeight(heightMeasureSpec);
        int measuredWidth = measureWidth(widthMeasureSpec);
        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    private int measureHeight(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        int result = 420;

        if (specMode == MeasureSpec.AT_MOST) {
            result = specSize;
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        }
        return result;
    }

    private int measureWidth(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        int result = 320;

        if (specMode == MeasureSpec.AT_MOST) {
            result = specSize;
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        }
        return result;
    }

    public void onShowPress(MotionEvent e) {
        Log.d(TAG, "onShowPress()");

    }
    public boolean onSingleTapUp(MotionEvent e) {
        Log.d(TAG, "onShowPress()");
        return false;
    }

/*    *//** 
     * 移动航拍图，原理是改变航拍图左上角坐标的偏移量
     * @param x 移动x的偏移量
              //                 * @param y 移动y的偏移量 000
     *//*
    public void move(float x, float y) {
        CoordinateUtil util = navigationMapModel.getCoordinateUtil();
        float oldOffsetX = util.getOffsetX();
        float oldOffsetY = util.getOffsetY();
        float newOffsetX = 0;
        float newOffsetY = 0;

        if (mCourseBitmap.getWidth() <= this.getMeasuredWidth()
                && mCourseBitmap.getHeight() <= this.getMeasuredHeight()) {
            return;
        } else if (mCourseBitmap.getWidth() <= this.getMeasuredWidth()
                && mCourseBitmap.getHeight() > this.getMeasuredHeight()) {
            x = 0;
            newOffsetX = oldOffsetX - x;
            newOffsetY = oldOffsetY - y;
            if (newOffsetY > 0
                    || newOffsetY < -mCourseBitmap.getHeight()
                            + this.getMeasuredHeight()) {
                return;
            }
        } else if (mCourseBitmap.getWidth() > this.getMeasuredWidth()
                && mCourseBitmap.getHeight() <= this.getMeasuredHeight()) {
            y = 0;
            newOffsetX = oldOffsetX - x;
            newOffsetY = oldOffsetY - y;
            if (newOffsetX > 0
                    || newOffsetX < -mCourseBitmap.getWidth()
                            + this.getMeasuredWidth()) {
                return;
            }
        } else {
            newOffsetX = oldOffsetX - x;
            newOffsetY = oldOffsetY - y;
            if (newOffsetX > 0
                    || newOffsetX < -mCourseBitmap.getWidth()
                            + this.getMeasuredWidth()) {
                return;
            }
            if (newOffsetY > 0
                    || newOffsetY < -mCourseBitmap.getHeight()
                            + this.getMeasuredHeight()) {
                return;
            }
        }

        util.setOffsetX(newOffsetX);
        util.setOffsetY(newOffsetY);
    }*/
}
