package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.GolfObstadeOrHole;

public class ObstadeAdapter extends BaseAdapter {
	private static final String TAG = "ObstadeAdapter" ;
	
	private Context mContext;
    private ObastadeView warp;
    private LayoutInflater inflater;
    private List<GolfObstadeOrHole> obstadelist;
    public ObstadeAdapter(Context context) {
        mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public List<GolfObstadeOrHole> getObstadelist() {
    	if (obstadelist == null) {
			obstadelist = new ArrayList<GolfObstadeOrHole>();
		}
		return obstadelist;
	}
	public void setObstadelist(List<GolfObstadeOrHole> obstadelist) {
		this.obstadelist = obstadelist;
	}
	public int getCount() {
        return getObstadelist().size();
    }
    public GolfObstadeOrHole getItem(int position) {
        return getObstadelist().get(position);
    }
    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
       
       if (convertView == null) {
    	    convertView = inflater.inflate(R.layout.hole_gps_item,null);
            warp = new ObastadeView();
            warp.number = (TextView) convertView.findViewById(R.id.number_tv);
            warp.name = (TextView) convertView.findViewById(R.id.info_tv);
            warp.len = (TextView) convertView.findViewById(R.id.dis_tv);
            convertView.setTag(warp);
        }else {
        	warp = (ObastadeView) convertView.getTag();
		}
       GolfObstadeOrHole bean = getItem(position);       
       warp.number.setText(Integer.toString(position+1));       
       warp.name.setText(bean.getName());  
       warp.len.setText(bean.getLen());  
//       if ((position+1)%2==0) {
//    	   convertView.setBackgroundResource(R.color.bg_Roadback_1);
//       }else {		
//    	   convertView.setBackgroundResource(R.color.bg_Roadback_2);
//       }
       return convertView;
    }

	public static String getTag() {
		return TAG;
	}

	class ObastadeView {
    	public TextView number;//
        public TextView name;//
        public TextView len;//
    }
   
}
