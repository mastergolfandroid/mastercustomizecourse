package com.teewell.samsunggolfmate.views;
import com.master.mastercourse.singlecourse.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PoleAdapter extends WheelViewAdapter {
    // Countries names
	private String[] poles = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18"};
    // Countries flags
    private String flags[];
    
    /**
     * Constructor
     */
    public PoleAdapter(Context context,String[] flags) {
        super(context, R.layout.adapter_poleselectview, NO_RESOURCE);
        this.flags = flags;
       // setItemTextResource(R.id.country_name);
    }

    @Override
    public View getItem(int index, View cachedView, ViewGroup parent) {
        View view = super.getItem(index, cachedView, parent);
       
        TextView flag = (TextView) view.findViewById(R.id.name);
        flag.setText(flags[index]);
        TextView name = (TextView) view.findViewById(R.id.polenum);
        name.setText(poles[index]);
        return view;
    }
    
    @Override
    public int getItemsCount() {
        return flags.length;
    }
    
    @Override
    protected CharSequence getItemText(int index) {
    	if (index >= 0 && index < flags.length) {
            String item = flags[index];
            if (item instanceof CharSequence) {
                return (CharSequence) item;
            }
            return item.toString();
        }
        return null;
    }
}
