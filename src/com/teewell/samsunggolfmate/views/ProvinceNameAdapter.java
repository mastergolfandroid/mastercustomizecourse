package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.ProvinceBean;

public class ProvinceNameAdapter extends BaseAdapter {
	private List<ProvinceBean> list;
	private LayoutInflater inflater;
	private TextSelectView view;
	public ProvinceNameAdapter(Context mContext){
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	public List<ProvinceBean> getList() {
		if (list == null) {
			list = new ArrayList<ProvinceBean>();
		}
		return list;
	}
	public void setList(List<ProvinceBean> list) {
		this.list = list;
	}
	@Override
	public int getCount() {
		return getList().size();
	}
	@Override
	public ProvinceBean getItem(int position) {
		return getList().get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		view = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.country_courses_child, null);
			view = new TextSelectView();
			view.name = (TextView) convertView.findViewById(R.id.tv_name);
			convertView.setTag(view);
		}else {
			view = (TextSelectView) convertView.getTag();
		}
		view.name.setText(getItem(position).getProvinceName());
		return convertView;
	}
	class TextSelectView{
		public TextView name;
		public ImageView select;
		public RelativeLayout layout;
	}

}
