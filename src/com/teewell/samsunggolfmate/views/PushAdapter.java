package com.teewell.samsunggolfmate.views;
import com.master.mastercourse.singlecourse.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PushAdapter extends WheelViewAdapter {
	
	private String[] num;
	public PushAdapter(Context context,String[] num){
		 super(context, R.layout.adapter_pushselectview, NO_RESOURCE);
		this.num = num;
	}
	@Override
	public int getItemsCount() {
		return num.length;
	}
	 @Override
	    public View getItem(int index, View cachedView, ViewGroup parent) {
	        View view = super.getItem(index, cachedView, parent);
	       
	        TextView name = (TextView) view.findViewById(R.id.pushnum);
	        name.setText(num[index]);
	        return view;
	    }
	@Override
	protected CharSequence getItemText(int index) {
		if (index >= 0 && index < num.length) {
            String item = num[index];
            if (item instanceof CharSequence) {
                return (CharSequence) item;
            }
            return item.toString();
        }
        return null;
	}

}
