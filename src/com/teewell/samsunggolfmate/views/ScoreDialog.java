package com.teewell.samsunggolfmate.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.views.WheelView.OnWheelChangedListener;
import com.teewell.samsunggolfmate.views.WheelView.OnWheelScrollListener;

/**选择年月日对话框
 * @author xsui
 * 
 *
 */
public class ScoreDialog extends Dialog implements android.view.View.OnClickListener{
	protected static final String TAG = ScoreDialog.class.getSimpleName();
	private ScoreListener listener;
	private int poleSelect;
	private int pushSelect;
	private String[] select_flag;
	private String select_push[][];
	private boolean scrollingPole = false;
	private boolean scrollingPush = false;
	private Context context;
	private WheelView poleView;
	private WheelView pushView;
//	private GolferObject golferObject;
	private int holeNumber;
	public ScoreDialog(Context context, ScoreListener listener) {
		super(context, R.style.time_dialog);
		this.listener = listener;
		this.context = context;
	}	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.dialog_score);
	    initView();
	}		
	private void initView() {
		TextView tv_name = (TextView) findViewById(R.id.tv_hole);
		tv_name.setText(holeNumber+1+"号球洞");
		poleView = (WheelView) findViewById(R.id.pole);
		pushView= (WheelView) findViewById(R.id.push);
		Button btn_store = (Button)findViewById(R.id.btn_store);
		btn_store.setOnClickListener(this);
		poleView.setVisibleItems(5);
    	poleView.setViewAdapter(new PoleAdapter(context,select_flag));
        pushView.setVisibleItems(5);
        poleView.addChangingListener(new OnWheelChangedListener() {
                        public void onChanged(WheelView wheel, int oldValue, int newValue) {
                            if (!scrollingPole) {
                            	refreshPushRod(pushView, select_push);
                                Log.i(TAG,newValue+"");
                            }
                        }
                });
        
        poleView.addScrollingListener( new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) {
                scrollingPole = true;
            }
            public void onScrollingFinished(WheelView wheel) {
                scrollingPole = false;
                Log.i(TAG,"onScrollingFinished"+wheel.getCurrentItem()+"");
                poleSelect = poleView.getCurrentItem();
                refreshPushRod(pushView, select_push);
                
            }
        });
        poleView.setCurrentItem(poleSelect);
//        poleView.setLabel(String.valueOf(poleSelect));
	}

   public void doView(String[] select_flag,final String select_push[][],int score,int pushRod,int holeNumber) {
	   this.pushSelect = pushRod;
	   this.poleSelect = score;
	   this.select_flag = select_flag;
	   this.select_push = select_push;
	   this.holeNumber = holeNumber;
	}
	   
	    private void refreshPushRod(final WheelView pushView, String pushs[][]) {
	        pushView.setViewAdapter(new PushAdapter(context, pushs[poleSelect]));
	        pushView.addChangingListener(new OnWheelChangedListener() {
	            public void onChanged(WheelView wheel, int oldValue, int newValue) {
	                if (!scrollingPush) {
	                    Log.i(TAG,newValue+"");
	                }
	            }
	        });

	        pushView.addScrollingListener( new OnWheelScrollListener() {
				public void onScrollingStarted(WheelView wheel) {
				    scrollingPush = true;
				}
				public void onScrollingFinished(WheelView wheel) {
				    scrollingPush = false;
				    Log.i(TAG,"onScrollingFinished"+wheel.getCurrentItem()+"");
				    pushSelect = pushView.getCurrentItem();
				    
				}
			});
	        if (poleSelect<2) {
				pushSelect = 0;
			}
	        pushView.setCurrentItem(pushSelect);   
	        
	    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btn_store){
			listener.refreshData(poleSelect+1, pushSelect);
			dismiss();
		}
	}
	
}