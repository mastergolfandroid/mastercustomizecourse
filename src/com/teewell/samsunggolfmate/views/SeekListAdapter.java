package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;

public class SeekListAdapter extends BaseAdapter implements Comparator<Integer>{
	private String unit;
	private ArrayList<Integer> list;
	private Context context;
	private boolean isdel;
	private final int max=24,step=10,min=120;
	
	public SeekListAdapter(String unit, ArrayList<Integer> list, Context context) {
		super();
		this.unit = unit;
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		SeekView view;
		convertView=null;
		view=new SeekView();
		convertView = LayoutInflater.from(context).inflate(R.layout.setting_item, null);
		view.bar = (SeekBar) convertView.findViewById(R.id.seekBar);
		view.img_del = (ImageView) convertView.findViewById(R.id.img_del);
		view.tv_unit = (TextView) convertView.findViewById(R.id.tv_distance);
		view.bar.setMax(max);
			view.bar.setProgress(getProgress(position));
		convertView.setTag(view);
		final int index = position;
		view.bar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				setProgress(index, seekBar.getProgress());
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				
			}
		});
		
		view.tv_unit.setText(list.get(position)+unit);
		view.bar.setFocusable(!isdel);
		view.bar.setEnabled(!isdel);
		if (isdel) {
			view.img_del.setVisibility(View.VISIBLE);
			
		}else{
			view.img_del.setVisibility(View.INVISIBLE);
		}
		return convertView;
	}
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
		notifyDataSetChanged();
	}

	private int getProgress(int position){
		return (list.get(position)-min)/step;
	}
	private int setProgress(int position,int Progressed){
		int temp = min+Progressed*step;
		list.set(position, temp);
		notifyDataSetChanged();
		return temp;
	}
	class SeekView{
		SeekBar bar;
		TextView tv_unit;
		ImageView img_del;
	}
	public ArrayList<Integer> getList() {
		return list;
	}
	public void setList(ArrayList<Integer> list) {
		this.list = list;
		notifyDataSetChanged();
	}
	public void setIsdel(boolean isdel) {
		this.isdel = isdel;
		notifyDataSetChanged();
	}
	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		boolean isnoswap = true;
		int temp= 0;
		while (isnoswap) {
			Collections.sort(list);
			isnoswap = false;
		for (int i = 0; i < list.size()-1; i++) {
			int a1=list.get(i).intValue(),a2=list.get(i+1).intValue();
				if(a1==a2){
				if (i==0&&a1==min) {
					a2+=step;
				}
				else{
					a2+=step;
				}
				isnoswap=true;
			}
			list.set(i, a1);
			list.set(i+1, a2);
			
		}
		temp++;
		System.out.println(temp+(isnoswap+""));
		}
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).intValue()>min+(max-(list.size()-1-i))*step) {
				list.set(i, min+(max-(list.size()-1-i))*step);
			}
		}
		Collections.sort(list);
		super.notifyDataSetChanged();
	}
	@Override
	public int compare(Integer object1, Integer object2) {
		// TODO Auto-generated method stub
		return object1>object2?object1:object2;
	}

	

	
	
}
