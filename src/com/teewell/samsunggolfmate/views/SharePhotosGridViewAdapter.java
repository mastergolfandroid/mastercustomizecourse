package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.beans.SharePhotoBean;
import com.teewell.samsunggolfmate.models.PhotoModel;

public class SharePhotosGridViewAdapter extends BaseAdapter {
	public final int STATE[] = {0,1,2};
	private LayoutInflater layoutInflater;
	private List<SharePhotoBean> list_data;
	private GridView.LayoutParams layoutParams;
	private final int VIEW_COUNT = 4;
	public SharePhotosGridViewAdapter(Context context) {
		layoutInflater = LayoutInflater.from(context);
	}
	
	public List<SharePhotoBean> getList_data() {
		return list_data;
	}

	public void setList_data(List<SharePhotoBean> list_data) {
		this.list_data = list_data;
	}
	public void setLayoutParams(int width,int height){
		layoutParams = new GridView.LayoutParams(width,height);
	}
	public ArrayList<String> getPhotoPathList(){
		ArrayList<String> list = new ArrayList<String>();
		for (SharePhotoBean bean : list_data) {
			list.add(bean.getBitmapPath());
		}
		return list;
	}
	@Override
	public int getCount() {
		if (null==list_data) {
			return 1;
		}
		if (list_data.size()==VIEW_COUNT) {
			return VIEW_COUNT;
		}
		return list_data.size()+1;
	}

	@Override
	public SharePhotoBean getItem(int position) {
		if (getList_data()==null || getList_data().size()<=position) {
			return null;
		}
		return getList_data().get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		SharePhotosGridViewItemView view = null;
		SharePhotoBean bean = getItem(position);
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.adapter_sharephotos_gridview, null);
			view = new SharePhotosGridViewItemView();
			view.imageView_photo = (ImageView) convertView.findViewById(R.id.imageView_photo);
			view.img_del = (ImageView) convertView.findViewById(R.id.imageView_del);
			
			convertView.setTag(view);
			convertView.setLayoutParams(layoutParams);
		}else {
			view = (SharePhotosGridViewItemView) convertView.getTag();
		}
		
		view.img_del.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharePhotoBean bean = getItem(position);
				System.out.println(PhotoModel.getInstance(v.getContext()).getList_uploadPhotoBean().remove(bean));
				setList_data(PhotoModel.getInstance(v.getContext()).getList_uploadPhotoBean());
				notifyDataSetChanged();
			}
		});
		if (bean == null) {
			view.imageView_photo.setImageResource(R.drawable.smiley_add_btn_nor);
			view.img_del.setVisibility(View.GONE);
			view.imageView_photo.setScaleType(ScaleType.CENTER);
		}else {
			view.imageView_photo.setImageBitmap(bean.getBitmap());
			view.img_del.setVisibility(View.VISIBLE);
			view.imageView_photo.setScaleType(ScaleType.FIT_CENTER);
		}
		return convertView;
	}
}
