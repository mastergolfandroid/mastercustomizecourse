package com.teewell.samsunggolfmate.views;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class SharePhotosGridViewItemView {
	public FrameLayout layout;
	public ImageView imageView_photo;
	public ImageView imageView_flap;
	public ImageView imageView_fail;
	public ImageView img_del;
	public ProgressBar progressBar;
}
