package com.teewell.samsunggolfmate.views;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;

public class TextAdapter extends BaseAdapter {
	private final String TAG = "TextAdapter";
	private List<String> list;
	private LayoutInflater inflater;
	private TextSelectView view;
	private String select = "";
	public TextAdapter(Context mContext,List<String> list,String showString){
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.list = list;
		Log.d(TAG, "list.size="+list.size());
		setSelect(showString);
	}
	
	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	@Override
	public int getCount() {
		return list.size();
	}
	@Override
	public Object getItem(int position) {
		return list.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d(TAG, position+"");
		view = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.textselectview, null);
			view = new TextSelectView();
			view.layout = (RelativeLayout) convertView.findViewById(R.id.select);
			view.name = (TextView) convertView.findViewById(R.id.name);
			view.select = (ImageView) convertView.findViewById(R.id.image);
			convertView.setTag(view);
		}else {
			view = (TextSelectView) convertView.getTag();
		}
//		if (list.size() == 1) {
//			view.layout.setBackgroundResource(R.drawable.bg_single_wane);
//		}else if (position == 0) {
//			view.layout.setBackgroundResource(R.drawable.bg_above_wane);
//		}else if (position == list.size()-1) {
//			view.layout.setBackgroundResource(R.drawable.bg_below_wane);
//		}else {
//			view.layout.setBackgroundResource(R.drawable.bg_middle_wane);
//		}
		view.name.setText(list.get(position));
		if (list.get(position).split("-")[0].equals(getSelect())) {
			view.select.setVisibility(View.VISIBLE);
		}else {
			view.select.setVisibility(View.GONE);
		}
		return convertView;
	}
	class TextSelectView{
		public TextView name;
		public ImageView select;
		public RelativeLayout layout;
	}

}
