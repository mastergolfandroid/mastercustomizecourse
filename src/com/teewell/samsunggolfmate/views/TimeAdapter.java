package com.teewell.samsunggolfmate.views;
import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.utils.TimeUtil;

public class TimeAdapter extends WheelViewAdapter {
	
	public static int start_year_num ;
	private static final int YEAR_SPACE = 80;
	public static int end_year_num;
	
	public static final int START_MONTH_NUM = 1;
	public static final int START_DAY_NUM = 1;

	public static final int START_HOUR_NUM = 0;
	public static final int START_MINUTE_NUM = 1;
	public static final int START_SECOND_NUM = 0;

	public static final int HOUR_MAX = 23;
	public static final int MINUTE_MAX = 59;
	public static final int SECOND_MAX = 59;

	private static ArrayList<String> yyyyList;
	private static ArrayList<String> MMList;

	private static ArrayList<String> ddList28;
	private static ArrayList<String> ddList29;
	private static ArrayList<String> ddList30;
	private static ArrayList<String> ddList31;
	
	private static ArrayList<String> HHList;
	private static ArrayList<String> mmList;
	private static ArrayList<String> ssList;

	
	private ArrayList<String> numList;
	public TimeAdapter(Context context){
		 super(context, R.layout.timeselectview, NO_RESOURCE);
		 end_year_num = TimeUtil.getCurrentYear();
		 start_year_num = end_year_num - YEAR_SPACE;
	}
	public TimeAdapter(Context context,int yearSpace){
		super(context, R.layout.timeselectview, NO_RESOURCE);
		end_year_num = TimeUtil.getCurrentYear();
		start_year_num = end_year_num - yearSpace;
	}
	
	public void setNumList(ArrayList<String> numList) {
		this.numList = numList;
	}
	
	@Override
	public int getItemsCount() {
		return numList.size();
	}
	 @Override
	public View getItem(int index, View cachedView, ViewGroup parent) {
	     View view = super.getItem(index, cachedView, parent);
	       
	     TextView nameTextView = (TextView) view.findViewById(R.id.tv_timeValue);
	     nameTextView.setText(numList.get(index));
	     
	     return view;
	}
	@Override
	protected CharSequence getItemText(int index) {
		if (index >= 0 && index < numList.size()) {
            String item = numList.get(index);
            if (item instanceof CharSequence) {
                return (CharSequence) item;
            }
            return item.toString();
        }
        return null;
	}

	public static ArrayList<String> getHourStrings() {
		if (null == HHList) {
			HHList = new ArrayList<String>();
			for (int i = START_HOUR_NUM; i <= HOUR_MAX; i++) {
				HHList.add(String.format("%1$,02d", i));
			}
		}

		return HHList;
	}

	public static ArrayList<String> getMinuteStrings() {
		if (null == mmList) {
			mmList = new ArrayList<String>();
			for (int i = START_MINUTE_NUM; i <= MINUTE_MAX; i++) {
				mmList.add(String.format("%1$,02d", i));
			}
		}

		return mmList;
	}

	public static ArrayList<String> getSecondStrings() {
		if (null == ssList) {
			ssList = new ArrayList<String>();
			for (int i = START_SECOND_NUM; i <= SECOND_MAX; i++) {
				ssList.add(String.format("%1$,02d", i));
			}
		}

		return mmList;
	}

	
	public static ArrayList<String> getYearStrings() {
		if (null == yyyyList) {
			yyyyList = new ArrayList<String>();
			for (int i = start_year_num; i <= end_year_num; i++) {
				yyyyList.add(String.valueOf(i));
			}
		}

		return yyyyList;
	}

	public static ArrayList<String> getMounthStrings() {
		
		if (null == MMList) {
			MMList = new ArrayList<String>();
			for (int i = START_MONTH_NUM; i <= 12; i++) {
				MMList.add(String.format("%1$,02d", i));
			}
		}

		return MMList;
	}

	private static ArrayList<String> getDayStringsByDayNum(int dayNum) {
		
		ArrayList<String > ddList;
		switch (dayNum) {
		case 28:
			ddList = ddList28;
			break;
		case 29:
			ddList = ddList29;
			break;
		case 30:
			ddList = ddList30;
			break;
		case 31:
			ddList = ddList31;
			break;

		default:
			ddList = ddList30;
			break;
		}

		if (null == ddList) {
			ddList = new ArrayList<String>(dayNum);
			for (int i = START_DAY_NUM; i <= dayNum; i++) {
				ddList.add(String.format("%1$,02d", i));
			}
			
			switch (dayNum) {
			case 28:
				ddList28 = ddList;
				break;
			case 29:
				ddList29 = ddList;
				break;
			case 30:
				ddList30 = ddList;
				break;
			case 31:
				ddList31 = ddList;
				break;

			default:
				ddList30 = ddList;
				break;
			}

		}

		return ddList;
	}

	public static ArrayList<String> getDayStrings(String year, String month) {
		
		int dayNum = TimeUtil.getDayNumByYearMonth(year, month);

		return getDayStringsByDayNum(dayNum);
	}

	
	
}
