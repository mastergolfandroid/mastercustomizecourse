package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.utils.TimeUtil;
import com.teewell.samsunggolfmate.views.WheelView.OnWheelChangedListener;

/**选择年月日对话框
 * @author xsui
 * 
 *
 */
public class WhellYearMonthDayDialog extends Dialog implements android.view.View.OnClickListener, OnWheelChangedListener{
	private YMDListener ymdListener;
	private static final String DATE_SEPERATOR_STRING="-";
	int yearIndex;
	int monthIndex;
	int dayIndex;

	public WhellYearMonthDayDialog(Context context, YMDListener ymdListener) {
		super(context, R.style.time_dialog);
		this.ymdListener = ymdListener;
	}

	public void setShowDate(String showDate){
		
		if (showDate.length() <= 0) {
			return;
		}
		
		String []dateStrings = showDate.split(DATE_SEPERATOR_STRING);
		yearIndex = Integer.parseInt(dateStrings[0]) - TimeAdapter.start_year_num;
		if (yearIndex < 0) {
			yearIndex = 0;
		}
		monthIndex = Integer.parseInt(dateStrings[1]) - TimeAdapter.START_MONTH_NUM;
		if (monthIndex < 0) {
			monthIndex = 0;
		}
		dayIndex = Integer.parseInt(dateStrings[2]) - TimeAdapter.START_DAY_NUM;
		if (dayIndex < 0) {
			dayIndex = 0;
		}
	}

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.wheel_ymd_dialog);
	    initView();
	}



	private WheelView yyyyView;
	private WheelView MMView;
	private WheelView ddView;
	
	private TimeAdapter yyyyAdapter;
	private TimeAdapter MMAdapter;
	private TimeAdapter ddAdapter;
	
	ArrayList<String> ddList;
	ArrayList<String> yyyyList;
	ArrayList<String> MMList;
	
	public final static int VISIBLE_ITEM_COUNT = 3;

	
	
	private void initView() {
		yyyyView = (WheelView)findViewById(R.id.yyyyView);
		yyyyAdapter = new TimeAdapter(getContext());
		yyyyList = TimeAdapter.getYearStrings(); 
		yyyyAdapter.setNumList(yyyyList);
		yyyyView.setViewAdapter(yyyyAdapter);
		yyyyView.setVisibleItems(VISIBLE_ITEM_COUNT);
		yyyyView.setCyclic(true);
		yyyyView.setCurrentItem(yearIndex);
		
		MMView = (WheelView)findViewById(R.id.MMView);
		MMAdapter = new TimeAdapter(getContext());
		MMList = TimeAdapter.getMounthStrings(); 
		MMAdapter.setNumList(MMList);
		MMView.setViewAdapter(MMAdapter);
		MMView.setVisibleItems(VISIBLE_ITEM_COUNT);
		MMView.setCyclic(true);
		MMView.setCurrentItem(monthIndex);
		MMView.addChangingListener(this);
			
		ddView = (WheelView)findViewById(R.id.ddView);
		ddAdapter = new TimeAdapter(getContext());
		ddList = TimeAdapter.getDayStrings(String.valueOf(TimeUtil.getCurrentYear()), "1"); 
		ddAdapter.setNumList(ddList);
		ddView.setViewAdapter(ddAdapter);
		ddView.setVisibleItems(VISIBLE_ITEM_COUNT);
		ddView.setCyclic(true);
		ddView.setCurrentItem(dayIndex);
		
		Button btn_commit = (Button)findViewById(R.id.btn_commit);
		btn_commit.setOnClickListener(this);
		//fill with usertyp string
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btn_commit){
			
			ymdListener.refreshYMD(yyyyList.get(yyyyView.getCurrentItem()) + "-" + MMList.get(MMView.getCurrentItem()) + "-" + ddList.get(ddView.getCurrentItem()));
			
			dismiss();
		}
	}



	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub
		String MMValue = MMList.get(newValue);
		String yyyyValue = yyyyList.get(yyyyView.getCurrentItem());
		ddList = TimeAdapter.getDayStrings(yyyyValue, MMValue);
		ddAdapter.setNumList(ddList);
		ddAdapter.notifyDataChangedEvent();
	}
	
}