package com.teewell.samsunggolfmate.views;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.master.mastercourse.singlecourse.R;
import com.teewell.samsunggolfmate.utils.TimeUtil;
import com.teewell.samsunggolfmate.views.WheelView.OnWheelChangedListener;

/**选择年月日对话框
 * @author xsui
 * 
 *
 */
public class WhellYearMonthDayHourMinuteDialog extends Dialog implements android.view.View.OnClickListener, OnWheelChangedListener{
	private YMDListener ymdListener;
	private static final String DATE_SEPERATOR_STRING="-";
	int yearIndex;
	int monthIndex;
	int dayIndex;
	int hourIndex;
	int minuteIndex;

	public WhellYearMonthDayHourMinuteDialog(Context context, YMDListener ymdListener) {
		super(context, R.style.time_dialog);
		this.ymdListener = ymdListener;
	}

	public void setShowDate(String showDate){
		
		if (showDate.length() <= 0) {
			return;
		}
		
		String []dateStrings = TimeUtil.getYearMonthDayHourMinute(showDate);
		yearIndex = Integer.parseInt(dateStrings[0]) - TimeAdapter.start_year_num;
		if (yearIndex < 0) {
			yearIndex = 0;
		}
		monthIndex = Integer.parseInt(dateStrings[1]) - TimeAdapter.START_MONTH_NUM;
		if (monthIndex < 0) {
			monthIndex = 0;
		}
		dayIndex = Integer.parseInt(dateStrings[2]) - TimeAdapter.START_DAY_NUM;
		if (dayIndex < 0) {
			dayIndex = 0;
		}
		hourIndex = Integer.parseInt(dateStrings[3]) - TimeAdapter.START_HOUR_NUM;
		if (hourIndex < 0) {
			hourIndex = 0;
		}
		minuteIndex = Integer.parseInt(dateStrings[4]) - TimeAdapter.START_MINUTE_NUM;
		if (minuteIndex < 0) {
			minuteIndex = 0;
		}
	}

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.dialog_wheel_ymd_hm);
	    initView();
	}



	private WheelView yyyyView;
	private WheelView MMView;
	private WheelView ddView;
	private WheelView hourView;
	private WheelView minuteView;
	
	private TimeAdapter yyyyAdapter;
	private TimeAdapter MMAdapter;
	private TimeAdapter ddAdapter;
	private TimeAdapter hourAdapter;
	private TimeAdapter minuteAdapter;
	
	ArrayList<String> ddList;
	ArrayList<String> yyyyList;
	ArrayList<String> MMList;
	ArrayList<String> hourList;
	ArrayList<String> minuteList;
	
	public final static int VISIBLE_ITEM_COUNT = 3;

	
	
	private void initView() {
		yyyyView = (WheelView)findViewById(R.id.yyyyView);
		yyyyAdapter = new TimeAdapter(getContext(),1);
		yyyyList = TimeAdapter.getYearStrings(); 
		yyyyAdapter.setNumList(yyyyList);
		yyyyView.setViewAdapter(yyyyAdapter);
		yyyyView.setVisibleItems(VISIBLE_ITEM_COUNT);
		yyyyView.setCyclic(true);
		yyyyView.setCurrentItem(yearIndex);
		
		MMView = (WheelView)findViewById(R.id.MMView);
		MMAdapter = new TimeAdapter(getContext(),1);
		MMList = TimeAdapter.getMounthStrings(); 
		MMAdapter.setNumList(MMList);
		MMView.setViewAdapter(MMAdapter);
		MMView.setVisibleItems(VISIBLE_ITEM_COUNT);
		MMView.setCyclic(true);
		MMView.setCurrentItem(monthIndex);
		MMView.addChangingListener(this);
			
		ddView = (WheelView)findViewById(R.id.ddView);
		ddAdapter = new TimeAdapter(getContext(),1);
		ddList = TimeAdapter.getDayStrings(String.valueOf(TimeUtil.getCurrentYear()), "1"); 
		ddAdapter.setNumList(ddList);
		ddView.setViewAdapter(ddAdapter);
		ddView.setVisibleItems(VISIBLE_ITEM_COUNT);
		ddView.setCyclic(true);
		ddView.setCurrentItem(dayIndex);
		
		hourView = (WheelView)findViewById(R.id.hourView);
		hourAdapter = new TimeAdapter(getContext(),1);
		hourList = TimeAdapter.getHourStrings(); 
		hourAdapter.setNumList(hourList);
		hourView.setViewAdapter(hourAdapter);
		hourView.setVisibleItems(VISIBLE_ITEM_COUNT);
		hourView.setCyclic(true);
		hourView.setCurrentItem(hourIndex);
		
		minuteView = (WheelView)findViewById(R.id.minuteView);
		minuteAdapter = new TimeAdapter(getContext(),1);
		minuteList = TimeAdapter.getMinuteStrings(); 
		minuteAdapter.setNumList(ddList);
		minuteView.setViewAdapter(ddAdapter);
		minuteView.setVisibleItems(VISIBLE_ITEM_COUNT);
		minuteView.setCyclic(true);
		minuteView.setCurrentItem(minuteIndex);
		
		Button btn_commit = (Button)findViewById(R.id.btn_commit);
		btn_commit.setOnClickListener(this);
		//fill with usertyp string
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btn_commit){
			
			ymdListener.refreshYMD(yyyyList.get(yyyyView.getCurrentItem()) + "-" + MMList.get(MMView.getCurrentItem()) + "-" + ddList.get(ddView.getCurrentItem())
					+ " " + hourList.get(hourView.getCurrentItem()) + ":" + minuteList.get(minuteView.getCurrentItem()));
			
			dismiss();
		}
	}



	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub
		String MMValue = MMList.get(newValue);
		String yyyyValue = yyyyList.get(yyyyView.getCurrentItem());
		ddList = TimeAdapter.getDayStrings(yyyyValue, MMValue);
		ddAdapter.setNumList(ddList);
		ddAdapter.notifyDataChangedEvent();
	}

	public static String getDateSeperatorString() {
		return DATE_SEPERATOR_STRING;
	}
	
}